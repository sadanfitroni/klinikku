﻿from flask import Flask, render_template, url_for
from whatsapp import WhatsApp

app = Flask(__name__)

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response

@app.route('/')
def index():
    return render_template('./index.html')

@app.route('/test')
def test():
    bot.send_anon_message("6288218489752", "qora ora ora ora ora")
    return "sent"

if __name__ == '__main__':
    bot = WhatsApp(10,'./static/screenshot.png')
    print('Login succeed')
    app.run()