'use strict';

module.exports = function(app) {
  var ds = app.dataSources.mssql_db;
  var ds_server = app.dataSources.mssql_server;
  
  var lbTables = [
  	"user",
  	"acl",
  	"pembayaran",
  	"visit",
  	"role",
  	"resep",
  	"menu_sidebar",
  	"menu_model",
  	"medrec",
  	"klinik_dokter",
  	"klinik",
  	"jadwal_praktek",
  	"history_praktek",
    "provinsi",
    "kabupaten_kota",
    "kecamatan",
    "kelurahan",
    "generic_param",
    "stock",
    "katalog_jasa",
  ];
  // ds.autoupdate(lbTables, function(er) {
  //   if (er) throw er;
  //   console.log('Loopback tables created in ' + ds.adapter.name);
  // });
};
