// THIS FILE IS AUTO GENERATED BY GK-CLI DO NOT EDIT!!
package endpoint

import (
	endpoint "github.com/go-kit/kit/endpoint"
	service "akuklinikid_demo/go-service/pembayaran/pkg/service"
)

// Endpoints collects all of the endpoints that compose a profile service. It's
// meant to be used as a helper struct, to collect all of the endpoints into a
// single parameter.
type Endpoints struct {
	AddItemPembayaranEndpoint        endpoint.Endpoint
	EntryPembayaranEndpoint          endpoint.Endpoint
	GetPembayaranEndpoint            endpoint.Endpoint
	GetTagihanPembayaranEndpoint     endpoint.Endpoint
	AddTagihanPembayaranEndpoint     endpoint.Endpoint
	EditTagihanPembayaranEndpoint    endpoint.Endpoint
	DeleteTagihanPembayaranEndpoint  endpoint.Endpoint
	GetTagihanPembayaranByIdEndpoint endpoint.Endpoint
}

// New returns a Endpoints struct that wraps the provided service, and wires in all of the
// expected endpoint middlewares
func New(s service.PembayaranService, mdw map[string][]endpoint.Middleware) Endpoints {
	eps := Endpoints{
		AddItemPembayaranEndpoint:        MakeAddItemPembayaranEndpoint(s),
		AddTagihanPembayaranEndpoint:     MakeAddTagihanPembayaranEndpoint(s),
		DeleteTagihanPembayaranEndpoint:  MakeDeleteTagihanPembayaranEndpoint(s),
		EditTagihanPembayaranEndpoint:    MakeEditTagihanPembayaranEndpoint(s),
		EntryPembayaranEndpoint:          MakeEntryPembayaranEndpoint(s),
		GetPembayaranEndpoint:            MakeGetPembayaranEndpoint(s),
		GetTagihanPembayaranByIdEndpoint: MakeGetTagihanPembayaranByIdEndpoint(s),
		GetTagihanPembayaranEndpoint:     MakeGetTagihanPembayaranEndpoint(s),
	}
	for _, m := range mdw["AddItemPembayaran"] {
		eps.AddItemPembayaranEndpoint = m(eps.AddItemPembayaranEndpoint)
	}
	for _, m := range mdw["EntryPembayaran"] {
		eps.EntryPembayaranEndpoint = m(eps.EntryPembayaranEndpoint)
	}
	for _, m := range mdw["GetPembayaran"] {
		eps.GetPembayaranEndpoint = m(eps.GetPembayaranEndpoint)
	}
	for _, m := range mdw["GetTagihanPembayaran"] {
		eps.GetTagihanPembayaranEndpoint = m(eps.GetTagihanPembayaranEndpoint)
	}
	for _, m := range mdw["AddTagihanPembayaran"] {
		eps.AddTagihanPembayaranEndpoint = m(eps.AddTagihanPembayaranEndpoint)
	}
	for _, m := range mdw["EditTagihanPembayaran"] {
		eps.EditTagihanPembayaranEndpoint = m(eps.EditTagihanPembayaranEndpoint)
	}
	for _, m := range mdw["DeleteTagihanPembayaran"] {
		eps.DeleteTagihanPembayaranEndpoint = m(eps.DeleteTagihanPembayaranEndpoint)
	}
	for _, m := range mdw["GetTagihanPembayaranById"] {
		eps.GetTagihanPembayaranByIdEndpoint = m(eps.GetTagihanPembayaranByIdEndpoint)
	}
	return eps
}
