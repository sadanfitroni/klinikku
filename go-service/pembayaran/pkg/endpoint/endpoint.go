package endpoint

import (
	"context"
	io "akuklinikid_demo/go-service/io"
	service "akuklinikid_demo/go-service/pembayaran/pkg/service"

	endpoint "github.com/go-kit/kit/endpoint"
)

// AddItemPembayaranRequest collects the request parameters for the AddItemPembayaran method.
type AddItemPembayaranRequest struct {
	VisitId    string        `json:"visit_id"`
	Pembayaran io.Pembayaran `json:"pembayaran"`
}

// AddItemPembayaranResponse collects the response parameters for the AddItemPembayaran method.
type AddItemPembayaranResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeAddItemPembayaranEndpoint returns an endpoint that invokes AddItemPembayaran on the service.
func MakeAddItemPembayaranEndpoint(s service.PembayaranService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddItemPembayaranRequest)
		message, err := s.AddItemPembayaran(ctx, req.VisitId, req.Pembayaran)
		return AddItemPembayaranResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r AddItemPembayaranResponse) Failed() error {
	return r.Err
}

// EntryPembayaranRequest collects the request parameters for the EntryPembayaran method.
type EntryPembayaranRequest struct {
	VisitId string   `json:"visit_id"`
	Visit   io.Visit `json:"visit"`
}

// EntryPembayaranResponse collects the response parameters for the EntryPembayaran method.
type EntryPembayaranResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeEntryPembayaranEndpoint returns an endpoint that invokes EntryPembayaran on the service.
func MakeEntryPembayaranEndpoint(s service.PembayaranService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(EntryPembayaranRequest)
		message, err := s.EntryPembayaran(ctx, req.VisitId, req.Visit)
		return EntryPembayaranResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r EntryPembayaranResponse) Failed() error {
	return r.Err
}

// Failure is an interface that should be implemented by response types.
// Response encoders can check if responses are Failer, and if so they've
// failed, and if so encode them using a separate write path based on the error.
type Failure interface {
	Failed() error
}

// AddItemPembayaran implements Service. Primarily useful in a client.
func (e Endpoints) AddItemPembayaran(ctx context.Context, visitId string, pembayaran io.Pembayaran) (message string, err error) {
	request := AddItemPembayaranRequest{
		Pembayaran: pembayaran,
		VisitId:    visitId,
	}
	response, err := e.AddItemPembayaranEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(AddItemPembayaranResponse).Message, response.(AddItemPembayaranResponse).Err
}

// EntryPembayaran implements Service. Primarily useful in a client.
func (e Endpoints) EntryPembayaran(ctx context.Context, visitId string, visit io.Visit) (message string, err error) {
	request := EntryPembayaranRequest{
		Visit:   visit,
		VisitId: visitId,
	}
	response, err := e.EntryPembayaranEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(EntryPembayaranResponse).Message, response.(EntryPembayaranResponse).Err
}

// GetPembayaranRequest collects the request parameters for the GetPembayaran method.
type GetPembayaranRequest struct{}

// GetPembayaranResponse collects the response parameters for the GetPembayaran method.
type GetPembayaranResponse struct {
	Pembayaran []io.Pembayaran `json:"pembayaran"`
	Message    string          `json:"message"`
	Err        error           `json:"err"`
}

// MakeGetPembayaranEndpoint returns an endpoint that invokes GetPembayaran on the service.
func MakeGetPembayaranEndpoint(s service.PembayaranService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		pembayaran, message, err := s.GetPembayaran(ctx)
		return GetPembayaranResponse{
			Err:        err,
			Message:    message,
			Pembayaran: pembayaran,
		}, nil
	}
}

// Failed implements Failer.
func (r GetPembayaranResponse) Failed() error {
	return r.Err
}

// GetPembayaran implements Service. Primarily useful in a client.
func (e Endpoints) GetPembayaran(ctx context.Context) (pembayaran []io.Pembayaran, message string, err error) {
	request := GetPembayaranRequest{}
	response, err := e.GetPembayaranEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetPembayaranResponse).Pembayaran, response.(GetPembayaranResponse).Message, response.(GetPembayaranResponse).Err
}

// GetTagihanPembayaranRequest collects the request parameters for the GetTagihanPembayaran method.
type GetTagihanPembayaranRequest struct{}

// GetTagihanPembayaranResponse collects the response parameters for the GetTagihanPembayaran method.
type GetTagihanPembayaranResponse struct {
	TagihanPembayaran []io.TagihanPembayaran `json:"tagihan_pembayaran"`
	Message           string                 `json:"message"`
	Err               error                  `json:"err"`
}

// MakeGetTagihanPembayaranEndpoint returns an endpoint that invokes GetTagihanPembayaran on the service.
func MakeGetTagihanPembayaranEndpoint(s service.PembayaranService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		tagihanPembayaran, message, err := s.GetTagihanPembayaran(ctx)
		return GetTagihanPembayaranResponse{
			Err:               err,
			Message:           message,
			TagihanPembayaran: tagihanPembayaran,
		}, nil
	}
}

// Failed implements Failer.
func (r GetTagihanPembayaranResponse) Failed() error {
	return r.Err
}

// AddTagihanPembayaranRequest collects the request parameters for the AddTagihanPembayaran method.
type AddTagihanPembayaranRequest struct {
	TagihanPembayaran io.TagihanPembayaran `json:"tagihan_pembayaran"`
}

// AddTagihanPembayaranResponse collects the response parameters for the AddTagihanPembayaran method.
type AddTagihanPembayaranResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeAddTagihanPembayaranEndpoint returns an endpoint that invokes AddTagihanPembayaran on the service.
func MakeAddTagihanPembayaranEndpoint(s service.PembayaranService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddTagihanPembayaranRequest)
		message, err := s.AddTagihanPembayaran(ctx, req.TagihanPembayaran)
		return AddTagihanPembayaranResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r AddTagihanPembayaranResponse) Failed() error {
	return r.Err
}

// EditTagihanPembayaranRequest collects the request parameters for the EditTagihanPembayaran method.
type EditTagihanPembayaranRequest struct {
	TagihanPembayaran io.TagihanPembayaran `json:"tagihan_pembayaran"`
}

// EditTagihanPembayaranResponse collects the response parameters for the EditTagihanPembayaran method.
type EditTagihanPembayaranResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeEditTagihanPembayaranEndpoint returns an endpoint that invokes EditTagihanPembayaran on the service.
func MakeEditTagihanPembayaranEndpoint(s service.PembayaranService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(EditTagihanPembayaranRequest)
		message, err := s.EditTagihanPembayaran(ctx, req.TagihanPembayaran)
		return EditTagihanPembayaranResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r EditTagihanPembayaranResponse) Failed() error {
	return r.Err
}

// DeleteTagihanPembayaranRequest collects the request parameters for the DeleteTagihanPembayaran method.
type DeleteTagihanPembayaranRequest struct {
	Id string `json:"id"`
}

// DeleteTagihanPembayaranResponse collects the response parameters for the DeleteTagihanPembayaran method.
type DeleteTagihanPembayaranResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeDeleteTagihanPembayaranEndpoint returns an endpoint that invokes DeleteTagihanPembayaran on the service.
func MakeDeleteTagihanPembayaranEndpoint(s service.PembayaranService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteTagihanPembayaranRequest)
		message, err := s.DeleteTagihanPembayaran(ctx, req.Id)
		return DeleteTagihanPembayaranResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r DeleteTagihanPembayaranResponse) Failed() error {
	return r.Err
}

// GetTagihanPembayaranByIdRequest collects the request parameters for the GetTagihanPembayaranById method.
type GetTagihanPembayaranByIdRequest struct {
	Id string `json:"id"`
}

// GetTagihanPembayaranByIdResponse collects the response parameters for the GetTagihanPembayaranById method.
type GetTagihanPembayaranByIdResponse struct {
	TagihanPembayaran io.TagihanPembayaran `json:"tagihan_pembayaran"`
	Message           string               `json:"message"`
	Err               error                `json:"err"`
}

// MakeGetTagihanPembayaranByIdEndpoint returns an endpoint that invokes GetTagihanPembayaranById on the service.
func MakeGetTagihanPembayaranByIdEndpoint(s service.PembayaranService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetTagihanPembayaranByIdRequest)
		tagihanPembayaran, message, err := s.GetTagihanPembayaranById(ctx, req.Id)
		return GetTagihanPembayaranByIdResponse{
			Err:               err,
			Message:           message,
			TagihanPembayaran: tagihanPembayaran,
		}, nil
	}
}

// Failed implements Failer.
func (r GetTagihanPembayaranByIdResponse) Failed() error {
	return r.Err
}

// GetTagihanPembayaran implements Service. Primarily useful in a client.
func (e Endpoints) GetTagihanPembayaran(ctx context.Context) (tagihanPembayaran []io.TagihanPembayaran, message string, err error) {
	request := GetTagihanPembayaranRequest{}
	response, err := e.GetTagihanPembayaranEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetTagihanPembayaranResponse).TagihanPembayaran, response.(GetTagihanPembayaranResponse).Message, response.(GetTagihanPembayaranResponse).Err
}

// AddTagihanPembayaran implements Service. Primarily useful in a client.
func (e Endpoints) AddTagihanPembayaran(ctx context.Context, tagihanPembayaran io.TagihanPembayaran) (message string, err error) {
	request := AddTagihanPembayaranRequest{TagihanPembayaran: tagihanPembayaran}
	response, err := e.AddTagihanPembayaranEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(AddTagihanPembayaranResponse).Message, response.(AddTagihanPembayaranResponse).Err
}

// EditTagihanPembayaran implements Service. Primarily useful in a client.
func (e Endpoints) EditTagihanPembayaran(ctx context.Context, tagihanPembayaran io.TagihanPembayaran) (message string, err error) {
	request := EditTagihanPembayaranRequest{TagihanPembayaran: tagihanPembayaran}
	response, err := e.EditTagihanPembayaranEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(EditTagihanPembayaranResponse).Message, response.(EditTagihanPembayaranResponse).Err
}

// DeleteTagihanPembayaran implements Service. Primarily useful in a client.
func (e Endpoints) DeleteTagihanPembayaran(ctx context.Context, id string) (message string, err error) {
	request := DeleteTagihanPembayaranRequest{Id: id}
	response, err := e.DeleteTagihanPembayaranEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(DeleteTagihanPembayaranResponse).Message, response.(DeleteTagihanPembayaranResponse).Err
}

// GetTagihanPembayaranById implements Service. Primarily useful in a client.
func (e Endpoints) GetTagihanPembayaranById(ctx context.Context, id string) (tagihanPembayaran io.TagihanPembayaran, message string, err error) {
	request := GetTagihanPembayaranByIdRequest{Id: id}
	response, err := e.GetTagihanPembayaranByIdEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetTagihanPembayaranByIdResponse).TagihanPembayaran, response.(GetTagihanPembayaranByIdResponse).Message, response.(GetTagihanPembayaranByIdResponse).Err
}
