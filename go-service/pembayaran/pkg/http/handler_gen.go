// THIS FILE IS AUTO GENERATED BY GK-CLI DO NOT EDIT!!
package http

import (
	http "github.com/go-kit/kit/transport/http"
	mux "github.com/gorilla/mux"
	endpoint "akuklinikid_demo/go-service/pembayaran/pkg/endpoint"
	http1 "net/http"
)

// NewHTTPHandler returns a handler that makes a set of endpoints available on
// predefined paths.
func NewHTTPHandler(endpoints endpoint.Endpoints, options map[string][]http.ServerOption) http1.Handler {
	m := mux.NewRouter()
	makeAddItemPembayaranHandler(m, endpoints, options["AddItemPembayaran"])
	makeEntryPembayaranHandler(m, endpoints, options["EntryPembayaran"])
	makeGetPembayaranHandler(m, endpoints, options["GetPembayaran"])
	makeGetTagihanPembayaranHandler(m, endpoints, options["GetTagihanPembayaran"])
	makeAddTagihanPembayaranHandler(m, endpoints, options["AddTagihanPembayaran"])
	makeEditTagihanPembayaranHandler(m, endpoints, options["EditTagihanPembayaran"])
	makeDeleteTagihanPembayaranHandler(m, endpoints, options["DeleteTagihanPembayaran"])
	makeGetTagihanPembayaranByIdHandler(m, endpoints, options["GetTagihanPembayaranById"])
	return m
}
