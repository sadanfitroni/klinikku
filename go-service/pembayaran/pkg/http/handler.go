package http

import (
	"context"
	"encoding/json"
	"errors"
	endpoint "akuklinikid_demo/go-service/pembayaran/pkg/endpoint"
	http1 "net/http"

	http "github.com/go-kit/kit/transport/http"
	handlers "github.com/gorilla/handlers"
	mux "github.com/gorilla/mux"
)

// makeAddItemPembayaranHandler creates the handler logic
func makeAddItemPembayaranHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/add-item-pembayaran").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.AddItemPembayaranEndpoint, decodeAddItemPembayaranRequest, encodeAddItemPembayaranResponse, options...)))
}

// decodeAddItemPembayaranRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeAddItemPembayaranRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.AddItemPembayaranRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeAddItemPembayaranResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeAddItemPembayaranResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeEntryPembayaranHandler creates the handler logic
func makeEntryPembayaranHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/entry-pembayaran").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.EntryPembayaranEndpoint, decodeEntryPembayaranRequest, encodeEntryPembayaranResponse, options...)))
}

// decodeEntryPembayaranRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeEntryPembayaranRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.EntryPembayaranRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeEntryPembayaranResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeEntryPembayaranResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}
func ErrorEncoder(_ context.Context, err error, w http1.ResponseWriter) {
	w.WriteHeader(err2code(err))
	json.NewEncoder(w).Encode(errorWrapper{Error: err.Error()})
}
func ErrorDecoder(r *http1.Response) error {
	var w errorWrapper
	if err := json.NewDecoder(r.Body).Decode(&w); err != nil {
		return err
	}
	return errors.New(w.Error)
}

// This is used to set the http status, see an example here :
// https://github.com/go-kit/kit/blob/master/examples/addsvc/pkg/addtransport/http.go#L133
func err2code(err error) int {
	return http1.StatusInternalServerError
}

type errorWrapper struct {
	Error string `json:"error"`
}

// makeGetPembayaranHandler creates the handler logic
func makeGetPembayaranHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("GET").Path("/get-pembayaran").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"GET"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetPembayaranEndpoint, decodeGetPembayaranRequest, encodeGetPembayaranResponse, options...)))
}

// decodeGetPembayaranRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetPembayaranRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetPembayaranRequest{}
	// err := json.NewDecoder(r.Body).Decode(&req)
	return req, nil
}

// encodeGetPembayaranResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetPembayaranResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetTagihanPembayaranHandler creates the handler logic
func makeGetTagihanPembayaranHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("GET").Path("/get-tagihan-pembayaran").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"GET"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
	)(http.NewServer(endpoints.GetTagihanPembayaranEndpoint, decodeGetTagihanPembayaranRequest, encodeGetTagihanPembayaranResponse, options...)))
}

// decodeGetTagihanPembayaranRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetTagihanPembayaranRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetTagihanPembayaranRequest{}
	// err := json.NewDecoder(r.Body).Decode(&req)
	return req, nil
}

// encodeGetTagihanPembayaranResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetTagihanPembayaranResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeAddTagihanPembayaranHandler creates the handler logic
func makeAddTagihanPembayaranHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/add-tagihan-pembayaran").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
	)(http.NewServer(endpoints.AddTagihanPembayaranEndpoint, decodeAddTagihanPembayaranRequest, encodeAddTagihanPembayaranResponse, options...)))
}

// decodeAddTagihanPembayaranRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeAddTagihanPembayaranRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.AddTagihanPembayaranRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeAddTagihanPembayaranResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeAddTagihanPembayaranResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeEditTagihanPembayaranHandler creates the handler logic
func makeEditTagihanPembayaranHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("PUT", "OPTIONS").Path("/edit-tagihan-pembayaran").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"PUT"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
	)(http.NewServer(endpoints.EditTagihanPembayaranEndpoint, decodeEditTagihanPembayaranRequest, encodeEditTagihanPembayaranResponse, options...)))
}

// decodeEditTagihanPembayaranRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeEditTagihanPembayaranRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.EditTagihanPembayaranRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeEditTagihanPembayaranResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeEditTagihanPembayaranResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeDeleteTagihanPembayaranHandler creates the handler logic
func makeDeleteTagihanPembayaranHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("PUT", "OPTIONS").Path("/delete-tagihan-pembayaran").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"PUT"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
	)(http.NewServer(endpoints.DeleteTagihanPembayaranEndpoint, decodeDeleteTagihanPembayaranRequest, encodeDeleteTagihanPembayaranResponse, options...)))
}

// decodeDeleteTagihanPembayaranRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeDeleteTagihanPembayaranRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.DeleteTagihanPembayaranRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeDeleteTagihanPembayaranResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeDeleteTagihanPembayaranResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetTagihanPembayaranByIdHandler creates the handler logic
func makeGetTagihanPembayaranByIdHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-tagihan-pembayaran-by-id").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
	)(http.NewServer(endpoints.GetTagihanPembayaranByIdEndpoint, decodeGetTagihanPembayaranByIdRequest, encodeGetTagihanPembayaranByIdResponse, options...)))
}

// decodeGetTagihanPembayaranByIdRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetTagihanPembayaranByIdRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetTagihanPembayaranByIdRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetTagihanPembayaranByIdResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetTagihanPembayaranByIdResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}
