package service

import (
	"context"
	io "akuklinikid_demo/go-service/io"

	log "github.com/go-kit/kit/log"
)

// Middleware describes a service middleware.
type Middleware func(PembayaranService) PembayaranService

type loggingMiddleware struct {
	logger log.Logger
	next   PembayaranService
}

// LoggingMiddleware takes a logger as a dependency
// and returns a PembayaranService Middleware.
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next PembayaranService) PembayaranService {
		return &loggingMiddleware{logger, next}
	}

}

func (l loggingMiddleware) AddItemPembayaran(ctx context.Context, visitId string, pembayaran io.Pembayaran) (message string, err error) {
	defer func() {
		l.logger.Log("method", "AddItemPembayaran", "visitId", visitId, "pembayaran", pembayaran, "message", message, "err", err)
	}()
	return l.next.AddItemPembayaran(ctx, visitId, pembayaran)
}
func (l loggingMiddleware) EntryPembayaran(ctx context.Context, visitId string, visit io.Visit) (message string, err error) {
	defer func() {
		l.logger.Log("method", "EntryPembayaran", "visitId", visitId, "visit", visit, "message", message, "err", err)
	}()
	return l.next.EntryPembayaran(ctx, visitId, visit)
}

func (l loggingMiddleware) GetPembayaran(ctx context.Context) (pembayaran []io.Pembayaran, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetPembayaran", "pembayaran", pembayaran, "message", message, "err", err)
	}()
	return l.next.GetPembayaran(ctx)
}

func (l loggingMiddleware) GetTagihanPembayaran(ctx context.Context) (tagihanPembayaran []io.TagihanPembayaran, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetTagihanPembayaran", "tagihanPembayaran", tagihanPembayaran, "message", message, "err", err)
	}()
	return l.next.GetTagihanPembayaran(ctx)
}
func (l loggingMiddleware) AddTagihanPembayaran(ctx context.Context, tagihanPembayaran io.TagihanPembayaran) (message string, err error) {
	defer func() {
		l.logger.Log("method", "AddTagihanPembayaran", "tagihanPembayaran", tagihanPembayaran, "message", message, "err", err)
	}()
	return l.next.AddTagihanPembayaran(ctx, tagihanPembayaran)
}
func (l loggingMiddleware) EditTagihanPembayaran(ctx context.Context, tagihanPembayaran io.TagihanPembayaran) (message string, err error) {
	defer func() {
		l.logger.Log("method", "EditTagihanPembayaran", "tagihanPembayaran", tagihanPembayaran, "message", message, "err", err)
	}()
	return l.next.EditTagihanPembayaran(ctx, tagihanPembayaran)
}
func (l loggingMiddleware) DeleteTagihanPembayaran(ctx context.Context, id string) (message string, err error) {
	defer func() {
		l.logger.Log("method", "DeleteTagihanPembayaran", "id", id, "message", message, "err", err)
	}()
	return l.next.DeleteTagihanPembayaran(ctx, id)
}
func (l loggingMiddleware) GetTagihanPembayaranById(ctx context.Context, id string) (tagihanPembayaran io.TagihanPembayaran, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetTagihanPembayaranById", "id", id, "tagihanPembayaran", tagihanPembayaran, "message", message, "err", err)
	}()
	return l.next.GetTagihanPembayaranById(ctx, id)
}
