package service

import (
	"context"
	"database/sql"
	"akuklinikid_demo/go-service/db"
	"akuklinikid_demo/go-service/io"
)

// PembayaranService describes the service.
type PembayaranService interface {
	// Add your methods here
	AddItemPembayaran(ctx context.Context, visitId string, pembayaran io.Pembayaran) (message string, err error)
	EntryPembayaran(ctx context.Context, visitId string, visit io.Visit) (message string, err error)
	GetPembayaran(ctx context.Context) (pembayaran []io.Pembayaran, message string, err error)

	GetTagihanPembayaran(ctx context.Context) (tagihanPembayaran []io.TagihanPembayaran, message string, err error)
	AddTagihanPembayaran(ctx context.Context, tagihanPembayaran io.TagihanPembayaran) (message string, err error)
	EditTagihanPembayaran(ctx context.Context, tagihanPembayaran io.TagihanPembayaran) (message string, err error)
	DeleteTagihanPembayaran(ctx context.Context, id string) (message string, err error)
	GetTagihanPembayaranById(ctx context.Context, id string) (tagihanPembayaran io.TagihanPembayaran, message string, err error)
}

type basicPembayaranService struct{}

func (b *basicPembayaranService) AddItemPembayaran(ctx context.Context, visitId string, pembayaran io.Pembayaran) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return message, err
	}
	defer session.Close()

	var klinikId int
	var dokterId int
	var pasienId int
	row := session.QueryRow("select visit.fk_dokter,visit.fk_pasien,jadwal_praktek.fk_klinik from visit,jadwal_praktek where "+
		"visit.fk_jadwal_praktek = jadwal_praktek.id and visit.id=?", visitId)
	err = row.Scan(&dokterId, &pasienId, &klinikId)
	if err != nil {
		message = "Data visit not found"
		return message, err
	}

	query, err := session.Prepare("insert into pembayaran values(?,?,?,?,?,?,?,?,?,?)")
	if err != nil {
		message = "Query error"
		return message, err
	}
	_, err = query.Exec(nil, klinikId, visitId, dokterId, pasienId, pembayaran.Item, pembayaran.Nominal, pembayaran.Quantity,
		pembayaran.Diskon, pembayaran.Subtotal)
	if err != nil {
		message = "Failed insert into pembayaran"
		return message, err
	}
	message = "Entry item pembayaran Success"
	return message, err
}
func (b *basicPembayaranService) EntryPembayaran(ctx context.Context, visitId string, visit io.Visit) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return message, err
	}
	defer session.Close()

	query, err := session.Prepare("update visit set status='Sudah Bayar',tipe_pembayaran=?,no_polis=?,nama_asuransi=?,kode_pembayaran=?,total=? " +
		"where id=?")
	if err != nil {
		message = "Query error"
		return message, err
	}
	_, err = query.Exec(
		visit.Tipe_pembayaran,
		visit.No_polis,
		visit.Nama_asuransi,
		visit.Kode_pembayaran,
		visit.Total,
		visitId)
	if err != nil {
		message = "Failed update into visit"
		return message, err
	}

	message = "Entry pembayaran Success"
	return message, err
}

// NewBasicPembayaranService returns a naive, stateless implementation of PembayaranService.
func NewBasicPembayaranService() PembayaranService {
	return &basicPembayaranService{}
}

// New returns a PembayaranService with all of the expected middleware wired in.
func New(middleware []Middleware) PembayaranService {
	var svc PembayaranService = NewBasicPembayaranService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}

func (b *basicPembayaranService) GetPembayaran(ctx context.Context) (pembayaran []io.Pembayaran, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return pembayaran, message, err
	}
	defer session.Close()

	rows, err := session.Query("select * from pembayaran;")
	if err != nil {
		message = "Query failed"
		return pembayaran, message, err
	}
	defer rows.Close()

	row := io.Pembayaran{}
	for rows.Next() {
		var err = rows.Scan(&row.Id, &row.Fk_klinik, &row.Fk_visit, &row.Fk_dokter, &row.Fk_pasien, &row.Item, &row.Nominal, &row.Quantity, &row.Diskon, &row.Subtotal)
		if err != nil {
			message = "Unmatched field name"
			return pembayaran, message, err
		}
		pembayaran = append(pembayaran, row)
	}
	return pembayaran, message, err
}

func (b *basicPembayaranService) GetTagihanPembayaran(ctx context.Context) (tagihanPembayaran []io.TagihanPembayaran, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return tagihanPembayaran, message, err
	}
	defer session.Close()

	rows, err := session.Query("select * from tagihan_langganan;")
	if err != nil {
		message = "Query failed"
		return tagihanPembayaran, message, err
	}
	defer rows.Close()

	row := io.TagihanPembayaran{}
	for rows.Next() {
		var err = rows.Scan(&row.Id, &row.Tahun, &row.Bulan, &row.Fk_klinik, &row.Dari_tanggal, &row.Sampai_tanggal, &row.Total_tagihan, &row.Total_kunjungan, &row.Status, &row.Tanggal_pembayaran, &row.Jumlah_pembayaran)
		if err != nil {
			message = "Unmatched field name"
			return tagihanPembayaran, message, err
		}
		tagihanPembayaran = append(tagihanPembayaran, row)
	}
	return tagihanPembayaran, message, err
}
func (b *basicPembayaranService) AddTagihanPembayaran(ctx context.Context, tagihanPembayaran io.TagihanPembayaran) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("insert into tagihan_langganan values(?,?,?,?,?,?,?,?,?,?,?);",nil, tagihanPembayaran.Tahun, tagihanPembayaran.Bulan, tagihanPembayaran.Fk_klinik, tagihanPembayaran.Dari_tanggal, tagihanPembayaran.Sampai_tanggal, tagihanPembayaran.Total_tagihan, tagihanPembayaran.Total_kunjungan, tagihanPembayaran.Status, tagihanPembayaran.Tanggal_pembayaran, tagihanPembayaran.Jumlah_pembayaran)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}
	message="insert success"
	return message, err
}
func (b *basicPembayaranService) EditTagihanPembayaran(ctx context.Context, tagihanPembayaran io.TagihanPembayaran) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("update tagihan_langganan set tahun=?, bulan=?, fk_klinik=?, dari_tanggal=?, sampai_tanggal=?, total_tagihan=?, total_kunjungan=?, status=?, tanggal_pembayaran=?, jumlah_pembayaran=? where id = ?", tagihanPembayaran.Tahun, tagihanPembayaran.Bulan, tagihanPembayaran.Fk_klinik, tagihanPembayaran.Dari_tanggal, tagihanPembayaran.Sampai_tanggal, tagihanPembayaran.Total_tagihan, tagihanPembayaran.Total_kunjungan, tagihanPembayaran.Status, tagihanPembayaran.Tanggal_pembayaran, tagihanPembayaran.Jumlah_pembayaran, tagihanPembayaran.Id)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}

	message = "Edit success"
	return message, err
}
func (b *basicPembayaranService) DeleteTagihanPembayaran(ctx context.Context, id string) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("delete from tagihan_langganan where id=?",id)
	if err != nil {
		message = "Failed execution delete query"
		return message, err
	}
	message = "Delete success"
	return message, err
}
func (b *basicPembayaranService) GetTagihanPembayaranById(ctx context.Context, id string) (tagihanPembayaran io.TagihanPembayaran, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return tagihanPembayaran, message, err
	}
	defer session.Close()

	query := "select * from tagihan_langganan where id=?"
	row := session.QueryRow(query, id)
	err = row.Scan(&tagihanPembayaran.Id, &tagihanPembayaran.Tahun, &tagihanPembayaran.Bulan, &tagihanPembayaran.Fk_klinik, &tagihanPembayaran.Dari_tanggal, &tagihanPembayaran.Sampai_tanggal, &tagihanPembayaran.Total_tagihan, &tagihanPembayaran.Total_kunjungan, &tagihanPembayaran.Status, &tagihanPembayaran.Tanggal_pembayaran, &tagihanPembayaran.Jumlah_pembayaran)

	switch err {
	case sql.ErrNoRows:
		message = "Data not found"
		return tagihanPembayaran, message, err
	case nil:
		message = "Sucecess get tagihanPembayaran by id"
		return tagihanPembayaran, message, err
	default:
		message = "Mysql error "
		return tagihanPembayaran, message, err
	}
}
