package service

import (
	"context"
	log "github.com/go-kit/kit/log"
	io "kliniku/go-service/io"
)

// Middleware describes a service middleware.
type Middleware func(MstDataService) MstDataService

type loggingMiddleware struct {
	logger log.Logger
	next   MstDataService
}

// LoggingMiddleware takes a logger as a dependency
// and returns a MstDataService Middleware.
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next MstDataService) MstDataService {
		return &loggingMiddleware{logger, next}
	}

}

func (l loggingMiddleware) GetKlinik(ctx context.Context) (klinik []io.Klinik, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetKlinik", "klinik", klinik, "message", message, "err", err)
	}()
	return l.next.GetKlinik(ctx)
}
func (l loggingMiddleware) AddKlinik(ctx context.Context, klinik io.Klinik) (message string, err error) {
	defer func() {
		l.logger.Log("method", "AddKlinik", "klinik", klinik, "message", message, "err", err)
	}()
	return l.next.AddKlinik(ctx, klinik)
}
func (l loggingMiddleware) EditKlinik(ctx context.Context, klinik io.Klinik) (message string, err error) {
	defer func() {
		l.logger.Log("method", "EditKlinik", "klinik", klinik, "message", message, "err", err)
	}()
	return l.next.EditKlinik(ctx, klinik)
}
func (l loggingMiddleware) DeleteKlinik(ctx context.Context, id string) (message string, err error) {
	defer func() {
		l.logger.Log("method", "DeleteKlinik", "id", id, "message", message, "err", err)
	}()
	return l.next.DeleteKlinik(ctx, id)
}
func (l loggingMiddleware) GetKlinikById(ctx context.Context, id string) (klinik io.Klinik, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetKlinikById", "id", id, "klinik", klinik, "message", message, "err", err)
	}()
	return l.next.GetKlinikById(ctx, id)
}
func (l loggingMiddleware) GetVisit(ctx context.Context) (visit []io.Visit, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetVisit", "visit", visit, "message", message, "err", err)
	}()
	return l.next.GetVisit(ctx)
}
func (l loggingMiddleware) AddVisit(ctx context.Context, visit io.Visit) (message string, err error) {
	defer func() {
		l.logger.Log("method", "AddVisit", "visit", visit, "message", message, "err", err)
	}()
	return l.next.AddVisit(ctx, visit)
}
func (l loggingMiddleware) EditVisit(ctx context.Context, visit io.Visit) (message string, err error) {
	defer func() {
		l.logger.Log("method", "EditVisit", "visit", visit, "message", message, "err", err)
	}()
	return l.next.EditVisit(ctx, visit)
}
func (l loggingMiddleware) DeleteVisit(ctx context.Context, id string) (message string, err error) {
	defer func() {
		l.logger.Log("method", "DeleteVisit", "id", id, "message", message, "err", err)
	}()
	return l.next.DeleteVisit(ctx, id)
}
func (l loggingMiddleware) GetVisitById(ctx context.Context, id string) (visit io.Visit, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetVisitById", "id", id, "visit", visit, "message", message, "err", err)
	}()
	return l.next.GetVisitById(ctx, id)
}
func (l loggingMiddleware) GetPraktek(ctx context.Context) (praktek []io.Praktek, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetPraktek", "praktek", praktek, "message", message, "err", err)
	}()
	return l.next.GetPraktek(ctx)
}
func (l loggingMiddleware) AddPraktek(ctx context.Context, praktek io.Praktek) (message string, err error) {
	defer func() {
		l.logger.Log("method", "AddPraktek", "praktek", praktek, "message", message, "err", err)
	}()
	return l.next.AddPraktek(ctx, praktek)
}
func (l loggingMiddleware) EditPraktek(ctx context.Context, praktek io.Praktek) (message string, err error) {
	defer func() {
		l.logger.Log("method", "EditPraktek", "praktek", praktek, "message", message, "err", err)
	}()
	return l.next.EditPraktek(ctx, praktek)
}
func (l loggingMiddleware) DeletePraktek(ctx context.Context, id string) (message string, err error) {
	defer func() {
		l.logger.Log("method", "DeletePraktek", "id", id, "message", message, "err", err)
	}()
	return l.next.DeletePraktek(ctx, id)
}
func (l loggingMiddleware) GetPraktekById(ctx context.Context, id string) (praktek io.Praktek, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetPraktekById", "id", id, "praktek", praktek, "message", message, "err", err)
	}()
	return l.next.GetPraktekById(ctx, id)
}
func (l loggingMiddleware) GetLangganan(ctx context.Context) (langganan []io.Langganan, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetLangganan", "langganan", langganan, "message", message, "err", err)
	}()
	return l.next.GetLangganan(ctx)
}
func (l loggingMiddleware) AddLangganan(ctx context.Context, langganan io.Langganan) (message string, err error) {
	defer func() {
		l.logger.Log("method", "AddLangganan", "langganan", langganan, "message", message, "err", err)
	}()
	return l.next.AddLangganan(ctx, langganan)
}
func (l loggingMiddleware) EditLangganan(ctx context.Context, langganan io.Langganan) (message string, err error) {
	defer func() {
		l.logger.Log("method", "EditLangganan", "langganan", langganan, "message", message, "err", err)
	}()
	return l.next.EditLangganan(ctx, langganan)
}
func (l loggingMiddleware) DeleteLangganan(ctx context.Context, id string) (message string, err error) {
	defer func() {
		l.logger.Log("method", "DeleteLangganan", "id", id, "message", message, "err", err)
	}()
	return l.next.DeleteLangganan(ctx, id)
}
func (l loggingMiddleware) GetLanggananById(ctx context.Context, id string) (langganan io.Langganan, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetLanggananById", "id", id, "langganan", langganan, "message", message, "err", err)
	}()
	return l.next.GetLanggananById(ctx, id)
}
func (l loggingMiddleware) GetLanggananByKlinik(ctx context.Context, klinikId string) (langganan []io.Langganan, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetLanggananByKlinik", "klinikId", klinikId, "langganan", langganan, "message", message, "err", err)
	}()
	return l.next.GetLanggananByKlinik(ctx, klinikId)
}
func (l loggingMiddleware) GetAllDokterByKlinik(ctx context.Context, klinikId string) (dokters []io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetAllDokterByKlinik", "klinikId", klinikId, "dokters", dokters, "message", message, "err", err)
	}()
	return l.next.GetAllDokterByKlinik(ctx, klinikId)
}
func (l loggingMiddleware) GetAllPasienByKlinik(ctx context.Context, klinikId string) (pasiens []io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetAllPasienByKlinik", "klinikId", klinikId, "pasiens", pasiens, "message", message, "err", err)
	}()
	return l.next.GetAllPasienByKlinik(ctx, klinikId)
}
func (l loggingMiddleware) GetAllKlinikIdByDokter(ctx context.Context, dokterId string) (klinikIds []string, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetAllKlinikIdByDokter", "dokterId", dokterId, "klinikIds", klinikIds, "message", message, "err", err)
	}()
	return l.next.GetAllKlinikIdByDokter(ctx, dokterId)
}
func (l loggingMiddleware) GetAllTodayAndLaterHistoryPraktek(ctx context.Context) (historyPrakteks []io.HistoryPraktek, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetAllTodayAndLaterHistoryPraktek", "historyPrakteks", historyPrakteks, "message", message, "err", err)
	}()
	return l.next.GetAllTodayAndLaterHistoryPraktek(ctx)
}
func (l loggingMiddleware) GetHistoryPraktek(ctx context.Context, jadwalPraktekId string) (historyPraktek io.HistoryPraktek, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetHistoryPraktek", "jadwalPraktekId", jadwalPraktekId, "historyPraktek", historyPraktek, "message", message, "err", err)
	}()
	return l.next.GetHistoryPraktek(ctx, jadwalPraktekId)
}
func (l loggingMiddleware) RegisterDokter(ctx context.Context, klinikId string, userId string) (message string, err error) {
	defer func() {
		l.logger.Log("method", "RegisterDokter", "klinikId", klinikId, "userId", userId, "message", message, "err", err)
	}()
	return l.next.RegisterDokter(ctx, klinikId, userId)
}
