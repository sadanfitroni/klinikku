package http

import (
	"context"
	"encoding/json"
	"errors"
	http "github.com/go-kit/kit/transport/http"
	handlers "github.com/gorilla/handlers"
	mux "github.com/gorilla/mux"
	endpoint "kliniku/go-service/mst_data/pkg/endpoint"
	http1 "net/http"
)

// makeGetKlinikHandler creates the handler logic
func makeGetKlinikHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-klinik").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetKlinikEndpoint, decodeGetKlinikRequest, encodeGetKlinikResponse, options...)))
}

// decodeGetKlinikRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetKlinikRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetKlinikRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetKlinikResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetKlinikResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeAddKlinikHandler creates the handler logic
func makeAddKlinikHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/add-klinik").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.AddKlinikEndpoint, decodeAddKlinikRequest, encodeAddKlinikResponse, options...)))
}

// decodeAddKlinikRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeAddKlinikRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.AddKlinikRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeAddKlinikResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeAddKlinikResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeEditKlinikHandler creates the handler logic
func makeEditKlinikHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/edit-klinik").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.EditKlinikEndpoint, decodeEditKlinikRequest, encodeEditKlinikResponse, options...)))
}

// decodeEditKlinikRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeEditKlinikRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.EditKlinikRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeEditKlinikResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeEditKlinikResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeDeleteKlinikHandler creates the handler logic
func makeDeleteKlinikHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/delete-klinik").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.DeleteKlinikEndpoint, decodeDeleteKlinikRequest, encodeDeleteKlinikResponse, options...)))
}

// decodeDeleteKlinikRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeDeleteKlinikRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.DeleteKlinikRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeDeleteKlinikResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeDeleteKlinikResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetKlinikByIdHandler creates the handler logic
func makeGetKlinikByIdHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-klinik-by-id").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetKlinikByIdEndpoint, decodeGetKlinikByIdRequest, encodeGetKlinikByIdResponse, options...)))
}

// decodeGetKlinikByIdRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetKlinikByIdRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetKlinikByIdRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetKlinikByIdResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetKlinikByIdResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetVisitHandler creates the handler logic
func makeGetVisitHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-visit").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetVisitEndpoint, decodeGetVisitRequest, encodeGetVisitResponse, options...)))
}

// decodeGetVisitRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetVisitRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetVisitRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetVisitResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetVisitResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeAddVisitHandler creates the handler logic
func makeAddVisitHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/add-visit").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.AddVisitEndpoint, decodeAddVisitRequest, encodeAddVisitResponse, options...)))
}

// decodeAddVisitRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeAddVisitRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.AddVisitRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeAddVisitResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeAddVisitResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeEditVisitHandler creates the handler logic
func makeEditVisitHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/edit-visit").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.EditVisitEndpoint, decodeEditVisitRequest, encodeEditVisitResponse, options...)))
}

// decodeEditVisitRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeEditVisitRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.EditVisitRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeEditVisitResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeEditVisitResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeDeleteVisitHandler creates the handler logic
func makeDeleteVisitHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/delete-visit").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.DeleteVisitEndpoint, decodeDeleteVisitRequest, encodeDeleteVisitResponse, options...)))
}

// decodeDeleteVisitRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeDeleteVisitRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.DeleteVisitRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeDeleteVisitResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeDeleteVisitResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetVisitByIdHandler creates the handler logic
func makeGetVisitByIdHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-visit-by-id").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetVisitByIdEndpoint, decodeGetVisitByIdRequest, encodeGetVisitByIdResponse, options...)))
}

// decodeGetVisitByIdRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetVisitByIdRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetVisitByIdRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetVisitByIdResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetVisitByIdResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetPraktekHandler creates the handler logic
func makeGetPraktekHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-praktek").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetPraktekEndpoint, decodeGetPraktekRequest, encodeGetPraktekResponse, options...)))
}

// decodeGetPraktekRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetPraktekRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetPraktekRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetPraktekResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetPraktekResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeAddPraktekHandler creates the handler logic
func makeAddPraktekHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/add-praktek").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.AddPraktekEndpoint, decodeAddPraktekRequest, encodeAddPraktekResponse, options...)))
}

// decodeAddPraktekRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeAddPraktekRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.AddPraktekRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeAddPraktekResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeAddPraktekResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeEditPraktekHandler creates the handler logic
func makeEditPraktekHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/edit-praktek").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.EditPraktekEndpoint, decodeEditPraktekRequest, encodeEditPraktekResponse, options...)))
}

// decodeEditPraktekRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeEditPraktekRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.EditPraktekRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeEditPraktekResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeEditPraktekResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeDeletePraktekHandler creates the handler logic
func makeDeletePraktekHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/delete-praktek").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.DeletePraktekEndpoint, decodeDeletePraktekRequest, encodeDeletePraktekResponse, options...)))
}

// decodeDeletePraktekRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeDeletePraktekRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.DeletePraktekRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeDeletePraktekResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeDeletePraktekResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetPraktekByIdHandler creates the handler logic
func makeGetPraktekByIdHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-praktek-by-id").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetPraktekByIdEndpoint, decodeGetPraktekByIdRequest, encodeGetPraktekByIdResponse, options...)))
}

// decodeGetPraktekByIdRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetPraktekByIdRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetPraktekByIdRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetPraktekByIdResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetPraktekByIdResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetLanggananHandler creates the handler logic
func makeGetLanggananHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-langganan").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetLanggananEndpoint, decodeGetLanggananRequest, encodeGetLanggananResponse, options...)))
}

// decodeGetLanggananRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetLanggananRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetLanggananRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetLanggananResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetLanggananResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeAddLanggananHandler creates the handler logic
func makeAddLanggananHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/add-langganan").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.AddLanggananEndpoint, decodeAddLanggananRequest, encodeAddLanggananResponse, options...)))
}

// decodeAddLanggananRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeAddLanggananRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.AddLanggananRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeAddLanggananResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeAddLanggananResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeEditLanggananHandler creates the handler logic
func makeEditLanggananHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/edit-langganan").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.EditLanggananEndpoint, decodeEditLanggananRequest, encodeEditLanggananResponse, options...)))
}

// decodeEditLanggananRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeEditLanggananRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.EditLanggananRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeEditLanggananResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeEditLanggananResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeDeleteLanggananHandler creates the handler logic
func makeDeleteLanggananHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/delete-langganan").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.DeleteLanggananEndpoint, decodeDeleteLanggananRequest, encodeDeleteLanggananResponse, options...)))
}

// decodeDeleteLanggananRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeDeleteLanggananRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.DeleteLanggananRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeDeleteLanggananResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeDeleteLanggananResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetLanggananByIdHandler creates the handler logic
func makeGetLanggananByIdHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-langganan-by-id").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetLanggananByIdEndpoint, decodeGetLanggananByIdRequest, encodeGetLanggananByIdResponse, options...)))
}

// decodeGetLanggananByIdRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetLanggananByIdRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetLanggananByIdRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetLanggananByIdResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetLanggananByIdResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetLanggananByKlinikHandler creates the handler logic
func makeGetLanggananByKlinikHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-langganan-by-klinik").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetLanggananByKlinikEndpoint, decodeGetLanggananByKlinikRequest, encodeGetLanggananByKlinikResponse, options...)))
}

// decodeGetLanggananByKlinikRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetLanggananByKlinikRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetLanggananByKlinikRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetLanggananByKlinikResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetLanggananByKlinikResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetAllDokterByKlinikHandler creates the handler logic
func makeGetAllDokterByKlinikHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-all-dokter-by-klinik").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetAllDokterByKlinikEndpoint, decodeGetAllDokterByKlinikRequest, encodeGetAllDokterByKlinikResponse, options...)))
}

// decodeGetAllDokterByKlinikRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetAllDokterByKlinikRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetAllDokterByKlinikRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetAllDokterByKlinikResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetAllDokterByKlinikResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetAllPasienByKlinikHandler creates the handler logic
func makeGetAllPasienByKlinikHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-all-pasien-by-klinik").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetAllPasienByKlinikEndpoint, decodeGetAllPasienByKlinikRequest, encodeGetAllPasienByKlinikResponse, options...)))
}

// decodeGetAllPasienByKlinikRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetAllPasienByKlinikRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetAllPasienByKlinikRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetAllPasienByKlinikResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetAllPasienByKlinikResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetAllKlinikIdByDokterHandler creates the handler logic
func makeGetAllKlinikIdByDokterHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-all-klinik-id-by-dokter").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetAllKlinikIdByDokterEndpoint, decodeGetAllKlinikIdByDokterRequest, encodeGetAllKlinikIdByDokterResponse, options...)))
}

// decodeGetAllKlinikIdByDokterRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetAllKlinikIdByDokterRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetAllKlinikIdByDokterRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetAllKlinikIdByDokterResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetAllKlinikIdByDokterResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetAllTodayAndLaterHistoryPraktekHandler creates the handler logic
func makeGetAllTodayAndLaterHistoryPraktekHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-all-today-and-later-history-praktek").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetAllTodayAndLaterHistoryPraktekEndpoint, decodeGetAllTodayAndLaterHistoryPraktekRequest, encodeGetAllTodayAndLaterHistoryPraktekResponse, options...)))
}

// decodeGetAllTodayAndLaterHistoryPraktekRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetAllTodayAndLaterHistoryPraktekRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetAllTodayAndLaterHistoryPraktekRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetAllTodayAndLaterHistoryPraktekResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetAllTodayAndLaterHistoryPraktekResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetHistoryPraktekHandler creates the handler logic
func makeGetHistoryPraktekHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-history-praktek").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetHistoryPraktekEndpoint, decodeGetHistoryPraktekRequest, encodeGetHistoryPraktekResponse, options...)))
}

// decodeGetHistoryPraktekRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetHistoryPraktekRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetHistoryPraktekRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetHistoryPraktekResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetHistoryPraktekResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeRegisterDokterHandler creates the handler logic
func makeRegisterDokterHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/register-dokter").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.RegisterDokterEndpoint, decodeRegisterDokterRequest, encodeRegisterDokterResponse, options...)))
}

// decodeRegisterDokterRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeRegisterDokterRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.RegisterDokterRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeRegisterDokterResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeRegisterDokterResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}
func ErrorEncoder(_ context.Context, err error, w http1.ResponseWriter) {
	w.WriteHeader(err2code(err))
	json.NewEncoder(w).Encode(errorWrapper{Error: err.Error()})
}
func ErrorDecoder(r *http1.Response) error {
	var w errorWrapper
	if err := json.NewDecoder(r.Body).Decode(&w); err != nil {
		return err
	}
	return errors.New(w.Error)
}

// This is used to set the http status, see an example here :
// https://github.com/go-kit/kit/blob/master/examples/addsvc/pkg/addtransport/http.go#L133
func err2code(err error) int {
	return http1.StatusInternalServerError
}

type errorWrapper struct {
	Error string `json:"error"`
}
