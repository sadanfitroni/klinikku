package endpoint

import (
	io "akuklinikid_demo/go-service/io"
	service "akuklinikid_demo/go-service/master_data/pkg/service"
	"context"

	endpoint "github.com/go-kit/kit/endpoint"
)

// GetKlinikRequest collects the request parameters for the GetKlinik method.
type GetKlinikRequest struct{}

// GetKlinikResponse collects the response parameters for the GetKlinik method.
type GetKlinikResponse struct {
	Klinik  []io.Klinik `json:"klinik"`
	Message string      `json:"message"`
	Err     error       `json:"err"`
}

// MakeGetKlinikEndpoint returns an endpoint that invokes GetKlinik on the service.
func MakeGetKlinikEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		klinik, message, err := s.GetKlinik(ctx)
		return GetKlinikResponse{
			Err:     err,
			Klinik:  klinik,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetKlinikResponse) Failed() error {
	return r.Err
}

// AddKlinikRequest collects the request parameters for the AddKlinik method.
type AddKlinikRequest struct {
	Klinik io.Klinik `json:"klinik"`
}

// AddKlinikResponse collects the response parameters for the AddKlinik method.
type AddKlinikResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeAddKlinikEndpoint returns an endpoint that invokes AddKlinik on the service.
func MakeAddKlinikEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddKlinikRequest)
		message, err := s.AddKlinik(ctx, req.Klinik)
		return AddKlinikResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r AddKlinikResponse) Failed() error {
	return r.Err
}

// EditKlinikRequest collects the request parameters for the EditKlinik method.
type EditKlinikRequest struct {
	Klinik io.Klinik `json:"klinik"`
}

// EditKlinikResponse collects the response parameters for the EditKlinik method.
type EditKlinikResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeEditKlinikEndpoint returns an endpoint that invokes EditKlinik on the service.
func MakeEditKlinikEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(EditKlinikRequest)
		message, err := s.EditKlinik(ctx, req.Klinik)
		return EditKlinikResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r EditKlinikResponse) Failed() error {
	return r.Err
}

// DeleteKlinikRequest collects the request parameters for the DeleteKlinik method.
type DeleteKlinikRequest struct {
	Id string `json:"id"`
}

// DeleteKlinikResponse collects the response parameters for the DeleteKlinik method.
type DeleteKlinikResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeDeleteKlinikEndpoint returns an endpoint that invokes DeleteKlinik on the service.
func MakeDeleteKlinikEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteKlinikRequest)
		message, err := s.DeleteKlinik(ctx, req.Id)
		return DeleteKlinikResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r DeleteKlinikResponse) Failed() error {
	return r.Err
}

// Failure is an interface that should be implemented by response types.
// Response encoders can check if responses are Failer, and if so they've
// failed, and if so encode them using a separate write path based on the error.
type Failure interface {
	Failed() error
}

// GetKlinik implements Service. Primarily useful in a client.
func (e Endpoints) GetKlinik(ctx context.Context) (klinik []io.Klinik, message string, err error) {
	request := GetKlinikRequest{}
	response, err := e.GetKlinikEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetKlinikResponse).Klinik, response.(GetKlinikResponse).Message, response.(GetKlinikResponse).Err
}

// AddKlinik implements Service. Primarily useful in a client.
func (e Endpoints) AddKlinik(ctx context.Context, klinik io.Klinik) (message string, err error) {
	request := AddKlinikRequest{Klinik: klinik}
	response, err := e.AddKlinikEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(AddKlinikResponse).Message, response.(AddKlinikResponse).Err
}

// EditKlinik implements Service. Primarily useful in a client.
func (e Endpoints) EditKlinik(ctx context.Context, klinik io.Klinik) (message string, err error) {
	request := EditKlinikRequest{Klinik: klinik}
	response, err := e.EditKlinikEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(EditKlinikResponse).Message, response.(EditKlinikResponse).Err
}

// DeleteKlinik implements Service. Primarily useful in a client.
func (e Endpoints) DeleteKlinik(ctx context.Context, id string) (message string, err error) {
	request := DeleteKlinikRequest{Id: id}
	response, err := e.DeleteKlinikEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(DeleteKlinikResponse).Message, response.(DeleteKlinikResponse).Err
}

// GetKlinikByIdRequest collects the request parameters for the GetKlinikById method.
type GetKlinikByIdRequest struct {
	Id string `json:"id"`
}

// GetKlinikByIdResponse collects the response parameters for the GetKlinikById method.
type GetKlinikByIdResponse struct {
	Klinik  io.Klinik `json:"klinik"`
	Message string    `json:"message"`
	Err     error     `json:"err"`
}

// MakeGetKlinikByIdEndpoint returns an endpoint that invokes GetKlinikById on the service.
func MakeGetKlinikByIdEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetKlinikByIdRequest)
		klinik, message, err := s.GetKlinikById(ctx, req.Id)
		return GetKlinikByIdResponse{
			Err:     err,
			Klinik:  klinik,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetKlinikByIdResponse) Failed() error {
	return r.Err
}

// GetKlinikById implements Service. Primarily useful in a client.
func (e Endpoints) GetKlinikById(ctx context.Context, id string) (klinik io.Klinik, message string, err error) {
	request := GetKlinikByIdRequest{Id: id}
	response, err := e.GetKlinikByIdEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetKlinikByIdResponse).Klinik, response.(GetKlinikByIdResponse).Message, response.(GetKlinikByIdResponse).Err
}

// GetVisitRequest collects the request parameters for the GetVisit method.
type GetVisitRequest struct{}

// GetVisitResponse collects the response parameters for the GetVisit method.
type GetVisitResponse struct {
	Visit   []io.Visit `json:"visit"`
	Message string     `json:"message"`
	Err     error      `json:"err"`
}

// MakeGetVisitEndpoint returns an endpoint that invokes GetVisit on the service.
func MakeGetVisitEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		visit, message, err := s.GetVisit(ctx)
		return GetVisitResponse{
			Err:     err,
			Message: message,
			Visit:   visit,
		}, nil
	}
}

// Failed implements Failer.
func (r GetVisitResponse) Failed() error {
	return r.Err
}

// AddVisitRequest collects the request parameters for the AddVisit method.
type AddVisitRequest struct {
	Visit io.Visit `json:"visit"`
}

// AddVisitResponse collects the response parameters for the AddVisit method.
type AddVisitResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeAddVisitEndpoint returns an endpoint that invokes AddVisit on the service.
func MakeAddVisitEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddVisitRequest)
		message, err := s.AddVisit(ctx, req.Visit)
		return AddVisitResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r AddVisitResponse) Failed() error {
	return r.Err
}

// EditVisitRequest collects the request parameters for the EditVisit method.
type EditVisitRequest struct {
	Visit io.Visit `json:"visit"`
}

// EditVisitResponse collects the response parameters for the EditVisit method.
type EditVisitResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeEditVisitEndpoint returns an endpoint that invokes EditVisit on the service.
func MakeEditVisitEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(EditVisitRequest)
		message, err := s.EditVisit(ctx, req.Visit)
		return EditVisitResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r EditVisitResponse) Failed() error {
	return r.Err
}

// DeleteVisitRequest collects the request parameters for the DeleteVisit method.
type DeleteVisitRequest struct {
	Id string `json:"id"`
}

// DeleteVisitResponse collects the response parameters for the DeleteVisit method.
type DeleteVisitResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeDeleteVisitEndpoint returns an endpoint that invokes DeleteVisit on the service.
func MakeDeleteVisitEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteVisitRequest)
		message, err := s.DeleteVisit(ctx, req.Id)
		return DeleteVisitResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r DeleteVisitResponse) Failed() error {
	return r.Err
}

// GetVisitByIdRequest collects the request parameters for the GetVisitById method.
type GetVisitByIdRequest struct {
	Id string `json:"id"`
}

// GetVisitByIdResponse collects the response parameters for the GetVisitById method.
type GetVisitByIdResponse struct {
	Visit   io.Visit `json:"visit"`
	Message string   `json:"message"`
	Err     error    `json:"err"`
}

// MakeGetVisitByIdEndpoint returns an endpoint that invokes GetVisitById on the service.
func MakeGetVisitByIdEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetVisitByIdRequest)
		visit, message, err := s.GetVisitById(ctx, req.Id)
		return GetVisitByIdResponse{
			Err:     err,
			Message: message,
			Visit:   visit,
		}, nil
	}
}

// Failed implements Failer.
func (r GetVisitByIdResponse) Failed() error {
	return r.Err
}

// GetVisit implements Service. Primarily useful in a client.
func (e Endpoints) GetVisit(ctx context.Context) (visit []io.Visit, message string, err error) {
	request := GetVisitRequest{}
	response, err := e.GetVisitEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetVisitResponse).Visit, response.(GetVisitResponse).Message, response.(GetVisitResponse).Err
}

// AddVisit implements Service. Primarily useful in a client.
func (e Endpoints) AddVisit(ctx context.Context, visit io.Visit) (message string, err error) {
	request := AddVisitRequest{Visit: visit}
	response, err := e.AddVisitEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(AddVisitResponse).Message, response.(AddVisitResponse).Err
}

// EditVisit implements Service. Primarily useful in a client.
func (e Endpoints) EditVisit(ctx context.Context, visit io.Visit) (message string, err error) {
	request := EditVisitRequest{Visit: visit}
	response, err := e.EditVisitEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(EditVisitResponse).Message, response.(EditVisitResponse).Err
}

// DeleteVisit implements Service. Primarily useful in a client.
func (e Endpoints) DeleteVisit(ctx context.Context, id string) (message string, err error) {
	request := DeleteVisitRequest{Id: id}
	response, err := e.DeleteVisitEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(DeleteVisitResponse).Message, response.(DeleteVisitResponse).Err
}

// GetVisitById implements Service. Primarily useful in a client.
func (e Endpoints) GetVisitById(ctx context.Context, id string) (visit io.Visit, message string, err error) {
	request := GetVisitByIdRequest{Id: id}
	response, err := e.GetVisitByIdEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetVisitByIdResponse).Visit, response.(GetVisitByIdResponse).Message, response.(GetVisitByIdResponse).Err
}

// GetPraktekRequest collects the request parameters for the GetPraktek method.
type GetPraktekRequest struct{}

// GetPraktekResponse collects the response parameters for the GetPraktek method.
type GetPraktekResponse struct {
	Praktek []io.Praktek `json:"praktek"`
	Message string       `json:"message"`
	Err     error        `json:"err"`
}

// MakeGetPraktekEndpoint returns an endpoint that invokes GetPraktek on the service.
func MakeGetPraktekEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		praktek, message, err := s.GetPraktek(ctx)
		return GetPraktekResponse{
			Err:     err,
			Message: message,
			Praktek: praktek,
		}, nil
	}
}

// Failed implements Failer.
func (r GetPraktekResponse) Failed() error {
	return r.Err
}

// AddPraktekRequest collects the request parameters for the AddPraktek method.
type AddPraktekRequest struct {
	Praktek io.Praktek `json:"praktek"`
}

// AddPraktekResponse collects the response parameters for the AddPraktek method.
type AddPraktekResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeAddPraktekEndpoint returns an endpoint that invokes AddPraktek on the service.
func MakeAddPraktekEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddPraktekRequest)
		message, err := s.AddPraktek(ctx, req.Praktek)
		return AddPraktekResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r AddPraktekResponse) Failed() error {
	return r.Err
}

// EditPraktekRequest collects the request parameters for the EditPraktek method.
type EditPraktekRequest struct {
	Praktek io.Praktek `json:"praktek"`
}

// EditPraktekResponse collects the response parameters for the EditPraktek method.
type EditPraktekResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeEditPraktekEndpoint returns an endpoint that invokes EditPraktek on the service.
func MakeEditPraktekEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(EditPraktekRequest)
		message, err := s.EditPraktek(ctx, req.Praktek)
		return EditPraktekResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r EditPraktekResponse) Failed() error {
	return r.Err
}

// DeletePraktekRequest collects the request parameters for the DeletePraktek method.
type DeletePraktekRequest struct {
	Id string `json:"id"`
}

// DeletePraktekResponse collects the response parameters for the DeletePraktek method.
type DeletePraktekResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeDeletePraktekEndpoint returns an endpoint that invokes DeletePraktek on the service.
func MakeDeletePraktekEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeletePraktekRequest)
		message, err := s.DeletePraktek(ctx, req.Id)
		return DeletePraktekResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r DeletePraktekResponse) Failed() error {
	return r.Err
}

// GetPraktekByIdRequest collects the request parameters for the GetPraktekById method.
type GetPraktekByIdRequest struct {
	Id string `json:"id"`
}

// GetPraktekByIdResponse collects the response parameters for the GetPraktekById method.
type GetPraktekByIdResponse struct {
	Praktek io.Praktek `json:"praktek"`
	Message string     `json:"message"`
	Err     error      `json:"err"`
}

// MakeGetPraktekByIdEndpoint returns an endpoint that invokes GetPraktekById on the service.
func MakeGetPraktekByIdEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetPraktekByIdRequest)
		praktek, message, err := s.GetPraktekById(ctx, req.Id)
		return GetPraktekByIdResponse{
			Err:     err,
			Message: message,
			Praktek: praktek,
		}, nil
	}
}

// Failed implements Failer.
func (r GetPraktekByIdResponse) Failed() error {
	return r.Err
}

// GetPraktek implements Service. Primarily useful in a client.
func (e Endpoints) GetPraktek(ctx context.Context) (praktek []io.Praktek, message string, err error) {
	request := GetPraktekRequest{}
	response, err := e.GetPraktekEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetPraktekResponse).Praktek, response.(GetPraktekResponse).Message, response.(GetPraktekResponse).Err
}

// AddPraktek implements Service. Primarily useful in a client.
func (e Endpoints) AddPraktek(ctx context.Context, praktek io.Praktek) (message string, err error) {
	request := AddPraktekRequest{Praktek: praktek}
	response, err := e.AddPraktekEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(AddPraktekResponse).Message, response.(AddPraktekResponse).Err
}

// EditPraktek implements Service. Primarily useful in a client.
func (e Endpoints) EditPraktek(ctx context.Context, praktek io.Praktek) (message string, err error) {
	request := EditPraktekRequest{Praktek: praktek}
	response, err := e.EditPraktekEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(EditPraktekResponse).Message, response.(EditPraktekResponse).Err
}

// DeletePraktek implements Service. Primarily useful in a client.
func (e Endpoints) DeletePraktek(ctx context.Context, id string) (message string, err error) {
	request := DeletePraktekRequest{Id: id}
	response, err := e.DeletePraktekEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(DeletePraktekResponse).Message, response.(DeletePraktekResponse).Err
}

// GetPraktekById implements Service. Primarily useful in a client.
func (e Endpoints) GetPraktekById(ctx context.Context, id string) (praktek io.Praktek, message string, err error) {
	request := GetPraktekByIdRequest{Id: id}
	response, err := e.GetPraktekByIdEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetPraktekByIdResponse).Praktek, response.(GetPraktekByIdResponse).Message, response.(GetPraktekByIdResponse).Err
}

// RegisterDokterRequest collects the request parameters for the RegisterDokter method.
type RegisterDokterRequest struct {
	KlinikId string `json:"klinik_id"`
	UserId   string `json:"user_id"`
}

// RegisterDokterResponse collects the response parameters for the RegisterDokter method.
type RegisterDokterResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeRegisterDokterEndpoint returns an endpoint that invokes RegisterDokter on the service.
func MakeRegisterDokterEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(RegisterDokterRequest)
		message, err := s.RegisterDokter(ctx, req.KlinikId, req.UserId)
		return RegisterDokterResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r RegisterDokterResponse) Failed() error {
	return r.Err
}

// RegisterDokter implements Service. Primarily useful in a client.
func (e Endpoints) RegisterDokter(ctx context.Context, klinikId string, userId string) (message string, err error) {
	request := RegisterDokterRequest{
		KlinikId: klinikId,
		UserId:   userId,
	}
	response, err := e.RegisterDokterEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(RegisterDokterResponse).Message, response.(RegisterDokterResponse).Err
}

// GetAllDokterByKlinikRequest collects the request parameters for the GetAllDokterByKlinik method.
type GetAllDokterByKlinikRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetAllDokterByKlinikResponse collects the response parameters for the GetAllDokterByKlinik method.
type GetAllDokterByKlinikResponse struct {
	Dokters []io.User `json:"dokters"`
	Message string    `json:"message"`
	Err     error     `json:"err"`
}

// MakeGetAllDokterByKlinikEndpoint returns an endpoint that invokes GetAllDokterByKlinik on the service.
func MakeGetAllDokterByKlinikEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetAllDokterByKlinikRequest)
		dokters, message, err := s.GetAllDokterByKlinik(ctx, req.KlinikId)
		return GetAllDokterByKlinikResponse{
			Dokters: dokters,
			Message: message,
			Err:     err,
		}, nil
	}
}

// Failed implements Failer.
func (r GetAllDokterByKlinikResponse) Failed() error {
	return r.Err
}

// GetAllDokterByKlinik implements Service. Primarily useful in a client.
func (e Endpoints) GetAllDokterByKlinik(ctx context.Context, klinikId string) (dokters []io.User, err error) {
	request := GetAllDokterByKlinikRequest{KlinikId: klinikId}
	response, err := e.GetAllDokterByKlinikEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetAllDokterByKlinikResponse).Dokters, response.(GetAllDokterByKlinikResponse).Err
}

// GetHistoryPraktekRequest collects the request parameters for the GetHistoryPraktek method.
type GetHistoryPraktekRequest struct {
	JadwalPraktekId string `json:"jadwal_praktek_id"`
}

// GetHistoryPraktekResponse collects the response parameters for the GetHistoryPraktek method.
type GetHistoryPraktekResponse struct {
	HistoryPraktek io.HistoryPraktek `json:"history_praktek"`
	Message        string            `json:"message"`
	Err            error             `json:"err"`
}

// MakeGetHistoryPraktekEndpoint returns an endpoint that invokes GetHistoryPraktek on the service.
func MakeGetHistoryPraktekEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetHistoryPraktekRequest)
		historyPraktek, message, err := s.GetHistoryPraktek(ctx, req.JadwalPraktekId)
		return GetHistoryPraktekResponse{
			Err:            err,
			HistoryPraktek: historyPraktek,
			Message:        message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetHistoryPraktekResponse) Failed() error {
	return r.Err
}

// GetHistoryPraktek implements Service. Primarily useful in a client.
func (e Endpoints) GetHistoryPraktek(ctx context.Context, jadwalPraktekId string) (historyPraktek io.HistoryPraktek, message string, err error) {
	request := GetHistoryPraktekRequest{JadwalPraktekId: jadwalPraktekId}
	response, err := e.GetHistoryPraktekEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetHistoryPraktekResponse).HistoryPraktek, response.(GetHistoryPraktekResponse).Message, response.(GetHistoryPraktekResponse).Err
}

// GetAllPasienByKlinikRequest collects the request parameters for the GetAllPasienByKlinik method.
type GetAllPasienByKlinikRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetAllPasienByKlinikResponse collects the response parameters for the GetAllPasienByKlinik method.
type GetAllPasienByKlinikResponse struct {
	Pasiens []io.User `json:"pasiens"`
	Message string    `json:"message"`
	Err     error     `json:"err"`
}

// MakeGetAllPasienByKlinikEndpoint returns an endpoint that invokes GetAllPasienByKlinik on the service.
func MakeGetAllPasienByKlinikEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetAllPasienByKlinikRequest)
		pasiens, message, err := s.GetAllPasienByKlinik(ctx, req.KlinikId)
		return GetAllPasienByKlinikResponse{
			Err:     err,
			Message: message,
			Pasiens: pasiens,
		}, nil
	}
}

// Failed implements Failer.
func (r GetAllPasienByKlinikResponse) Failed() error {
	return r.Err
}

// GetAllPasienByKlinik implements Service. Primarily useful in a client.
func (e Endpoints) GetAllPasienByKlinik(ctx context.Context, klinikId string) (pasiens []io.User, message string, err error) {
	request := GetAllPasienByKlinikRequest{KlinikId: klinikId}
	response, err := e.GetAllPasienByKlinikEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetAllPasienByKlinikResponse).Pasiens, response.(GetAllPasienByKlinikResponse).Message, response.(GetAllPasienByKlinikResponse).Err
}

// GetAllKlinikIdByDokterRequest collects the request parameters for the GetAllKlinikIdByDokter method.
type GetAllKlinikIdByDokterRequest struct {
	DokterId string `json:"dokter_id"`
}

// GetAllKlinikIdByDokterResponse collects the response parameters for the GetAllKlinikIdByDokter method.
type GetAllKlinikIdByDokterResponse struct {
	KlinikIds []string `json:"klinik_ids"`
	Message   string   `json:"message"`
	Err       error    `json:"err"`
}

// MakeGetAllKlinikIdByDokterEndpoint returns an endpoint that invokes GetAllKlinikIdByDokter on the service.
func MakeGetAllKlinikIdByDokterEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetAllKlinikIdByDokterRequest)
		klinikIds, message, err := s.GetAllKlinikIdByDokter(ctx, req.DokterId)
		return GetAllKlinikIdByDokterResponse{
			Err:       err,
			KlinikIds: klinikIds,
			Message:   message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetAllKlinikIdByDokterResponse) Failed() error {
	return r.Err
}

// GetAllKlinikIdByDokter implements Service. Primarily useful in a client.
func (e Endpoints) GetAllKlinikIdByDokter(ctx context.Context, dokterId string) (klinikIds []string, message string, err error) {
	request := GetAllKlinikIdByDokterRequest{DokterId: dokterId}
	response, err := e.GetAllKlinikIdByDokterEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetAllKlinikIdByDokterResponse).KlinikIds, response.(GetAllKlinikIdByDokterResponse).Message, response.(GetAllKlinikIdByDokterResponse).Err
}

// GetAllTodayAndLaterHistoryPraktekRequest collects the request parameters for the GetAllTodayAndLaterHistoryPraktek method.
type GetAllTodayAndLaterHistoryPraktekRequest struct{}

// GetAllTodayAndLaterHistoryPraktekResponse collects the response parameters for the GetAllTodayAndLaterHistoryPraktek method.
type GetAllTodayAndLaterHistoryPraktekResponse struct {
	HistoryPrakteks []io.HistoryPraktek `json:"history_prakteks"`
	Message         string              `json:"message"`
	Err             error               `json:"err"`
}

// MakeGetAllTodayAndLaterHistoryPraktekEndpoint returns an endpoint that invokes GetAllTodayAndLaterHistoryPraktek on the service.
func MakeGetAllTodayAndLaterHistoryPraktekEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		historyPrakteks, message, err := s.GetAllTodayAndLaterHistoryPraktek(ctx)
		return GetAllTodayAndLaterHistoryPraktekResponse{
			Err:             err,
			HistoryPrakteks: historyPrakteks,
			Message:         message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetAllTodayAndLaterHistoryPraktekResponse) Failed() error {
	return r.Err
}

// GetAllTodayAndLaterHistoryPraktek implements Service. Primarily useful in a client.
func (e Endpoints) GetAllTodayAndLaterHistoryPraktek(ctx context.Context) (historyPrakteks []io.HistoryPraktek, message string, err error) {
	request := GetAllTodayAndLaterHistoryPraktekRequest{}
	response, err := e.GetAllTodayAndLaterHistoryPraktekEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetAllTodayAndLaterHistoryPraktekResponse).HistoryPrakteks, response.(GetAllTodayAndLaterHistoryPraktekResponse).Message, response.(GetAllTodayAndLaterHistoryPraktekResponse).Err
}

// GetLanggananRequest collects the request parameters for the GetLangganan method.
type GetLanggananRequest struct{}

// GetLanggananResponse collects the response parameters for the GetLangganan method.
type GetLanggananResponse struct {
	Langganan []io.Langganan `json:"langganan"`
	Message   string         `json:"message"`
	Err       error          `json:"err"`
}

// MakeGetLanggananEndpoint returns an endpoint that invokes GetLangganan on the service.
func MakeGetLanggananEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		langganan, message, err := s.GetLangganan(ctx)
		return GetLanggananResponse{
			Err:       err,
			Langganan: langganan,
			Message:   message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetLanggananResponse) Failed() error {
	return r.Err
}

// AddLanggananRequest collects the request parameters for the AddLangganan method.
type AddLanggananRequest struct {
	Langganan io.Langganan `json:"langganan"`
}

// AddLanggananResponse collects the response parameters for the AddLangganan method.
type AddLanggananResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeAddLanggananEndpoint returns an endpoint that invokes AddLangganan on the service.
func MakeAddLanggananEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddLanggananRequest)
		message, err := s.AddLangganan(ctx, req.Langganan)
		return AddLanggananResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r AddLanggananResponse) Failed() error {
	return r.Err
}

// EditLanggananRequest collects the request parameters for the EditLangganan method.
type EditLanggananRequest struct {
	Langganan io.Langganan `json:"langganan"`
}

// EditLanggananResponse collects the response parameters for the EditLangganan method.
type EditLanggananResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeEditLanggananEndpoint returns an endpoint that invokes EditLangganan on the service.
func MakeEditLanggananEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(EditLanggananRequest)
		message, err := s.EditLangganan(ctx, req.Langganan)
		return EditLanggananResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r EditLanggananResponse) Failed() error {
	return r.Err
}

// DeleteLanggananRequest collects the request parameters for the DeleteLangganan method.
type DeleteLanggananRequest struct {
	Id string `json:"id"`
}

// DeleteLanggananResponse collects the response parameters for the DeleteLangganan method.
type DeleteLanggananResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeDeleteLanggananEndpoint returns an endpoint that invokes DeleteLangganan on the service.
func MakeDeleteLanggananEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteLanggananRequest)
		message, err := s.DeleteLangganan(ctx, req.Id)
		return DeleteLanggananResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r DeleteLanggananResponse) Failed() error {
	return r.Err
}

// GetLanggananByIdRequest collects the request parameters for the GetLanggananById method.
type GetLanggananByIdRequest struct {
	Id string `json:"id"`
}

// GetLanggananByIdResponse collects the response parameters for the GetLanggananById method.
type GetLanggananByIdResponse struct {
	Langganan io.Langganan `json:"langganan"`
	Message   string       `json:"message"`
	Err       error        `json:"err"`
}

// MakeGetLanggananByIdEndpoint returns an endpoint that invokes GetLanggananById on the service.
func MakeGetLanggananByIdEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetLanggananByIdRequest)
		langganan, message, err := s.GetLanggananById(ctx, req.Id)
		return GetLanggananByIdResponse{
			Err:       err,
			Langganan: langganan,
			Message:   message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetLanggananByIdResponse) Failed() error {
	return r.Err
}

// GetLangganan implements Service. Primarily useful in a client.
func (e Endpoints) GetLangganan(ctx context.Context) (langganan []io.Langganan, message string, err error) {
	request := GetLanggananRequest{}
	response, err := e.GetLanggananEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetLanggananResponse).Langganan, response.(GetLanggananResponse).Message, response.(GetLanggananResponse).Err
}

// AddLangganan implements Service. Primarily useful in a client.
func (e Endpoints) AddLangganan(ctx context.Context, langganan io.Langganan) (message string, err error) {
	request := AddLanggananRequest{Langganan: langganan}
	response, err := e.AddLanggananEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(AddLanggananResponse).Message, response.(AddLanggananResponse).Err
}

// EditLangganan implements Service. Primarily useful in a client.
func (e Endpoints) EditLangganan(ctx context.Context, langganan io.Langganan) (message string, err error) {
	request := EditLanggananRequest{Langganan: langganan}
	response, err := e.EditLanggananEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(EditLanggananResponse).Message, response.(EditLanggananResponse).Err
}

// DeleteLangganan implements Service. Primarily useful in a client.
func (e Endpoints) DeleteLangganan(ctx context.Context, id string) (message string, err error) {
	request := DeleteLanggananRequest{Id: id}
	response, err := e.DeleteLanggananEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(DeleteLanggananResponse).Message, response.(DeleteLanggananResponse).Err
}

// GetLanggananById implements Service. Primarily useful in a client.
func (e Endpoints) GetLanggananById(ctx context.Context, id string) (langganan io.Langganan, message string, err error) {
	request := GetLanggananByIdRequest{Id: id}
	response, err := e.GetLanggananByIdEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetLanggananByIdResponse).Langganan, response.(GetLanggananByIdResponse).Message, response.(GetLanggananByIdResponse).Err
}

// GetLanggananByKlinikRequest collects the request parameters for the GetLanggananByKlinik method.
type GetLanggananByKlinikRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetLanggananByKlinikResponse collects the response parameters for the GetLanggananByKlinik method.
type GetLanggananByKlinikResponse struct {
	Langganan []io.Langganan `json:"langganan"`
	Message   string         `json:"message"`
	Err       error          `json:"err"`
}

// MakeGetLanggananByKlinikEndpoint returns an endpoint that invokes GetLanggananByKlinik on the service.
func MakeGetLanggananByKlinikEndpoint(s service.MasterDataService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetLanggananByKlinikRequest)
		langganan, message, err := s.GetLanggananByKlinik(ctx, req.KlinikId)
		return GetLanggananByKlinikResponse{
			Err:       err,
			Langganan: langganan,
			Message:   message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetLanggananByKlinikResponse) Failed() error {
	return r.Err
}

// GetLanggananByKlinik implements Service. Primarily useful in a client.
func (e Endpoints) GetLanggananByKlinik(ctx context.Context) (langganan []io.Langganan, message string, err error) {
	request := GetLanggananByKlinikRequest{}
	response, err := e.GetLanggananByKlinikEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetLanggananByKlinikResponse).Langganan, response.(GetLanggananByKlinikResponse).Message, response.(GetLanggananByKlinikResponse).Err
}
