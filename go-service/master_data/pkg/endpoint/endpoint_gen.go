// THIS FILE IS AUTO GENERATED BY GK-CLI DO NOT EDIT!!
package endpoint

import (
	endpoint "github.com/go-kit/kit/endpoint"
	service "akuklinikid_demo/go-service/master_data/pkg/service"
)

// Endpoints collects all of the endpoints that compose a profile service. It's
// meant to be used as a helper struct, to collect all of the endpoints into a
// single parameter.
type Endpoints struct {
	GetKlinikEndpoint                         endpoint.Endpoint
	AddKlinikEndpoint                         endpoint.Endpoint
	EditKlinikEndpoint                        endpoint.Endpoint
	DeleteKlinikEndpoint                      endpoint.Endpoint
	GetKlinikByIdEndpoint                     endpoint.Endpoint
	GetVisitEndpoint                          endpoint.Endpoint
	AddVisitEndpoint                          endpoint.Endpoint
	EditVisitEndpoint                         endpoint.Endpoint
	DeleteVisitEndpoint                       endpoint.Endpoint
	GetVisitByIdEndpoint                      endpoint.Endpoint
	GetPraktekEndpoint                        endpoint.Endpoint
	AddPraktekEndpoint                        endpoint.Endpoint
	EditPraktekEndpoint                       endpoint.Endpoint
	DeletePraktekEndpoint                     endpoint.Endpoint
	GetPraktekByIdEndpoint                    endpoint.Endpoint
	GetLanggananEndpoint                      endpoint.Endpoint
	AddLanggananEndpoint                      endpoint.Endpoint
	EditLanggananEndpoint                     endpoint.Endpoint
	DeleteLanggananEndpoint                   endpoint.Endpoint
	GetLanggananByIdEndpoint                  endpoint.Endpoint
	GetLanggananByKlinikEndpoint              endpoint.Endpoint
	GetAllDokterByKlinikEndpoint              endpoint.Endpoint
	GetAllPasienByKlinikEndpoint              endpoint.Endpoint
	GetAllKlinikIdByDokterEndpoint            endpoint.Endpoint
	GetAllTodayAndLaterHistoryPraktekEndpoint endpoint.Endpoint
	GetHistoryPraktekEndpoint                 endpoint.Endpoint
	RegisterDokterEndpoint                    endpoint.Endpoint
}

// New returns a Endpoints struct that wraps the provided service, and wires in all of the
// expected endpoint middlewares
func New(s service.MasterDataService, mdw map[string][]endpoint.Middleware) Endpoints {
	eps := Endpoints{
		AddKlinikEndpoint:                         MakeAddKlinikEndpoint(s),
		AddLanggananEndpoint:                      MakeAddLanggananEndpoint(s),
		AddPraktekEndpoint:                        MakeAddPraktekEndpoint(s),
		AddVisitEndpoint:                          MakeAddVisitEndpoint(s),
		DeleteKlinikEndpoint:                      MakeDeleteKlinikEndpoint(s),
		DeleteLanggananEndpoint:                   MakeDeleteLanggananEndpoint(s),
		DeletePraktekEndpoint:                     MakeDeletePraktekEndpoint(s),
		DeleteVisitEndpoint:                       MakeDeleteVisitEndpoint(s),
		EditKlinikEndpoint:                        MakeEditKlinikEndpoint(s),
		EditLanggananEndpoint:                     MakeEditLanggananEndpoint(s),
		EditPraktekEndpoint:                       MakeEditPraktekEndpoint(s),
		EditVisitEndpoint:                         MakeEditVisitEndpoint(s),
		GetAllDokterByKlinikEndpoint:              MakeGetAllDokterByKlinikEndpoint(s),
		GetAllKlinikIdByDokterEndpoint:            MakeGetAllKlinikIdByDokterEndpoint(s),
		GetAllPasienByKlinikEndpoint:              MakeGetAllPasienByKlinikEndpoint(s),
		GetAllTodayAndLaterHistoryPraktekEndpoint: MakeGetAllTodayAndLaterHistoryPraktekEndpoint(s),
		GetHistoryPraktekEndpoint:                 MakeGetHistoryPraktekEndpoint(s),
		GetKlinikByIdEndpoint:                     MakeGetKlinikByIdEndpoint(s),
		GetKlinikEndpoint:                         MakeGetKlinikEndpoint(s),
		GetLanggananByIdEndpoint:                  MakeGetLanggananByIdEndpoint(s),
		GetLanggananByKlinikEndpoint:              MakeGetLanggananByKlinikEndpoint(s),
		GetLanggananEndpoint:                      MakeGetLanggananEndpoint(s),
		GetPraktekByIdEndpoint:                    MakeGetPraktekByIdEndpoint(s),
		GetPraktekEndpoint:                        MakeGetPraktekEndpoint(s),
		GetVisitByIdEndpoint:                      MakeGetVisitByIdEndpoint(s),
		GetVisitEndpoint:                          MakeGetVisitEndpoint(s),
		RegisterDokterEndpoint:                    MakeRegisterDokterEndpoint(s),
	}
	for _, m := range mdw["GetKlinik"] {
		eps.GetKlinikEndpoint = m(eps.GetKlinikEndpoint)
	}
	for _, m := range mdw["AddKlinik"] {
		eps.AddKlinikEndpoint = m(eps.AddKlinikEndpoint)
	}
	for _, m := range mdw["EditKlinik"] {
		eps.EditKlinikEndpoint = m(eps.EditKlinikEndpoint)
	}
	for _, m := range mdw["DeleteKlinik"] {
		eps.DeleteKlinikEndpoint = m(eps.DeleteKlinikEndpoint)
	}
	for _, m := range mdw["GetKlinikById"] {
		eps.GetKlinikByIdEndpoint = m(eps.GetKlinikByIdEndpoint)
	}
	for _, m := range mdw["GetVisit"] {
		eps.GetVisitEndpoint = m(eps.GetVisitEndpoint)
	}
	for _, m := range mdw["AddVisit"] {
		eps.AddVisitEndpoint = m(eps.AddVisitEndpoint)
	}
	for _, m := range mdw["EditVisit"] {
		eps.EditVisitEndpoint = m(eps.EditVisitEndpoint)
	}
	for _, m := range mdw["DeleteVisit"] {
		eps.DeleteVisitEndpoint = m(eps.DeleteVisitEndpoint)
	}
	for _, m := range mdw["GetVisitById"] {
		eps.GetVisitByIdEndpoint = m(eps.GetVisitByIdEndpoint)
	}
	for _, m := range mdw["GetPraktek"] {
		eps.GetPraktekEndpoint = m(eps.GetPraktekEndpoint)
	}
	for _, m := range mdw["AddPraktek"] {
		eps.AddPraktekEndpoint = m(eps.AddPraktekEndpoint)
	}
	for _, m := range mdw["EditPraktek"] {
		eps.EditPraktekEndpoint = m(eps.EditPraktekEndpoint)
	}
	for _, m := range mdw["DeletePraktek"] {
		eps.DeletePraktekEndpoint = m(eps.DeletePraktekEndpoint)
	}
	for _, m := range mdw["GetPraktekById"] {
		eps.GetPraktekByIdEndpoint = m(eps.GetPraktekByIdEndpoint)
	}
	for _, m := range mdw["GetLangganan"] {
		eps.GetLanggananEndpoint = m(eps.GetLanggananEndpoint)
	}
	for _, m := range mdw["AddLangganan"] {
		eps.AddLanggananEndpoint = m(eps.AddLanggananEndpoint)
	}
	for _, m := range mdw["EditLangganan"] {
		eps.EditLanggananEndpoint = m(eps.EditLanggananEndpoint)
	}
	for _, m := range mdw["DeleteLangganan"] {
		eps.DeleteLanggananEndpoint = m(eps.DeleteLanggananEndpoint)
	}
	for _, m := range mdw["GetLanggananById"] {
		eps.GetLanggananByIdEndpoint = m(eps.GetLanggananByIdEndpoint)
	}
	for _, m := range mdw["GetLanggananByKlinik"] {
		eps.GetLanggananByKlinikEndpoint = m(eps.GetLanggananByKlinikEndpoint)
	}
	for _, m := range mdw["GetAllDokterByKlinik"] {
		eps.GetAllDokterByKlinikEndpoint = m(eps.GetAllDokterByKlinikEndpoint)
	}
	for _, m := range mdw["GetAllPasienByKlinik"] {
		eps.GetAllPasienByKlinikEndpoint = m(eps.GetAllPasienByKlinikEndpoint)
	}
	for _, m := range mdw["GetAllKlinikIdByDokter"] {
		eps.GetAllKlinikIdByDokterEndpoint = m(eps.GetAllKlinikIdByDokterEndpoint)
	}
	for _, m := range mdw["GetAllTodayAndLaterHistoryPraktek"] {
		eps.GetAllTodayAndLaterHistoryPraktekEndpoint = m(eps.GetAllTodayAndLaterHistoryPraktekEndpoint)
	}
	for _, m := range mdw["GetHistoryPraktek"] {
		eps.GetHistoryPraktekEndpoint = m(eps.GetHistoryPraktekEndpoint)
	}
	for _, m := range mdw["RegisterDokter"] {
		eps.RegisterDokterEndpoint = m(eps.RegisterDokterEndpoint)
	}
	return eps
}
