package service

import (
	"context"
	"fmt"

	"database/sql"
	"kliniku/go-service/db"
	"kliniku/go-service/io"
)

// MasterDataService describes the service.
type MasterDataService interface {
	// Add your methods here
	// e.x: Foo(ctx context.Context,s string)(rs string, err error)

	// klinik service
	GetKlinik(ctx context.Context) (klinik []io.Klinik, message string, err error)
	AddKlinik(ctx context.Context, klinik io.Klinik) (message string, err error)
	EditKlinik(ctx context.Context, klinik io.Klinik) (message string, err error)
	DeleteKlinik(ctx context.Context, id string) (message string, err error)
	GetKlinikById(ctx context.Context, id string) (klinik io.Klinik, message string, err error)

	// visit service
	GetVisit(ctx context.Context) (visit []io.Visit, message string, err error)
	AddVisit(ctx context.Context, visit io.Visit) (message string, err error)
	EditVisit(ctx context.Context, visit io.Visit) (message string, err error)
	DeleteVisit(ctx context.Context, id string) (message string, err error)
	GetVisitById(ctx context.Context, id string) (visit io.Visit, message string, err error)

	// praktek service
	GetPraktek(ctx context.Context) (praktek []io.Praktek, message string, err error)
	AddPraktek(ctx context.Context, praktek io.Praktek) (message string, err error)
	EditPraktek(ctx context.Context, praktek io.Praktek) (message string, err error)
	DeletePraktek(ctx context.Context, id string) (message string, err error)
	GetPraktekById(ctx context.Context, id string) (praktek io.Praktek, message string, err error)

	// langganan service
	GetLangganan(ctx context.Context) (langganan []io.Langganan, message string, err error)
	AddLangganan(ctx context.Context, langganan io.Langganan) (message string, err error)
	EditLangganan(ctx context.Context, langganan io.Langganan) (message string, err error)
	DeleteLangganan(ctx context.Context, id string) (message string, err error)
	GetLanggananById(ctx context.Context, id string) (langganan io.Langganan, message string, err error)
	GetLanggananByKlinik(ctx context.Context, klinikId string) (langganan []io.Langganan, message string, err error)

	GetAllDokterByKlinik(ctx context.Context, klinikId string) (dokters []io.User, message string, err error)
	GetAllPasienByKlinik(ctx context.Context, klinikId string) (pasiens []io.User, message string, err error)
	GetAllKlinikIdByDokter(ctx context.Context, dokterId string) (klinikIds []string, message string, err error)
	GetAllTodayAndLaterHistoryPraktek(ctx context.Context) (historyPrakteks []io.HistoryPraktek, message string, err error)
	GetHistoryPraktek(ctx context.Context, jadwalPraktekId string) (historyPraktek io.HistoryPraktek, message string, err error)
	RegisterDokter(ctx context.Context, klinikId string, userId string) (message string, err error)
}

type basicMasterDataService struct{}

func (b *basicMasterDataService) GetKlinik(ctx context.Context) (klinik []io.Klinik, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return klinik, message, err
	}
	defer session.Close()

	rows, err := session.Query("select * from klinik;")
	if err != nil {
		message = "Query failed"
		return klinik, message, err
	}
	defer rows.Close()

	row := io.Klinik{}
	for rows.Next() {
		var err = rows.Scan(
			&row.Id, &row.Code, &row.Name, &row.Alamat, &row.Pict_1, &row.Pict_2, &row.Pict_3, &row.Logo, &row.Phone,
			&row.Provinsi, &row.Kota, &row.Kecamatan, &row.Kelurahan, &row.Email, &row.Fk_langganan, &row.Lon, &row.Lat)
		if err != nil {
			message = "Unmatched field name"
			return klinik, message, err
		}
		klinik = append(klinik, row)
	}
	return klinik, message, err
}
func (b *basicMasterDataService) AddKlinik(ctx context.Context, klinik io.Klinik) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("insert into klinik values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",
		nil, klinik.Code, klinik.Name, klinik.Alamat, klinik.Pict_1, klinik.Pict_2, klinik.Pict_3, klinik.Logo, klinik.Phone,
		klinik.Provinsi, klinik.Kota, klinik.Kecamatan, klinik.Kelurahan, klinik.Email, klinik.Fk_langganan, klinik.Lon, klinik.Lat)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}
	message = "insert success"
	return message, err
}
func (b *basicMasterDataService) EditKlinik(ctx context.Context, klinik io.Klinik) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query(`"update klinik set code=?, name=?, alamat=?, pict_1=?, pict_2=?, pict_3=?, logo=?, phone=?, 
		provinsi=?, kota=?, kecamatan=?, kelurahan=?, email=?, fk_klinik=?, lon=?, lat=? where id = ?"`, klinik.Code, klinik.Name,
		klinik.Alamat, klinik.Pict_1, klinik.Pict_2, klinik.Pict_3, klinik.Logo, klinik.Phone, klinik.Provinsi, klinik.Kota,
		klinik.Kecamatan, klinik.Kelurahan, klinik.Email, klinik.Fk_langganan, klinik.Lon, klinik.Lat, klinik.Id)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}

	message = "Edit success"
	return message, err
}
func (b *basicMasterDataService) DeleteKlinik(ctx context.Context, id string) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("delete from klinik where id=?", id)
	if err != nil {
		message = "Failed execution delete query"
		return message, err
	}
	message = "Delete success"
	return message, err
}

// NewBasicMasterDataService returns a naive, stateless implementation of MasterDataService.
func NewBasicMasterDataService() MasterDataService {
	return &basicMasterDataService{}
}

// New returns a MasterDataService with all of the expected middleware wired in.
func New(middleware []Middleware) MasterDataService {
	var svc MasterDataService = NewBasicMasterDataService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}

func (b *basicMasterDataService) GetKlinikById(ctx context.Context, id string) (klinik io.Klinik, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return klinik, message, err
	}
	defer session.Close()

	query := "select * from klinik where id=?"
	row := session.QueryRow(query, id)
	err = row.Scan(&klinik.Id, &klinik.Code, &klinik.Name, &klinik.Alamat, &klinik.Pict_1, &klinik.Pict_2, &klinik.Pict_3, &klinik.Logo, &klinik.Phone, &klinik.Provinsi, &klinik.Kota, &klinik.Kecamatan, &klinik.Kelurahan, &klinik.Email, &klinik.Fk_langganan, &klinik.Lon, &klinik.Lat)

	switch err {
	case sql.ErrNoRows:
		message = "Data not found"
		return klinik, message, err
	case nil:
		message = "Sucecess get klinik by id"
		return klinik, message, err
	default:
		message = "Mysql error "
		return klinik, message, err
	}
}

func (b *basicMasterDataService) GetVisit(ctx context.Context) (visit []io.Visit, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return visit, message, err
	}
	defer session.Close()

	rows, err := session.Query("select * from visit;")
	if err != nil {
		message = "Query failed"
		return visit, message, err
	}
	defer rows.Close()

	row := io.Visit{}
	for rows.Next() {
		var err = rows.Scan(&row.Id, &row.Code, &row.Date, &row.Booknum, &row.Fk_dokter, &row.Fk_jadwal_praktek, &row.Fk_perawat, &row.Fk_pasien, &row.Fk_history_praktek, &row.Total, &row.Rating, &row.Estimasi_tindakan, &row.Checking_time, &row.Status, &row.Tipe_pembayaran, &row.No_polis, &row.Nama_asuransi, &row.Kode_pembayaran)
		if err != nil {
			message = "Unmatched field name"
			return visit, message, err
		}
		visit = append(visit, row)
	}
	return visit, message, err
}
func (b *basicMasterDataService) AddVisit(ctx context.Context, visit io.Visit) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("insert into visit values(?,?,?,?,?,?,?,?,?,?,now(),?);", nil, visit.Code, visit.Date, visit.Booknum, visit.Fk_dokter, visit.Fk_jadwal_praktek, visit.Fk_pasien, visit.Total, visit.Rating, visit.Estimasi_tindakan, visit.Status, visit.Tipe_pembayaran, visit.No_polis, visit.Nama_asuransi, visit.Kode_pembayaran)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}
	message = "insert success"
	return message, err
}
func (b *basicMasterDataService) EditVisit(ctx context.Context, visit io.Visit) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("update visit set id=?, code=?, date=?, booknum=?, fk_dokter=?, fk_jadwal_praktek=?, fk_pasien=?, total=?, rating=?, estimasi_tindakan=?, checking_time=?, status=? where id = ?", visit.Id, visit.Code, visit.Date, visit.Booknum, visit.Fk_dokter, visit.Fk_jadwal_praktek, visit.Fk_pasien, visit.Total, visit.Rating, visit.Estimasi_tindakan, visit.Checking_time, visit.Status, visit.Id)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}

	message = "Edit success"
	return message, err
}
func (b *basicMasterDataService) DeleteVisit(ctx context.Context, id string) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("delete from visit where id=?", id)
	if err != nil {
		message = "Failed execution delete query"
		return message, err
	}
	message = "Delete success"
	return message, err
}
func (b *basicMasterDataService) GetVisitById(ctx context.Context, id string) (visit io.Visit, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return visit, message, err
	}
	defer session.Close()

	query := "select * from visit where id=?"
	row := session.QueryRow(query, id)
	err = row.Scan(&visit.Id, &visit.Code, &visit.Date, &visit.Booknum, &visit.Fk_dokter, &visit.Fk_jadwal_praktek, &visit.Fk_perawat, &visit.Fk_pasien, &visit.Fk_history_praktek, &visit.Total, &visit.Rating, &visit.Estimasi_tindakan, &visit.Checking_time, &visit.Status, &visit.Tipe_pembayaran, &visit.No_polis, &visit.Nama_asuransi, &visit.Kode_pembayaran)

	switch err {
	case sql.ErrNoRows:
		message = "Data not found"
		return visit, message, err
	case nil:
		message = "Sucecess get visit by id"
		return visit, message, err
	default:
		message = "Mysql error "
		return visit, message, err
	}
}
func (b *basicMasterDataService) GetPraktek(ctx context.Context) (praktek []io.Praktek, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return praktek, message, err
	}
	defer session.Close()

	rows, err := session.Query("select * from jadwal_praktek;")
	if err != nil {
		message = "Query failed"
		return praktek, message, err
	}
	defer rows.Close()

	row := io.Praktek{}
	for rows.Next() {
		var err = rows.Scan(&row.Id,
			&row.Code,
			&row.Poli_name,
			&row.Poli_type,
			&row.Day,
			&row.Time_from,
			&row.Time_to,
			&row.Fk_dokter,
			&row.Fk_klinik,
			&row.Price_rate,
			&row.Rating,
			&row.Status,
			&row.Lama_tindakan)
		fmt.Println(row)
		if err != nil {
			message = "Unmatched field name"
			return praktek, message, err
		}
		praktek = append(praktek, row)
	}
	return praktek, message, err
}
func (b *basicMasterDataService) AddPraktek(ctx context.Context, praktek io.Praktek) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("insert into jadwal_praktek values(?,?,?,?,?,?,?,?,?,?,?,?,10);", nil, praktek.Code, praktek.Poli_name, praktek.Poli_type, praktek.Day, praktek.Time_from, praktek.Time_to, praktek.Fk_dokter, praktek.Fk_klinik, praktek.Price_rate, praktek.Rating, praktek.Status)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}
	message = "insert success"
	return message, err
}
func (b *basicMasterDataService) EditPraktek(ctx context.Context, praktek io.Praktek) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("update jadwal_praktek set id=?, code=?, poli_name=?, poli_type=?, day=?, time_from=?, time_to=?, fk_dokter=?, fk_klinik=?, price_rate=?, rating=?, status=?, lama_tindakan=? where id = ?", praktek.Id, praktek.Code, praktek.Poli_name, praktek.Poli_type, praktek.Day, praktek.Time_from, praktek.Time_to, praktek.Fk_dokter, praktek.Fk_klinik, praktek.Price_rate, praktek.Rating, praktek.Status, praktek.Lama_tindakan, praktek.Id)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}

	message = "Edit success"
	return message, err
}
func (b *basicMasterDataService) DeletePraktek(ctx context.Context, id string) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("delete from jadwal_praktek where id=?", id)
	if err != nil {
		message = "Failed execution delete query"
		return message, err
	}
	message = "Delete success"
	return message, err
}
func (b *basicMasterDataService) GetPraktekById(ctx context.Context, id string) (praktek io.Praktek, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return praktek, message, err
	}
	defer session.Close()

	query := "select * from jadwal_praktek where id=?"
	row := session.QueryRow(query, id)
	err = row.Scan(&praktek.Id, &praktek.Code, &praktek.Poli_name, &praktek.Poli_type, &praktek.Day, &praktek.Time_from, &praktek.Time_to, &praktek.Fk_dokter, &praktek.Fk_klinik, &praktek.Price_rate, &praktek.Rating, &praktek.Status, &praktek.Lama_tindakan)

	switch err {
	case sql.ErrNoRows:
		message = "Data not found"
		return praktek, message, err
	case nil:
		message = "Sucecess get praktek by id"
		return praktek, message, err
	default:
		message = "Mysql error "
		return praktek, message, err
	}
}
func (b *basicMasterDataService) RegisterDokter(ctx context.Context, klinikId string, userId string) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("insert into klinik_dokter values(?,?,?);", klinikId, userId, nil)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}
	message = "Sukses diregister ke klinik"
	return message, err
}

func (b *basicMasterDataService) GetAllDokterByKlinik(ctx context.Context, klinikId string) (dokters []io.User, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return dokters, message, err
	}
	defer session.Close()

	rows, err := session.Query("select user.* from user, klinik_dokter where user.Fk_role=25 AND user.id = klinik_dokter.fk_user "+
		"AND klinik_dokter.fk_klinik = ?", klinikId)
	if err != nil {
		message = "Query failed"
		return dokters, message, err
	}
	defer rows.Close()

	row := io.User{}
	for rows.Next() {
		var err = rows.Scan(
			&row.Id,
			&row.Email,
			&row.Password,
			&row.Mobile,
			&row.Fullname,
			&row.Gender,
			&row.Birth_date,
			&row.Blood_type,
			&row.Address,
			&row.Code,
			&row.Fk_role,
			&row.Jabatan,
			&row.Session,
			&row.Fk_klinik,
			&row.Incomplete,
			&row.Realm,
			&row.Username,
			&row.EmailVerified,
			&row.VerificationToken,
		)
		if err != nil {
			message = "Unmatched field name"
			return dokters, message, err
		}
		dokters = append(dokters, row)
	}

	message = "success get all dokters"
	return dokters, message, err
}

func (b *basicMasterDataService) GetHistoryPraktek(ctx context.Context, jadwalPraktekId string) (historyPraktek io.HistoryPraktek, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return historyPraktek, message, err
	}
	defer session.Close()

	query := "select * from history_praktek where fk_jadwal_praktek=? order by id desc limit 1"
	row := session.QueryRow(query, jadwalPraktekId)
	err = row.Scan(&historyPraktek.Id, &historyPraktek.Praktek_name, &historyPraktek.Book_count, &historyPraktek.Open_time, &historyPraktek.Close_time, &historyPraktek.Fk_jadwal_praktek, &historyPraktek.Status, &historyPraktek.Remark, &historyPraktek.Fk_dokter)

	switch err {
	case sql.ErrNoRows:
		message = "Data not found"
		return historyPraktek, message, err
	case nil:
		message = "Sucecess get historyPraktek"
		return historyPraktek, message, err
	default:
		message = "Mysql error "
		return historyPraktek, message, err
	}
	return historyPraktek, message, err
}

func (b *basicMasterDataService) GetAllPasienByKlinik(ctx context.Context, klinikId string) (pasiens []io.User, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return pasiens, message, err
	}
	defer session.Close()

	rows, err := session.Query("SELECT * FROM user WHERE id IN "+
		"(SELECT fk_user FROM klinik_dokter WHERE klinik_dokter.fk_klinik=?) AND fk_role=26", klinikId)
	if err != nil {
		message = "Query failed"
		return pasiens, message, err
	}
	defer rows.Close()

	row := io.User{}
	for rows.Next() {
		var err = rows.Scan(
			&row.Id,
			&row.Email,
			&row.Password,
			&row.Mobile,
			&row.Fullname,
			&row.Gender,
			&row.Birth_date,
			&row.Blood_type,
			&row.Address,
			&row.Code,
			&row.Fk_role,
			&row.Jabatan,
			&row.Session,
			&row.Fk_klinik,
			&row.Incomplete,
			&row.Realm,
			&row.Username,
			&row.EmailVerified,
			&row.VerificationToken,
		)
		if err != nil {
			message = "Unmatched field name"
			return pasiens, message, err
		}
		pasiens = append(pasiens, row)
	}

	message = "success get all pasiens"
	return pasiens, message, err
}

func (b *basicMasterDataService) GetAllKlinikIdByDokter(ctx context.Context, dokterId string) (klinikIds []string, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return klinikIds, message, err
	}
	defer session.Close()

	rows, err := session.Query("SELECT fk_klinik FROM klinik_dokter WHERE fk_user=?", dokterId)
	if err != nil {
		message = "Query failed"
		return klinikIds, message, err
	}
	defer rows.Close()

	var row string
	for rows.Next() {
		var err = rows.Scan(&row)
		if err != nil {
			message = "Unmatched field name"
			return klinikIds, message, err
		}
		klinikIds = append(klinikIds, row)
	}

	message = "success get all pasiens"
	return klinikIds, message, err
}

func (b *basicMasterDataService) GetAllTodayAndLaterHistoryPraktek(ctx context.Context) (historyPrakteks []io.HistoryPraktek, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return historyPrakteks, message, err
	}
	defer session.Close()

	rows, err := session.Query("SELECT * FROM history_praktek WHERE open_time >= CURDATE()")
	if err != nil {
		message = "Query failed"
		return historyPrakteks, message, err
	}
	defer rows.Close()

	var row io.HistoryPraktek
	for rows.Next() {
		var err = rows.Scan(&row.Id, &row.Praktek_name, &row.Book_count, &row.Open_time, &row.Close_time, &row.Fk_jadwal_praktek, &row.Status, &row.Remark, &row.Fk_dokter)
		if err != nil {
			message = "Unmatched field name"
			return historyPrakteks, message, err
		}
		historyPrakteks = append(historyPrakteks, row)
	}

	return historyPrakteks, message, err
}

func (b *basicMasterDataService) GetLangganan(ctx context.Context) (langganan []io.Langganan, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return langganan, message, err
	}
	defer session.Close()

	rows, err := session.Query("select * from langganan;")
	if err != nil {
		message = "Query failed"
		return langganan, message, err
	}
	defer rows.Close()

	row := io.Langganan{}
	for rows.Next() {
		var err = rows.Scan(&row.Id, &row.Paket, &row.Dari_tanggal, &row.Sampai_tanggal, &row.No_kontrak, &row.Deskripsi, &row.Status, &row.Fk_klinik)
		if err != nil {
			message = "Unmatched field name"
			return langganan, message, err
		}
		langganan = append(langganan, row)
	}
	return langganan, message, err
}
func (b *basicMasterDataService) AddLangganan(ctx context.Context, langganan io.Langganan) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("insert into langganan values(?,?,?,?,?,?,?,?);", nil, langganan.Paket, langganan.Dari_tanggal, langganan.Sampai_tanggal, langganan.No_kontrak, langganan.Deskripsi, langganan.Status, langganan.Fk_klinik)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}
	message = "insert success"
	return message, err
}
func (b *basicMasterDataService) EditLangganan(ctx context.Context, langganan io.Langganan) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("update langganan set field=? where id = ?", langganan.Paket, langganan.Dari_tanggal, langganan.Sampai_tanggal, langganan.No_kontrak, langganan.Deskripsi, langganan.Status, langganan.Fk_klinik, langganan.Id)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}

	message = "Edit success"
	return message, err
}
func (b *basicMasterDataService) DeleteLangganan(ctx context.Context, id string) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("delete from langganan where id=?", id)
	if err != nil {
		message = "Failed execution delete query"
		return message, err
	}
	message = "Delete success"
	return message, err
}
func (b *basicMasterDataService) GetLanggananById(ctx context.Context, id string) (langganan io.Langganan, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return langganan, message, err
	}
	defer session.Close()

	query := "select * from langganan where id=?"
	row := session.QueryRow(query, id)
	err = row.Scan(&langganan.Id, &langganan.Paket, &langganan.Dari_tanggal, &langganan.Sampai_tanggal, &langganan.No_kontrak, &langganan.Deskripsi, &langganan.Status, &langganan.Fk_klinik)

	switch err {
	case sql.ErrNoRows:
		message = "Data not found"
		return langganan, message, err
	case nil:
		message = "Sucecess get langganan by id"
		return langganan, message, err
	default:
		message = "Mysql error "
		return langganan, message, err
	}
}

func (b *basicMasterDataService) GetLanggananByKlinik(ctx context.Context, klinikId string) (langganan []io.Langganan, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return langganan, message, err
	}
	defer session.Close()

	query := "select * from langganan where fk_klinik=?;"
	rows, err := session.Query(query, klinikId)
	if err != nil {
		message = "Query failed"
		return langganan, message, err
	}
	defer rows.Close()

	row := io.Langganan{}
	for rows.Next() {
		var err = rows.Scan(&row.Id, &row.Paket, &row.Dari_tanggal, &row.Sampai_tanggal, &row.No_kontrak, &row.Deskripsi, &row.Status, &row.Fk_klinik)
		if err != nil {
			message = "Unmatched field name"
			return langganan, message, err
		}
		langganan = append(langganan, row)
	}
	return langganan, message, err
}
