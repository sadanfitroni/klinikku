package main

import service "akuklinikid_demo/go-service/master_data/cmd/service"

func main() {
	service.Run()
}
