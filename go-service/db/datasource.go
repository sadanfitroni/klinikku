package db

import (
	"database/sql"
	// "context"
	_ "github.com/go-sql-driver/mysql"
	// "go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
	// "time"
	// "log"
	// "fmt"
)

func GetMysqlSession() (*sql.DB, error) {
	// dbDriver := "mysql"
	// dbUser := "root"
	// dbPass := ""
	// dbAddress := "localhost"
	// dbPort := "3306"
	// dbName := "klinikku_db"
	dbDriver := "mysql"
	dbUser := "root"
	dbPass := ""
	dbName := "klinikku_db"
	dbAddress := "localhost"
	dbPort := "3306"

	dbMysql, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp("+dbAddress+":"+dbPort+")/"+dbName+"?parseTime=true")

	if err != nil {
		return nil, err
		// panic(err.Error())
	}
	return dbMysql, nil
}

// func GetMongoDbSession() (*mongo.Database, error){

//     clientOptions := options.Client().ApplyURI("mongodb://localhost:27017/klinikdb")

//     // Connect to MongoDB
//     client, err := mongo.Connect(context.TODO(), clientOptions)

//     if err != nil {
//         log.Fatal(err)
//     }

//     // Check the connection
//     err = client.Ping(context.TODO(), nil)

//     if err != nil {
//         log.Fatal(err)
//     }

//     fmt.Println("Connected to MongoDB!")

//     return client.Database("klinikdb"), nil
// }
