package main

import service "akuklinikid_demo/go-service/rbac/cmd/service"

func main() {
	service.Run()
}
