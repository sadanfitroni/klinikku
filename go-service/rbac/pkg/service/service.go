package service

import (
	"context"
	"fmt"

	"database/sql"
	"akuklinikid_demo/go-service/db"
	"akuklinikid_demo/go-service/io"
	"net/http"
	"io/ioutil"
	"encoding/json"
	// "time"
	// "go.mongodb.org/mongo-driver/bson"
	"errors"
)

// RbacService describes the service.
type RbacService interface {
	// Add your methods here
	// e.x: Foo(ctx context.Context,s string)(rs string, err error)

	//signin signout service
	Signin(ctx context.Context, email string, password string) (user io.User, message string, err error)
	Signout(ctx context.Context, id string) (message string, err error)

	// user service
	GetUser(ctx context.Context) (user []io.User, message string, err error)
	AddUser(ctx context.Context, user io.User) (message string, err error)
	UpdateUser(ctx context.Context, user io.User) (message string, err error)
	DeleteUser(ctx context.Context, id string) (message string, err error)
	GetUserById(ctx context.Context, id string) (user io.User, message string, err error)
	GetUserByRole(ctx context.Context, roleId string) (user []io.User, message string, err error)
	GetUserByVisit(ctx context.Context, visitId string) (user io.User, message string, err error)

	// FilterUser(ctx context.Context)()

	// //role service
	GetRole(ctx context.Context) (role []io.Role, message string, err error)
	AddRole(ctx context.Context, role io.Role) (message string, err error)
	EditRole(ctx context.Context, role io.Role) (message string, err error)
	DeleteRole(ctx context.Context, id string) (message string, err error)
	GetRoleById(ctx context.Context, id string) (role io.Role, message string, err error)
	// FilterRole(ctx context.Context)()

	// //menu service
	GetMenu(ctx context.Context) (menu []io.MenuSidebar, message string, err error)
	AddMenu(ctx context.Context, menu io.MenuSidebar) (newMenu io.MenuSidebar, message string, err error)
	DeleteMenu(ctx context.Context, id string) (message string, err error)
	EditMenu(ctx context.Context, menu io.MenuSidebar) (message string, err error)
	GetMenuById(ctx context.Context, id string) (menu io.MenuSidebar, message string, err error)
	// FilterMenu(ctx context.Context)()

	//service for menu model
	GetMenuModelByMenu(ctx context.Context, id_menu string) (menuModel []io.MenuModel, message string, err error)
	AddMenuModel(ctx context.Context, menuModel io.MenuModel) (message string, err error)

	//service for acl
	GetAclByRole(ctx context.Context, roleName string) (acl []io.Acl, message string, err error)
	AddAcl(ctx context.Context, acl io.Acl) (message string, err error)

	GetMenuByUser(ctx context.Context, id_user string) (menu []io.MenuSidebar, message string, err error)
	RegisterPasien(ctx context.Context, user io.User) (message string, err error)
}

type basicRbacService struct{}

func (b *basicRbacService) Signin(ctx context.Context, email string, password string) (user io.User, message string, err error) {
	// session, err := db.GetMongoDbSession()
	
	// if err != nil {
	// 	message = "Failed connect to database"
	// 	return user, message, err
	// }
	// ctx, _ = context.WithTimeout(context.Background(), 30*time.Second)
	// // curr, err := session.Collection("user").Find(ctx,bson.D{})
	// // for curr.Next(ctx) {
	// // 	var result bson.M
	// // 	err := curr.Decode(&result)
	// // 	fmt.Println("\n\n", result)
	// // 	if err != nil {
	// // 		return user, message, err
	// // 	}
	// // }

	// defer curr.Close(ctx)
	
	session, err := db.GetMysqlSession()

	if err != nil {
		message = "Failed connect to database"
		return user, message, err
	}

	defer session.Close()
	var tmp_user io.User

	query := "select * from user WHERE (email=? or BINARY CODE=?)"
	row := session.QueryRow(query, email, email)
	err = row.Scan(
		&tmp_user.Id,
		&tmp_user.Email,
		&tmp_user.Password,
		&tmp_user.Mobile,
		&tmp_user.Fullname,
		&tmp_user.Gender,
		&tmp_user.Birth_date,
		&tmp_user.Blood_type,
		&tmp_user.Address,
		&tmp_user.Code,
		&tmp_user.Fk_role,
		&tmp_user.Jabatan,
		&tmp_user.Session,
		&tmp_user.Fk_klinik,
		&tmp_user.Incomplete,
		&tmp_user.Realm,
		&tmp_user.Username,
		&tmp_user.EmailVerified,
		&tmp_user.VerificationToken,
	)

	// fmt.Println(tmp_user)

	if err!=nil {
		message = "Email or username unregistered\n"
		return user, message, err
	}

	response, err := http.Get("http://117.53.47.25:3004/api/users/checkPassword?decodePassword="+ tmp_user.Password +"&password="+ password)
	defer response.Body.Close();

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		message = "error when calling loopback api"
		return user, message, err;
	}

	var res map[string]interface{}

	err = json.Unmarshal(body, &res)
	// fmt.Println("\n\n", stat)
	if (res["result"].(bool)) {
		statusLanggananKlinik:="active"
		// if tmp_user.Fk_role==1 || tmp_user.Fk_role==24 {
		// 	query := "select status from langganan,klinik where klinik.fk_langganan=langganan.id and klinik.id=?"
		// 	row := session.QueryRow(query, tmp_user.Fk_klinik)

		// 	err = row.Scan(&statusLanggananKlinik)
		// }
		if statusLanggananKlinik=="active" {
			query, err_update := session.Prepare("update user set session=1 where id=?")
			_, err_update = query.Exec(tmp_user.Id)
			if err_update != nil {
				message = "Failed execution query"
				return user, message, err_update
			}

			if err_update != nil {
				message = "Update status failed"
				return user, message, err_update
				// panic(err.error)
			}

			if err_update != nil {
				message = "Update status failed"
				return user, message, err_update
			}
			user.Id = tmp_user.Id
			user.Fullname = tmp_user.Fullname
			user.Fk_klinik = tmp_user.Fk_klinik
			user.Fk_role = tmp_user.Fk_role
			user.Jabatan = tmp_user.Jabatan
			message = "Signin success\n"
			return user, message, err
		} else {
			message = "Klinik anda sudah melewati masa langganan"
			return user, message, err
		}
	}else{
		err = errors.New("Login Failed")
		message = "Email or password wrong\n"
		return user, message, err
	}
}

func (b *basicRbacService) Signout(ctx context.Context, id string) (message string, err error) {
	// TODO implement the business logic of Signout
	session, err := db.GetMysqlSession ()
	query, err := session.Prepare("update user set session=0 where id=?")
	if err != nil {
		message = "Failed exection query"
		return message, err
	}

	_, err = query.Exec(id)
	if err != nil {
		message = "Update status failed"
		return message, err
		// panic(err.error)
	}
	message = "Signout success"
	return message, err
}

// NewBasicRbacService returns a naive, stateless implementation of RbacService.
func NewBasicRbacService() RbacService {
	return &basicRbacService{}
}

// New returns a RbacService with all of the expected middleware wired in.
func New(middleware []Middleware) RbacService {
	var svc RbacService = NewBasicRbacService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}

func (b *basicRbacService) GetUser(ctx context.Context) (user []io.User, message string, err error) {
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Failed connect to database"
		return user, message, err
	}
	defer session.Close()

	rows, err := session.Query("select * from user;")
	if err != nil {
		message = "Query failed"
		return user, message, err
	}
	defer rows.Close()

	for rows.Next() {
		var each = io.User{}
		var err = rows.Scan(
			&each.Id, 
			&each.Email, 
			&each.Password, 
			&each.Mobile, 
			&each.Fullname, 
			&each.Gender, 
			&each.Birth_date, 
			&each.Blood_type, 
			&each.Address, 
			&each.Code, 
			&each.Fk_role, 
			&each.Jabatan, 
			&each.Session, 
			&each.Fk_klinik,
			&each.Incomplete,
			&each.Realm,
			&each.Username,
			&each.EmailVerified,
			&each.VerificationToken)
		if err != nil {
			message = "Unmatched field name"
			return user, message, err
		}
		user = append(user, each)
	}
	return user, message, err
}
func (b *basicRbacService) AddUser(ctx context.Context, user io.User) (message string, err error) {
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	response, err := http.Get("http://117.53.47.25:3004/api/users/encodePassword?password="+ user.Password)
	defer response.Body.Close();

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		message = "error when calling loopback api"
		return message, err;
	}
	var res map[string]interface{}

	err = json.Unmarshal(body, &res)

	_, err = session.Query("insert into user values(?,?,?,?,?,?,?,?,?,?,?,?,?,?, ? , ? , ? , ?,?);", 
		nil, 
		user.Email, 
		res["result"], 
		user.Mobile, 
		user.Fullname, 
		user.Gender, 
		user.Birth_date, 
		user.Blood_type, 
		user.Address, 
		user.Code, 
		user.Fk_role,
		user.Jabatan, 
		0, 
		user.Fk_klinik,
		user.Incomplete,
		"",
		"",
		1,
		"")
	if err != nil {
		message = "Failed execution query"
		return message, err
	}
	message = "insert success"
	return message, err
}
func (b *basicRbacService) UpdateUser(ctx context.Context, user io.User) (message string, err error) {
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("update user set email=?, password=?, mobile=?, fullname=?, gender=?, birth_date=?, blood_type=?, address=?, code=?, fk_role=?, jabatan=?, incomplete=? where id = ?", user.Email, user.Password, user.Mobile, user.Fullname, user.Gender, user.Birth_date, user.Blood_type, user.Address, user.Code, user.Fk_role, user.Jabatan, user.Incomplete, user.Id)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}

	message = "Edit success"
	return message, err
}
func (b *basicRbacService) DeleteUser(ctx context.Context, id string) (message string, err error) {
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("delete from user where id=?", id)
	if err != nil {
		message = "Failed execution delete query"
		return message, err
	}
	message = "Delete success"
	return message, err
}
func (b *basicRbacService) GetRole(ctx context.Context) (role []io.Role, message string, err error) {
	roleData := io.Role{}
	//get mysql connection
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Failed connec to database"
		return role, message, err
	}

	//close connection when function is done
	defer session.Close()

	result, err := session.Query("select * from role")

	if err != nil {
		message = "Query error"
		return role, message, err
	}

	for result.Next() {
		err = result.Scan(
			&roleData.Id,
			&roleData.Code,
			&roleData.Name,
		)
		if err != nil {
			message = "Failed get role data"
			return role, message, err
		}
		role = append(role, roleData)
	}
	return role, message, err
}
func (b *basicRbacService) AddRole(ctx context.Context, role io.Role) (message string, err error) {
	session, err := db.GetMysqlSession ()

	if err != nil {
		message = "Failed connect to database"
		return message, err
	}

	defer session.Close()

	query, err := session.Prepare("insert into role set name=?, code=?")
	_, err = query.Exec(role.Name, role.Code)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}

	message = "Insert success"
	return message, err
}
func (b *basicRbacService) EditRole(ctx context.Context, role io.Role) (message string, err error) {
	session, err := db.GetMysqlSession ()

	if err != nil {
		message = "Failed connect to database"
		return message, err
	}

	defer session.Close()

	query, err := session.Prepare("delete from acl where principalId=?")
	if err != nil {
		return "acl query delete error", err
	}
	_, err = query.Exec(role.Name)
	if err != nil {
		message = "Failed execution delete query"
		return message, err
	}

	query, err = session.Prepare("update role set name=?, code=? where id=?")
	_, err = query.Exec(role.Name, role.Code, role.Id)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}

	message = "Edit success"
	return message, err
}
func (b *basicRbacService) DeleteRole(ctx context.Context, id string) (message string, err error) {
	session, err := db.GetMysqlSession ()

	if err != nil {
		message = "Failed connect to database"
		return message, err
	}

	defer session.Close()

	query := "select name from role where id = ?"
	row := session.QueryRow(query, id)
	var principalId string
	err = row.Scan(&principalId)

	query = "select user.id from user,role where fk_role = role.id and role.id = ?"
	row = session.QueryRow(query, id)
	var result int
	err = row.Scan(&result)

	switch err {
	case sql.ErrNoRows:
		query, err_delete := session.Prepare("delete from role where id=?")
		if err_delete != nil {
			return "Role query delete error", err_delete
		}
		_, err_delete = query.Exec(id)
		if err_delete != nil {
			message = "Failed execution delete query"
			return message, err
		}

		query, err_delete = session.Prepare("delete from acl where principalId=?")
		if err_delete != nil {
			return "acl query delete error", err_delete
		}
		_, err_delete = query.Exec(principalId)
		if err_delete != nil {
			message = "Failed execution delete query"
			return message, err_delete
		}

		message = "Delete success"
		return message, nil
	case nil:
		message = "Role has user, can't be delete"
		return message, err
	default:
		message = "Mysql error "
		return message, err
	}
	message = "TEST"
	return message, err
}
func (b *basicRbacService) GetMenu(ctx context.Context) (menu []io.MenuSidebar, message string, err error) {
	menuData := io.MenuSidebar{}
	//get mysql connection
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Failed connec to database"
		return menu, message, err
	}

	//close connection when function is done
	defer session.Close()

	result, err := session.Query("select * from menu_sidebar")

	if err != nil {
		message = "Query error"
		return menu, message, err
		// panic(err.error)
	}

	for result.Next() {
		err = result.Scan(
			&menuData.Id,
			&menuData.Url,
			&menuData.Label,
			&menuData.ParentId,
			&menuData.OrderMenu,
			&menuData.Icon,
		)
		if err != nil {
			message = "Failed get menu data"
			return menu, message, err
		}
		menu = append(menu, menuData)
	}
	return menu, message, err
}
func (b *basicRbacService) AddMenu(ctx context.Context, menu io.MenuSidebar) (newMenu io.MenuSidebar, message string, err error) {
	session, err := db.GetMysqlSession ()

	if err != nil {
		message = "Failed connect to database"
		return newMenu, message, err
	}

	defer session.Close()

	query, err := session.Prepare("insert into menu_sidebar set url=?, label=?, parentId=?,orderMenu=?, icon=?")
	if err != nil {
		message = "Query Error"
		return newMenu, message, err
	}
	_, err = query.Exec(menu.Url, menu.Label, menu.ParentId, menu.OrderMenu, menu.Icon)
	if err != nil {
		message = "Failed execution query"
		return newMenu, message, err
	}

	query_stmt := "select * from menu_sidebar where url=? and label=? order by id DESC LIMIT 1"
	row := session.QueryRow(query_stmt, menu.Url, menu.Label)
	err = row.Scan(
		&newMenu.Id,
		&newMenu.Url,
		&newMenu.Label,
		&newMenu.ParentId,
		&newMenu.OrderMenu,
		&newMenu.Icon,
	)

	if err != nil {
		message = "Failed insert menu"
		return newMenu, message, err
	}

	message = "Insert success"
	return newMenu, message, err
}
func (b *basicRbacService) DeleteMenu(ctx context.Context, id string) (message string, err error) {
	session, err := db.GetMysqlSession ()

	if err != nil {
		message = "Failed connect to database"
		return message, err
	}

	defer session.Close()

	query_stmt := "select label from menu_sidebar where id = ?"
	row := session.QueryRow(query_stmt, id)
	var label string
	err = row.Scan(&label)

	if err == nil {
		query, err := session.Prepare("delete from acl where menu=?")
		if err != nil {
			message = "Delete acl query error"
			return message, err
		}
		_, err = query.Exec(label)
		if err != nil {
			message = "Failed execution delete acl query"
			return message, err
		}
	}

	if err == nil {
		query, err := session.Prepare("delete from menu_model where fk_menu=?")
		if err != nil {
			message = "Delete menu_model query error"
			return message, err
		}
		_, err = query.Exec(id)
		if err != nil {
			message = "Failed execution delete menu_model query"
			return message, err
		}
	}

	query, err := session.Prepare("delete from menu_sidebar where id=?")
	if err != nil {
		message = "Delete query error"
		return message, err
	}
	_, err = query.Exec(id)
	if err != nil {
		message = "Failed execution delete query"
		return message, err
	}
	message = "Delete menu success"
	return message, err
}
func (b *basicRbacService) EditMenu(ctx context.Context, menu io.MenuSidebar) (message string, err error) {
	session, err := db.GetMysqlSession ()

	if err != nil {
		message = "Failed connect to database"
		return message, err
	}

	defer session.Close()

	query_stmt := "select label from menu_sidebar where id = ?"
	row := session.QueryRow(query_stmt, menu.Id)
	var oldLabel string
	err = row.Scan(&oldLabel)

	if err == nil {
		query, err := session.Prepare("update acl set menu=? where menu=? ")
		if err != nil {
			message = "Update acl query error"
			return message, err
		}
		_, err = query.Exec(menu.Label, oldLabel)
		if err != nil {
			message = "Failed execution update acl query"
			return message, err
		}
	}

	if err == nil {
		query, err := session.Prepare("delete from menu_model where fk_menu=?")
		if err != nil {
			message = "Delete menu_model query error"
			return message, err
		}
		_, err = query.Exec(menu.Id)
		if err != nil {
			message = "Failed execution delete menu_model query"
			return message, err
		}
	}

	query, err := session.Prepare("delete from menu_model where fk_menu=?")
	if err != nil {
		message = "Delete acl query error"
		return message, err
	}
	_, err = query.Exec(menu.Id)
	if err != nil {
		message = "Failed execution delete acl query"
		return message, err
	}

	query, err = session.Prepare("Update menu_sidebar set url=?, label=?, parentId=?,orderMenu=?, icon=?  where id=? ")
	if err != nil {
		message = "Update query menu error"
		return message, err
	}
	_, err = query.Exec(menu.Url, menu.Label, menu.ParentId, menu.OrderMenu, menu.Icon, menu.Id)
	if err != nil {
		message = "Failed execution update menu query"
		return message, err
	}
	message = "Udpate menu success"
	return message, err
}

func (b *basicRbacService) GetUserById(ctx context.Context, id string) (user io.User, message string, err error) {
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Failed connect to database"
		return user, message, err
	}
	defer session.Close()

	query := "select * from user where id=?"
	row := session.QueryRow(query, id)
	err = row.Scan(
		&user.Id, 
		&user.Email, 
		&user.Password, 
		&user.Mobile, 
		&user.Fullname, 
		&user.Gender, 
		&user.Birth_date, 
		&user.Blood_type, 
		&user.Address, 
		&user.Code, 
		&user.Fk_role, 
		&user.Jabatan, 
		&user.Session, 
		&user.Fk_klinik,
		&user.Incomplete,
		&user.Realm,
		&user.Username,
		&user.EmailVerified,
		&user.VerificationToken,
	)

	switch err {
	case sql.ErrNoRows:
		message = "Data not found"
		return user, message, err
	case nil:
		message = "Sucecess get role by id"
		return user, message, err
	default:
		message = "Mysql error "
		return user, message, err
	}
}
func (b *basicRbacService) GetRoleById(ctx context.Context, id string) (role io.Role, message string, err error) {
	session, err := db.GetMysqlSession ()

	if err != nil {
		message = "Failed connect to database"
		return role, message, err
	}

	defer session.Close()
	query := "select * from role where id=?"
	row := session.QueryRow(query, id)
	err = row.Scan(&role.Id, &role.Code, &role.Name)

	switch err {
	case sql.ErrNoRows:
		message = "Data not found"
		return role, message, err
	case nil:
		message = "Sucecess get role by id"
		return role, message, err
	default:
		message = "Mysql error "
		return role, message, err
	}
}
func (b *basicRbacService) GetMenuById(ctx context.Context, id string) (menu io.MenuSidebar, message string, err error) {
	session, err := db.GetMysqlSession ()

	if err != nil {
		message = "Failed connect to database"
		return menu, message, err
	}

	defer session.Close()
	query := "select * from menu_sidebar where id=?"
	row := session.QueryRow(query, id)
	err = row.Scan(
		&menu.Id,
		&menu.Url,
		&menu.Label,
		&menu.ParentId,
		&menu.OrderMenu,
		&menu.Icon,
	)

	switch err {
	case sql.ErrNoRows:
		message = "Data not found"
		return menu, message, err
	case nil:
		message = "Sucecess get menu by id"
		return menu, message, err
	default:
		message = "Mysql error "
		return menu, message, err
	}
}

func (b *basicRbacService) GetMenuModelByMenu(ctx context.Context, id_menu string) (menuModel []io.MenuModel, message string, err error) {
	// TODO implement the business logic of GetMenuModelByMenu
	menuModelData := io.MenuModel{}
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Can't connect to mysql"
		return menuModel, message, err
	}
	defer session.Close()

	result, err := session.Query("select * from menu_model where fk_menu =?", id_menu)

	if err != nil {
		message = "Query error"
		return menuModel, message, err
	}

	for result.Next() {
		err = result.Scan(
			&menuModelData.Id,
			&menuModelData.Model,
			&menuModelData.Permission,
			&menuModelData.AccessType,
			&menuModelData.Fk_menu,
		)
		if err != nil {
			message = "Failed get menu model data"
			return menuModel, message, err
		}
		// fmt.Println(menuModelData)
		// message = menuModelData.Permission

		menuModel = append(menuModel, menuModelData)
	}
	// message = "Get menu model success"
	return menuModel, message, err
}

func (b *basicRbacService) GetAclByRole(ctx context.Context, roleName string) (acl []io.Acl, message string, err error) {
	aclData := io.Acl{}
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Can't connect mysql"
		return acl, message, err
	}
	defer session.Close()

	result, err := session.Query("select * from acl where principalId=?", roleName)

	if err != nil {
		message = "Query error"
		return acl, message, err
	}

	message = "out"
	fmt.Println(roleName)
	for result.Next() {
		err = result.Scan(
			&aclData.Id,
			&aclData.Menu,
			&aclData.Property,
			&aclData.AccessType,
			&aclData.Permission,
			&aclData.PrincipalType,
			&aclData.PrincipalId,
			&aclData.Model,
		)
		if err != nil {
			message = "Failed get role data"
			return acl, message, err
		}
		fmt.Println(aclData)
		message = "abcsd"

		acl = append(acl, aclData)
	}
	// message = "Get acl success"
	return acl, message, err
}

func (b *basicRbacService) AddAcl(ctx context.Context, acl io.Acl) (message string, err error) {
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Can't connect to mysql"
		return message, err
	}
	defer session.Close()

	query, err := session.Prepare("insert into acl " +
		"set menu=?, property=?, accessType=?, permission=?, " +
		"principalType=?, principalId=?, model=?")
	_, err = query.Exec(
		acl.Menu,
		acl.Property,
		acl.AccessType,
		acl.Permission,
		acl.PrincipalType,
		acl.PrincipalId,
		acl.Model,
	)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}

	message = "Insert success"
	return message, err
}

func (b *basicRbacService) AddMenuModel(ctx context.Context, menuModel io.MenuModel) (message string, err error) {
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Can't connect to mysql"
		return message, err
	}
	defer session.Close()

	query, err := session.Prepare("insert into menu_model " +
		"set model=?,permission=?, accessType=?, fk_menu=?")
	_, err = query.Exec(
		&menuModel.Model,
		&menuModel.Permission,
		&menuModel.AccessType,
		&menuModel.Fk_menu,
	)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}

	message = "Insert success"
	return message, err
}

func (b *basicRbacService) GetMenuByUser(ctx context.Context, id_user string) (menu []io.MenuSidebar, message string, err error) {
	// TODO implement the business logic of GetMenuByUser
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Can't connect to mysql"
		return menu, message, err
	}
	defer session.Close()

	query_menu := "SELECT menu_sidebar.* FROM user,role,acl,menu_sidebar " +
		"WHERE user.id = ? " +
		"AND fk_role = role.id " +
		"AND acl.principalId = role.NAME " +
		"AND menu_sidebar.label = acl.menu " +
		"AND (acl.accessType = '*' OR acl.accessType = 'MENU') "
	// list_menu := []io.MenuSidebar{}
	menu_user := io.MenuSidebar{}
	result, err := session.Query(query_menu, id_user)

	if err != nil {
		message = "Failed Exec query"
		return menu, message, err
	}

	for result.Next() {
		err := result.Scan(
			&menu_user.Id,
			&menu_user.Url,
			&menu_user.Label,
			&menu_user.ParentId,
			&menu_user.OrderMenu,
			&menu_user.Icon,
		)

		if err != nil {
			message = "Failed get menu data"
			return menu, message, err
		}

		menu = append(menu, menu_user)
	}

	return menu, message, err
}
func (b *basicRbacService) GetUserByRole(ctx context.Context, roleId string) (user []io.User, message string, err error) {
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Failed connect to database"
		return user, message, err
	}
	defer session.Close()

	rows, err := session.Query("select * from user where fk_role=?", roleId)
	if err != nil {
		message = "Query failed"
		return user, message, err
	}
	defer rows.Close()

	for rows.Next() {
		var each = io.User{}
		var err = rows.Scan( 
			&each.Id, 
			&each.Email, 
			&each.Password, 
			&each.Mobile, 
			&each.Fullname, 
			&each.Gender, 
			&each.Birth_date, 
			&each.Blood_type, 
			&each.Address, 
			&each.Code, 
			&each.Fk_role, 
			&each.Jabatan, 
			&each.Session, 
			&each.Fk_klinik,
			&each.Incomplete,
			&each.Realm,
			&each.Username,
			&each.EmailVerified,
			&each.VerificationToken,
		)
		if err != nil {
			message = "Unmatched field name"
			return user, message, err
		}
		user = append(user, each)
	}
	return user, message, err
}

func (b *basicRbacService) RegisterPasien(ctx context.Context, user io.User) (message string, err error) {
	// TODO implement the business logic of RegisterPasien
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Failed connect to database"
	}
	defer session.Close()
	var pasienId int
	row := session.QueryRow("select id from role where name='pasien'")
	err = row.Scan(&pasienId)

	query, err := session.Prepare("insert into user set code = ?, fullname = ?, email = ?, gender = ?, password=?,mobile=?,fk_role=? ")

	if err != nil {
		message = "Failed insert new pasien"
		return message, err
	}

	response, err := http.Get("http://117.53.47.25:3004/api/users/encodePassword?password="+ user.Password)
	defer response.Body.Close();

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		message = "error when calling loopback api"
		return message, err;
	}
	var res map[string]interface{}

	err = json.Unmarshal(body, &res)

	_, err = query.Exec(
		user.Code,
		user.Fullname,
		user.Email,
		user.Gender,
		res["result"],
		user.Mobile,
		pasienId,
	)

	if err != nil {
		message = "Failed insert new pasien"
		return message, err
	}

	message = "Inser pasien success"
	return message, err
}

func (b *basicRbacService) GetUserByVisit(ctx context.Context, visitId string) (user io.User, message string, err error) {
	session, err := db.GetMysqlSession ()
	if err != nil {
		message = "Failed connect to database"
		return user, message, err
	}
	defer session.Close()

	row := session.QueryRow("select user.id,email,mobile,fullname,gender,birth_date,blood_type,address " +
		" from user, visit where fk_pasien = user.id and visit.id = ?", visitId)
	err = row.Scan(
		&user.Id,
		&user.Email,
		&user.Mobile,
		&user.Fullname,
		&user.Gender,
		&user.Birth_date,
		&user.Blood_type,
		&user.Address,
	)
	if err != nil {
		message = "Data user not found"
		return user, message, err
	}
	message = "Get user success"
	return user, message, err
}
