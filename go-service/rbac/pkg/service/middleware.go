package service

import (
	"context"
	io "akuklinikid_demo/go-service/io"

	log "github.com/go-kit/kit/log"
)

// Middleware describes a service middleware.
type Middleware func(RbacService) RbacService

type loggingMiddleware struct {
	logger log.Logger
	next   RbacService
}

// LoggingMiddleware takes a logger as a dependency
// and returns a RbacService Middleware.
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next RbacService) RbacService {
		return &loggingMiddleware{logger, next}
	}

}

func (l loggingMiddleware) Signin(ctx context.Context, email string, password string) (user io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "Signin", "email", email, "password", password, "user", user, "message", message, "err", err)
	}()
	return l.next.Signin(ctx, email, password)
}
func (l loggingMiddleware) Signout(ctx context.Context, id string) (message string, err error) {
	defer func() {
		l.logger.Log("method", "Signout", "id", id, "message", message, "err", err)
	}()
	return l.next.Signout(ctx, id)
}
func (l loggingMiddleware) GetUser(ctx context.Context) (user []io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetUser", "user", user, "message", message, "err", err)
	}()
	return l.next.GetUser(ctx)
}
func (l loggingMiddleware) AddUser(ctx context.Context, user io.User) (message string, err error) {
	defer func() {
		l.logger.Log("method", "AddUser", "user", user, "message", message, "err", err)
	}()
	return l.next.AddUser(ctx, user)
}
func (l loggingMiddleware) UpdateUser(ctx context.Context, user io.User) (message string, err error) {
	defer func() {
		l.logger.Log("method", "UpdateUser", "user", user, "message", message, "err", err)
	}()
	return l.next.UpdateUser(ctx, user)
}
func (l loggingMiddleware) DeleteUser(ctx context.Context, id string) (message string, err error) {
	defer func() {
		l.logger.Log("method", "DeleteUser", "id", id, "message", message, "err", err)
	}()
	return l.next.DeleteUser(ctx, id)
}
func (l loggingMiddleware) GetUserById(ctx context.Context, id string) (user io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetUserById", "id", id, "user", user, "message", message, "err", err)
	}()
	return l.next.GetUserById(ctx, id)
}
func (l loggingMiddleware) GetUserByRole(ctx context.Context, roleId string) (user []io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetUserByRole", "roleId", roleId, "user", user, "message", message, "err", err)
	}()
	return l.next.GetUserByRole(ctx, roleId)
}
func (l loggingMiddleware) GetRole(ctx context.Context) (role []io.Role, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetRole", "role", role, "message", message, "err", err)
	}()
	return l.next.GetRole(ctx)
}
func (l loggingMiddleware) AddRole(ctx context.Context, role io.Role) (message string, err error) {
	defer func() {
		l.logger.Log("method", "AddRole", "role", role, "message", message, "err", err)
	}()
	return l.next.AddRole(ctx, role)
}
func (l loggingMiddleware) EditRole(ctx context.Context, role io.Role) (message string, err error) {
	defer func() {
		l.logger.Log("method", "EditRole", "role", role, "message", message, "err", err)
	}()
	return l.next.EditRole(ctx, role)
}
func (l loggingMiddleware) DeleteRole(ctx context.Context, id string) (message string, err error) {
	defer func() {
		l.logger.Log("method", "DeleteRole", "id", id, "message", message, "err", err)
	}()
	return l.next.DeleteRole(ctx, id)
}
func (l loggingMiddleware) GetRoleById(ctx context.Context, id string) (role io.Role, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetRoleById", "id", id, "role", role, "message", message, "err", err)
	}()
	return l.next.GetRoleById(ctx, id)
}
func (l loggingMiddleware) GetMenu(ctx context.Context) (menu []io.MenuSidebar, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetMenu", "menu", menu, "message", message, "err", err)
	}()
	return l.next.GetMenu(ctx)
}
func (l loggingMiddleware) AddMenu(ctx context.Context, menu io.MenuSidebar) (newMenu io.MenuSidebar, message string, err error) {
	defer func() {
		l.logger.Log("method", "AddMenu", "menu", menu, "newMenu", newMenu, "message", message, "err", err)
	}()
	return l.next.AddMenu(ctx, menu)
}
func (l loggingMiddleware) DeleteMenu(ctx context.Context, id string) (message string, err error) {
	defer func() {
		l.logger.Log("method", "DeleteMenu", "id", id, "message", message, "err", err)
	}()
	return l.next.DeleteMenu(ctx, id)
}
func (l loggingMiddleware) EditMenu(ctx context.Context, menu io.MenuSidebar) (message string, err error) {
	defer func() {
		l.logger.Log("method", "EditMenu", "menu", menu, "message", message, "err", err)
	}()
	return l.next.EditMenu(ctx, menu)
}
func (l loggingMiddleware) GetMenuById(ctx context.Context, id string) (menu io.MenuSidebar, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetMenuById", "id", id, "menu", menu, "message", message, "err", err)
	}()
	return l.next.GetMenuById(ctx, id)
}
func (l loggingMiddleware) GetMenuModelByMenu(ctx context.Context, id_menu string) (menuModel []io.MenuModel, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetMenuModelByMenu", "id_menu", id_menu, "menuModel", menuModel, "message", message, "err", err)
	}()
	return l.next.GetMenuModelByMenu(ctx, id_menu)
}
func (l loggingMiddleware) AddMenuModel(ctx context.Context, menuModel io.MenuModel) (message string, err error) {
	defer func() {
		l.logger.Log("method", "AddMenuModel", "menuModel", menuModel, "message", message, "err", err)
	}()
	return l.next.AddMenuModel(ctx, menuModel)
}
func (l loggingMiddleware) GetAclByRole(ctx context.Context, roleName string) (acl []io.Acl, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetAclByRole", "roleName", roleName, "acl", acl, "message", message, "err", err)
	}()
	return l.next.GetAclByRole(ctx, roleName)
}
func (l loggingMiddleware) AddAcl(ctx context.Context, acl io.Acl) (message string, err error) {
	defer func() {
		l.logger.Log("method", "AddAcl", "acl", acl, "message", message, "err", err)
	}()
	return l.next.AddAcl(ctx, acl)
}
func (l loggingMiddleware) GetMenuByUser(ctx context.Context, id_user string) (menu []io.MenuSidebar, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetMenuByUser", "id_user", id_user, "menu", menu, "message", message, "err", err)
	}()
	return l.next.GetMenuByUser(ctx, id_user)
}

func (l loggingMiddleware) RegisterPasien(ctx context.Context, user io.User) (message string, err error) {
	defer func() {
		l.logger.Log("method", "RegisterPasien", "user", user, "message", message, "err", err)
	}()
	return l.next.RegisterPasien(ctx, user)
}

func (l loggingMiddleware) GetUserByVisit(ctx context.Context, visitId string) (user io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetUserByVisit", "visitId", visitId, "user", user, "message", message, "err", err)
	}()
	return l.next.GetUserByVisit(ctx, visitId)
}
