package endpoint

import (
	"context"
	io "akuklinikid_demo/go-service/io"
	service "akuklinikid_demo/go-service/rbac/pkg/service"

	endpoint "github.com/go-kit/kit/endpoint"
)

// SigninRequest collects the request parameters for the Signin method.
type SigninRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// SigninResponse collects the response parameters for the Signin method.
type SigninResponse struct {
	User    io.User `json:"user"`
	Message string  `json:"message"`
	Err     error   `json:"err"`
}

// MakeSigninEndpoint returns an endpoint that invokes Signin on the service.
func MakeSigninEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SigninRequest)
		user, message, err := s.Signin(ctx, req.Email, req.Password)
		return SigninResponse{
			Err:     err,
			Message: message,
			User:    user,
		}, nil
	}
}

// Failed implements Failer.
func (r SigninResponse) Failed() error {
	return r.Err
}

// SignoutRequest collects the request parameters for the Signout method.
type SignoutRequest struct {
	Id string `json:"id"`
}

// SignoutResponse collects the response parameters for the Signout method.
type SignoutResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeSignoutEndpoint returns an endpoint that invokes Signout on the service.
func MakeSignoutEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SignoutRequest)
		message, err := s.Signout(ctx, req.Id)
		return SignoutResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r SignoutResponse) Failed() error {
	return r.Err
}

// GetUserRequest collects the request parameters for the GetUser method.
type GetUserRequest struct{}

// GetUserResponse collects the response parameters for the GetUser method.
type GetUserResponse struct {
	User    []io.User `json:"user"`
	Message string    `json:"message"`
	Err     error     `json:"err"`
}

// MakeGetUserEndpoint returns an endpoint that invokes GetUser on the service.
func MakeGetUserEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		user, message, err := s.GetUser(ctx)
		return GetUserResponse{
			Err:     err,
			Message: message,
			User:    user,
		}, nil
	}
}

// Failed implements Failer.
func (r GetUserResponse) Failed() error {
	return r.Err
}

// AddUserRequest collects the request parameters for the AddUser method.
type AddUserRequest struct {
	User io.User `json:"user"`
}

// AddUserResponse collects the response parameters for the AddUser method.
type AddUserResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeAddUserEndpoint returns an endpoint that invokes AddUser on the service.
func MakeAddUserEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddUserRequest)
		message, err := s.AddUser(ctx, req.User)
		return AddUserResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r AddUserResponse) Failed() error {
	return r.Err
}

// UpdateUserRequest collects the request parameters for the UpdateUser method.
type UpdateUserRequest struct {
	User io.User `json:"user"`
}

// UpdateUserResponse collects the response parameters for the UpdateUser method.
type UpdateUserResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeUpdateUserEndpoint returns an endpoint that invokes UpdateUser on the service.
func MakeUpdateUserEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(UpdateUserRequest)
		message, err := s.UpdateUser(ctx, req.User)
		return UpdateUserResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r UpdateUserResponse) Failed() error {
	return r.Err
}

// DeleteUserRequest collects the request parameters for the DeleteUser method.
type DeleteUserRequest struct {
	Id string `json:"id"`
}

// DeleteUserResponse collects the response parameters for the DeleteUser method.
type DeleteUserResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeDeleteUserEndpoint returns an endpoint that invokes DeleteUser on the service.
func MakeDeleteUserEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteUserRequest)
		message, err := s.DeleteUser(ctx, req.Id)
		return DeleteUserResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r DeleteUserResponse) Failed() error {
	return r.Err
}

// GetUserByIdRequest collects the request parameters for the GetUserById method.
type GetUserByIdRequest struct {
	Id string `json:"id"`
}

// GetUserByIdResponse collects the response parameters for the GetUserById method.
type GetUserByIdResponse struct {
	User    io.User `json:"user"`
	Message string  `json:"message"`
	Err     error   `json:"err"`
}

// MakeGetUserByIdEndpoint returns an endpoint that invokes GetUserById on the service.
func MakeGetUserByIdEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetUserByIdRequest)
		user, message, err := s.GetUserById(ctx, req.Id)
		return GetUserByIdResponse{
			Err:     err,
			Message: message,
			User:    user,
		}, nil
	}
}

// Failed implements Failer.
func (r GetUserByIdResponse) Failed() error {
	return r.Err
}

// GetUserByRoleRequest collects the request parameters for the GetUserByRole method.
type GetUserByRoleRequest struct {
	RoleId string `json:"role_id"`
}

// GetUserByRoleResponse collects the response parameters for the GetUserByRole method.
type GetUserByRoleResponse struct {
	User    []io.User `json:"user"`
	Message string    `json:"message"`
	Err     error     `json:"err"`
}

// MakeGetUserByRoleEndpoint returns an endpoint that invokes GetUserByRole on the service.
func MakeGetUserByRoleEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetUserByRoleRequest)
		user, message, err := s.GetUserByRole(ctx, req.RoleId)
		return GetUserByRoleResponse{
			Err:     err,
			Message: message,
			User:    user,
		}, nil
	}
}

// Failed implements Failer.
func (r GetUserByRoleResponse) Failed() error {
	return r.Err
}

// GetRoleRequest collects the request parameters for the GetRole method.
type GetRoleRequest struct{}

// GetRoleResponse collects the response parameters for the GetRole method.
type GetRoleResponse struct {
	Role    []io.Role `json:"role"`
	Message string    `json:"message"`
	Err     error     `json:"err"`
}

// MakeGetRoleEndpoint returns an endpoint that invokes GetRole on the service.
func MakeGetRoleEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		role, message, err := s.GetRole(ctx)
		return GetRoleResponse{
			Err:     err,
			Message: message,
			Role:    role,
		}, nil
	}
}

// Failed implements Failer.
func (r GetRoleResponse) Failed() error {
	return r.Err
}

// AddRoleRequest collects the request parameters for the AddRole method.
type AddRoleRequest struct {
	Role io.Role `json:"role"`
}

// AddRoleResponse collects the response parameters for the AddRole method.
type AddRoleResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeAddRoleEndpoint returns an endpoint that invokes AddRole on the service.
func MakeAddRoleEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddRoleRequest)
		message, err := s.AddRole(ctx, req.Role)
		return AddRoleResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r AddRoleResponse) Failed() error {
	return r.Err
}

// EditRoleRequest collects the request parameters for the EditRole method.
type EditRoleRequest struct {
	Role io.Role `json:"role"`
}

// EditRoleResponse collects the response parameters for the EditRole method.
type EditRoleResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeEditRoleEndpoint returns an endpoint that invokes EditRole on the service.
func MakeEditRoleEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(EditRoleRequest)
		message, err := s.EditRole(ctx, req.Role)
		return EditRoleResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r EditRoleResponse) Failed() error {
	return r.Err
}

// DeleteRoleRequest collects the request parameters for the DeleteRole method.
type DeleteRoleRequest struct {
	Id string `json:"id"`
}

// DeleteRoleResponse collects the response parameters for the DeleteRole method.
type DeleteRoleResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeDeleteRoleEndpoint returns an endpoint that invokes DeleteRole on the service.
func MakeDeleteRoleEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteRoleRequest)
		message, err := s.DeleteRole(ctx, req.Id)
		return DeleteRoleResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r DeleteRoleResponse) Failed() error {
	return r.Err
}

// GetRoleByIdRequest collects the request parameters for the GetRoleById method.
type GetRoleByIdRequest struct {
	Id string `json:"id"`
}

// GetRoleByIdResponse collects the response parameters for the GetRoleById method.
type GetRoleByIdResponse struct {
	Role    io.Role `json:"role"`
	Message string  `json:"message"`
	Err     error   `json:"err"`
}

// MakeGetRoleByIdEndpoint returns an endpoint that invokes GetRoleById on the service.
func MakeGetRoleByIdEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetRoleByIdRequest)
		role, message, err := s.GetRoleById(ctx, req.Id)
		return GetRoleByIdResponse{
			Err:     err,
			Message: message,
			Role:    role,
		}, nil
	}
}

// Failed implements Failer.
func (r GetRoleByIdResponse) Failed() error {
	return r.Err
}

// GetMenuRequest collects the request parameters for the GetMenu method.
type GetMenuRequest struct{}

// GetMenuResponse collects the response parameters for the GetMenu method.
type GetMenuResponse struct {
	Menu    []io.MenuSidebar `json:"menu"`
	Message string           `json:"message"`
	Err     error            `json:"err"`
}

// MakeGetMenuEndpoint returns an endpoint that invokes GetMenu on the service.
func MakeGetMenuEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		menu, message, err := s.GetMenu(ctx)
		return GetMenuResponse{
			Err:     err,
			Menu:    menu,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetMenuResponse) Failed() error {
	return r.Err
}

// AddMenuRequest collects the request parameters for the AddMenu method.
type AddMenuRequest struct {
	Menu io.MenuSidebar `json:"menu"`
}

// AddMenuResponse collects the response parameters for the AddMenu method.
type AddMenuResponse struct {
	NewMenu io.MenuSidebar `json:"new_menu"`
	Message string         `json:"message"`
	Err     error          `json:"err"`
}

// MakeAddMenuEndpoint returns an endpoint that invokes AddMenu on the service.
func MakeAddMenuEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddMenuRequest)
		newMenu, message, err := s.AddMenu(ctx, req.Menu)
		return AddMenuResponse{
			Err:     err,
			Message: message,
			NewMenu: newMenu,
		}, nil
	}
}

// Failed implements Failer.
func (r AddMenuResponse) Failed() error {
	return r.Err
}

// DeleteMenuRequest collects the request parameters for the DeleteMenu method.
type DeleteMenuRequest struct {
	Id string `json:"id"`
}

// DeleteMenuResponse collects the response parameters for the DeleteMenu method.
type DeleteMenuResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeDeleteMenuEndpoint returns an endpoint that invokes DeleteMenu on the service.
func MakeDeleteMenuEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteMenuRequest)
		message, err := s.DeleteMenu(ctx, req.Id)
		return DeleteMenuResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r DeleteMenuResponse) Failed() error {
	return r.Err
}

// EditMenuRequest collects the request parameters for the EditMenu method.
type EditMenuRequest struct {
	Menu io.MenuSidebar `json:"menu"`
}

// EditMenuResponse collects the response parameters for the EditMenu method.
type EditMenuResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeEditMenuEndpoint returns an endpoint that invokes EditMenu on the service.
func MakeEditMenuEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(EditMenuRequest)
		message, err := s.EditMenu(ctx, req.Menu)
		return EditMenuResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r EditMenuResponse) Failed() error {
	return r.Err
}

// GetMenuByIdRequest collects the request parameters for the GetMenuById method.
type GetMenuByIdRequest struct {
	Id string `json:"id"`
}

// GetMenuByIdResponse collects the response parameters for the GetMenuById method.
type GetMenuByIdResponse struct {
	Menu    io.MenuSidebar `json:"menu"`
	Message string         `json:"message"`
	Err     error          `json:"err"`
}

// MakeGetMenuByIdEndpoint returns an endpoint that invokes GetMenuById on the service.
func MakeGetMenuByIdEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetMenuByIdRequest)
		menu, message, err := s.GetMenuById(ctx, req.Id)
		return GetMenuByIdResponse{
			Err:     err,
			Menu:    menu,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetMenuByIdResponse) Failed() error {
	return r.Err
}

// GetMenuModelByMenuRequest collects the request parameters for the GetMenuModelByMenu method.
type GetMenuModelByMenuRequest struct {
	IdMenu string `json:"id_menu"`
}

// GetMenuModelByMenuResponse collects the response parameters for the GetMenuModelByMenu method.
type GetMenuModelByMenuResponse struct {
	MenuModel []io.MenuModel `json:"menu_model"`
	Message   string         `json:"message"`
	Err       error          `json:"err"`
}

// MakeGetMenuModelByMenuEndpoint returns an endpoint that invokes GetMenuModelByMenu on the service.
func MakeGetMenuModelByMenuEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetMenuModelByMenuRequest)
		menuModel, message, err := s.GetMenuModelByMenu(ctx, req.IdMenu)
		return GetMenuModelByMenuResponse{
			Err:       err,
			MenuModel: menuModel,
			Message:   message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetMenuModelByMenuResponse) Failed() error {
	return r.Err
}

// AddMenuModelRequest collects the request parameters for the AddMenuModel method.
type AddMenuModelRequest struct {
	MenuModel io.MenuModel `json:"menu_model"`
}

// AddMenuModelResponse collects the response parameters for the AddMenuModel method.
type AddMenuModelResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeAddMenuModelEndpoint returns an endpoint that invokes AddMenuModel on the service.
func MakeAddMenuModelEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddMenuModelRequest)
		message, err := s.AddMenuModel(ctx, req.MenuModel)
		return AddMenuModelResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r AddMenuModelResponse) Failed() error {
	return r.Err
}

// GetAclByRoleRequest collects the request parameters for the GetAclByRole method.
type GetAclByRoleRequest struct {
	RoleName string `json:"role_name"`
}

// GetAclByRoleResponse collects the response parameters for the GetAclByRole method.
type GetAclByRoleResponse struct {
	Acl     []io.Acl `json:"acl"`
	Message string   `json:"message"`
	Err     error    `json:"err"`
}

// MakeGetAclByRoleEndpoint returns an endpoint that invokes GetAclByRole on the service.
func MakeGetAclByRoleEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetAclByRoleRequest)
		acl, message, err := s.GetAclByRole(ctx, req.RoleName)
		return GetAclByRoleResponse{
			Acl:     acl,
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetAclByRoleResponse) Failed() error {
	return r.Err
}

// AddAclRequest collects the request parameters for the AddAcl method.
type AddAclRequest struct {
	Acl io.Acl `json:"acl"`
}

// AddAclResponse collects the response parameters for the AddAcl method.
type AddAclResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeAddAclEndpoint returns an endpoint that invokes AddAcl on the service.
func MakeAddAclEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddAclRequest)
		message, err := s.AddAcl(ctx, req.Acl)
		return AddAclResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r AddAclResponse) Failed() error {
	return r.Err
}

// GetMenuByUserRequest collects the request parameters for the GetMenuByUser method.
type GetMenuByUserRequest struct {
	IdUser string `json:"id_user"`
}

// GetMenuByUserResponse collects the response parameters for the GetMenuByUser method.
type GetMenuByUserResponse struct {
	Menu    []io.MenuSidebar `json:"menu"`
	Message string           `json:"message"`
	Err     error            `json:"err"`
}

// MakeGetMenuByUserEndpoint returns an endpoint that invokes GetMenuByUser on the service.
func MakeGetMenuByUserEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetMenuByUserRequest)
		menu, message, err := s.GetMenuByUser(ctx, req.IdUser)
		return GetMenuByUserResponse{
			Err:     err,
			Menu:    menu,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetMenuByUserResponse) Failed() error {
	return r.Err
}

// Failure is an interface that should be implemented by response types.
// Response encoders can check if responses are Failer, and if so they've
// failed, and if so encode them using a separate write path based on the error.
type Failure interface {
	Failed() error
}

// Signin implements Service. Primarily useful in a client.
func (e Endpoints) Signin(ctx context.Context, email string, password string) (user io.User, message string, err error) {
	request := SigninRequest{
		Email:    email,
		Password: password,
	}
	response, err := e.SigninEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(SigninResponse).User, response.(SigninResponse).Message, response.(SigninResponse).Err
}

// Signout implements Service. Primarily useful in a client.
func (e Endpoints) Signout(ctx context.Context, id string) (message string, err error) {
	request := SignoutRequest{Id: id}
	response, err := e.SignoutEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(SignoutResponse).Message, response.(SignoutResponse).Err
}

// GetUser implements Service. Primarily useful in a client.
func (e Endpoints) GetUser(ctx context.Context) (user []io.User, message string, err error) {
	request := GetUserRequest{}
	response, err := e.GetUserEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetUserResponse).User, response.(GetUserResponse).Message, response.(GetUserResponse).Err
}

// AddUser implements Service. Primarily useful in a client.
func (e Endpoints) AddUser(ctx context.Context, user io.User) (message string, err error) {
	request := AddUserRequest{User: user}
	response, err := e.AddUserEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(AddUserResponse).Message, response.(AddUserResponse).Err
}

// UpdateUser implements Service. Primarily useful in a client.
func (e Endpoints) UpdateUser(ctx context.Context, user io.User) (message string, err error) {
	request := UpdateUserRequest{User: user}
	response, err := e.UpdateUserEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(UpdateUserResponse).Message, response.(UpdateUserResponse).Err
}

// DeleteUser implements Service. Primarily useful in a client.
func (e Endpoints) DeleteUser(ctx context.Context, id string) (message string, err error) {
	request := DeleteUserRequest{Id: id}
	response, err := e.DeleteUserEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(DeleteUserResponse).Message, response.(DeleteUserResponse).Err
}

// GetUserById implements Service. Primarily useful in a client.
func (e Endpoints) GetUserById(ctx context.Context, id string) (user io.User, message string, err error) {
	request := GetUserByIdRequest{Id: id}
	response, err := e.GetUserByIdEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetUserByIdResponse).User, response.(GetUserByIdResponse).Message, response.(GetUserByIdResponse).Err
}

// GetUserByRole implements Service. Primarily useful in a client.
func (e Endpoints) GetUserByRole(ctx context.Context, roleId string) (user []io.User, message string, err error) {
	request := GetUserByRoleRequest{RoleId: roleId}
	response, err := e.GetUserByRoleEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetUserByRoleResponse).User, response.(GetUserByRoleResponse).Message, response.(GetUserByRoleResponse).Err
}

// GetRole implements Service. Primarily useful in a client.
func (e Endpoints) GetRole(ctx context.Context) (role []io.Role, message string, err error) {
	request := GetRoleRequest{}
	response, err := e.GetRoleEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetRoleResponse).Role, response.(GetRoleResponse).Message, response.(GetRoleResponse).Err
}

// AddRole implements Service. Primarily useful in a client.
func (e Endpoints) AddRole(ctx context.Context, role io.Role) (message string, err error) {
	request := AddRoleRequest{Role: role}
	response, err := e.AddRoleEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(AddRoleResponse).Message, response.(AddRoleResponse).Err
}

// EditRole implements Service. Primarily useful in a client.
func (e Endpoints) EditRole(ctx context.Context, role io.Role) (message string, err error) {
	request := EditRoleRequest{Role: role}
	response, err := e.EditRoleEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(EditRoleResponse).Message, response.(EditRoleResponse).Err
}

// DeleteRole implements Service. Primarily useful in a client.
func (e Endpoints) DeleteRole(ctx context.Context, id string) (message string, err error) {
	request := DeleteRoleRequest{Id: id}
	response, err := e.DeleteRoleEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(DeleteRoleResponse).Message, response.(DeleteRoleResponse).Err
}

// GetRoleById implements Service. Primarily useful in a client.
func (e Endpoints) GetRoleById(ctx context.Context, id string) (role io.Role, message string, err error) {
	request := GetRoleByIdRequest{Id: id}
	response, err := e.GetRoleByIdEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetRoleByIdResponse).Role, response.(GetRoleByIdResponse).Message, response.(GetRoleByIdResponse).Err
}

// GetMenu implements Service. Primarily useful in a client.
func (e Endpoints) GetMenu(ctx context.Context) (menu []io.MenuSidebar, message string, err error) {
	request := GetMenuRequest{}
	response, err := e.GetMenuEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetMenuResponse).Menu, response.(GetMenuResponse).Message, response.(GetMenuResponse).Err
}

// AddMenu implements Service. Primarily useful in a client.
func (e Endpoints) AddMenu(ctx context.Context, menu io.MenuSidebar) (newMenu io.MenuSidebar, message string, err error) {
	request := AddMenuRequest{Menu: menu}
	response, err := e.AddMenuEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(AddMenuResponse).NewMenu, response.(AddMenuResponse).Message, response.(AddMenuResponse).Err
}

// DeleteMenu implements Service. Primarily useful in a client.
func (e Endpoints) DeleteMenu(ctx context.Context, id string) (message string, err error) {
	request := DeleteMenuRequest{Id: id}
	response, err := e.DeleteMenuEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(DeleteMenuResponse).Message, response.(DeleteMenuResponse).Err
}

// EditMenu implements Service. Primarily useful in a client.
func (e Endpoints) EditMenu(ctx context.Context, menu io.MenuSidebar) (message string, err error) {
	request := EditMenuRequest{Menu: menu}
	response, err := e.EditMenuEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(EditMenuResponse).Message, response.(EditMenuResponse).Err
}

// GetMenuById implements Service. Primarily useful in a client.
func (e Endpoints) GetMenuById(ctx context.Context, id string) (menu io.MenuSidebar, message string, err error) {
	request := GetMenuByIdRequest{Id: id}
	response, err := e.GetMenuByIdEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetMenuByIdResponse).Menu, response.(GetMenuByIdResponse).Message, response.(GetMenuByIdResponse).Err
}

// GetMenuModelByMenu implements Service. Primarily useful in a client.
func (e Endpoints) GetMenuModelByMenu(ctx context.Context, id_menu string) (menuModel []io.MenuModel, message string, err error) {
	request := GetMenuModelByMenuRequest{IdMenu: id_menu}
	response, err := e.GetMenuModelByMenuEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetMenuModelByMenuResponse).MenuModel, response.(GetMenuModelByMenuResponse).Message, response.(GetMenuModelByMenuResponse).Err
}

// AddMenuModel implements Service. Primarily useful in a client.
func (e Endpoints) AddMenuModel(ctx context.Context, menuModel io.MenuModel) (message string, err error) {
	request := AddMenuModelRequest{MenuModel: menuModel}
	response, err := e.AddMenuModelEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(AddMenuModelResponse).Message, response.(AddMenuModelResponse).Err
}

// GetAclByRole implements Service. Primarily useful in a client.
func (e Endpoints) GetAclByRole(ctx context.Context, roleName string) (acl []io.Acl, message string, err error) {
	request := GetAclByRoleRequest{RoleName: roleName}
	response, err := e.GetAclByRoleEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetAclByRoleResponse).Acl, response.(GetAclByRoleResponse).Message, response.(GetAclByRoleResponse).Err
}

// AddAcl implements Service. Primarily useful in a client.
func (e Endpoints) AddAcl(ctx context.Context, acl io.Acl) (message string, err error) {
	request := AddAclRequest{Acl: acl}
	response, err := e.AddAclEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(AddAclResponse).Message, response.(AddAclResponse).Err
}

// GetMenuByUser implements Service. Primarily useful in a client.
func (e Endpoints) GetMenuByUser(ctx context.Context, id_user string) (menu []io.MenuSidebar, message string, err error) {
	request := GetMenuByUserRequest{IdUser: id_user}
	response, err := e.GetMenuByUserEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetMenuByUserResponse).Menu, response.(GetMenuByUserResponse).Message, response.(GetMenuByUserResponse).Err
}

// RegisterPasienRequest collects the request parameters for the RegisterPasien method.
type RegisterPasienRequest struct {
	User io.User `json:"user"`
}

// RegisterPasienResponse collects the response parameters for the RegisterPasien method.
type RegisterPasienResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeRegisterPasienEndpoint returns an endpoint that invokes RegisterPasien on the service.
func MakeRegisterPasienEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(RegisterPasienRequest)
		message, err := s.RegisterPasien(ctx, req.User)
		return RegisterPasienResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r RegisterPasienResponse) Failed() error {
	return r.Err
}

// RegisterPasien implements Service. Primarily useful in a client.
func (e Endpoints) RegisterPasien(ctx context.Context, user io.User) (message string, err error) {
	request := RegisterPasienRequest{User: user}
	response, err := e.RegisterPasienEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(RegisterPasienResponse).Message, response.(RegisterPasienResponse).Err
}

// GetUserByVisitRequest collects the request parameters for the GetUserByVisit method.
type GetUserByVisitRequest struct {
	VisitId string `json:"visit_id"`
}

// GetUserByVisitResponse collects the response parameters for the GetUserByVisit method.
type GetUserByVisitResponse struct {
	User    io.User `json:"user"`
	Message string  `json:"message"`
	Err     error   `json:"err"`
}

// MakeGetUserByVisitEndpoint returns an endpoint that invokes GetUserByVisit on the service.
func MakeGetUserByVisitEndpoint(s service.RbacService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetUserByVisitRequest)
		user, message, err := s.GetUserByVisit(ctx, req.VisitId)
		return GetUserByVisitResponse{
			Err:     err,
			Message: message,
			User:    user,
		}, nil
	}
}

// Failed implements Failer.
func (r GetUserByVisitResponse) Failed() error {
	return r.Err
}

// GetUserByVisit implements Service. Primarily useful in a client.
func (e Endpoints) GetUserByVisit(ctx context.Context, visitId string) (user io.User, message string, err error) {
	request := GetUserByVisitRequest{VisitId: visitId}
	response, err := e.GetUserByVisitEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetUserByVisitResponse).User, response.(GetUserByVisitResponse).Message, response.(GetUserByVisitResponse).Err
}
