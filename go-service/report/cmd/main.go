package main

import service "akuklinikid_demo/go-service/report/cmd/service"

func main() {
	service.Run()
}
