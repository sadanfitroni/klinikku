package http

import (
	"context"
	"encoding/json"
	"errors"
	endpoint "akuklinikid_demo/go-service/report/pkg/endpoint"
	http1 "net/http"

	http "github.com/go-kit/kit/transport/http"
	handlers "github.com/gorilla/handlers"
	mux "github.com/gorilla/mux"
)

// makeGetTodayVisitHandler creates the handler logic
func makeGetTodayVisitHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-today-visit").
		Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetTodayVisitEndpoint, decodeGetTodayVisitRequest, encodeGetTodayVisitResponse, options...)))
}

// decodeGetTodayVisitRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetTodayVisitRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetTodayVisitRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetTodayVisitResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetTodayVisitResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetTodayCancelVisitHandler creates the handler logic
func makeGetTodayCancelVisitHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-today-cancel-visit").
		Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetTodayCancelVisitEndpoint, decodeGetTodayCancelVisitRequest, encodeGetTodayCancelVisitResponse, options...)))
}

// decodeGetTodayCancelVisitRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetTodayCancelVisitRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetTodayCancelVisitRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetTodayCancelVisitResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetTodayCancelVisitResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetTodayComplateVisitHandler creates the handler logic
func makeGetTodayComplateVisitHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-today-complate-visit").
		Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetTodayComplateVisitEndpoint, decodeGetTodayComplateVisitRequest, encodeGetTodayComplateVisitResponse, options...)))
}

// decodeGetTodayComplateVisitRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetTodayComplateVisitRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetTodayComplateVisitRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetTodayComplateVisitResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetTodayComplateVisitResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetTodayRevenueHandler creates the handler logic
func makeGetTodayRevenueHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-today-revenue").
		Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetTodayRevenueEndpoint, decodeGetTodayRevenueRequest, encodeGetTodayRevenueResponse, options...)))
}

// decodeGetTodayRevenueRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetTodayRevenueRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetTodayRevenueRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetTodayRevenueResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetTodayRevenueResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetTodayOldMemberVisitHandler creates the handler logic
func makeGetTodayOldMemberVisitHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-today-old-member-visit").
		Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetTodayOldMemberVisitEndpoint, decodeGetTodayOldMemberVisitRequest, encodeGetTodayOldMemberVisitResponse, options...)))
}

// decodeGetTodayOldMemberVisitRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetTodayOldMemberVisitRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetTodayOldMemberVisitRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetTodayOldMemberVisitResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetTodayOldMemberVisitResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetTodayNewMemberVisitHandler creates the handler logic
func makeGetTodayNewMemberVisitHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-today-new-member-visit").
			Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetTodayNewMemberVisitEndpoint, decodeGetTodayNewMemberVisitRequest, encodeGetTodayNewMemberVisitResponse, options...)))
}

// decodeGetTodayNewMemberVisitRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetTodayNewMemberVisitRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetTodayNewMemberVisitRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetTodayNewMemberVisitResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetTodayNewMemberVisitResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}
func ErrorEncoder(_ context.Context, err error, w http1.ResponseWriter) {
	w.WriteHeader(err2code(err))
	json.NewEncoder(w).Encode(errorWrapper{Error: err.Error()})
}
func ErrorDecoder(r *http1.Response) error {
	var w errorWrapper
	if err := json.NewDecoder(r.Body).Decode(&w); err != nil {
		return err
	}
	return errors.New(w.Error)
}

// This is used to set the http status, see an example here :
// https://github.com/go-kit/kit/blob/master/examples/addsvc/pkg/addtransport/http.go#L133
func err2code(err error) int {
	return http1.StatusInternalServerError
}

type errorWrapper struct {
	Error string `json:"error"`
}

// makeGetReportDataHandler creates the handler logic
func makeGetReportDataHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST","OPTIONS").Path("/get-report-data").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}), 
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetReportDataEndpoint, decodeGetReportDataRequest, encodeGetReportDataResponse, options...)))
}

// decodeGetReportDataRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetReportDataRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetReportDataRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetReportDataResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetReportDataResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}
