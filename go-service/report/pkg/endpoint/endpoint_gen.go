// THIS FILE IS AUTO GENERATED BY GK-CLI DO NOT EDIT!!
package endpoint

import (
	endpoint "github.com/go-kit/kit/endpoint"
	service "akuklinikid_demo/go-service/report/pkg/service"
)

// Endpoints collects all of the endpoints that compose a profile service. It's
// meant to be used as a helper struct, to collect all of the endpoints into a
// single parameter.
type Endpoints struct {
	GetTodayVisitEndpoint          endpoint.Endpoint
	GetTodayCancelVisitEndpoint    endpoint.Endpoint
	GetTodayComplateVisitEndpoint  endpoint.Endpoint
	GetTodayRevenueEndpoint        endpoint.Endpoint
	GetTodayOldMemberVisitEndpoint endpoint.Endpoint
	GetTodayNewMemberVisitEndpoint endpoint.Endpoint
	GetReportDataEndpoint          endpoint.Endpoint
}

// New returns a Endpoints struct that wraps the provided service, and wires in all of the
// expected endpoint middlewares
func New(s service.ReportService, mdw map[string][]endpoint.Middleware) Endpoints {
	eps := Endpoints{
		GetReportDataEndpoint:          MakeGetReportDataEndpoint(s),
		GetTodayCancelVisitEndpoint:    MakeGetTodayCancelVisitEndpoint(s),
		GetTodayComplateVisitEndpoint:  MakeGetTodayComplateVisitEndpoint(s),
		GetTodayNewMemberVisitEndpoint: MakeGetTodayNewMemberVisitEndpoint(s),
		GetTodayOldMemberVisitEndpoint: MakeGetTodayOldMemberVisitEndpoint(s),
		GetTodayRevenueEndpoint:        MakeGetTodayRevenueEndpoint(s),
		GetTodayVisitEndpoint:          MakeGetTodayVisitEndpoint(s),
	}
	for _, m := range mdw["GetTodayVisit"] {
		eps.GetTodayVisitEndpoint = m(eps.GetTodayVisitEndpoint)
	}
	for _, m := range mdw["GetTodayCancelVisit"] {
		eps.GetTodayCancelVisitEndpoint = m(eps.GetTodayCancelVisitEndpoint)
	}
	for _, m := range mdw["GetTodayComplateVisit"] {
		eps.GetTodayComplateVisitEndpoint = m(eps.GetTodayComplateVisitEndpoint)
	}
	for _, m := range mdw["GetTodayRevenue"] {
		eps.GetTodayRevenueEndpoint = m(eps.GetTodayRevenueEndpoint)
	}
	for _, m := range mdw["GetTodayOldMemberVisit"] {
		eps.GetTodayOldMemberVisitEndpoint = m(eps.GetTodayOldMemberVisitEndpoint)
	}
	for _, m := range mdw["GetTodayNewMemberVisit"] {
		eps.GetTodayNewMemberVisitEndpoint = m(eps.GetTodayNewMemberVisitEndpoint)
	}
	for _, m := range mdw["GetReportData"] {
		eps.GetReportDataEndpoint = m(eps.GetReportDataEndpoint)
	}
	return eps
}
