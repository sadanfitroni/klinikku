package endpoint

import (
	"context"
	service "akuklinikid_demo/go-service/report/pkg/service"
	"akuklinikid_demo/go-service/io"
	endpoint "github.com/go-kit/kit/endpoint"
)

// GetTodayVisitRequest collects the request parameters for the GetTodayVisit method.
type GetTodayVisitRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetTodayVisitResponse collects the response parameters for the GetTodayVisit method.
type GetTodayVisitResponse struct {
	Count   int    `json:"count"`
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeGetTodayVisitEndpoint returns an endpoint that invokes GetTodayVisit on the service.
func MakeGetTodayVisitEndpoint(s service.ReportService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetTodayVisitRequest)
		count, message, err := s.GetTodayVisit(ctx, req.KlinikId)
		return GetTodayVisitResponse{
			Count:   count,
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetTodayVisitResponse) Failed() error {
	return r.Err
}

// GetTodayCancelVisitRequest collects the request parameters for the GetTodayCancelVisit method.
type GetTodayCancelVisitRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetTodayCancelVisitResponse collects the response parameters for the GetTodayCancelVisit method.
type GetTodayCancelVisitResponse struct {
	Count   int    `json:"count"`
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeGetTodayCancelVisitEndpoint returns an endpoint that invokes GetTodayCancelVisit on the service.
func MakeGetTodayCancelVisitEndpoint(s service.ReportService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetTodayCancelVisitRequest)
		count, message, err := s.GetTodayCancelVisit(ctx, req.KlinikId)
		return GetTodayCancelVisitResponse{
			Count:   count,
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetTodayCancelVisitResponse) Failed() error {
	return r.Err
}

// GetTodayComplateVisitRequest collects the request parameters for the GetTodayComplateVisit method.
type GetTodayComplateVisitRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetTodayComplateVisitResponse collects the response parameters for the GetTodayComplateVisit method.
type GetTodayComplateVisitResponse struct {
	Count   int    `json:"count"`
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeGetTodayComplateVisitEndpoint returns an endpoint that invokes GetTodayComplateVisit on the service.
func MakeGetTodayComplateVisitEndpoint(s service.ReportService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetTodayComplateVisitRequest)
		count, message, err := s.GetTodayComplateVisit(ctx, req.KlinikId)
		return GetTodayComplateVisitResponse{
			Count:   count,
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetTodayComplateVisitResponse) Failed() error {
	return r.Err
}

// GetTodayRevenueRequest collects the request parameters for the GetTodayRevenue method.
type GetTodayRevenueRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetTodayRevenueResponse collects the response parameters for the GetTodayRevenue method.
type GetTodayRevenueResponse struct {
	Revenue int    `json:"revenue"`
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeGetTodayRevenueEndpoint returns an endpoint that invokes GetTodayRevenue on the service.
func MakeGetTodayRevenueEndpoint(s service.ReportService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetTodayRevenueRequest)
		revenue, message, err := s.GetTodayRevenue(ctx, req.KlinikId)
		return GetTodayRevenueResponse{
			Err:     err,
			Message: message,
			Revenue: revenue,
		}, nil
	}
}

// Failed implements Failer.
func (r GetTodayRevenueResponse) Failed() error {
	return r.Err
}

// GetTodayOldMemberVisitRequest collects the request parameters for the GetTodayOldMemberVisit method.
type GetTodayOldMemberVisitRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetTodayOldMemberVisitResponse collects the response parameters for the GetTodayOldMemberVisit method.
type GetTodayOldMemberVisitResponse struct {
	Count   int    `json:"count"`
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeGetTodayOldMemberVisitEndpoint returns an endpoint that invokes GetTodayOldMemberVisit on the service.
func MakeGetTodayOldMemberVisitEndpoint(s service.ReportService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetTodayOldMemberVisitRequest)
		count, message, err := s.GetTodayOldMemberVisit(ctx, req.KlinikId)
		return GetTodayOldMemberVisitResponse{
			Count:   count,
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetTodayOldMemberVisitResponse) Failed() error {
	return r.Err
}

// GetTodayNewMemberVisitRequest collects the request parameters for the GetTodayNewMemberVisit method.
type GetTodayNewMemberVisitRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetTodayNewMemberVisitResponse collects the response parameters for the GetTodayNewMemberVisit method.
type GetTodayNewMemberVisitResponse struct {
	Count   int    `json:"count"`
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeGetTodayNewMemberVisitEndpoint returns an endpoint that invokes GetTodayNewMemberVisit on the service.
func MakeGetTodayNewMemberVisitEndpoint(s service.ReportService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetTodayNewMemberVisitRequest)
		count, message, err := s.GetTodayNewMemberVisit(ctx, req.KlinikId)
		return GetTodayNewMemberVisitResponse{
			Count:   count,
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetTodayNewMemberVisitResponse) Failed() error {
	return r.Err
}

// Failure is an interface that should be implemented by response types.
// Response encoders can check if responses are Failer, and if so they've
// failed, and if so encode them using a separate write path based on the error.
type Failure interface {
	Failed() error
}

// GetTodayVisit implements Service. Primarily useful in a client.
func (e Endpoints) GetTodayVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	request := GetTodayVisitRequest{KlinikId: klinikId}
	response, err := e.GetTodayVisitEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetTodayVisitResponse).Count, response.(GetTodayVisitResponse).Message, response.(GetTodayVisitResponse).Err
}

// GetTodayCancelVisit implements Service. Primarily useful in a client.
func (e Endpoints) GetTodayCancelVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	request := GetTodayCancelVisitRequest{KlinikId: klinikId}
	response, err := e.GetTodayCancelVisitEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetTodayCancelVisitResponse).Count, response.(GetTodayCancelVisitResponse).Message, response.(GetTodayCancelVisitResponse).Err
}

// GetTodayComplateVisit implements Service. Primarily useful in a client.
func (e Endpoints) GetTodayComplateVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	request := GetTodayComplateVisitRequest{KlinikId: klinikId}
	response, err := e.GetTodayComplateVisitEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetTodayComplateVisitResponse).Count, response.(GetTodayComplateVisitResponse).Message, response.(GetTodayComplateVisitResponse).Err
}

// GetTodayRevenue implements Service. Primarily useful in a client.
func (e Endpoints) GetTodayRevenue(ctx context.Context, klinikId string) (revenue int, message string, err error) {
	request := GetTodayRevenueRequest{KlinikId: klinikId}
	response, err := e.GetTodayRevenueEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetTodayRevenueResponse).Revenue, response.(GetTodayRevenueResponse).Message, response.(GetTodayRevenueResponse).Err
}

// GetTodayOldMemberVisit implements Service. Primarily useful in a client.
func (e Endpoints) GetTodayOldMemberVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	request := GetTodayOldMemberVisitRequest{KlinikId: klinikId}
	response, err := e.GetTodayOldMemberVisitEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetTodayOldMemberVisitResponse).Count, response.(GetTodayOldMemberVisitResponse).Message, response.(GetTodayOldMemberVisitResponse).Err
}

// GetTodayNewMemberVisit implements Service. Primarily useful in a client.
func (e Endpoints) GetTodayNewMemberVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	request := GetTodayNewMemberVisitRequest{KlinikId: klinikId}
	response, err := e.GetTodayNewMemberVisitEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetTodayNewMemberVisitResponse).Count, response.(GetTodayNewMemberVisitResponse).Message, response.(GetTodayNewMemberVisitResponse).Err
}

// GetReportDataRequest collects the request parameters for the GetReportData method.
type GetReportDataRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetReportDataResponse collects the response parameters for the GetReportData method.
type GetReportDataResponse struct {
	User    []io.User           `json:"user"`
	Visit   []io.Visit          `json:"visit"`
	Jadwal  []io.JadwalPraktek `json:"jadwal"`
	Message string              `json:"message"`
	Err     error               `json:"err"`
}

// MakeGetReportDataEndpoint returns an endpoint that invokes GetReportData on the service.
func MakeGetReportDataEndpoint(s service.ReportService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetReportDataRequest)
		user, Visit, jadwal, message, err := s.GetReportData(ctx, req.KlinikId)
		return GetReportDataResponse{
			Err:     err,
			Jadwal:  jadwal,
			Message: message,
			User:    user,
			Visit:   Visit,
		}, nil
	}
}

// Failed implements Failer.
func (r GetReportDataResponse) Failed() error {
	return r.Err
}

// GetReportData implements Service. Primarily useful in a client.
func (e Endpoints) GetReportData(ctx context.Context, klinikId string) (user []io.User, Visit []io.Visit, jadwal []io.JadwalPraktek, message string, err error) {
	request := GetReportDataRequest{KlinikId: klinikId}
	response, err := e.GetReportDataEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetReportDataResponse).User, response.(GetReportDataResponse).Visit, response.(GetReportDataResponse).Jadwal, response.(GetReportDataResponse).Message, response.(GetReportDataResponse).Err
}
