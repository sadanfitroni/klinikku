package service

import (
	"context"

	log "github.com/go-kit/kit/log"
	io "akuklinikid_demo/go-service/io"
)

// Middleware describes a service middleware.
type Middleware func(ReportService) ReportService

type loggingMiddleware struct {
	logger log.Logger
	next   ReportService
}

// LoggingMiddleware takes a logger as a dependency
// and returns a ReportService Middleware.
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next ReportService) ReportService {
		return &loggingMiddleware{logger, next}
	}

}

func (l loggingMiddleware) GetTodayVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetTodayVisit", "klinikId", klinikId, "count", count, "message", message, "err", err)
	}()
	return l.next.GetTodayVisit(ctx, klinikId)
}
func (l loggingMiddleware) GetTodayCancelVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetTodayCancelVisit", "klinikId", klinikId, "count", count, "message", message, "err", err)
	}()
	return l.next.GetTodayCancelVisit(ctx, klinikId)
}
func (l loggingMiddleware) GetTodayComplateVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetTodayComplateVisit", "klinikId", klinikId, "count", count, "message", message, "err", err)
	}()
	return l.next.GetTodayComplateVisit(ctx, klinikId)
}
func (l loggingMiddleware) GetTodayRevenue(ctx context.Context, klinikId string) (revenue int, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetTodayRevenue", "klinikId", klinikId, "revenue", revenue, "message", message, "err", err)
	}()
	return l.next.GetTodayRevenue(ctx, klinikId)
}
func (l loggingMiddleware) GetTodayOldMemberVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetTodayOldMemberVisit", "klinikId", klinikId, "count", count, "message", message, "err", err)
	}()
	return l.next.GetTodayOldMemberVisit(ctx, klinikId)
}
func (l loggingMiddleware) GetTodayNewMemberVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetTodayNewMemberVisit", "klinikId", klinikId, "count", count, "message", message, "err", err)
	}()
	return l.next.GetTodayNewMemberVisit(ctx, klinikId)
}

func (l loggingMiddleware) GetReportData(ctx context.Context, klinikId string) (user []io.User, Visit []io.Visit, jadwal []io.JadwalPraktek, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetReportData", "klinikId", klinikId, "user", user, "Visit", Visit, "jadwal", jadwal, "message", message, "err", err)
	}()
	return l.next.GetReportData(ctx, klinikId)
}
