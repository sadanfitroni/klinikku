package service

import (
	"context"

	"akuklinikid_demo/go-service/db"
	"akuklinikid_demo/go-service/io"

)

// ReportService describes the service.
type ReportService interface {
	// Add your methods here

	GetTodayVisit(ctx context.Context, klinikId string) (count int, message string, err error)
	GetTodayCancelVisit(ctx context.Context, klinikId string) (count int, message string, err error)
	GetTodayComplateVisit(ctx context.Context, klinikId string) (count int, message string, err error)
	GetTodayRevenue(ctx context.Context, klinikId string) (revenue int, message string, err error)
	GetTodayOldMemberVisit(ctx context.Context, klinikId string) (count int, message string, err error)
	GetTodayNewMemberVisit(ctx context.Context, klinikId string) (count int, message string, err error)
	// e.x: Foo(ctx context.Context,s string)(rs string, err error)
	GetReportData(ctx context.Context, klinikId string) (user []io.User, visit []io.Visit, jadwal []io.JadwalPraktek, message string, err error)
}

type basicReportService struct{}

func (b *basicReportService) GetTodayVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return count, message, err
	}
	defer session.Close()
	row := session.QueryRow(""+
		"	SELECT count(visit.id) AS visit_count FROM visit,jadwal_praktek "+
		"		WHERE "+
		"			visit.DATE = DATE(NOW()) "+
		"			AND visit.fk_jadwal_praktek = jadwal_praktek.id"+
		"			AND jadwal_praktek.fk_klinik = ?", klinikId)
	err = row.Scan(
		&count,
	)
	if err != nil {
		message = "Failed get Count today visit"
		return count, message, err
	}

	message = "Success get Count today visit"
	return count, message, err
}
func (b *basicReportService) GetTodayCancelVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return count, message, err
	}
	defer session.Close()
	row := session.QueryRow(""+
		"	SELECT count(visit.id) AS visit_count FROM visit,jadwal_praktek "+
		"		WHERE "+
		"			visit.DATE = DATE(NOW()) "+
		"			AND visit.fk_jadwal_praktek = jadwal_praktek.id"+
		"			AND visit.status != 'Bayar' "+
		"			AND jadwal_praktek.fk_klinik = ?", klinikId)
	// row := session.QueryRow("SELECT count(id) AS visit_count FROM visit WHERE visit.DATE = DATE(NOW()) AND status = 'Booking' ")
	err = row.Scan(
		&count,
	)
	if err != nil {
		message = "Failed get count cancel today visit"
		return count, message, err
	}

	message = "Success get count today cancel visit"
	return count, message, err
}
func (b *basicReportService) GetTodayComplateVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return count, message, err
	}
	defer session.Close()
	row := session.QueryRow(""+
		"	SELECT count(visit.id) AS visit_count FROM visit,jadwal_praktek "+
		"		WHERE "+
		"			visit.DATE = DATE(NOW()) "+
		"			AND visit.fk_jadwal_praktek = jadwal_praktek.id"+
		"			AND visit.status = 'Bayar' "+
		"			AND jadwal_praktek.fk_klinik = ?", klinikId)
	// row := session.QueryRow("SELECT count(id) AS visit_count FROM visit WHERE visit.DATE = DATE(NOW()) AND status = 'Bayar' ")
	err = row.Scan(
		&count,
	)
	if err != nil {
		message = "Failed get count complate today visit"
		return count, message, err
	}

	message = "Success get count today complate visit"
	return count, message, err
}
func (b *basicReportService) GetTodayRevenue(ctx context.Context, klinikId string) (revenue int, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return revenue, message, err
	}
	defer session.Close()
	row := session.QueryRow(""+
		"	SELECT IF(SUM(visit.total) IS NULL,0, SUM(visit.total)) AS total_penghasilan FROM visit,jadwal_praktek "+
		"		WHERE "+
		"			visit.DATE = DATE(NOW()) "+
		"			AND visit.fk_jadwal_praktek = jadwal_praktek.id"+
		"			AND visit.status = 'Bayar' "+
		"			AND jadwal_praktek.fk_klinik = ?", klinikId)
	// row := session.QueryRow("SELECT SUM(total) AS total_penghasilan FROM visit WHERE visit.DATE = DATE(NOW()) AND visit.STATUS = 'Bayar'")
	err = row.Scan(
		&revenue,
	)
	if err != nil {
		message = "Failed get total revenue today "
		return revenue, message, err
	}

	message = "Success get total revenue today "
	return revenue, message, err
}
func (b *basicReportService) GetTodayOldMemberVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return count, message, err
	}
	defer session.Close()
	row := session.QueryRow(""+
		" SELECT count(distinct(user.fullname)) FROM user,visit,klinik_dokter,jadwal_praktek"+
		" 	WHERE  	"+
		" 		user.id = visit.fk_pasien"+
		"		AND visit.fk_jadwal_praktek = jadwal_praktek.id "+
		" 		AND visit.DATE = DATE(NOW()) "+
		" 		AND visit.fk_pasien = klinik_dokter.fk_user "+
		" 		AND klinik_dokter.tanggal_daftar < DATE(NOW())"+
		"		AND jadwal_praktek.fk_klinik = ?", klinikId)
	err = row.Scan(
		&count,
	)
	if err != nil {
		message = "Failed get count today old member visit "
		return count, message, err
	}

	message = "Success get count today old member visit"
	return count, message, err
}
func (b *basicReportService) GetTodayNewMemberVisit(ctx context.Context, klinikId string) (count int, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return count, message, err
	}
	defer session.Close()
	row := session.QueryRow(""+
		" SELECT count(distinct(user.fullname)) FROM user,visit,klinik_dokter,jadwal_praktek"+
		" 	WHERE  	"+
		" 		user.id = visit.fk_pasien"+
		"		AND visit.fk_jadwal_praktek = jadwal_praktek.id "+
		" 		AND visit.DATE = DATE(NOW()) "+
		" 		AND visit.fk_pasien = klinik_dokter.fk_user "+
		" 		AND klinik_dokter.tanggal_daftar = DATE(NOW())"+
		"		AND jadwal_praktek.fk_klinik = ?", klinikId)
	err = row.Scan(
		&count,
	)
	if err != nil {
		message = "Failed get count today new member visit"
		return count, message, err
	}

	message = "Success get count today new member visit"
	return count, message, err
}

// NewBasicReportService returns a naive, stateless implementation of ReportService.
func NewBasicReportService() ReportService {
	return &basicReportService{}
}

// New returns a ReportService with all of the expected middleware wired in.
func New(middleware []Middleware) ReportService {
	var svc ReportService = NewBasicReportService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}

func (b *basicReportService) GetReportData(ctx context.Context, klinikId string) (user []io.User, visit []io.Visit, jadwal []io.JadwalPraktek, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return user, visit, jadwal, message, err
	}
	defer session.Close()

	result, err := session.Query( "" + 
	"	SELECT visit.date,visit.status, jadwal_praktek.poli_name, visit.total, user.fullname "  + 
		" FROM visit  " +
	"		INNER JOIN jadwal_praktek ON visit.fk_jadwal_praktek = jadwal_praktek.id " +
	"		INNER JOIN user ON visit.fk_dokter = user.id " +
	"		WHERE jadwal_praktek.fk_klinik = ? ", klinikId)

	if err != nil {
		message = "Failed connect to database"
		return user, visit, jadwal, message, err
	}

	visitData := io.Visit{}
	userData := io.User{}
	jadwalData := io.JadwalPraktek{}
	for result.Next() {
		result.Scan(
			&visitData.Date,
			&visitData.Status,
			&jadwalData.Poli_name,
			&visitData.Total,
			&userData.Fullname,
		)
		user = append(user,userData)
		visit = append(visit,visitData)
		jadwal = append(jadwal,jadwalData)
	}
	message = "Success get data"
	return user, visit, jadwal, message, err
}
