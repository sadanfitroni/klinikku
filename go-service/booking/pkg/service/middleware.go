package service

import (
	"context"
	"akuklinikid_demo/go-service/io"

	log "github.com/go-kit/kit/log"
)

type Middleware func(BookingService) BookingService

type loggingMiddleware struct {
	logger log.Logger
	next   BookingService
}

func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next BookingService) BookingService {
		return &loggingMiddleware{logger, next}
	}

}

func (l loggingMiddleware) Book(ctx context.Context, userId string, jadwalPraktekId string) (message string, visitId int64, err error) {
	defer func() {
		l.logger.Log("method", "Book", "userId", userId, "jadwalPraktekId", jadwalPraktekId, "message", message, "visitId", visitId, "err", err)
	}()
	return l.next.Book(ctx, userId, jadwalPraktekId)
}

func (l loggingMiddleware) GetBookingDetail(ctx context.Context, userId string) (visit io.Visit, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetBookingDetail", "userId", userId, "visit", visit, "message", message, "err", err)
	}()
	return l.next.GetBookingDetail(ctx, userId)
}

func (l loggingMiddleware) GetBookingDetailByVisitId(ctx context.Context, visitId string) (visit io.Visit, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetBookingDetailByVisitId", "visitId", visitId, "visit", visit, "message", message, "err", err)
	}()
	return l.next.GetBookingDetailByVisitId(ctx, visitId)
}
