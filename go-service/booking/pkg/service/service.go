package service

import (
	"context"
	// "fmt"
	// "encoding/json"
	// "net/http"
	"strconv"
	"strings"
	"time"

	// "bytes"
	"database/sql"
	"akuklinikid_demo/go-service/db"
	"akuklinikid_demo/go-service/io"
	// "net/url"
)

// BookingService describes the service.
type BookingService interface {
	// Add your methods here
	Book(ctx context.Context, userId string, jadwalPraktekId string) (message string, visitId int64, err error)
	GetBookingDetail(ctx context.Context, userId string) (visit io.Visit, message string, err error)
	GetBookingDetailByVisitId(ctx context.Context, visitId string) (visit io.Visit, message string, err error)
}

type basicBookingService struct{}

func (b *basicBookingService) Book(ctx context.Context, userId string, jadwalPraktekId string) (message string, visitId int64, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, visitId, err
	}
	defer session.Close()

	praktek := io.Praktek{}
	query := "select * from jadwal_praktek where id=?"
	row := session.QueryRow(query, jadwalPraktekId)
	err = row.Scan(&praktek.Id, &praktek.Code, &praktek.Poli_name, &praktek.Poli_type, &praktek.Day, &praktek.Time_from, &praktek.Time_to, &praktek.Fk_dokter, &praktek.Fk_klinik, &praktek.Price_rate, &praktek.Rating, &praktek.Status, &praktek.Lama_tindakan)
	if err != nil {
		message = "Failed getting jadwal_praktek"
		return message, visitId, err
	}

	days := [7]string{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}
	var daysOfWeek = map[string]time.Weekday{
		"Sunday":    time.Sunday,
		"Monday":    time.Monday,
		"Tuesday":   time.Tuesday,
		"Wednesday": time.Wednesday,
		"Thursday":  time.Thursday,
		"Friday":    time.Friday,
		"Saturday":  time.Saturday,
	}
	bookDate := time.Now()
	for true {
		if bookDate.Weekday() == daysOfWeek[days[praktek.Day]] {
			break
		}
		bookDate = bookDate.AddDate(0, 0, 1)
	}
	// fmt.Println(praktek.Fk_dokter)
	historyPraktek := io.HistoryPraktek{}
	row = session.QueryRow("select * from history_praktek where fk_jadwal_praktek=? order by id desc limit 1", jadwalPraktekId)
	err = row.Scan(&historyPraktek.Id, &historyPraktek.Praktek_name, &historyPraktek.Book_count, &historyPraktek.Open_time, &historyPraktek.Close_time, &historyPraktek.Fk_jadwal_praktek, &historyPraktek.Status, &historyPraktek.Remark, &historyPraktek.Fk_dokter)
	if err != nil {
		_, err = session.Query("insert into history_praktek values(?,?,?,?,?,?,?,?,?);", nil, "", 0, bookDate.Format("2006-01-02 ")+praktek.Time_from, time.Now().Format("2006-01-02 ")+praktek.Time_to, jadwalPraktekId, "", "", praktek.Fk_dokter)
		if err != nil {
			message = "Failed insert historyPraktek"
			return message, visitId, err
		}
		row = session.QueryRow("select * from history_praktek where fk_jadwal_praktek=? order by id desc limit 1", jadwalPraktekId)
		err = row.Scan(&historyPraktek.Id, &historyPraktek.Praktek_name, &historyPraktek.Book_count, &historyPraktek.Open_time, &historyPraktek.Close_time, &historyPraktek.Fk_jadwal_praktek, &historyPraktek.Status, &historyPraktek.Remark, &historyPraktek.Fk_dokter)
		if err != nil {
			message = "Failed get historyPraktek"
			return message, visitId, err
		}
	} else {
		if strings.Split(historyPraktek.Open_time, "T")[0] != bookDate.Format("2006-01-02") {
			_, err = session.Query("insert into history_praktek values(?,?,?,?,?,?,?,?,?);",
				nil, "", 0, bookDate.Format("2006-01-02 ")+praktek.Time_from, time.Now().Format("2006-01-02 ")+praktek.Time_to, jadwalPraktekId,
				"", "", praktek.Fk_dokter)
			if err != nil {
				message = "Failed historyPraktek query"
				return message, visitId, err
			}
			row = session.QueryRow("select * from history_praktek where fk_jadwal_praktek=? order by id desc limit 1", jadwalPraktekId)
			err = row.Scan(&historyPraktek.Id, &historyPraktek.Praktek_name, &historyPraktek.Book_count, &historyPraktek.Open_time, &historyPraktek.Close_time, &historyPraktek.Fk_jadwal_praktek, &historyPraktek.Status, &historyPraktek.Remark, &historyPraktek.Fk_dokter)
			if err != nil {
				message = "Failed getting history_praktek"
				return message, visitId, err
			}
		}
	}

	var booknum int
	booknum = historyPraktek.Book_count + 1
	var code string
	code = praktek.Code+"a"+strconv.Itoa(booknum)

	stmt, err := session.Prepare("insert into visit(id,code,date,booknum,fk_dokter,fk_jadwal_praktek,fk_perawat,fk_pasien,fk_history_praktek,fk_klinik,total,rating,estimasi_tindakan,checking_time,status,tipe_pembayaran,no_polis,kode_pembayaran,nama_asuransi) values(?,?,?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?);")
	if err != nil {
        message="Cannot prepare DB statement"
        return message, visitId, err
    }

    res, err := stmt.Exec(nil, code, nil, booknum, historyPraktek.Fk_dokter, jadwalPraktekId, 0, userId, historyPraktek.Id,praktek.Fk_klinik, 0, 0, (booknum-1)*praktek.Lama_tindakan, "booking", "", "", "", "")
    if err != nil {
        message="Cannot run insert statement"
        return message, visitId, err
    }

    visitId, _ = res.LastInsertId()

	var fk_klinik int
	var fk_user int
	query = "select fk_klinik, fk_user from klinik_dokter where fk_klinik=? and fk_user=?"
	row = session.QueryRow(query, praktek.Fk_klinik, userId)
	err = row.Scan(&fk_klinik, &fk_user)
	if err != nil {
		_, err = session.Query("insert into klinik_dokter values(?,?,?);", praktek.Fk_klinik, userId, nil)
	}
	_, err = session.Query("update history_praktek set book_count=? where id=?", booknum, historyPraktek.Id)
	if err != nil {
		message = "Failed update visit"
		return message, visitId, err
	}
	message = "booking success"
	return message, visitId, err
}

// NewBasicBookingService returns a naive, stateless implementation of BookingService.
func NewBasicBookingService() BookingService {
	return &basicBookingService{}
}

// New returns a BookingService with all of the expected middleware wired in.
func New(middleware []Middleware) BookingService {
	var svc BookingService = NewBasicBookingService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}

func (b *basicBookingService) GetBookingDetail(ctx context.Context, userId string) (visit io.Visit, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return visit, message, err
	}
	defer session.Close()

	query := "select id, booknum, fk_jadwal_praktek, fk_history_praktek, estimasi_tindakan from visit where fk_pasien=? AND status='booking' order by id desc limit 1"
	row := session.QueryRow(query, userId)
	err = row.Scan(&visit.Id, &visit.Booknum, &visit.Fk_jadwal_praktek, &visit.Fk_history_praktek, &visit.Estimasi_tindakan)
	// err = row.Scan(&visit.Id, &visit.Code, &visit.Date, &visit.Booknum, &visit.Fk_dokter, &visit.Fk_jadwal_praktek, &visit.Fk_pasien, &visit.Total, &visit.Rating, &visit.Estimasi_tindakan, &visit.Checking_time, &visit.Status)

	switch err {
	case sql.ErrNoRows:
		message = "Data not found"
		return visit, message, err
	case nil:
		message = "Sucecess get booking detail"
		return visit, message, err
	default:
		message = "Mysql error "
		return visit, message, err
	}
}

func (b *basicBookingService) GetBookingDetailByVisitId(ctx context.Context, visitId string) (visit io.Visit, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return visit, message, err
	}
	defer session.Close()

	query := "select id, booknum, fk_jadwal_praktek, fk_history_praktek, estimasi_tindakan from visit where id=?"
	row := session.QueryRow(query, visitId)
	err = row.Scan(&visit.Id, &visit.Booknum, &visit.Fk_jadwal_praktek, &visit.Fk_history_praktek, &visit.Estimasi_tindakan)
	// err = row.Scan(&visit.Id, &visit.Code, &visit.Date, &visit.Booknum, &visit.Fk_dokter, &visit.Fk_jadwal_praktek, &visit.Fk_pasien, &visit.Total, &visit.Rating, &visit.Estimasi_tindakan, &visit.Checking_time, &visit.Status)

	switch err {
	case sql.ErrNoRows:
		message = "Data not found"
		return visit, message, err
	case nil:
		message = "Sucecess get booking detail"
		return visit, message, err
	default:
		message = "Mysql error "
		return visit, message, err
	}
}
