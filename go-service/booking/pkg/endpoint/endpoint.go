package endpoint

import (
	"context"
	service "akuklinikid_demo/go-service/booking/pkg/service"
	"akuklinikid_demo/go-service/io"

	endpoint "github.com/go-kit/kit/endpoint"
)

type BookRequest struct {
	UserId          string `json:"user_id"`
	JadwalPraktekId string `json:"jadwal_praktek_id"`
}

type BookResponse struct {
	Message string `json:"message"`
	VisitId int64 `json:"visit_id"`
	Err     error  `json:"err"`
}

func MakeBookEndpoint(s service.BookingService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(BookRequest)
		message, visit_id, err := s.Book(ctx, req.UserId, req.JadwalPraktekId)
		return BookResponse{
			Err:     err,
			VisitId: visit_id,
			Message: message,
		}, nil
	}
}

func (r BookResponse) Failed() error {
	return r.Err
}

type Failure interface {
	Failed() error
}

func (e Endpoints) Book(ctx context.Context, userId string, jadwalPraktekId string) (message string, visitId int64, err error) {
	request := BookRequest{
		JadwalPraktekId: jadwalPraktekId,
		UserId:          userId,
	}
	response, err := e.BookEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(BookResponse).Message, response.(BookResponse).VisitId, response.(BookResponse).Err
}

type GetBookingDetailRequest struct {
	UserId string `json:"user_id"`
}

type GetBookingDetailResponse struct {
	Visit   io.Visit `json:"visit"`
	Message string   `json:"message"`
	Err     error    `json:"err"`
}

func MakeGetBookingDetailEndpoint(s service.BookingService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetBookingDetailRequest)
		visit, message, err := s.GetBookingDetail(ctx, req.UserId)
		return GetBookingDetailResponse{
			Err:     err,
			Message: message,
			Visit:   visit,
		}, nil
	}
}

func (r GetBookingDetailResponse) Failed() error {
	return r.Err
}

func (e Endpoints) GetBookingDetail(ctx context.Context, userId string) (visit io.Visit, message string, err error) {
	request := GetBookingDetailRequest{UserId: userId}
	response, err := e.GetBookingDetailEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetBookingDetailResponse).Visit, response.(GetBookingDetailResponse).Message, response.(GetBookingDetailResponse).Err
}

// GetBookingDetailByVisitIdRequest collects the request parameters for the GetBookingDetailByVisitId method.
type GetBookingDetailByVisitIdRequest struct {
	VisitId string `json:"visit_id"`
}

// GetBookingDetailByVisitIdResponse collects the response parameters for the GetBookingDetailByVisitId method.
type GetBookingDetailByVisitIdResponse struct {
	Visit   io.Visit `json:"visit"`
	Message string   `json:"message"`
	Err     error    `json:"err"`
}

// MakeGetBookingDetailByVisitIdEndpoint returns an endpoint that invokes GetBookingDetailByVisitId on the service.
func MakeGetBookingDetailByVisitIdEndpoint(s service.BookingService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetBookingDetailByVisitIdRequest)
		visit, message, err := s.GetBookingDetailByVisitId(ctx, req.VisitId)
		return GetBookingDetailByVisitIdResponse{
			Err:     err,
			Message: message,
			Visit:   visit,
		}, nil
	}
}

// Failed implements Failer.
func (r GetBookingDetailByVisitIdResponse) Failed() error {
	return r.Err
}

// GetBookingDetailByVisitId implements Service. Primarily useful in a client.
func (e Endpoints) GetBookingDetailByVisitId(ctx context.Context, visitId string) (visit io.Visit, message string, err error) {
	request := GetBookingDetailByVisitIdRequest{VisitId: visitId}
	response, err := e.GetBookingDetailByVisitIdEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetBookingDetailByVisitIdResponse).Visit, response.(GetBookingDetailByVisitIdResponse).Message, response.(GetBookingDetailByVisitIdResponse).Err
}
