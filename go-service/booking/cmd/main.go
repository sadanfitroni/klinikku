package main

import service "akuklinikid_demo/go-service/booking/cmd/service"

func main() {
	service.Run()
}
