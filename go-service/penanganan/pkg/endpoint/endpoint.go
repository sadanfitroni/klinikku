package endpoint

import (
	"context"
	io "akuklinikid_demo/go-service/io"
	service "akuklinikid_demo/go-service/penanganan/pkg/service"

	endpoint "github.com/go-kit/kit/endpoint"
)

// CekTensiRequest collects the request parameters for the CekTensi method.
type CekTensiRequest struct {
	VisitId string    `json:"visit_id"`
	Medrec  io.Medrec `json:"medrec"`
}

// CekTensiResponse collects the response parameters for the CekTensi method.
type CekTensiResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeCekTensiEndpoint returns an endpoint that invokes CekTensi on the service.
func MakeCekTensiEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CekTensiRequest)
		message, err := s.CekTensi(ctx, req.VisitId, req.Medrec)
		return CekTensiResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r CekTensiResponse) Failed() error {
	return r.Err
}

// EntryTindakanRequest collects the request parameters for the EntryTindakan method.
type EntryTindakanRequest struct {
	VisitId string    `json:"visit_id"`
	Medrec  io.Medrec `json:"medrec"`
}

// EntryTindakanResponse collects the response parameters for the EntryTindakan method.
type EntryTindakanResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeEntryTindakanEndpoint returns an endpoint that invokes EntryTindakan on the service.
func MakeEntryTindakanEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(EntryTindakanRequest)
		message, err := s.EntryTindakan(ctx, req.VisitId, req.Medrec)
		return EntryTindakanResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r EntryTindakanResponse) Failed() error {
	return r.Err
}

// CheckVisitByCodeRequest collects the request parameters for the CheckVisitByCode method.
type CheckVisitByCodeRequest struct {
	VisitId string `json:"visit_id"`
}

// CheckVisitByCodeResponse collects the response parameters for the CheckVisitByCode method.
type CheckVisitByCodeResponse struct {
	Status  bool   `json:"status"`
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeCheckVisitByCodeEndpoint returns an endpoint that invokes CheckVisitByCode on the service.
func MakeCheckVisitByCodeEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CheckVisitByCodeRequest)
		status, message, err := s.CheckVisitByCode(ctx, req.VisitId)
		return CheckVisitByCodeResponse{
			Err:     err,
			Message: message,
			Status:  status,
		}, nil
	}
}

// Failed implements Failer.
func (r CheckVisitByCodeResponse) Failed() error {
	return r.Err
}

// EntryResepRequest collects the request parameters for the EntryResep method.
type EntryResepRequest struct {
	VisitId string   `json:"visit_id"`
	Resep   io.Resep `json:"resep"`
}

// EntryResepResponse collects the response parameters for the EntryResep method.
type EntryResepResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeEntryResepEndpoint returns an endpoint that invokes EntryResep on the service.
func MakeEntryResepEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(EntryResepRequest)
		message, err := s.EntryResep(ctx, req.VisitId, req.Resep)
		return EntryResepResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r EntryResepResponse) Failed() error {
	return r.Err
}

// GetPasienListRequest collects the request parameters for the GetPasienList method.
type GetPasienListRequest struct{}

// GetPasienListResponse collects the response parameters for the GetPasienList method.
type GetPasienListResponse struct {
	Visit   []io.Visit         `json:"visit"`
	Jadwal  []io.JadwalPraktek `json:"jadwal"`
	Pasien  []io.User          `json:"pasien"`
	Message string             `json:"message"`
	Err     error              `json:"err"`
}

// MakeGetPasienListEndpoint returns an endpoint that invokes GetPasienList on the service.
func MakeGetPasienListEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		visit, jadwal, pasien, message, err := s.GetPasienList(ctx)
		return GetPasienListResponse{
			Err:     err,
			Jadwal:  jadwal,
			Message: message,
			Pasien:  pasien,
			Visit:   visit,
		}, nil
	}
}

// Failed implements Failer.
func (r GetPasienListResponse) Failed() error {
	return r.Err
}

// GetPasienListDoctorRequest collects the request parameters for the GetPasienListDoctor method.
type GetPasienListDoctorRequest struct {
	DoctorId string `json:"doctor_id"`
}

// GetPasienListDoctorResponse collects the response parameters for the GetPasienListDoctor method.
type GetPasienListDoctorResponse struct {
	Visit   []io.Visit         `json:"visit"`
	Jadwal  []io.JadwalPraktek `json:"jadwal"`
	Pasien  []io.User          `json:"pasien"`
	Message string             `json:"message"`
	Err     error              `json:"err"`
}

// MakeGetPasienListDoctorEndpoint returns an endpoint that invokes GetPasienListDoctor on the service.
func MakeGetPasienListDoctorEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetPasienListDoctorRequest)
		visit, jadwal, pasien, message, err := s.GetPasienListDoctor(ctx, req.DoctorId)
		return GetPasienListDoctorResponse{
			Err:     err,
			Jadwal:  jadwal,
			Message: message,
			Pasien:  pasien,
			Visit:   visit,
		}, nil
	}
}

// Failed implements Failer.
func (r GetPasienListDoctorResponse) Failed() error {
	return r.Err
}

// GetMedrecPasienRequest collects the request parameters for the GetMedrecPasien method.
type GetMedrecPasienRequest struct {
	VisitId string `json:"visit_id"`
}

// GetMedrecPasienResponse collects the response parameters for the GetMedrecPasien method.
type GetMedrecPasienResponse struct {
	Medrec  []io.Medrec `json:"medrec"`
	Visit   []io.Visit  `json:"visit"`
	Pasien  io.User     `json:"pasien"`
	Message string      `json:"message"`
	Err     error       `json:"err"`
}

// MakeGetMedrecPasienEndpoint returns an endpoint that invokes GetMedrecPasien on the service.
func MakeGetMedrecPasienEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetMedrecPasienRequest)
		medrec, visit, pasien, message, err := s.GetMedrecPasien(ctx, req.VisitId)
		return GetMedrecPasienResponse{
			Err:     err,
			Medrec:  medrec,
			Message: message,
			Pasien:  pasien,
			Visit:   visit,
		}, nil
	}
}

// Failed implements Failer.
func (r GetMedrecPasienResponse) Failed() error {
	return r.Err
}

// GetMedrecByVisitIdRequest collects the request parameters for the GetMedrecByVisitId method.
type GetMedrecByVisitIdRequest struct {
	VisitId string `json:"visit_id"`
}

// GetMedrecByVisitIdResponse collects the response parameters for the GetMedrecByVisitId method.
type GetMedrecByVisitIdResponse struct {
	Medrec  io.Medrec `json:"medrec"`
	Message string    `json:"message"`
	Err     error     `json:"err"`
}

// MakeGetMedrecByVisitIdEndpoint returns an endpoint that invokes GetMedrecByVisitId on the service.
func MakeGetMedrecByVisitIdEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetMedrecByVisitIdRequest)
		medrec, message, err := s.GetMedrecByVisitId(ctx, req.VisitId)
		return GetMedrecByVisitIdResponse{
			Err:     err,
			Medrec:  medrec,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetMedrecByVisitIdResponse) Failed() error {
	return r.Err
}

// GetPasienListPerawatRequest collects the request parameters for the GetPasienListPerawat method.
type GetPasienListPerawatRequest struct {
	PerawatId string `json:"perawat_id"`
}

// GetPasienListPerawatResponse collects the response parameters for the GetPasienListPerawat method.
type GetPasienListPerawatResponse struct {
	Visit   []io.Visit         `json:"visit"`
	Jadwal  []io.JadwalPraktek `json:"jadwal"`
	Pasien  []io.User          `json:"pasien"`
	Message string             `json:"message"`
	Err     error              `json:"err"`
}

// MakeGetPasienListPerawatEndpoint returns an endpoint that invokes GetPasienListPerawat on the service.
func MakeGetPasienListPerawatEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetPasienListPerawatRequest)
		visit, jadwal, pasien, message, err := s.GetPasienListPerawat(ctx, req.PerawatId)
		return GetPasienListPerawatResponse{
			Err:     err,
			Jadwal:  jadwal,
			Message: message,
			Pasien:  pasien,
			Visit:   visit,
		}, nil
	}
}

// Failed implements Failer.
func (r GetPasienListPerawatResponse) Failed() error {
	return r.Err
}

// GetResepRequest collects the request parameters for the GetResep method.
type GetResepRequest struct{}

// GetResepResponse collects the response parameters for the GetResep method.
type GetResepResponse struct {
	Resep   []io.Resep `json:"resep"`
	Message string     `json:"message"`
	Err     error      `json:"err"`
}

// MakeGetResepEndpoint returns an endpoint that invokes GetResep on the service.
func MakeGetResepEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		resep, message, err := s.GetResep(ctx)
		return GetResepResponse{
			Err:     err,
			Message: message,
			Resep:   resep,
		}, nil
	}
}

// Failed implements Failer.
func (r GetResepResponse) Failed() error {
	return r.Err
}

// GetListResepByDokterPasienRequest collects the request parameters for the GetListResepByDokterPasien method.
type GetListResepByDokterPasienRequest struct {
	PasienId string `json:"pasien_id"`
	DoctorId string `json:"doctor_id"`
}

// GetListResepByDokterPasienResponse collects the response parameters for the GetListResepByDokterPasien method.
type GetListResepByDokterPasienResponse struct {
	Resep   []io.Resep `json:"resep"`
	Message string     `json:"message"`
	Err     error      `json:"err"`
}

// MakeGetListResepByDokterPasienEndpoint returns an endpoint that invokes GetListResepByDokterPasien on the service.
func MakeGetListResepByDokterPasienEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetListResepByDokterPasienRequest)
		resep, message, err := s.GetListResepByDokterPasien(ctx, req.PasienId, req.DoctorId)
		return GetListResepByDokterPasienResponse{
			Err:     err,
			Message: message,
			Resep:   resep,
		}, nil
	}
}

// Failed implements Failer.
func (r GetListResepByDokterPasienResponse) Failed() error {
	return r.Err
}

// GetListResepByPasienVisitRequest collects the request parameters for the GetListResepByPasienVisit method.
type GetListResepByPasienVisitRequest struct {
	VisitId  string `json:"visit_id"`
	DoctorId string `json:"doctor_id"`
}

// GetListResepByPasienVisitResponse collects the response parameters for the GetListResepByPasienVisit method.
type GetListResepByPasienVisitResponse struct {
	Resep   []io.Resep `json:"resep"`
	Message string     `json:"message"`
	Err     error      `json:"err"`
}

// MakeGetListResepByPasienVisitEndpoint returns an endpoint that invokes GetListResepByPasienVisit on the service.
func MakeGetListResepByPasienVisitEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetListResepByPasienVisitRequest)
		resep, message, err := s.GetListResepByPasienVisit(ctx, req.VisitId, req.DoctorId)
		return GetListResepByPasienVisitResponse{
			Err:     err,
			Message: message,
			Resep:   resep,
		}, nil
	}
}

// Failed implements Failer.
func (r GetListResepByPasienVisitResponse) Failed() error {
	return r.Err
}

// GetListResepDetailByPasienDokterRequest collects the request parameters for the GetListResepDetailByPasienDokter method.
type GetListResepDetailByPasienDokterRequest struct {
	VisitId  string `json:"visit_id"`
	DoctorId string `json:"doctor_id"`
}

// GetListResepDetailByPasienDokterResponse collects the response parameters for the GetListResepDetailByPasienDokter method.
type GetListResepDetailByPasienDokterResponse struct {
	Resep   []io.Resep `json:"resep"`
	Visit   []io.Visit `json:"visit"`
	Message string     `json:"message"`
	Err     error      `json:"err"`
}

// MakeGetListResepDetailByPasienDokterEndpoint returns an endpoint that invokes GetListResepDetailByPasienDokter on the service.
func MakeGetListResepDetailByPasienDokterEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetListResepDetailByPasienDokterRequest)
		resep, visit, message, err := s.GetListResepDetailByPasienDokter(ctx, req.VisitId, req.DoctorId)
		return GetListResepDetailByPasienDokterResponse{
			Err:     err,
			Message: message,
			Resep:   resep,
			Visit:   visit,
		}, nil
	}
}

// Failed implements Failer.
func (r GetListResepDetailByPasienDokterResponse) Failed() error {
	return r.Err
}

// GetHistoryPraktekByKlinikRequest collects the request parameters for the GetHistoryPraktekByKlinik method.
type GetHistoryPraktekByKlinikRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetHistoryPraktekByKlinikResponse collects the response parameters for the GetHistoryPraktekByKlinik method.
type GetHistoryPraktekByKlinikResponse struct {
	HistoryPraktek []io.HistoryPraktek `json:"history_praktek"`
	Jadwal         []io.JadwalPraktek  `json:"jadwal"`
	Message        string              `json:"message"`
	Err            error               `json:"err"`
}

// MakeGetHistoryPraktekByKlinikEndpoint returns an endpoint that invokes GetHistoryPraktekByKlinik on the service.
func MakeGetHistoryPraktekByKlinikEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetHistoryPraktekByKlinikRequest)
		history_praktek, jadwal, message, err := s.GetHistoryPraktekByKlinik(ctx, req.KlinikId)
		return GetHistoryPraktekByKlinikResponse{
			Err:            err,
			HistoryPraktek: history_praktek,
			Jadwal:         jadwal,
			Message:        message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetHistoryPraktekByKlinikResponse) Failed() error {
	return r.Err
}

// GetDetailHistoryPraktekByKlinikRequest collects the request parameters for the GetDetailHistoryPraktekByKlinik method.
type GetDetailHistoryPraktekByKlinikRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetDetailHistoryPraktekByKlinikResponse collects the response parameters for the GetDetailHistoryPraktekByKlinik method.
type GetDetailHistoryPraktekByKlinikResponse struct {
	HistoryPraktek []io.HistoryPraktek `json:"history_praktek"`
	Jadwal         []io.JadwalPraktek  `json:"jadwal"`
	Dokter         []io.User           `json:"dokter"`
	Message        string              `json:"message"`
	Err            error               `json:"err"`
}

// MakeGetDetailHistoryPraktekByKlinikEndpoint returns an endpoint that invokes GetDetailHistoryPraktekByKlinik on the service.
func MakeGetDetailHistoryPraktekByKlinikEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetDetailHistoryPraktekByKlinikRequest)
		history_praktek, jadwal, dokter, message, err := s.GetDetailHistoryPraktekByKlinik(ctx, req.KlinikId)
		return GetDetailHistoryPraktekByKlinikResponse{
			Dokter:         dokter,
			Err:            err,
			HistoryPraktek: history_praktek,
			Jadwal:         jadwal,
			Message:        message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetDetailHistoryPraktekByKlinikResponse) Failed() error {
	return r.Err
}

// GetHistoryPraktekByIdRequest collects the request parameters for the GetHistoryPraktekById method.
type GetHistoryPraktekByIdRequest struct {
	HistoryJadwalId string `json:"history_jadwal_id"`
}

// GetHistoryPraktekByIdResponse collects the response parameters for the GetHistoryPraktekById method.
type GetHistoryPraktekByIdResponse struct {
	Jadwal  io.HistoryPraktek `json:"jadwal"`
	Message string            `json:"message"`
	Err     error             `json:"err"`
}

// MakeGetHistoryPraktekByIdEndpoint returns an endpoint that invokes GetHistoryPraktekById on the service.
func MakeGetHistoryPraktekByIdEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetHistoryPraktekByIdRequest)
		jadwal, message, err := s.GetHistoryPraktekById(ctx, req.HistoryJadwalId)
		return GetHistoryPraktekByIdResponse{
			Err:     err,
			Jadwal:  jadwal,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetHistoryPraktekByIdResponse) Failed() error {
	return r.Err
}

// GetDokterByKlinikRequest collects the request parameters for the GetDokterByKlinik method.
type GetDokterByKlinikRequest struct {
	KlinikId string `json:"klinik_id"`
}

// GetDokterByKlinikResponse collects the response parameters for the GetDokterByKlinik method.
type GetDokterByKlinikResponse struct {
	Dokter  []io.User `json:"dokter"`
	Message string    `json:"message"`
	Err     error     `json:"err"`
}

// MakeGetDokterByKlinikEndpoint returns an endpoint that invokes GetDokterByKlinik on the service.
func MakeGetDokterByKlinikEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetDokterByKlinikRequest)
		dokter, message, err := s.GetDokterByKlinik(ctx, req.KlinikId)
		return GetDokterByKlinikResponse{
			Dokter:  dokter,
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r GetDokterByKlinikResponse) Failed() error {
	return r.Err
}

// UpdateHistoryPraktekRequest collects the request parameters for the UpdateHistoryPraktek method.
type UpdateHistoryPraktekRequest struct {
	Jadwal io.HistoryPraktek `json:"jadwal"`
}

// UpdateHistoryPraktekResponse collects the response parameters for the UpdateHistoryPraktek method.
type UpdateHistoryPraktekResponse struct {
	Message string `json:"message"`
	Err     error  `json:"err"`
}

// MakeUpdateHistoryPraktekEndpoint returns an endpoint that invokes UpdateHistoryPraktek on the service.
func MakeUpdateHistoryPraktekEndpoint(s service.PenangananService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(UpdateHistoryPraktekRequest)
		message, err := s.UpdateHistoryPraktek(ctx, req.Jadwal)
		return UpdateHistoryPraktekResponse{
			Err:     err,
			Message: message,
		}, nil
	}
}

// Failed implements Failer.
func (r UpdateHistoryPraktekResponse) Failed() error {
	return r.Err
}

// Failure is an interface that should be implemented by response types.
// Response encoders can check if responses are Failer, and if so they've
// failed, and if so encode them using a separate write path based on the error.
type Failure interface {
	Failed() error
}

// CekTensi implements Service. Primarily useful in a client.
func (e Endpoints) CekTensi(ctx context.Context, visitId string, medrec io.Medrec) (message string, err error) {
	request := CekTensiRequest{
		Medrec:  medrec,
		VisitId: visitId,
	}
	response, err := e.CekTensiEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(CekTensiResponse).Message, response.(CekTensiResponse).Err
}

// EntryTindakan implements Service. Primarily useful in a client.
func (e Endpoints) EntryTindakan(ctx context.Context, visitId string, medrec io.Medrec) (message string, err error) {
	request := EntryTindakanRequest{
		Medrec:  medrec,
		VisitId: visitId,
	}
	response, err := e.EntryTindakanEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(EntryTindakanResponse).Message, response.(EntryTindakanResponse).Err
}

// CheckVisitByCode implements Service. Primarily useful in a client.
func (e Endpoints) CheckVisitByCode(ctx context.Context, visitId string) (status bool, message string, err error) {
	request := CheckVisitByCodeRequest{VisitId: visitId}
	response, err := e.CheckVisitByCodeEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(CheckVisitByCodeResponse).Status, response.(CheckVisitByCodeResponse).Message, response.(CheckVisitByCodeResponse).Err
}

// EntryResep implements Service. Primarily useful in a client.
func (e Endpoints) EntryResep(ctx context.Context, visitId string, resep io.Resep) (message string, err error) {
	request := EntryResepRequest{
		Resep:   resep,
		VisitId: visitId,
	}
	response, err := e.EntryResepEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(EntryResepResponse).Message, response.(EntryResepResponse).Err
}

// GetPasienList implements Service. Primarily useful in a client.
func (e Endpoints) GetPasienList(ctx context.Context) (visit []io.Visit, jadwal []io.JadwalPraktek, pasien []io.User, message string, err error) {
	request := GetPasienListRequest{}
	response, err := e.GetPasienListEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetPasienListResponse).Visit, response.(GetPasienListResponse).Jadwal, response.(GetPasienListResponse).Pasien, response.(GetPasienListResponse).Message, response.(GetPasienListResponse).Err
}

// GetPasienListDoctor implements Service. Primarily useful in a client.
func (e Endpoints) GetPasienListDoctor(ctx context.Context, doctorId string) (visit []io.Visit, jadwal []io.JadwalPraktek, pasien []io.User, message string, err error) {
	request := GetPasienListDoctorRequest{DoctorId: doctorId}
	response, err := e.GetPasienListDoctorEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetPasienListDoctorResponse).Visit, response.(GetPasienListDoctorResponse).Jadwal, response.(GetPasienListDoctorResponse).Pasien, response.(GetPasienListDoctorResponse).Message, response.(GetPasienListDoctorResponse).Err
}

// GetMedrecPasien implements Service. Primarily useful in a client.
func (e Endpoints) GetMedrecPasien(ctx context.Context, visitId string) (medrec []io.Medrec, visit []io.Visit, pasien io.User, message string, err error) {
	request := GetMedrecPasienRequest{VisitId: visitId}
	response, err := e.GetMedrecPasienEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetMedrecPasienResponse).Medrec, response.(GetMedrecPasienResponse).Visit, response.(GetMedrecPasienResponse).Pasien, response.(GetMedrecPasienResponse).Message, response.(GetMedrecPasienResponse).Err
}

// GetMedrecByVisitId implements Service. Primarily useful in a client.
func (e Endpoints) GetMedrecByVisitId(ctx context.Context, visitId string) (medrec io.Medrec, message string, err error) {
	request := GetMedrecByVisitIdRequest{VisitId: visitId}
	response, err := e.GetMedrecByVisitIdEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetMedrecByVisitIdResponse).Medrec, response.(GetMedrecByVisitIdResponse).Message, response.(GetMedrecByVisitIdResponse).Err
}

// GetPasienListPerawat implements Service. Primarily useful in a client.
func (e Endpoints) GetPasienListPerawat(ctx context.Context, perawatId string) (visit []io.Visit, jadwal []io.JadwalPraktek, pasien []io.User, message string, err error) {
	request := GetPasienListPerawatRequest{PerawatId: perawatId}
	response, err := e.GetPasienListPerawatEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetPasienListPerawatResponse).Visit, response.(GetPasienListPerawatResponse).Jadwal, response.(GetPasienListPerawatResponse).Pasien, response.(GetPasienListPerawatResponse).Message, response.(GetPasienListPerawatResponse).Err
}

// GetResep implements Service. Primarily useful in a client.
func (e Endpoints) GetResep(ctx context.Context) (resep []io.Resep, message string, err error) {
	request := GetResepRequest{}
	response, err := e.GetResepEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetResepResponse).Resep, response.(GetResepResponse).Message, response.(GetResepResponse).Err
}

// GetListResepByDokterPasien implements Service. Primarily useful in a client.
func (e Endpoints) GetListResepByDokterPasien(ctx context.Context, pasienId string, doctorId string) (resep []io.Resep, message string, err error) {
	request := GetListResepByDokterPasienRequest{
		DoctorId: doctorId,
		PasienId: pasienId,
	}
	response, err := e.GetListResepByDokterPasienEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetListResepByDokterPasienResponse).Resep, response.(GetListResepByDokterPasienResponse).Message, response.(GetListResepByDokterPasienResponse).Err
}

// GetListResepByPasienVisit implements Service. Primarily useful in a client.
func (e Endpoints) GetListResepByPasienVisit(ctx context.Context, visitId string, doctorId string) (resep []io.Resep, message string, err error) {
	request := GetListResepByPasienVisitRequest{
		DoctorId: doctorId,
		VisitId:  visitId,
	}
	response, err := e.GetListResepByPasienVisitEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetListResepByPasienVisitResponse).Resep, response.(GetListResepByPasienVisitResponse).Message, response.(GetListResepByPasienVisitResponse).Err
}

// GetListResepDetailByPasienDokter implements Service. Primarily useful in a client.
func (e Endpoints) GetListResepDetailByPasienDokter(ctx context.Context, visitId string, doctorId string) (resep []io.Resep, visit []io.Visit, message string, err error) {
	request := GetListResepDetailByPasienDokterRequest{
		DoctorId: doctorId,
		VisitId:  visitId,
	}
	response, err := e.GetListResepDetailByPasienDokterEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetListResepDetailByPasienDokterResponse).Resep, response.(GetListResepDetailByPasienDokterResponse).Visit, response.(GetListResepDetailByPasienDokterResponse).Message, response.(GetListResepDetailByPasienDokterResponse).Err
}

// GetHistoryPraktekByKlinik implements Service. Primarily useful in a client.
func (e Endpoints) GetHistoryPraktekByKlinik(ctx context.Context, klinikId string) (history_praktek []io.HistoryPraktek, jadwal []io.JadwalPraktek, message string, err error) {
	request := GetHistoryPraktekByKlinikRequest{KlinikId: klinikId}
	response, err := e.GetHistoryPraktekByKlinikEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetHistoryPraktekByKlinikResponse).HistoryPraktek, response.(GetHistoryPraktekByKlinikResponse).Jadwal, response.(GetHistoryPraktekByKlinikResponse).Message, response.(GetHistoryPraktekByKlinikResponse).Err
}

// GetDetailHistoryPraktekByKlinik implements Service. Primarily useful in a client.
func (e Endpoints) GetDetailHistoryPraktekByKlinik(ctx context.Context, klinikId string) (history_praktek []io.HistoryPraktek, jadwal []io.JadwalPraktek, dokter []io.User, message string, err error) {
	request := GetDetailHistoryPraktekByKlinikRequest{KlinikId: klinikId}
	response, err := e.GetDetailHistoryPraktekByKlinikEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetDetailHistoryPraktekByKlinikResponse).HistoryPraktek, response.(GetDetailHistoryPraktekByKlinikResponse).Jadwal, response.(GetDetailHistoryPraktekByKlinikResponse).Dokter, response.(GetDetailHistoryPraktekByKlinikResponse).Message, response.(GetDetailHistoryPraktekByKlinikResponse).Err
}

// GetHistoryPraktekById implements Service. Primarily useful in a client.
func (e Endpoints) GetHistoryPraktekById(ctx context.Context, historyJadwalId string) (jadwal io.HistoryPraktek, message string, err error) {
	request := GetHistoryPraktekByIdRequest{HistoryJadwalId: historyJadwalId}
	response, err := e.GetHistoryPraktekByIdEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetHistoryPraktekByIdResponse).Jadwal, response.(GetHistoryPraktekByIdResponse).Message, response.(GetHistoryPraktekByIdResponse).Err
}

// GetDokterByKlinik implements Service. Primarily useful in a client.
func (e Endpoints) GetDokterByKlinik(ctx context.Context, klinikId string) (dokter []io.User, message string, err error) {
	request := GetDokterByKlinikRequest{KlinikId: klinikId}
	response, err := e.GetDokterByKlinikEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(GetDokterByKlinikResponse).Dokter, response.(GetDokterByKlinikResponse).Message, response.(GetDokterByKlinikResponse).Err
}

// UpdateHistoryPraktek implements Service. Primarily useful in a client.
func (e Endpoints) UpdateHistoryPraktek(ctx context.Context, jadwal io.HistoryPraktek) (message string, err error) {
	request := UpdateHistoryPraktekRequest{Jadwal: jadwal}
	response, err := e.UpdateHistoryPraktekEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(UpdateHistoryPraktekResponse).Message, response.(UpdateHistoryPraktekResponse).Err
}
