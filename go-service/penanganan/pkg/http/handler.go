package http

import (
	"context"
	"encoding/json"
	"errors"
	endpoint "akuklinikid_demo/go-service/penanganan/pkg/endpoint"
	http1 "net/http"

	http "github.com/go-kit/kit/transport/http"
	handlers "github.com/gorilla/handlers"
	mux "github.com/gorilla/mux"
)

// makeCekTensiHandler creates the handler logic
func makeCekTensiHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/cek-tensi").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.CekTensiEndpoint, decodeCekTensiRequest, encodeCekTensiResponse, options...)))
}

// decodeCekTensiRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeCekTensiRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.CekTensiRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeCekTensiResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeCekTensiResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeEntryTindakanHandler creates the handler logic
func makeEntryTindakanHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/entry-tindakan").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.EntryTindakanEndpoint, decodeEntryTindakanRequest, encodeEntryTindakanResponse, options...)))
}

// decodeEntryTindakanRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeEntryTindakanRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.EntryTindakanRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeEntryTindakanResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeEntryTindakanResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}
func ErrorEncoder(_ context.Context, err error, w http1.ResponseWriter) {
	w.WriteHeader(err2code(err))
	json.NewEncoder(w).Encode(errorWrapper{Error: err.Error()})
}
func ErrorDecoder(r *http1.Response) error {
	var w errorWrapper
	if err := json.NewDecoder(r.Body).Decode(&w); err != nil {
		return err
	}
	return errors.New(w.Error)
}

// This is used to set the http status, see an example here :
// https://github.com/go-kit/kit/blob/master/examples/addsvc/pkg/addtransport/http.go#L133
func err2code(err error) int {
	return http1.StatusInternalServerError
}

type errorWrapper struct {
	Error string `json:"error"`
}

// makeCheckVisitByCodeHandler creates the handler logic
func makeCheckVisitByCodeHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/check-visit-by-code").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.CheckVisitByCodeEndpoint, decodeCheckVisitByCodeRequest, encodeCheckVisitByCodeResponse, options...)))
}

// decodeCheckVisitByCodeRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeCheckVisitByCodeRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.CheckVisitByCodeRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeCheckVisitByCodeResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeCheckVisitByCodeResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// m.Methods("POST","OPTIONS").Path("/entry-resep").Handler(
// makeEntryResepHandler creates the handler logic
func makeEntryResepHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/entry-resep").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.EntryResepEndpoint, decodeEntryResepRequest, encodeEntryResepResponse, options...)))
}

// decodeEntryResepRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeEntryResepRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.EntryResepRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeEntryResepResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeEntryResepResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetPasienListHandler creates the handler logic
func makeGetPasienListHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("GET").Path("/get-pasien-list").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"GET"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetPasienListEndpoint, decodeGetPasienListRequest, encodeGetPasienListResponse, options...)))
}

// decodeGetPasienListRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetPasienListRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetPasienListRequest{}
	// err := json.NewDecoder(r.Body).Decode(&req)
	return req, nil
}

// encodeGetPasienListResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetPasienListResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetPasienListDoctorHandler creates the handler logic
func makeGetPasienListDoctorHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-pasien-list-doctor").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetPasienListDoctorEndpoint, decodeGetPasienListDoctorRequest, encodeGetPasienListDoctorResponse, options...)))
}

// decodeGetPasienListDoctorRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetPasienListDoctorRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetPasienListDoctorRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetPasienListDoctorResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetPasienListDoctorResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetMedrecPasienHandler creates the handler logic
func makeGetMedrecPasienHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-medrec-pasien").Handler(
		handlers.CORS(handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetMedrecPasienEndpoint, decodeGetMedrecPasienRequest, encodeGetMedrecPasienResponse, options...)))
}

// decodeGetMedrecPasienRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetMedrecPasienRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetMedrecPasienRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetMedrecPasienResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetMedrecPasienResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetMedrecByVisitIdHandler creates the handler logic
func makeGetMedrecByVisitIdHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-medrec-by-visit-id").Handler(
		handlers.CORS(handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetMedrecByVisitIdEndpoint, decodeGetMedrecByVisitIdRequest, encodeGetMedrecByVisitIdResponse, options...)))
}

// decodeGetMedrecByVisitIdRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetMedrecByVisitIdRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetMedrecByVisitIdRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetMedrecByVisitIdResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetMedrecByVisitIdResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetPasienListPerawatHandler creates the handler logic
func makeGetPasienListPerawatHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-pasien-list-perawat").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetPasienListPerawatEndpoint, decodeGetPasienListPerawatRequest, encodeGetPasienListPerawatResponse, options...)))
}

// decodeGetPasienListPerawatRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetPasienListPerawatRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetPasienListPerawatRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetPasienListPerawatResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetPasienListPerawatResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetResepHandler creates the handler logic
func makeGetResepHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("GET").Path("/get-resep").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"GET"}),
			// handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetResepEndpoint, decodeGetResepRequest, encodeGetResepResponse, options...)))
}

// decodeGetResepRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetResepRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetResepRequest{}
	// err := json.NewDecoder(r.Body).Decode(&req)
	return req, nil
}

// encodeGetResepResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetResepResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetListResepByDokterPasienHandler creates the handler logic
func makeGetListResepByDokterPasienHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-list-resep-by-dokter-pasien").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetListResepByDokterPasienEndpoint, decodeGetListResepByDokterPasienRequest, encodeGetListResepByDokterPasienResponse, options...)))
}

// decodeGetListResepByDokterPasienRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetListResepByDokterPasienRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetListResepByDokterPasienRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetListResepByDokterPasienResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetListResepByDokterPasienResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetListResepByPasienVisitHandler creates the handler logic
func makeGetListResepByPasienVisitHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-list-resep-by-pasien-visit").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetListResepByPasienVisitEndpoint, decodeGetListResepByPasienVisitRequest, encodeGetListResepByPasienVisitResponse, options...)))
}

// decodeGetListResepByPasienVisitRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetListResepByPasienVisitRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetListResepByPasienVisitRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetListResepByPasienVisitResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetListResepByPasienVisitResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetListResepDetailByPasienDokterHandler creates the handler logic
func makeGetListResepDetailByPasienDokterHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-list-resep-detail-by-pasien-dokter").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetListResepDetailByPasienDokterEndpoint, decodeGetListResepDetailByPasienDokterRequest, encodeGetListResepDetailByPasienDokterResponse, options...)))
}

// decodeGetListResepDetailByPasienDokterRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetListResepDetailByPasienDokterRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetListResepDetailByPasienDokterRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetListResepDetailByPasienDokterResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetListResepDetailByPasienDokterResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetHistoryPraktekByKlinikHandler creates the handler logic
func makeGetHistoryPraktekByKlinikHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/get-history-praktek-by-klinik").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.GetHistoryPraktekByKlinikEndpoint, decodeGetHistoryPraktekByKlinikRequest, encodeGetHistoryPraktekByKlinikResponse, options...)))
}

// decodeGetHistoryPraktekByKlinikRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetHistoryPraktekByKlinikRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetHistoryPraktekByKlinikRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetHistoryPraktekByKlinikResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetHistoryPraktekByKlinikResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetDetailHistoryPraktekByKlinikHandler creates the handler logic
func makeGetDetailHistoryPraktekByKlinikHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-detail-history-praktek-by-klinik").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetDetailHistoryPraktekByKlinikEndpoint, decodeGetDetailHistoryPraktekByKlinikRequest, encodeGetDetailHistoryPraktekByKlinikResponse, options...)))
}

// decodeGetDetailHistoryPraktekByKlinikRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetDetailHistoryPraktekByKlinikRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetDetailHistoryPraktekByKlinikRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetDetailHistoryPraktekByKlinikResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetDetailHistoryPraktekByKlinikResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetHistoryPraktekByIdHandler creates the handler logic
func makeGetHistoryPraktekByIdHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-history-praktek-by-id").Handler(
		handlers.CORS(handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetHistoryPraktekByIdEndpoint, decodeGetHistoryPraktekByIdRequest, encodeGetHistoryPraktekByIdResponse, options...)))
}

// decodeGetHistoryPraktekByIdRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetHistoryPraktekByIdRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetHistoryPraktekByIdRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetHistoryPraktekByIdResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetHistoryPraktekByIdResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeGetDokterByKlinikHandler creates the handler logic
func makeGetDokterByKlinikHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/get-dokter-by-klinik").Handler(
		handlers.CORS(handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.GetDokterByKlinikEndpoint, decodeGetDokterByKlinikRequest, encodeGetDokterByKlinikResponse, options...)))
}

// decodeGetDokterByKlinikRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeGetDokterByKlinikRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.GetDokterByKlinikRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeGetDokterByKlinikResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeGetDokterByKlinikResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}

// makeUpdateHistoryPraktekHandler creates the handler logic
func makeUpdateHistoryPraktekHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST", "OPTIONS").Path("/update-history-praktek").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
		)(http.NewServer(endpoints.UpdateHistoryPraktekEndpoint, decodeUpdateHistoryPraktekRequest, encodeUpdateHistoryPraktekResponse, options...)))
}

// decodeUpdateHistoryPraktekRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
func decodeUpdateHistoryPraktekRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.UpdateHistoryPraktekRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// encodeUpdateHistoryPraktekResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeUpdateHistoryPraktekResponse(ctx context.Context, w http1.ResponseWriter, response interface{}) (err error) {
	if f, ok := response.(endpoint.Failure); ok && f.Failed() != nil {
		ErrorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(response)
	return
}
