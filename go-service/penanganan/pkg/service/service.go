package service

import (
	"context"
	"database/sql"
	"fmt"
	"akuklinikid_demo/go-service/db"
	"akuklinikid_demo/go-service/io"
	"strconv"
	"time"
)

// VisitServiceService describes the service.
type PenangananService interface {
	// Add your methods here
	// e.x: Foo(ctx context.Context,s string)(rs string, err error)
	CekTensi(ctx context.Context, visitId string, medrec io.Medrec) (message string, err error)
	// melakaukan pencatantan di medrec berdasarkan visit oleh perawat
	EntryTindakan(ctx context.Context, visitId string, medrec io.Medrec) (message string, err error)

	CheckVisitByCode(ctx context.Context, visitId string) (status bool, message string, err error)

	EntryResep(ctx context.Context, visitId string, resep io.Resep) (message string, err error)

	GetPasienList(ctx context.Context) (visit []io.Visit, jadwal []io.JadwalPraktek, pasien []io.User, message string, err error)

	GetPasienListDoctor(ctx context.Context, doctorId string) (visit []io.Visit, jadwal []io.JadwalPraktek, pasien []io.User, message string, err error)

	GetMedrecPasien(ctx context.Context, visitId string) (medrec []io.Medrec, visit []io.Visit, pasien io.User, message string, err error)

	GetMedrecByVisitId(ctx context.Context, visitId string) (medrec io.Medrec, message string, err error)

	GetPasienListPerawat(ctx context.Context, perawatId string) (visit []io.Visit, jadwal []io.JadwalPraktek, pasien []io.User, message string, err error)

	GetResep(ctx context.Context) (resep []io.Resep, message string, err error)

	GetListResepByDokterPasien(ctx context.Context, pasienId string, doctorId string) (resep []io.Resep, message string, err error)

	GetListResepByPasienVisit(ctx context.Context, visitId string, doctorId string) (resep []io.Resep, message string, err error)

	GetListResepDetailByPasienDokter(ctx context.Context, visitId string, doctorId string) (resep []io.Resep, visit []io.Visit, message string, err error)

	GetHistoryPraktekByKlinik(ctx context.Context, klinikId string) (history_praktek []io.HistoryPraktek, jadwal []io.JadwalPraktek, message string, err error)

	GetDetailHistoryPraktekByKlinik(ctx context.Context, klinikId string) (history_praktek []io.HistoryPraktek, jadwal []io.JadwalPraktek, dokter []io.User, message string, err error)

	GetHistoryPraktekById(ctx context.Context, historyJadwalId string) (jadwal io.HistoryPraktek, message string, err error)

	GetDokterByKlinik(ctx context.Context, klinikId string) (dokter []io.User, message string, err error)

	UpdateHistoryPraktek(ctx context.Context, jadwal io.HistoryPraktek) (message string, err error)
}

type basicPenangananService struct{}

func (b *basicPenangananService) CekTensi(ctx context.Context, visitId string, medrec io.Medrec) (message string, err error) {
	// TODO implement the business logic of CekTensi
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return message, err
	}
	defer session.Close()
	var pasien_id int
	var jadwal_praktek_id int
	var fk_dokter int
	var fk_klinik int
	var count int
	fmt.Println("visit id ", visitId, "\n\n", medrec)
	row := session.QueryRow("select fk_pasien,jadwal_praktek.id,visit.fk_dokter, "+
		"jadwal_praktek.fk_klinik from visit,jadwal_praktek where visit.id=? and fk_jadwal_praktek=jadwal_praktek.id", visitId)
	err = row.Scan(&pasien_id, &jadwal_praktek_id, &fk_dokter, &fk_klinik)
	if err != nil {
		message = "Data visit not found"
		return message, err
	}

	var pasien_code string
	var dokter_code string
	row = session.QueryRow("select code from user where id=?",pasien_id)
	err = row.Scan(&pasien_code)
	row = session.QueryRow("select code from user where id=?",fk_dokter)
	err = row.Scan(&dokter_code)

	row = session.QueryRow("SELECT COUNT(id) FROM medrec WHERE fk_visit =?", visitId)
	err = row.Scan(&count)
	if err != nil {
		message = "Data query count error"
		return message, err
	}

	if count == 0 {
		query, err := session.Prepare("insert into medrec set tensi=?, berat_badan=?, fk_pasien=?,keluhan=?, fk_visit=?, fk_dokter=?, fk_klinik=?, code=?")
		if err != nil {
			message = "Query error"
			return message, err
		}
		_, err = query.Exec(medrec.Tensi, medrec.Berat_badan, pasien_id, medrec.Keluhan, visitId, fk_dokter, fk_klinik, "k"+strconv.Itoa(fk_klinik)+dokter_code+pasien_code)
		if err != nil {
			message = "Failed insert into medrec"
			return message, err
		}
	} else {
		query, err := session.Prepare("update medrec set tensi=?, berat_badan=?, fk_pasien=?,keluhan=? where fk_visit = ?")
		if err != nil {
			message = "Query error"
			return message, err
		}
		_, err = query.Exec(medrec.Tensi, medrec.Berat_badan, pasien_id, medrec.Keluhan, visitId)
		if err != nil {
			message = "Failed update into medrec"
			return message, err
		}
	}

	query, err := session.Prepare("Update visit set status = 'Sudah Cek Tensi' where id = ?")
	if err != nil {
		message = "Query error"
		return message, err
	}
	_, err = query.Exec(visitId)
	if err != nil {
		message = "Failed update status visit"
		return message, err
	}
	// SELECT (COUNT(id) > 0) AS status  FROM history_praktek

	message = "Cek tensi Success"
	return message, err
}

func (b *basicPenangananService) EntryTindakan(ctx context.Context, visitId string, medrec io.Medrec) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return message, err
	}
	defer session.Close()
	var visit_id int
	var klinik_id int
	row := session.QueryRow("select visit.id,visit.fk_klinik from visit,jadwal_praktek where fk_jadwal_praktek = jadwal_praktek.id and visit.id=?", visitId)
	err = row.Scan(&visit_id, &klinik_id)
	if err != nil {
		message = "Data visit not found"
		return message, err
	}

	query, err := session.Prepare("update medrec set tensi=?,berat_badan=?," +
		"gejala=?,indikasi=?," +
		"diagnosa=?,fk_dokter=?," +
		"flag_bayar=?, flag_konsul=?, flag_pemeriksaan=?, flag_resep=?, flag_inap=?, flag_operasi=?," +
		"fk_klinik=?,fk_perawat_isi_tindakan=? where fk_visit=?")
	if err != nil {
		message = "Query error"
		return message, err
	}
	_, err = query.Exec(
		medrec.Tensi,
		medrec.Berat_badan,
		medrec.Gejala,
		medrec.Indikasi,
		medrec.Diagnosa,
		medrec.Fk_dokter,
		medrec.Flag_bayar,
		medrec.Flag_konsul,
		medrec.Flag_pemeriksaan,
		medrec.Flag_resep,
		medrec.Flag_inap,
		medrec.Flag_operasi,
		klinik_id,
		medrec.Fk_perawat_isi_tindakan,
		visit_id)
	if err != nil {
		message = "Failed update into medrec"
		return message, err
	}

	query, err = session.Prepare("Update visit set status = 'Sudah Entry Tindakan' where id = ?")
	if err != nil {
		message = "Query error"
		return message, err
	}
	_, err = query.Exec(visitId)
	if err != nil {
		message = "Failed update status visit"
		return message, err
	}

	message = "Entry tindakan Success"
	return message, err
}

func (b *basicPenangananService) CheckVisitByCode(ctx context.Context, visitId string) (status bool, message string, err error) {
	// TODO implement the business logic of CheckVisitByCode
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		status = false
		return status, message, err
	}
	defer session.Close()
	var visit_id int
	row := session.QueryRow("select id from visit where code=?", visitId)
	err = row.Scan(&visit_id)

	switch err {
	case sql.ErrNoRows:
		message = "Data visit not found"
		status = false
		return status, message, err
	case nil:
		status = true
		return status, message, err
	default:
		message = "Find Visit Error"
		status = false
		return status, message, err
	}
}

func (b *basicPenangananService) EntryResep(ctx context.Context, visitId string, resep io.Resep) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return message, err
	}
	defer session.Close()
	var visit_id int
	// var klinik_id int
	row := session.QueryRow("select visit.id from visit,jadwal_praktek where fk_jadwal_praktek = jadwal_praktek.id and visit.id=?", visitId)
	err = row.Scan(&visit_id)
	if err != nil {
		message = "Data visit not found"
		return message, err
	}

	query, err := session.Prepare("insert into resep set fk_visit=?,obat=?,dosis=?,quantity=?")
	if err != nil {
		message = "Query error"
		return message, err
	}
	_, err = query.Exec(visit_id, resep.Obat, resep.Dosis, resep.Quantity)
	if err != nil {
		message = "Failed insert into resep"
		return message, err
	}
	message = "Entry resep Success"
	return message, err
}

func (b *basicPenangananService) GetPasienList(ctx context.Context) (visit []io.Visit, jadwal []io.JadwalPraktek, pasien []io.User, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return visit, jadwal, pasien, message, err
	}
	defer session.Close()

	result, err := session.Query("" +
		" SELECT visit.id, visit.code,visit.status, visit.booknum, user.fullname,jadwal_praktek.poli_name,jadwal_praktek.poli_type  FROM visit,jadwal_praktek,user " +
		" WHERE " +
		" visit.fk_pasien = user.id AND " +
		" visit.fk_jadwal_praktek = jadwal_praktek.id AND " +
		" DATE(visit.DATE) = DATE(NOW()) ")

	if err != nil {
		message = "Failed get data "
		return visit, jadwal, pasien, message, err
	}
	visitData := io.Visit{}
	JadwalPraktekData := io.JadwalPraktek{}
	pasienData := io.User{}

	for result.Next() {
		err := result.Scan(&visitData.Id, &visitData.Code, &visitData.Status, &visitData.Booknum,
			&pasienData.Fullname, &JadwalPraktekData.Poli_name, &JadwalPraktekData.Poli_type)

		if err != nil {
			message = "Failed copy data "
			return visit, jadwal, pasien, message, err
		}
		visit = append(visit, visitData)
		jadwal = append(jadwal, JadwalPraktekData)
		pasien = append(pasien, pasienData)
	}
	message = "Get pasien list success"
	return visit, jadwal, pasien, message, err
}

func (b *basicPenangananService) GetPasienListDoctor(ctx context.Context, doctorId string) (visit []io.Visit, jadwal []io.JadwalPraktek, pasien []io.User, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return visit, jadwal, pasien, message, err
	}
	defer session.Close()

	// result, err := session.Query(""+
	// 	" SELECT visit.id, visit.code, visit.booknum,visit.status," +
	// 	" user.fullname,jadwal_praktek.poli_name,jadwal_praktek.poli_type  FROM visit,jadwal_praktek,user "+
	// 	" WHERE "+
	// 	" visit.fk_pasien = user.id AND "+
	// 	" visit.fk_jadwal_praktek = jadwal_praktek.id AND "+
	// 	" DATE(visit.DATE) = DATE(NOW()) AND "+
	// 	// " visit.status = 'Sudah Cek Tensi' AND "+
	// 	" visit.fk_dokter = ?", doctorId)

	result, err := session.Query(""+
		"SELECT 	visit.id, visit.code, visit.booknum,visit.status,		"+
		"user.fullname,jadwal_praktek.poli_name,jadwal_praktek.Poli_type 	"+
		"from history_praktek,jadwal_praktek,visit,user 					"+
		"	WHERE 															"+
		"		visit.fk_jadwal_praktek = jadwal_praktek.id   				"+
		"		AND visit.fk_pasien = user.id 								"+
		"		AND history_praktek.fk_jadwal_praktek = jadwal_praktek.id 	"+
		"		AND TIME(NOW()) >= TIME(open_time)  						"+
		"		AND TIME(NOW()) <= TIME(close_time) 						"+
		"		AND DATE(visit.DATE) = DATE(NOW()) 							"+
		"	AND history_praktek.fk_dokter = ?", doctorId)

	if err != nil {
		message = "Failed get data "
		return visit, jadwal, pasien, message, err
	}
	visitData := io.Visit{}
	JadwalPraktekData := io.JadwalPraktek{}
	pasienData := io.User{}

	for result.Next() {
		err := result.Scan(
			&visitData.Id,
			&visitData.Code,
			&visitData.Booknum,
			&visitData.Status,
			&pasienData.Fullname,
			&JadwalPraktekData.Poli_name,
			&JadwalPraktekData.Poli_type)

		if err != nil {
			message = "Failed copy data "
			return visit, jadwal, pasien, message, err
		}
		visit = append(visit, visitData)
		jadwal = append(jadwal, JadwalPraktekData)
		pasien = append(pasien, pasienData)
	}

	message = "Get pasien list docktor success"
	return visit, jadwal, pasien, message, err
}

func (b *basicPenangananService) GetMedrecPasien(ctx context.Context, visitId string) (medrec []io.Medrec, visit []io.Visit, pasien io.User, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return medrec, visit, pasien, message, err
	}
	defer session.Close()
	var fk_dokter int
	row := session.QueryRow("select user.fullname,user.id, visit.fk_dokter from visit,user where visit.id=? and visit.fk_pasien = user.id", visitId)
	err = row.Scan(&pasien.Fullname, &pasien.Id, &fk_dokter)
	if err != nil {
		message = "Data visit not found"
		return medrec, visit, pasien, message, err
	}

	result, err := session.Query(""+
		"SELECT medrec.*,visit.DATE,visit.fk_pasien FROM medrec,visit,user "+
		"WHERE "+
		"medrec.fk_visit = visit.id AND "+
		"user.id = medrec.fk_pasien AND "+
		"medrec.fk_pasien = ? AND "+
		"visit.fk_dokter = ?", pasien.Id, fk_dokter)
	medrecData := io.Medrec{}
	visitData := io.Visit{}
	for result.Next() {
		err = result.Scan(
			&medrecData.Id,
			&medrecData.Code,
			&medrecData.Fk_pasien,
			&medrecData.Fk_visit,
			&medrecData.Tensi,
			&medrecData.Berat_badan,
			&medrecData.Keluhan,
			&medrecData.Gejala,
			&medrecData.Indikasi,
			&medrecData.Diagnosa,
			&medrecData.Fk_dokter,
			&medrecData.Fk_klinik,
			&medrecData.Fk_perawat_isi_tindakan,
			&visitData.Date,
			&visitData.Fk_pasien,
		)
		if err != nil {
			message = "Failed copy data "
			return medrec, visit, pasien, message, err
		}
		visit = append(visit, visitData)
		medrec = append(medrec, medrecData)
	}
	message = "get list medrec user success"
	return medrec, visit, pasien, message, err
}

// NewBasicPenangananService returns a naive, stateless implementation of PenangananService.
func NewBasicPenangananService() PenangananService {
	return &basicPenangananService{}
}

// New returns a PenangananService with all of the expected middleware wired in.
func New(middleware []Middleware) PenangananService {
	var svc PenangananService = NewBasicPenangananService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}

func (b *basicPenangananService) GetMedrecByVisitId(ctx context.Context, visitId string) (medrec io.Medrec, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return medrec, message, err
	}
	defer session.Close()

	result := session.QueryRow("select * from medrec where fk_visit=?", visitId)

	err = result.Scan(
		&medrec.Id,
		&medrec.Code,
		&medrec.Fk_pasien,
		&medrec.Fk_visit,
		&medrec.Tensi,
		&medrec.Berat_badan,
		&medrec.Keluhan,
		&medrec.Gejala,
		&medrec.Indikasi,
		&medrec.Diagnosa,
		&medrec.Fk_dokter,
		&medrec.Fk_klinik,
		&medrec.Fk_perawat_isi_tindakan,
	)
	if err != nil {
		message = "Failed copy data "
		return medrec, message, err
	}

	message = "get medrec user success"
	return medrec, message, err
}

func (b *basicPenangananService) GetPasienListPerawat(ctx context.Context, perawatId string) (visit []io.Visit, jadwal []io.JadwalPraktek, pasien []io.User, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return visit, jadwal, pasien, message, err
	}
	defer session.Close()
	var fk_klinik_perawat int
	getFkKlinik := session.QueryRow("select fk_klinik from user where id=?", perawatId)
	err = getFkKlinik.Scan(&fk_klinik_perawat)

	if err != nil {
		message = "Perawat belum meiliki inforamasi klinik"
		return visit, jadwal, pasien, message, err
	}

	result, err := session.Query(""+
		" SELECT DISTINCT visit.id, visit.code,visit.status, visit.booknum, user.fullname,jadwal_praktek.poli_name,jadwal_praktek.poli_type,history_praktek.fk_dokter "+
		" FROM visit,jadwal_praktek,user,history_praktek "+
		" WHERE "+
		" visit.fk_pasien = user.id AND "+
		" visit.fk_jadwal_praktek = jadwal_praktek.id AND "+
		" history_praktek.fk_jadwal_praktek = jadwal_praktek.id AND "+
		" DATE(history_praktek.open_time) = DATE(NOW()) AND "+
		" jadwal_praktek.fk_klinik = ?", fk_klinik_perawat)

	if err != nil {
		message = "Failed get data "
		return visit, jadwal, pasien, message, err
	}
	visitData := io.Visit{}
	JadwalPraktekData := io.JadwalPraktek{}
	pasienData := io.User{}

	for result.Next() {
		err := result.Scan(&visitData.Id, &visitData.Code, &visitData.Status, &visitData.Booknum,
			&pasienData.Fullname, &JadwalPraktekData.Poli_name, &JadwalPraktekData.Poli_type, &visitData.Fk_dokter)

		if err != nil {
			message = "Failed copy data "
			return visit, jadwal, pasien, message, err
		}
		visit = append(visit, visitData)
		jadwal = append(jadwal, JadwalPraktekData)
		pasien = append(pasien, pasienData)
	}
	message = "Get pasien list success"
	return visit, jadwal, pasien, message, err
}

func (b *basicPenangananService) GetResep(ctx context.Context) (resep []io.Resep, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return resep, message, err
	}
	defer session.Close()

	result, err := session.Query("SELECT * FROM resep")

	if err != nil {
		message = "Failed get data "
		return resep, message, err
	}
	resepData := io.Resep{}

	for result.Next() {
		err := result.Scan(&resepData.Id, &resepData.Fk_visit, &resepData.Obat, &resepData.Dosis, &resepData.Price, &resepData.Quantity)

		if err != nil {
			message = "Failed copy data "
			return resep, message, err
		}
		resep = append(resep, resepData)
	}
	message = "Get resep success"
	return resep, message, err
}

func (b *basicPenangananService) GetListResepByDokterPasien(ctx context.Context, pasienId string, doctorId string) (resep []io.Resep, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return resep, message, err
	}
	defer session.Close()

	result, err := session.Query("SELECT resep.* FROM resep, visit where resep.fk_visit = visit.id and visit.fk_pasien = ? and fk_dokter = ?", pasienId, doctorId)

	if err != nil {
		message = "Failed get data "
		return resep, message, err
	}
	resepData := io.Resep{}

	for result.Next() {
		err := result.Scan(&resepData.Id, &resepData.Fk_visit, &resepData.Obat, &resepData.Dosis, &resepData.Quantity, &resepData.Price)

		if err != nil {
			message = "Failed copy data "
			return resep, message, err
		}
		resep = append(resep, resepData)
	}
	message = "Get resep success"
	return resep, message, err
}

func (b *basicPenangananService) GetListResepByPasienVisit(ctx context.Context, visitId string, doctorId string) (resep []io.Resep, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return resep, message, err
	}
	defer session.Close()

	var pasienId int

	getFkKlinik := session.QueryRow("select visit.fk_pasien from visit where id=?", visitId)
	err = getFkKlinik.Scan(&pasienId)

	if err != nil {
		message = "Pasien belum meiliki inforamasi resep"
		return resep, message, err
	}

	result, err := session.Query("SELECT resep.* FROM resep, visit where resep.fk_visit = visit.id and visit.fk_pasien = ? and fk_dokter = ?", pasienId, doctorId)

	if err != nil {
		message = "Failed get data "
		return resep, message, err
	}
	resepData := io.Resep{}

	for result.Next() {
		err := result.Scan(&resepData.Id, &resepData.Fk_visit, &resepData.Obat, &resepData.Dosis, &resepData.Quantity, &resepData.Price)

		if err != nil {
			message = "Failed copy data "
			return resep, message, err
		}
		resep = append(resep, resepData)
	}
	message = "Get resep success"
	return resep, message, err
}

func (b *basicPenangananService) GetListResepDetailByPasienDokter(ctx context.Context, visitId string, doctorId string) (resep []io.Resep, visit []io.Visit, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return resep, visit, message, err
	}
	defer session.Close()

	var pasienId int

	getFkKlinik := session.QueryRow("select visit.fk_pasien from visit where id=?", visitId)
	err = getFkKlinik.Scan(&pasienId)

	if err != nil {
		message = "Pasien belum meiliki inforamasi resep"
		return resep, visit, message, err
	}

	result, err := session.Query("SELECT resep.*,visit.date FROM resep, visit "+
		" where resep.fk_visit = visit.id and visit.fk_pasien = ? and fk_dokter = ?", pasienId, doctorId)

	if err != nil {
		message = "Failed get data "
		return resep, visit, message, err
	}
	resepData := io.Resep{}
	visitData := io.Visit{}

	for result.Next() {
		err := result.Scan(
			&resepData.Id,
			&resepData.Fk_visit,
			&resepData.Obat,
			&resepData.Dosis,
			&resepData.Price,
			&resepData.Quantity,
			&visitData.Date,
		)

		if err != nil {
			message = "Failed copy data "
			return resep, visit, message, err
		}
		resep = append(resep, resepData)
		visit = append(visit, visitData)
	}
	message = "Get resep success"
	return resep, visit, message, err
}

func (b *basicPenangananService) GetHistoryPraktekByKlinik(ctx context.Context, klinikId string) (history_praktek []io.HistoryPraktek, jadwal []io.JadwalPraktek, message string, err error) {
	return history_praktek, jadwal, message, err
}

func (b *basicPenangananService) GetDetailHistoryPraktekByKlinik(ctx context.Context, klinikId string) (history_praktek []io.HistoryPraktek, jadwal []io.JadwalPraktek, dokter []io.User, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return history_praktek, jadwal, dokter, message, err
	}
	defer session.Close()

	result, err := session.Query(""+
		"SELECT history_praktek.id,open_time,close_time,history_praktek.fk_dokter,user.fullname,"+
		"jadwal_praktek.id,jadwal_praktek.poli_name,jadwal_praktek.poli_type,"+
		"history_praktek.status,history_praktek.remark, jadwal_praktek.id "+
		" 	FROM jadwal_praktek, history_praktek, user  "+
		"		WHERE "+
		" jadwal_praktek.id = history_praktek.fk_jadwal_praktek   "+
		" AND date(open_time) = DATE(NOW()) "+
		" AND user.id = history_praktek.fk_dokter "+
		" AND jadwal_praktek.fk_klinik = ?", klinikId)

	if err != nil {
		message = "Failed get data "
		return history_praktek, jadwal, dokter, message, err
	}

	historyData := io.HistoryPraktek{}
	jadwalData := io.JadwalPraktek{}
	dokterData := io.User{}

	for result.Next() {
		err := result.Scan(
			&historyData.Id,
			&historyData.Open_time,
			&historyData.Close_time,
			&historyData.Fk_dokter,
			&dokterData.Fullname,
			&jadwalData.Id,
			&jadwalData.Poli_name,
			&jadwalData.Poli_type,
			&historyData.Status,
			&historyData.Remark,
			&jadwalData.Id,
		)

		if err != nil {
			message = "Failed copy data "
			return history_praktek, jadwal, dokter, message, err
		}

		history_praktek = append(history_praktek, historyData)
		jadwal = append(jadwal, jadwalData)
		dokter = append(dokter, dokterData)
	}

	message = "Get HistoryPraktek data success"
	return history_praktek, jadwal, dokter, message, err
}

func (b *basicPenangananService) GetHistoryPraktekById(ctx context.Context, historyJadwalId string) (jadwal io.HistoryPraktek, message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return jadwal, message, err
	}
	defer session.Close()

	result := session.QueryRow("select open_time,close_time,status,remark,fk_jadwal_praktek,fk_dokter"+
		" from history_praktek where id = ?", historyJadwalId)

	err = result.Scan(
		&jadwal.Open_time,
		&jadwal.Close_time,
		&jadwal.Status,
		&jadwal.Remark,
		&jadwal.Fk_jadwal_praktek,
		&jadwal.Fk_dokter,
	)

	if err != nil {
		message = "Failed get history_praktek data"
		return jadwal, message, err
	}

	message = "Success get history_praktek data"
	return jadwal, message, err
}

func (b *basicPenangananService) GetDokterByKlinik(ctx context.Context, klinikId string) (dokter []io.User, message string, err error) {
	// TODO implement the business logic of GetDokterByKlinik
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return dokter, message, err
	}
	defer session.Close()

	result, err := session.Query(""+
		"	SELECT user.id, user.fullname FROM user,role, klinik_dokter "+
		"	WHERE "+
		"		role.id = user.fk_role"+
		"		AND role.NAME = 'dokter' "+
		"		AND klinik_dokter.fk_user = user.id"+
		"		AND klinik_dokter.fk_klinik = ?", klinikId)

	if err != nil {
		message = "Query error"
	}

	dokterData := io.User{}
	for result.Next() {
		err := result.Scan(
			&dokterData.Id,
			&dokterData.Fullname,
		)
		if err != nil {
			message = "Failed copy data "
		}
		dokter = append(dokter, dokterData)
	}
	message = "Get list dokter success"
	return dokter, message, err
}

func (b *basicPenangananService) UpdateHistoryPraktek(ctx context.Context, jadwal io.HistoryPraktek) (message string, err error) {
	session, err := db.GetMysqlSession()
	if err != nil {
		message = "Fail connect db"
		return message, err
	}
	defer session.Close()

	open_time, err := time.Parse("2006-01-02T15:04:05Z07:00",jadwal.Open_time);
	if err != nil {
		message = "Failed convert time"
		return message, err
	}
	close_time, err := time.Parse("2006-01-02T15:04:05Z07:00",jadwal.Close_time); if err != nil {
		message = "Failed convert time"
		return message, err
	}
	loc, err := time.LoadLocation("Asia/Bangkok")
    if err != nil {
        fmt.Println(err)
    }
 	open_time = time.Date(open_time.In(loc).Year(),
							open_time.In(loc).Month(), 
							open_time.In(loc).Day(), 
							open_time.In(loc).Hour(),
							open_time.In(loc).Minute(),
							open_time.In(loc).Second(), 
							0,
							loc )
	close_time = time.Date(close_time.In(loc).Year(),
							close_time.In(loc).Month(), 
							close_time.In(loc).Day(), 
							close_time.In(loc).Hour(),
							close_time.In(loc).Minute(),
							close_time.In(loc).Second(), 
							0,
							loc )
	jadwal.Close_time = close_time.Format("2006-01-02 15:04:05")
	jadwal.Open_time = open_time.Format("2006-01-02 15:04:05")


	_, err = session.Query("Update history_praktek set"+
		" close_time = ?, open_time = ? ," + 
		" fk_dokter=?,status=?,remark =?"+
		" where id=?", 
		jadwal.Close_time, jadwal.Open_time, 
		jadwal.Fk_dokter,jadwal.Status, jadwal.Remark, jadwal.Id)

	if err != nil {
		message = "Failed update history_praktek data"
		return message, err
	}

	_, err = session.Query("UPDATE visit SET fk_dokter = ? "+
		" where Date(visit.DATE) = DATE(NOW()) "+
		" AND fk_jadwal_praktek = ?", jadwal.Fk_dokter, jadwal.Fk_jadwal_praktek)

	if err != nil {
		message = "Failed update visit data"
		return message, err
	}

	message = "Success update history_praktek data"
	return message, err
}
