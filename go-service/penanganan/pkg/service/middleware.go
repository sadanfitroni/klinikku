package service

import (
	"context"
	io "akuklinikid_demo/go-service/io"

	log "github.com/go-kit/kit/log"
)

// Middleware describes a service middleware.
type Middleware func(PenangananService) PenangananService

type loggingMiddleware struct {
	logger log.Logger
	next   PenangananService
}

// LoggingMiddleware takes a logger as a dependency
// and returns a PenangananService Middleware.
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next PenangananService) PenangananService {
		return &loggingMiddleware{logger, next}
	}

}

func (l loggingMiddleware) CekTensi(ctx context.Context, visitId string, medrec io.Medrec) (message string, err error) {
	defer func() {
		l.logger.Log("method", "CekTensi", "visitId", visitId, "medrec", medrec, "message", message, "err", err)
	}()
	return l.next.CekTensi(ctx, visitId, medrec)
}
func (l loggingMiddleware) EntryTindakan(ctx context.Context, visitId string, medrec io.Medrec) (message string, err error) {
	defer func() {
		l.logger.Log("method", "EntryTindakan", "visitId", visitId, "medrec", medrec, "message", message, "err", err)
	}()
	return l.next.EntryTindakan(ctx, visitId, medrec)
}
func (l loggingMiddleware) CheckVisitByCode(ctx context.Context, visitId string) (status bool, message string, err error) {
	defer func() {
		l.logger.Log("method", "CheckVisitByCode", "visitId", visitId, "status", status, "message", message, "err", err)
	}()
	return l.next.CheckVisitByCode(ctx, visitId)
}
func (l loggingMiddleware) EntryResep(ctx context.Context, visitId string, resep io.Resep) (message string, err error) {
	defer func() {
		l.logger.Log("method", "EntryResep", "visitId", visitId, "resep", resep, "message", message, "err", err)
	}()
	return l.next.EntryResep(ctx, visitId, resep)
}
func (l loggingMiddleware) GetPasienList(ctx context.Context) (visit []io.Visit, jadwal []io.JadwalPraktek, pasien []io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetPasienList", "visit", visit, "jadwal", jadwal, "pasien", pasien, "message", message, "err", err)
	}()
	return l.next.GetPasienList(ctx)
}
func (l loggingMiddleware) GetPasienListDoctor(ctx context.Context, doctorId string) (visit []io.Visit, jadwal []io.JadwalPraktek, pasien []io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetPasienListDoctor", "doctorId", doctorId, "visit", visit, "jadwal", jadwal, "pasien", pasien, "message", message, "err", err)
	}()
	return l.next.GetPasienListDoctor(ctx, doctorId)
}
func (l loggingMiddleware) GetMedrecPasien(ctx context.Context, visitId string) (medrec []io.Medrec, visit []io.Visit, pasien io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetMedrecPasien", "visitId", visitId, "medrec", medrec, "visit", visit, "pasien", pasien, "message", message, "err", err)
	}()
	return l.next.GetMedrecPasien(ctx, visitId)
}
func (l loggingMiddleware) GetMedrecByVisitId(ctx context.Context, visitId string) (medrec io.Medrec, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetMedrecByVisitId", "visitId", visitId, "medrec", medrec, "message", message, "err", err)
	}()
	return l.next.GetMedrecByVisitId(ctx, visitId)
}
func (l loggingMiddleware) GetPasienListPerawat(ctx context.Context, perawatId string) (visit []io.Visit, jadwal []io.JadwalPraktek, pasien []io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetPasienListPerawat", "perawatId", perawatId, "visit", visit, "jadwal", jadwal, "pasien", pasien, "message", message, "err", err)
	}()
	return l.next.GetPasienListPerawat(ctx, perawatId)
}

func (l loggingMiddleware) GetResep(ctx context.Context) (resep []io.Resep, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetResep", "resep", resep, "message", message, "err", err)
	}()
	return l.next.GetResep(ctx)
}

func (l loggingMiddleware) GetListResepByDokterPasien(ctx context.Context, pasienId string, doctorId string) (resep []io.Resep, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetListResepByDokterPasien", "pasienId", pasienId, "doctorId", doctorId, "resep", resep, "message", message, "err", err)
	}()
	return l.next.GetListResepByDokterPasien(ctx, pasienId, doctorId)
}

func (l loggingMiddleware) GetListResepByPasienVisit(ctx context.Context, visitId string, doctorId string) (resep []io.Resep, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetListResepByPasienVisit", "visitId", visitId, "doctorId", doctorId, "resep", resep, "message", message, "err", err)
	}()
	return l.next.GetListResepByPasienVisit(ctx, visitId, doctorId)
}

func (l loggingMiddleware) GetListResepDetailByPasienDokter(ctx context.Context, visitId string, doctorId string) (resep []io.Resep, visit []io.Visit, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetListResepDetailByPasienDokter", "visitId", visitId, "doctorId", doctorId, "resep", resep, "visit", visit, "message", message, "err", err)
	}()
	return l.next.GetListResepDetailByPasienDokter(ctx, visitId, doctorId)
}

func (l loggingMiddleware) GetHistoryPraktekByKlinik(ctx context.Context, klinikId string) (history_praktek []io.HistoryPraktek, jadwal []io.JadwalPraktek, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetHistoryPraktekByKlinik", "klinikId", klinikId, "history_praktek", history_praktek, "jadwal", jadwal, "message", message, "err", err)
	}()
	return l.next.GetHistoryPraktekByKlinik(ctx, klinikId)
}

func (l loggingMiddleware) GetDetailHistoryPraktekByKlinik(ctx context.Context, klinikId string) (history_praktek []io.HistoryPraktek, jadwal []io.JadwalPraktek, dokter []io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetDetailHistoryPraktekByKlinik", "klinikId", klinikId, "history_praktek", history_praktek, "jadwal", jadwal, "dokter", dokter, "message", message, "err", err)
	}()
	return l.next.GetDetailHistoryPraktekByKlinik(ctx, klinikId)
}

func (l loggingMiddleware) GetHistoryPraktekById(ctx context.Context, historyJadwalId string) (jadwal io.HistoryPraktek, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetHistoryPraktekById", "historyJadwalId", historyJadwalId, "jadwal", jadwal, "message", message, "err", err)
	}()
	return l.next.GetHistoryPraktekById(ctx, historyJadwalId)
}
func (l loggingMiddleware) GetDokterByKlinik(ctx context.Context, klinikId string) (dokter []io.User, message string, err error) {
	defer func() {
		l.logger.Log("method", "GetDokterByKlinik", "klinikId", klinikId, "dokter", dokter, "message", message, "err", err)
	}()
	return l.next.GetDokterByKlinik(ctx, klinikId)
}

func (l loggingMiddleware) UpdateHistoryPraktek(ctx context.Context, jadwal io.HistoryPraktek) (message string, err error) {
	defer func() {
		l.logger.Log("method", "UpdateHistoryPraktek", "jadwal", jadwal, "message", message, "err", err)
	}()
	return l.next.UpdateHistoryPraktek(ctx, jadwal)
}
