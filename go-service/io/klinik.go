package io

import (
	"encoding/json"
)

type Klinik struct {
	Id int `json:"id"`
	Code string `json:"code"`
	Name string `json:"name"`
	Alamat string `json:"alamat"`
	Pict_1 string `json:"pict_1"`
	Pict_2 string `json:"pict_2"`
	Pict_3 string `json:"pict_3"`
	Logo string `json:"logo"`
	Phone string `json:"phone"`
	Provinsi int `json:"provinsi"`
	Kota int `json:"kota"`
	Kecamatan int `json:"kecamatan"`
	Kelurahan int `json:"kelurahan"`
	Email string `json:"email"`
	Fk_langganan int `json:"fk_langganan"`
	Lon float64 `json:"lon"`
	Lat float64 `json:"lat"`
}

func (t Klinik) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}
