package io

import (
	"encoding/json"
)

type Pembayaran struct {
	Id int `json:"id" `
	Fk_klinik int `json:"fk_klinik" `
	Fk_visit int `json:"fk_visit" `
	Fk_dokter int `json:"fk_dokter" `
	Fk_pasien int `json:"fk_pasien" `
	Item string `json:"item"`
	Nominal int `json:"nominal"`
	Quantity int `json:"quantity"`
	Diskon int `json:"diskon"`
	Subtotal int `json:"subtotal"`
}

func (t Pembayaran) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}