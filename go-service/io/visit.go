package io

import (
	"encoding/json"
	"time"
)

type Visit struct {
	Id int `json:"id" `
	Code string `json:"code" `
	Fk_dokter int `json:"fk_dokter" `
	Fk_pasien int  `json:"fk_pasien" `
	Fk_jadwal_praktek int `json:"fk_jadwal_praktek" `
	Fk_history_praktek int `json:"fk_history_praktek" `
	Fk_perawat int `json:"fk_perawat"`
	Date time.Time `json:"date" `
	Booknum int `json:"booknum" `
	Total int `json:"total" `
	Rating float32 `json:"rating" `
	Estimasi_tindakan int `json:"estimasi_tindakan" `
	Checking_time time.Time `json:"checking_time" `
	Status string `json:"status" `
	Tipe_pembayaran string `json:"tipe_pembayaran"`
	No_polis string `json:"no_polis"`
	Nama_asuransi string `json:"nama_asuransi"`
	Kode_pembayaran string `json:"kode_pembayaran"`
}

func (t Visit) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}