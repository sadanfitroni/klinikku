package io

import (
	"encoding/json"
)

type Resep struct {
	Id int `json:"id" `
	Fk_visit int `json:"fk_visit" `
	Obat string `json:"obat"`
	Dosis string `json:"dosis"`
	Quantity int `json:"quantity"`
	Price int `json:"price"`
}

func (t Resep) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}