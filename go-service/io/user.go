package io

import (
	"encoding/json"
	"time"
)

type User struct {
	Id int `json:"id"`
	Email string `json:"email"`
	Password string `json:"password"`
	Mobile string `json:"mobile"`
	Fullname string `json:"fullname"`
	Gender string `json:"gender"`
	Birth_date time.Time `json:"birth_date"`
	Blood_type string `json:"blood_type"`
	Address string `json:"address"`
	Code string `json:"code"`
	Fk_role int `json:"fk_role"`
	Jabatan string `json:"jabatan"`
	Session int `json:"session"`
	Fk_klinik int `json:"fk_klinik"`
	Realm string `json:"realm"`
	Username string `json:"username"`
	EmailVerified int `json:"emailVerified"`
	VerificationToken string `json:"verificationToken"`
	Incomplete int `json:"incomplete"`
}

func (t User) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}
