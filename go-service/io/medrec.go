package io

import (
	"encoding/json"
)

type Medrec struct {
	Code string `json:"code" `
	Fk_pasien int `json:"fk_pasien" `
	Id int `json:"id" `
	Fk_visit int `json:"fk_visit" `
	Tensi string `json:"tensi" `
	Berat_badan float32 `json:"berat_badan" `
	Keluhan string `json:"keluhan" `
	Gejala string `json:"gejala" `
	Indikasi string `json:"indikasi" `
	Diagnosa string `json:"diagnosa" `
	Fk_dokter int `json:"fk_dokter" `
	Fk_klinik int  `json:"fk_klinik" `
	Fk_perawat_isi_tindakan int `json:"fk_perawat_isi_tindakan"`
	Flag_bayar bool `json:"flag_bayar"`
	Flag_konsul bool `json:"flag_konsul"`
	Flag_pemeriksaan bool `json:"flag_pemeriksaan"`
	Flag_resep bool `json:"flag_resep"`
	Flag_inap bool `json:"flag_inap"`
	Flag_operasi bool `json:"flag_operasi"`
}

func (t Medrec) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}