package io

import (
	"encoding/json"
)

type Acl struct {
	Id 				int `json:"id"`
	Menu			string `json:"menu"`
	Property 		string `json:"property"`
	AccessType		string `json:"accessType"`
	Permission		string `json:"permision"`
	PrincipalType 	string `json:"principalType"`
	PrincipalId 	string `json:"principalId"`
	Model			string `json:"model"`
}

func (t Acl) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}