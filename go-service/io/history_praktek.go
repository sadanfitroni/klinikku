package io

import (
	"encoding/json"
	// "time"
)

type HistoryPraktek struct {
	Id int `json:"id"`
	Praktek_name string `json:"praktek_name"`
	Book_count int `json:"book_count"`
	Open_time string `json:"open_time"`
	Close_time string `json:"close_time"`
	Fk_jadwal_praktek int `json:"fk_jadwal_praktek"`
	Status string `json:"status"`
	Remark string `json:"remark"`
	Fk_dokter int `json:"Fk_dokter"`
}

func (t HistoryPraktek) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}
