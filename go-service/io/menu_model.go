package io

import (
	"encoding/json"
)

type MenuModel struct {
	Id int `json:"id"`
	Model string `json:"model"`
	Permission string `json:"permission"`
	AccessType string  `json:"accessType"`
	Fk_menu int `json:"fk_menu"`
}

func (t MenuModel) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}