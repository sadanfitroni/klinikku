package io

import (
	"encoding/json"
)

type MenuSidebar struct {
	Id int `json:"id"`
	Url string `json:"url"`
	Label string `json:"label"`
	ParentId int `json:"parentId"`
	OrderMenu int  `json:"orderMenu"`
	Icon string `json:"icon"`
}

func (t MenuSidebar) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}

