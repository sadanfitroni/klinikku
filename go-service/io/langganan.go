package io

import (
	"encoding/json"
	"time"
)

type Langganan struct {
	Id int `json:"id"`
	Paket int `json:"paket"`
	Dari_tanggal time.Time `json:"dari_tanggal"`
	Sampai_tanggal time.Time `json:"sampai_tanggal"`
	No_kontrak int `json:"no_kontrak"`
	Deskripsi string `json:"deskripsi"`
	Status string `json:"status"`
	Fk_klinik int `json:"fk_klinik"`
}

func (t Langganan) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}
