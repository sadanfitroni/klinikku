package io

import (
	"encoding/json"
)

type Role struct {
	Id int `json:"id" `
	Code string `json:"code" `
	Name string `json:"name" `
}

func (t Role) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}