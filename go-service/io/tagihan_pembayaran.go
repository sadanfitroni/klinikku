package io

import (
	"encoding/json"
	"time"
)

type TagihanPembayaran struct {
	Id int `json:"id"`
	Tahun int `json:"tahun"`
	Bulan int `json:"bulan"`
	Fk_klinik int `json:"fk_klinik"`
	Dari_tanggal time.Time `json:"dari_tanggal"`
	Sampai_tanggal time.Time `json:"sampai_tanggal"`
	Total_tagihan int `json:"total_tagihan"`
	Total_kunjungan int `json:"total_kunjungan"`
	Status string `json:"status"`
	Tanggal_pembayaran time.Time `json:"tanggal_pembayaran"`
	Jumlah_pembayaran int `json:"jumlah_pembayaran"`
}

func (t TagihanPembayaran) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}