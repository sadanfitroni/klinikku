package io

import (
	"encoding/json"
	// "time"
)

type Praktek struct {
	Id int `json:"id"`
	Code string `json:"code"`
	Poli_name string `json:"poli_name"`
	Poli_type string `json:"poli_type"`
	Day int `json:"day"`
	Time_from string `json:"time_from"`
	Time_to string `json:"time_to"`
	Fk_dokter int `json:"fk_dokter"`
	Fk_klinik int `json:"fk_klinik"`
	Price_rate int `json:"price_rate"`
	Rating float32 `json:"rating"`
	Status string `json:"status"`
	Lama_tindakan int `json:"lama_tindakan"`
}

func (t Praktek) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}
