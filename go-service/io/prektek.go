package io

import (
	"encoding/json"
	"time"
)

type JadwalPraktek struct {
	Id int `json:"id" `
	Code string `json:"code" `
	Poli_name string `json:"poli_name" `
	Poli_type string `json:"poli_type" `
	Day int `json:"day" `
	Time_from time.Time `json:"time_from" `
	Time_to time.Time `json:"time_to" `
	Price_rate int `json:"price_rate" `
	Rating float32 `json:"rating" `
	Status string `json:"status" `
	Lama_tindakan int `json:"lama_tindakan" `
	Fk_dokter int `json:"fk_dokter" `
	Fk_klinik int  `json:"fk_klinik" `

}

func (t JadwalPraktek) String() string {
   b, err := json.Marshal(t)
   if err != nil {
      return "unsupported value type"
   }
   return string(b)
}