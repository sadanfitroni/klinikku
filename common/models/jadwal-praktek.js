'use strict';
var Promise = require("bluebird");
var async = require("async");

module.exports = function(jadwal_praktek) {
    jadwal_praktek.on('dataSourceAttached', function (obj) {
        // wrap the whole model in Promise
        // but we need to avoid 'validate' method 
        jadwal_praktek = Promise.promisifyAll(jadwal_praktek, {
            filter: function (name, func, target) {
                return !(name == 'validate');
            }
        });
    });

    jadwal_praktek.beforeRemote('*', function (ctx, unused, next) {
        if (ctx.req.accessToken) {
            ctx.req.app.models.user.findById(ctx.req.accessToken.userId, function (err, user) {
                if (err) {
                    console.log(err);
                    return next(err);
                }

                if (!user) {
                    return next();
                }
            });
            next();
        } else {
            console.log('Invalid Access Token');
            next(new Error('Invalid Access Token'));
        }
    });

    /**
   * generate monthly report of purchasing
   * @param  {[type]}   filterParams [month, id_region, id_sam, id_distributor, id_province, id_regency, id_product_segment]
   * @param  {Function} cb           [description]
   * @return {[array]}                [array of distributor and its summarized data of total volume]
   */
    jadwal_praktek.searchJadwal = function searchJadwal(filterParams, cb) {
        console.log('incoming request: ', filterParams)
        var ds = jadwal_praktek.dataSource;
        var params = []
        var whereClause = "";
        var innerwhereClause = "";
        if (filterParams.searchText) {
            innerwhereClause += " AND (dr.fullname like ? OR k.name like ? ) ";
            params.push('%'+filterParams.searchText+'%');
            params.push('%'+filterParams.searchText+'%');
        };
        if (filterParams.open_time) {
            innerwhereClause += " AND hp.open_time = ? ";
            params.push(filterParams.open_time);
        };
        if (filterParams.poli_type) {
            innerwhereClause += " AND jp.poli_type = ? ";
            params.push(filterParams.poli_type);
        };

        if (filterParams.kota) {
            innerwhereClause += " AND k.kota = ? ";
            params.push(filterParams.kota);
        };
        if (filterParams.kecamatan) {
            innerwhereClause += " AND k.kecamatan = ? ";
            params.push(filterParams.kecamatan);
        };
        if (filterParams.fk_dokter) {
            innerwhereClause += " AND jp.fk_dokter = ? ";
            params.push(filterParams.fk_dokter);
        };
        if (filterParams.fk_klinik) {
            innerwhereClause += " AND jp.fk_klinik = ? ";
            params.push(filterParams.fk_klinik);
        };
        
        var sql = "SELECT jp.*, hp.*, k.*, dr.* from jadwal_praktek jp "+
        "left join history_praktek hp on jp.id = hp.fk_jadwal_praktek "+
        "left join klinik k on k.id = jp.fk_klinik "+
        "left join user dr on dr.fk_role = 25 and dr.id = jp.fk_dokter "+
        "WHERE jp.status = 'Active' " + innerwhereClause;
        console.log('SQL', sql)
        ds.connector.query(sql, params, function (err, result) {
            if (err) console.error(err);
            if (result) 
              result.forEach(function(itm){
                delete(itm.password);
              })
            cb(err, result);
        });
    }

    // new endpoint upload selling-in trx
    jadwal_praktek.remoteMethod('searchJadwal', {
        accepts: [{
            arg: 'filterParams', type: 'object', http: { source: 'body' }, root: true,
            description: 'filter params of jadwal praktek by: searchText, poli_type, kota, kecamatan, fk_dokter, open_time, fk_klinik, and sortby'
        }],
        http: { path: '/searchJadwal', verb: 'post' },
        description: 'Pencarian jadwal praktek dokter',
        returns: [{ arg: 'data', type: ['jadwal_praktek'], root: true }]
    });
};
