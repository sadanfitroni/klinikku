'use strict';
var crypto = require('crypto');
var MAX_PASSWORD_LENGTH = 72;
var bcrypt;
try {
  bcrypt = require('bcrypt');
  if (bcrypt && typeof bcrypt.compare !== 'function') {
    bcrypt = require('bcryptjs');
  }
} catch (err) {
  bcrypt = require('bcryptjs');
}

module.exports = function(User) {
	User.remoteMethod ('checkPassword',{
		http : {path : '/checkPassword', verb : 'get'},
		accepts :[ {arg : 'decodePassword' , type : 'string'},{arg : 'password' , type : 'string'}],
		description: 'check loopback password',
		returns: { arg: 'result', type: 'boolean' }
	});

	User.checkPassword = function (decodePassword,password,cb){
		let result = bcrypt.compareSync(password, decodePassword);
		cb(null,result);
	}

	User.remoteMethod ('encodePassword',{
		http : {path : '/encodePassword', verb : 'get'},
		accepts : {arg : 'password' , type : 'string'},
		description: 'encode loopback password',
		returns: { arg: 'result', type: 'string', root: true }
	});

	User.encodePassword = function (password,cb){
		let encode= bcrypt.hashSync(password,10);
		let result = { result : encode}; 
		cb(null,result);
	}
	
};
