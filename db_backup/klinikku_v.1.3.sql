/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE IF NOT EXISTS `klinikku_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `klinikku_db`;

CREATE TABLE IF NOT EXISTS `acl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(512) NOT NULL DEFAULT '',
  `property` varchar(512) NOT NULL DEFAULT '',
  `accessType` varchar(512) NOT NULL DEFAULT '',
  `permission` varchar(512) NOT NULL DEFAULT '',
  `principalType` varchar(512) NOT NULL DEFAULT '',
  `principalId` varchar(512) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `klinik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `pict_1` varchar(255) DEFAULT NULL,
  `pict_2` varchar(255) DEFAULT NULL,
  `pict_3` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `medrec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `fk_pasien` int(11) DEFAULT NULL,
  `fk_visit` int(11) DEFAULT NULL,
  `tensi` varchar(255) DEFAULT NULL,
  `bb` float DEFAULT NULL,
  `keluhan` varchar(255) DEFAULT NULL,
  `gejala` varchar(255) DEFAULT NULL,
  `indikasi` varchar(255) DEFAULT NULL,
  `diagnosa` varchar(255) DEFAULT NULL,
  `fk_dokter` int(11) DEFAULT NULL,
  `fk_klinik` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `menu_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(512) NOT NULL,
  `permission` varchar(512) DEFAULT NULL,
  `accessType` varchar(512) DEFAULT NULL,
  `fk_menu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `menu_sidebar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL DEFAULT '',
  `label` varchar(255) NOT NULL DEFAULT '',
  `parentId` int(11) NOT NULL DEFAULT '0',
  `orderMenu` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `praktek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `poli_name` varchar(255) DEFAULT NULL,
  `poli_type` varchar(255) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL,
  `fk_dokter` int(11) DEFAULT NULL,
  `fk_klinik` int(11) DEFAULT NULL,
  `price_rate` int(11) DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `lama_tindakan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `resep` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_visit` int(11) DEFAULT NULL,
  `obat` varchar(255) DEFAULT NULL,
  `dosis` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT '',
  `password` varchar(512) NOT NULL DEFAULT '',
  `mobile` varchar(255) DEFAULT '',
  `fullname` varchar(255) DEFAULT '',
  `gender` char(1) DEFAULT '',
  `birth_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `blood_type` char(3) DEFAULT '',
  `address` varchar(255) DEFAULT '',
  `code` varchar(255) DEFAULT '',
  `fk_role` int(11) DEFAULT '0',
  `jabatan` varchar(255) DEFAULT '',
  `session` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `booknum` int(11) DEFAULT NULL,
  `fk_dokter` int(11) DEFAULT NULL,
  `fk_praktek` int(11) DEFAULT NULL,
  `fk_pasien` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `estimasi_tindakan` varchar(255) DEFAULT NULL,
  `checking_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
