/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE IF NOT EXISTS `klinikku_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `klinikku_db`;

CREATE TABLE IF NOT EXISTS `acl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(512) NOT NULL DEFAULT '',
  `property` varchar(512) NOT NULL DEFAULT '',
  `accessType` varchar(512) NOT NULL DEFAULT '',
  `permission` varchar(512) NOT NULL DEFAULT '',
  `principalType` varchar(512) NOT NULL DEFAULT '',
  `principalId` varchar(512) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `acl` DISABLE KEYS */;
INSERT INTO `acl` (`id`, `menu`, `property`, `accessType`, `permission`, `principalType`, `principalId`) VALUES
	(33, 'Menu', '', '*', 'ALLOW', 'ROLE', 'Admin'),
	(34, 'User', '', '*', 'ALLOW', 'ROLE', 'Admin'),
	(35, 'Role', '', '*', 'ALLOW', 'ROLE', 'Admin'),
	(40, 'Entry Tindakan', '', '*', 'ALLOW', 'ROLE', 'Dokter'),
	(41, 'Cek Tensi', '', '*', 'ALLOW', 'ROLE', 'Perawat');
/*!40000 ALTER TABLE `acl` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `klinik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `pict_1` varchar(255) DEFAULT NULL,
  `pict_2` varchar(255) DEFAULT NULL,
  `pict_3` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `klinik` DISABLE KEYS */;
/*!40000 ALTER TABLE `klinik` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `medrec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `fk_pasien` int(11) DEFAULT NULL,
  `fk_visit` int(11) DEFAULT NULL,
  `tensi` varchar(255) DEFAULT NULL,
  `berat_badan` float DEFAULT NULL,
  `keluhan` varchar(255) DEFAULT NULL,
  `gejala` varchar(255) DEFAULT NULL,
  `indikasi` varchar(255) DEFAULT NULL,
  `diagnosa` varchar(255) DEFAULT NULL,
  `fk_dokter` int(11) DEFAULT NULL,
  `fk_klinik` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `medrec` DISABLE KEYS */;
/*!40000 ALTER TABLE `medrec` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `menu_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(512) NOT NULL DEFAULT '',
  `permission` varchar(512) NOT NULL DEFAULT '',
  `accessType` varchar(512) NOT NULL DEFAULT '',
  `fk_menu` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `menu_model` DISABLE KEYS */;
INSERT INTO `menu_model` (`id`, `model`, `permission`, `accessType`, `fk_menu`) VALUES
	(32, 'user', 'ALLOW', '*', 31),
	(33, 'medrec', 'ALLOW', '*', 31),
	(34, 'visit', 'ALLOW', '*', 35),
	(35, 'resep', 'ALLOW', '*', 35),
	(36, 'medrec', 'ALLOW', '*', 35),
	(37, 'user', 'ALLOW', '*', 3),
	(38, 'role', 'ALLOW', '*', 2),
	(39, 'menu_sidebar', 'ALLOW', '*', 1),
	(40, 'praktek', 'ALLOW', '*', 36),
	(41, 'resep', 'ALLOW', '*', 36),
	(42, 'medrec', 'ALLOW', '*', 36),
	(43, 'visit', 'ALLOW', '*', 36);
/*!40000 ALTER TABLE `menu_model` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `menu_sidebar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL DEFAULT '',
  `label` varchar(255) NOT NULL DEFAULT '',
  `parentId` int(11) NOT NULL DEFAULT '0',
  `orderMenu` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `menu_sidebar` DISABLE KEYS */;
INSERT INTO `menu_sidebar` (`id`, `url`, `label`, `parentId`, `orderMenu`, `icon`) VALUES
	(1, 'app.menu', 'Menu', 0, 0, ''),
	(2, 'app.role', 'Role', 0, 0, ''),
	(3, 'app.user', 'User', 0, 0, ''),
	(35, 'app.cek_tensi', 'Cek Tensi', 0, 0, ''),
	(36, 'app.entry_tindakan', 'Entry Tindakan', 0, 0, '');
/*!40000 ALTER TABLE `menu_sidebar` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `praktek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `poli_name` varchar(255) DEFAULT NULL,
  `poli_type` varchar(255) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL,
  `fk_dokter` int(11) DEFAULT NULL,
  `fk_klinik` int(11) DEFAULT NULL,
  `price_rate` int(11) DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `lama_tindakan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `praktek` DISABLE KEYS */;
/*!40000 ALTER TABLE `praktek` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `resep` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_visit` int(11) DEFAULT NULL,
  `obat` varchar(255) DEFAULT NULL,
  `dosis` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `resep` DISABLE KEYS */;
/*!40000 ALTER TABLE `resep` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `code`, `name`) VALUES
	(1, 'kode_1', 'Admin'),
	(24, 'kode_2', 'Perawat'),
	(25, 'kode_dokter', 'Dokter');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT '',
  `password` varchar(512) NOT NULL DEFAULT '',
  `mobile` varchar(255) DEFAULT '',
  `fullname` varchar(255) DEFAULT '',
  `gender` char(1) DEFAULT '',
  `birth_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `blood_type` char(3) DEFAULT '',
  `address` varchar(255) DEFAULT '',
  `code` varchar(255) DEFAULT '',
  `fk_role` int(11) DEFAULT '0',
  `jabatan` varchar(255) DEFAULT '',
  `session` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `email`, `password`, `mobile`, `fullname`, `gender`, `birth_date`, `blood_type`, `address`, `code`, `fk_role`, `jabatan`, `session`) VALUES
	(1, 'admin@admin.com', 'aaa', '', '', '', '2019-06-26 19:28:16', '', '', '', 1, '', 0),
	(2, 'perawat@perawat.com', 'aaa', '', '', '', '2019-07-01 16:24:31', '', '', '', 24, '', 1),
	(3, 'dokter@dokter.com', 'aaa', '11', 'dokter', 'M', '2019-07-16 17:00:00', 'A+', 'Komp.batan indah blok c-48, rt/rw 020/004 kademangan,setu, tangerang selatan, banten', 'kode_1', 25, 'aaa', 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `booknum` int(11) DEFAULT NULL,
  `fk_dokter` int(11) DEFAULT NULL,
  `fk_praktek` int(11) DEFAULT NULL,
  `fk_pasien` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `estimasi_tindakan` varchar(255) DEFAULT NULL,
  `checking_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `visit` DISABLE KEYS */;
/*!40000 ALTER TABLE `visit` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
