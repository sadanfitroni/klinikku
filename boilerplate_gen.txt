//turn sql syntax to go struct

`(.+)` ([^ ]+)[\( ].+
\1 \2 `json:"\1"`
============================================================================================================================
//turn fields to grid column defs

(.+)
{displayName:'\1',field:'\1',width:"10%"},

{displayName:'(.)
{displayName:'\U$1
============================================================================================================================
//turn fields to form html
(.+)

<div class="col-sm-3">
	<div class="form-group-sm">
		<label>\1</label>
		<input class="form-control" ng-model="something.\1" value="" required>
	</div>
</div>
============================================================================================================================
//turn fields to set field update query
(.+)
\1=?,
//to object field list
(.+)\n
something.\1, 
//to scan parameter
(.+)\n
&row.\1, 
============================================================================================================================
//getSomething

(.+)

session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return \1, message, err
	}
	defer session.Close()

	rows, err := session.Query("select * from \1;")
	if err != nil {
		message = "Query failed"
		return \1, message, err
	}
	defer rows.Close()

	//row := io.Something{}
	for rows.Next() {
	//	var err = rows.Scan(&row.Field)
		if err != nil {
			message = "Unmatched field name"
			return \1, message, err
		}
		\1 = append(\1, row)
	}
	return \1, message, err
============================================================================================================================
//addSomething

(.+)

session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return \1Id, message, err
	}
	defer session.Close()

	//stmt, err := session.Prepare("insert into \1 values(?,?);")
	if err != nil {
        message="Cannot prepare DB statement"
        return \1Id, message, err
    }

    //res, err := stmt.Exec(nil, \1.Field)
    if err != nil {
        message="Cannot run insert statement"
        return \1Id, message, err
    }

    \1Id, _ = res.LastInsertId()

	message="insert success"
	return \1Id, message, err
============================================================================================================================
//editSomething

(.+)

session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	//_, err = session.Query("update \1 set field=? where id = ?", \1.Field, \1.Id)
	if err != nil {
		message = "Failed execution query"
		return message, err
	}

	message = "Edit success"
	return message, err
============================================================================================================================
//deleteSomething

(.+)

session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return message, err
	}
	defer session.Close()

	_, err = session.Query("delete from \1 where id=?",id)
	if err != nil {
		message = "Failed execution delete query"
		return message, err
	}
	message = "Delete success"
	return message, err
============================================================================================================================
//getSomethingById

(.+)

session, err := db.GetMysqlSession()
	if err != nil {
		message = "Failed connect to database"
		return \1, message, err
	}
	defer session.Close()

	query := "select * from \1 where id=?"
	row := session.QueryRow(query, id)
	//err = row.Scan(&\1.Field, &\1.Field)

	switch err {
	case sql.ErrNoRows:
		message = "Data not found"
		return \1, message, err
	case nil:
		message = "Sucecess get \1 by id"
		return \1, message, err
	default:
		message = "Mysql error "
		return \1, message, err
	}
============================================================================================================================
//editHandler

m\.Methods\("POST"\)\.Path\("(.+)"\)\.Handler\(handlers\.CORS\(handlers\.AllowedMethods\(\[\]string{"POST"}\), handlers\.AllowedOrigins\(\[\]string{"\*"}\)\)\(http\.NewServer\(endpoints\.(.+)\.\.\.\)\)\)

m.Methods("POST", "OPTIONS").Path("\1").Handler(
		handlers.CORS(
			handlers.AllowedMethods([]string{"POST"}),
			handlers.AllowedHeaders([]string{"Content-Type", "Content-Length"}),
			handlers.AllowedOrigins([]string{"*"}),
	)(http.NewServer(endpoints.\2...)))
============================================================================================================================