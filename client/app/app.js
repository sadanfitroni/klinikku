'use strict';

angular.module('BlurAdmin', [
  'ngAnimate',
  'ui.bootstrap',
  'ui.sortable',
  'ui.router',
  'ngTouch',
  'toastr',
  'smart-table',
  "xeditable",
  'ui.slimscroll',
  'ngJsTree',
  'angular-progress-button-styles',

  // looback Services
  'lbServices',
  'angularFileUpload',
  'ngResource',
  'ngRoute',
  'ui.grid',
  'ui.grid.pagination',
  'ui.grid.autoResize',
  'ui.grid.exporter',
  'ngStorage',
  'ng.deviceDetector',
  'ui.select',
  'oc.lazyLoad',

  //databales
  'datatables',

  'BlurAdmin.theme',
  'BlurAdmin.pages'
]);

