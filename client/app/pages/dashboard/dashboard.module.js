/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.dashboard', [
        'BlurAdmin.pages'
    ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider,$ocLazyLoadProvider) {
    $stateProvider
        .state('app.dashboard', {
          url: '/dashboard',
          templateUrl: 'app/pages/dashboard/dashboard.html',
          title: 'DashboardController',
          authenticate: true,
          controller: 'DashboardController',
          resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/dashboard/DashboardController.js'); 
              }]
          }
          // sidebarMeta: {
          //   icon: 'ion-android-home',
          //   order: 0,
          // },
        })
        .state('app.homepage',{
          url: '/homepage',
          templateUrl: 'homepage.html',
        });
  }
})();
