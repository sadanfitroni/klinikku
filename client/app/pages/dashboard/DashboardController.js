angular
  .module('BlurAdmin').controller('DashboardController', DashboardController);


  function DashboardController($q, $scope, $rootScope, $state,
  $routeParams, $route, $filter, $location, toastr, $timeout, $http) {
  	console.log("Controller has been registered")
  	$scope.dashboard = {};


  	$scope.init = function init(){
  		$scope.dashboard.total_pasien = 0;
		$http.post(
	        "http://" + $rootScope.address +":"+ $rootScope.port.report + "/get-today-visit",
	        {klinik_id: String($scope.currentUser.fk_klinik) },
	        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
	      ).then(function onSuccess(result){
	      console.log("api call success",result);
	        console.log(result)
	        // if(result.data.count)
	        $scope.dashboard.total_pasien = result.data.count;
	        // toastr.success(message);
	      }).catch(function onError(result){
	      	$scope.dashboard.total_pasien = "Error";
	        console.log('get total pasien error',result)
	        toastr.warning('get total visit error', 'Warning');
      	})

	    $http.post(
	        "http://" + $rootScope.address +":"+ $rootScope.port.report + "/get-today-cancel-visit",
	        {klinik_id: String($scope.currentUser.fk_klinik) },
	        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
	      ).then(function onSuccess(result){
	      console.log("api call success",result);
	        console.log(result)
	        $scope.dashboard.belum_diperiksa = result.data.count;
	        // toastr.success(message);
	      }).catch(function onError(result){
	      	$scope.dashboard.belum_diperiksa = "Error";
	        console.log('get unfinished vist error',result)
	        toastr.warning('get unfinished visit error', 'Warning');
      	})

	    $http.post(
	        "http://" + $rootScope.address +":"+ $rootScope.port.report + "/get-today-complate-visit",
	        {klinik_id: String($scope.currentUser.fk_klinik) },
	        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
	      ).then(function onSuccess(result){
	     	 // console.log("api call success",result);
	        console.log("diperiksa",result)
	        $scope.dashboard.pasien_diperiksa = result.data.count;
	        // toastr.success(message);
	      }).catch(function onError(result){
	      	$scope.dashboard.pasien_diperiksa = "Error";
	        console.log('get complate visit error',result)
	        toastr.warning('get complate visit rror', 'Warning');
      	})

      	$http.post(
	        "http://" + $rootScope.address +":"+ $rootScope.port.report + "/get-today-revenue",
	        {klinik_id: String($scope.currentUser.fk_klinik) },
	        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
	      ).then(function onSuccess(result){
	      console.log("api call success",result);
	        console.log(result)
	        $scope.dashboard.total_pemasukan = result.data.revenue;
	        // toastr.success(message);
	      }).catch(function onError(result){
	      	$scope.dashboard.total_pemasukan = "Error";
	        console.log('get total revenue error',result)
	        toastr.warning('get total revenue error', 'Warning');
      	})   

      	$http.post(
	        "http://" + $rootScope.address +":"+ $rootScope.port.report + "/get-today-old-member-visit",
	        {klinik_id: String($scope.currentUser.fk_klinik) },
	        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
	      ).then(function onSuccess(result){
	      console.log("api call success",result);
	        console.log(result)
	        $scope.dashboard.total_pasien_lama = result.data.count;
	        // toastr.success(message);
	      }).catch(function onError(result){
	      	$scope.dashboard.total_pasien_lama = "Error";
	        console.log('get old pasien visit error',result)
	        toastr.warning('get old pasien visit error', 'Warning');
      	})


      	$http.post(
	        "http://" + $rootScope.address +":"+ $rootScope.port.report + "/get-today-new-member-visit",
	        {klinik_id: String($scope.currentUser.fk_klinik) },
	        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
	      ).then(function onSuccess(result){
	      console.log("api call success",result);
	        console.log(result)
	        $scope.dashboard.total_pasien_baru = result.data.count;
	        // toastr.success(message);
	      }).catch(function onError(result){
	      	$scope.dashboard.total_pasien_baru = "Error";
	        console.log('get old pasien visit error',result)
	        toastr.warning('get old pasien visit error', 'Warning');
      	})    


  

  	}
  	$scope.init();
  }