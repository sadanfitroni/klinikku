/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';
  var serviceSidebar;

  angular.module('BlurAdmin.pages', [
  ]).config(routeConfig)
  .run(run);


  /** @ngInject */
  function routeConfig($urlRouterProvider, baSidebarServiceProvider,$stateProvider,
    $locationProvider,LoopBackResourceProvider,$ocLazyLoadProvider) {
    $stateProvider
        .state('app',{
          url: '',
          title : 'app',
          abstract: true,
          templateUrl: 'template.html',
        })
        .state('signin',{
            url: '/signin',
            controller: 'LoginController',
            templateUrl: 'auth.html',
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/auth/AuthController.js'); 
              }]
            }
        })
        .state('signout',{
            url: '/signout',
            controller: 'LogoutController',
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/auth/AuthController.js'); 
              }]
            }
        })
        .state('app.dashboard', {
          url: '/dashboard',
          templateUrl: 'app/pages/dashboard/dashboard.html',
          title: 'DashboardController',
          authenticate: true,
          controller: 'DashboardController',
          resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/dashboard/DashboardController.js'); 
              }]
          }
          // sidebarMeta: {
          //   icon: 'ion-android-home',
          //   order: 0,
          // },
        })
        .state('app.homepage',{
          url: '/homepage',
          templateUrl: 'homepage.html',
        })
        .state('app.klinik',{
            url: '/klinik',
            controller: 'KlinikController',
            templateUrl: 'app/pages/masterData/klinik/klinik.list.html',
            title: 'List Klinik',
            // authenticate: true,
            sidebarMeta: {
              order: 0,
            },
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/KlinikController.js'); 
              }]
            }
        })
        .state('app.klinik_new',{
            url: '/klinik/new',
            controller: 'KlinikNewController',
            templateUrl: 'app/pages/masterData/klinik/klinik.new.html',
            title: 'Add Klinik',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/KlinikController.js'); 
              }]
            }
        })
        .state('app.klinik_edit',{
            url: '/klinik/:id/edit',
            controller: 'KlinikEditController',
            templateUrl: 'app/pages/masterData/klinik/klinik.new.html',
            title: 'Edit Klinik',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/KlinikController.js'); 
              }]
            }
        })
        .state('app.klinik_add_langganan',{
            url: '/klinik/:id/add-langganan',
            controller: 'KlinikAddLanggananController',
            templateUrl: 'app/pages/masterData/klinik/langganan.new.html',
            title: 'Add Langganan',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/KlinikController.js'); 
              }]
            }
        })

        .state('app.visit',{
            url: '/visit',
            controller: 'VisitController',
            templateUrl: 'app/pages/masterData/visit/visit.list.html',
            title: 'List Visit',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/VisitController.js'); 
              }]
            }
        })
        .state('app.visit_new',{
            url: '/visit/new',
            controller: 'VisitNewController',
            templateUrl: 'app/pages/masterData/visit/visit.new.html',
            title: 'Add Visit',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/VisitController.js'); 
              }]
            }
        })
        .state('app.visit_edit',{
            url: '/visit/:id/edit',
            controller: 'VisitEditController',
            templateUrl: 'app/pages/masterData/visit/visit.new.html',
            title: 'Edit Visit',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/VisitController.js'); 
              }]
            }
        })
        .state('app.praktek',{
            url: '/praktek',
            controller: 'PraktekController',
            templateUrl: 'app/pages/masterData/praktek/praktek.list.html',
            title: 'List Jadwal Praktek',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/PraktekController.js'); 
              }]
            }
        })
        .state('app.praktek_new',{
            url: '/praktek/new',
            controller: 'PraktekNewController',
            templateUrl: 'app/pages/masterData/praktek/praktek.new.html',
            title: 'Add Jadwal Praktek',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/PraktekController.js'); 
              }]
            }
        })
        .state('app.praktek_edit',{
            url: '/praktek/:id/edit',
            controller: 'PraktekEditController',
            templateUrl: 'app/pages/masterData/praktek/praktek.new.html',
            title: 'Edit Jadwal Praktek',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/PraktekController.js'); 
              }]
            }
        })
        .state('app.register_dokter',{
          url: '/register-dokter',
          controller: 'RegisterDokterController',
          templateUrl: 'app/pages/masterData/klinik/register-dokter.html',
          title: 'Register Dokter',
          resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('app/pages/masterData/KlinikController.js'); 
            }]
          }
        })
        .state('app.entry_pembayaran',{
            url: '/entry-pembayaran',
            controller: 'EntryPembayaranController',
            templateUrl: 'app/pages/pembayaran/pembayaran/entry.pembayaran.html',
            title: 'List Pembayaran',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/pembayaran/PembayaranController.js'); 
              }]
            }
        })
        .state('app.entry_pembayaran_add',{
            url: '/entry-pembayaran/:id',
            controller: 'EntryPembayaranAddController',
            templateUrl: 'app/pages/pembayaran/pembayaran/entry.pembayaran.new.html',
            title: 'Entry Pembayaran',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/pembayaran/PembayaranController.js'); 
              }]
            }
        })
        .state('app.praktek_dokter',{
            url: '/praktek-dokter',
            controller: 'PraktekDokterController',
            templateUrl: 'app/pages/penanganan/praktek_dokter/praktek_dokter.list.html',
            title: 'Daftar Praktek Dokter',
            // authenticate: true,
            sidebarMeta: {
              order: 0,
            },
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/penanganan/PraktekDokterController.js'); 
              }]
            }
        })
        .state('app.praktek_dokter_new',{
            url: '/praktek-dokter/:id',
            controller: 'PraktekDokterNewController',
            templateUrl: 'app/pages/penanganan/praktek_dokter/praktek_dokter.new.html',
            title: 'Form Cek Tensi',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/penanganan/PraktekDokterController.js'); 
              }]
            }
        })
        .state('app.entry_tindakan',{
            url: '/entry-tindakan',
            controller: 'EntryTindakanController',
            templateUrl: 'app/pages/penanganan/entry_tindakan/entry_tindakan.list.html',
            title: 'Entry Tindakan',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/penanganan/EntryTindakanController.js'); 
              }]
            }
        })
        .state('app.entry_tindakan_add',{
            url: '/entry-tindakan/:id',
            controller: 'EntryTindakanAddController',
            templateUrl: 'app/pages/penanganan/entry_tindakan/entry_tindakan.new.html',
            title: 'Entry Tindakan',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/penanganan/EntryTindakanController.js'); 
              }]
            }
        })
        .state('app.edit_jadwal_praktek',{
            url: '/edit-praktek/:id',
            controller: 'EditJadwalPraktekController',
            templateUrl: 'app/pages/penanganan/praktek_dokter/jadwal_praktek.new.html',
            title: 'Edit Jadwal Praktek',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/penanganan/PraktekDokterController.js'); 
              }]
            }
        })
        .state('register',{
            url: '/register',
            controller: 'RegisterController',
            templateUrl: 'reg.html',
            title: 'Register',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/UserController.js'); 
              }]
            }
        })
        .state('app.user',{
            url: '/user',
            controller: 'UserController',
            templateUrl: 'app/pages/rbac/user/user.list.html',
            title: 'List User',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/UserController.js'); 
              }]
            }
        })
        .state('app.user_new',{
            url: '/user/new',
            controller: 'UserNewController',
            templateUrl: 'app/pages/rbac/user/user.new.html',
            title: 'Add User',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/UserController.js'); 
              }]
            }
        })
        .state('app.user_edit',{
            url: '/user/:id/edit',
            controller: 'UserEditController',
            templateUrl: 'app/pages/rbac/user/user.new.html',
            title: 'Edit User',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/UserController.js'); 
              }]
            }
        })
        .state('app.edit_profile',{
          url: '/edit-profile',
          controller: 'UserEditController',
          templateUrl: 'app/pages/rbac/user/user.new.html',
          title: 'Edit Profile',
          // authenticate: true
          resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('app/pages/rbac/UserController.js'); 
            }]
          }
        })
        .state('app.daftar_pasien',{
            url: '/daftar_pasien',
            controller: 'UserController',
            templateUrl: 'app/pages/rbac/user/user.list.html',
            title: 'Daftar Pasien',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/UserController.js'); 
              }]
            }
        })
        .state('app.menu',{
            url: '/menu',
            controller: 'MenuController',
            templateUrl: 'app/pages/rbac/menu/menu.list.html',
            title: 'List Menu',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/MenuController.js'); 
              }]
            }
        })
        .state('app.menu_new',{
            url: '/menu/new',
            controller: 'MenuNewController',
            templateUrl: 'app/pages/rbac/menu/menu.new.html',
            title: 'Add Menu',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/MenuController.js'); 
              }]
            }
        })
        .state('app.menu_edit',{
            url: '/menu/:id/edit',
            controller: 'MenuEditController',
            templateUrl: 'app/pages/rbac/menu/menu.new.html',
            title: 'Edit Menu',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/MenuController.js'); 
              }]
            }
        })
        .state('app.role',{
            url: '/role',
            controller: 'RoleController',
            templateUrl: 'app/pages/rbac/role/role.list.html',
            title: 'Daftar Posisi',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/RoleController.js'); 
              }]
            }
        })
        .state('app.role_new',{
            url: '/role/new',
            controller: 'RoleNewController',
            templateUrl: 'app/pages/rbac/role/role.new.html',
            title: 'Tambah Posisi',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/RoleController.js'); 
              }]
            }
        })
        .state('app.role_edit',{
            url: '/role/:id/edit',
            controller: 'RoleEditController',
            templateUrl: 'app/pages/rbac/role/role.new.html',
            title: 'Sunting Posisi',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/RoleController.js'); 
              }]
            }
        })
        .state('app.poli',{
            url: '/list-poli',
            controller: 'ListPoliController',
            templateUrl: 'app/pages/booking/poli/poli.list.html',
            title: 'Cari Poli',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/booking/ListPoliController.js'); 
              }]
            }
        })
        .state('app.poli_detail',{
            url: '/list-poli/:id/detail',
            controller: 'DetailPoliController',
            templateUrl: 'app/pages/booking/poli/poli.detail.html',
            title: 'Detail Poli',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/booking/ListPoliController.js'); 
              }]
            }
        })
        .state('app.riwayat_pesanan',{
            url: '/riwayat-pesanan',
            controller: 'UserEditController',
            templateUrl: 'app/pages/rbac/user/user.new.html',
            title: 'Riwayat Pesanan',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/UserController.js'); 
              }]
            }
        })
        .state('app.report',{
            url: '/report',
            controller: 'ReportController',
            templateUrl: 'app/pages/report/report.list.html',
            title: 'Report',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/report/ReportController.js'); 
              }]
            }
        })
        .state('app.booking_detail',{
            url: '/detail-pesanan/:id',
            controller: 'DetailBookingController',
            templateUrl: 'app/pages/booking/poli/booking.detail.html',
            title: 'Detail Pesanan',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/booking/ListPoliController.js'); 
              }]
            }
        })

        .state('app.pendapatan_klinik',{
            url: '/pendapatan-klinik',
            controller: 'PendapatanKlinikController',
            templateUrl: 'app/pages/masterData/pendapatan_klinik.html',
            title: 'Pendapatan Klinik',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/PendapatanKlinikController.js'); 
              }]
            }
        })

        .state('app.tagihan_langganan',{
            url: '/tagihan-langganan',
            controller: 'TagihanPembayaranController',
            templateUrl: 'app/pages/pembayaran/tagihan_pembayaran/tagihan_pembayaran.list.html',
            title: 'List Tagihan Langganan',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/pembayaran/TagihanPembayaranController.js'); 
              }]
            }
        })
        .state('app.tagihan_langganan_new',{
            url: '/tagihan-langganan/new',
            controller: 'TagihanPembayaranNewController',
            templateUrl: 'app/pages/pembayaran/tagihan_pembayaran/tagihan_pembayaran.new.html',
            title: 'Add Tagihan Langganan',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/pembayaran/TagihanPembayaranController.js'); 
              }]
            }
        })
        .state('app.tagihan_langganan_edit',{
            url: '/tagihan-langganan/:id/edit',
            controller: 'TagihanPembayaranEditController',
            templateUrl: 'app/pages/pembayaran/tagihan_pembayaran/tagihan_pembayaran.new.html',
            title: 'Edit Tagihan Langganan',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/pembayaran/TagihanPembayaranController.js'); 
              }]
            }
        })
        ;
    $urlRouterProvider.otherwise('homepage');
    serviceSidebar = baSidebarServiceProvider;  
    // serviceSidebar.addStaticItem(
    // { title: 'Dashboard',
    //     icon: 'ion-grid',
    //     stateRef: 'app.dashboard',        
    // }) 

     // baSidebarServiceProvider.addStaticItem(
     //  { title: 'Dashboard',
     //    icon: 'ion-grid',
     //    stateRef: 'Dashboard',        
     //  })
  }
  
  function run( $rootScope, $state, $stateParams, LoopBackAuth, toastr,$http,
  $localStorage, $timeout,$location) {
  // $rootScope.address = "localhost";
  $rootScope.address = "117.53.47.25";
  $rootScope.port = {};
  $rootScope.port.fe = "3004";
  $rootScope.port.rbac = "3551";
  $rootScope.port.master_data = "3555";
  $rootScope.port.penanganan = "3560";
  $rootScope.port.booking = "3565";
  $rootScope.port.report = "3569";
  $rootScope.port.pembayaran = "3573";
  // console.log("element",angular.element(document)[0].styleSheets);

  $http.post("http://hirata.id:3003/api/users/login",{
          "email":"superadmin@superadmin.com",
          "password":"aaa"
  }).then(function onSuccess(response){
          $rootScope.token=response.data.id;
  }).catch(function onError(response){
          console.log(response);
  });
  
  $rootScope.list_menu = [];
  $rootScope.serviceSidebar = serviceSidebar; 
  $rootScope.appMenus = [];
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
  // console.log(serviceSidebar.getStaticItems)
  $rootScope.$on('$stateChangeStart', function(event, next) {
      // redirect to login page if not logged in
      // console.log("next",$localStorage[LoopBackAuth.accessTokenId]);
      $rootScope.currentUser = $localStorage['session'];
      $rootScope.userRole = $localStorage['role'];
      
      // console.log('stateChangeStart: ', event, $rootScope.currentUser,$rootScope.userRole, next);
      if($rootScope.currentUser){
        // console.log("cek user");
        if(next.url == "/signin"){
          $state.go('app.homepage');
        }

        // console.log("existing menu", serviceSidebar.getStaticItems())
        $rootScope.list_menu = $localStorage["menu"];
        if($rootScope.list_menu){
          for(let menu of $rootScope.list_menu){
            let menu_sidebar = {};
            menu_sidebar.title = menu.label;
            menu_sidebar.icon = menu.icon;
            menu_sidebar.stateRef = menu.url;
            // console.log(menu_sidebar, serviceSidebar.getStaticItems)
            if(checkMenu(menu_sidebar))
              serviceSidebar.addStaticItem(menu_sidebar);
          }
          // console.log("Menu user", $rootScope.list_menu);
        }

        if($rootScope.currentUser.session == 1){
          console.log("has login")
          // $rootScope.currentUser.session = 0;
          $localStorage.$reset();
          $rootScope = {};
          return $state.go('signout');
        }
      }
      else if(next.url != "/signin" && next.url != "/register" ){
          $rootScope.goToLogin = 0;
          console.log('goto login page',LoopBackAuth, next)
          event.preventDefault(); //prevent current page from loading
          $state.go('signin');
      }
  });

  function checkMenu(menu) {
    for(let exist_menu of serviceSidebar.getStaticItems()){
      // console.log(exist_menu, " : ", menu)
      if(exist_menu.title == menu.title && exist_menu.stateRef == menu.stateRef)
        return false
    }

    return true
  }

  $rootScope.appModel = [
    {name: 'User', model : 'user'},
    {name: 'Role', model : 'role' },
    {name: 'Menu SideBar', model : 'menu_sidebar' },
    {name: 'Medical Record', model : 'medrec' },
    {name: 'Praktek', model : 'praktek' },
    {name: 'Resep', model : 'resep' },
    {name: 'Visit', model : 'visit' },
    {name: 'Detail Pasien', model : 'detail_pasien' },
    {name: 'Generic Parameter', model : 'generic_param' },
    {name: 'Riwayat Praktek', model : 'history_praktek' },
    {name: 'Jadwal Dokter', model : 'jadwal_praktek' },
    {name: 'Katalog Tarif', model : 'katalog_jasa' },
    {name: 'Provinsi', model : 'provinsi' },
    {name: 'Kabupaten', model : 'kabupaten_kota' },
    {name: 'Kecamatan', model : 'kecamatan' },
    {name: 'Kelurahan', model : 'kelurahan' },
    {name: 'RS Dokter', model : 'klinik_dokter' },
    {name: 'RS User', model : 'klinik_user' },
    {name: 'RS', model : 'klinik' },
    {name: 'Menu to Model', model : 'menu_model' },
    {name: 'Pembayaran', model : 'pembayaran' },
    {name: 'Penjamin Pasien', model : 'penjamin_pasien' },
    {name: 'Stock', model : 'stock' },

  ];

  $rootScope.aclAccessType = [
    {label:'WRITE',value:'WRITE'},
    {label:'READ',value:'READ'},
    {label: 'MENU',value: 'MENU'},
  ];

}



})();
