angular
.module('BlurAdmin').controller('PraktekDokterController', PraktekDokterController);

angular
.module('BlurAdmin').controller('PraktekDokterNewController', PraktekDokterNewController);

angular
.module('BlurAdmin').controller('EditJadwalPraktekController', EditJadwalPraktekController);

function PraktekDokterController($q, $scope, $rootScope, $state, DTOptionsBuilder, DTColumnBuilder, 
  $routeParams, $route, $filter, $location, toastr, $timeout, $http) {
	$scope.filterOptions = {
		filterText: "",
		poli_name : "",
		poli_type : "",
		useExternalFilter: false,
	};
	$scope.filterParams = {};
	$scope.filter = {};
	$scope.pagingOptions = {
		pageSizes: [10, 25, 50],
		pageSize: 10,
		currentPage: 1
	};
	$scope.open = open;
	$scope.datepicker = {};
	$scope.datepicker.opened = false;
	var dateToday = new Date();
    var sixDaysLater = new Date();
    sixDaysLater.setDate(dateToday.getDate()+6);
    $scope.datepicker.options = {
        minDate: dateToday.getTime(),
        maxDate: sixDaysLater.getTime(),
    };
	function open() {
        // console.log($scope.datepicker.opened);
        $scope.datepicker.opened = true;
    }
    $scope.filterDataPraktek = function (){
    	$scope.historyPraktekData = angular.copy($scope.unfilteredHistoryPraktekData);
    	if($scope.filter.dokter!=null)
    		$scope.historyPraktekData=$scope.historyPraktekData.filter(data=>data.fullname==$scope.filter.dokter.fullname);
    	// if($scope.historyPraktekData[0]!=null){
    		// console.log($scope.historyPraktekData.getDate());
    		// console.log(new Date($scope.historyPraktekData[0].open_time));
    	// }
    	if($scope.filter.tanggal!=null)
    		$scope.historyPraktekData=$scope.historyPraktekData.filter(data=>new Date(data.open_time).getDate()==$scope.filter.tanggal.getDate());
    	console.log($scope.filter);
    }
	$http.post(
        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-dokter-by-klinik",
        {klinik_id : $scope.currentUser.fk_klinik.toString()},
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
        // console.log(result);
        $scope.dokters = result.data.dokters;
        // $scope.usersMap={};
        // if(result.data.dokters!=null){
        //   result.data.dokters.forEach(function(user){
        //       $scope.usersMap[user.id]=user;
        //   });
        // }
        // console.log($scope.dokters);
      }).catch(function onError(result){
        console.log('get all dokters error',result)
        toastr.warning('Get data error', 'Warning');
      })

	$scope.setPagingData = function setPagingData(data) {
		$scope.listData = data;
		if (!$scope.$$phase) {
			$scope.$apply();
		}
	};

	$scope.praktek_dokter = function praktek_dokter(visit_id){
		console.log(visit_id)
		$location.path('/praktek-dokter/' + visit_id );
	}

    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (angular.isDefined($scope.filterTimeout)) {
            $timeout.cancel($scope.filterTimeout);
        }
        $scope.filterTimeout = $timeout(function () {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.filterOptions);
            }
        }, 500);
    }, true);

    $scope.filterData = function filterData(){
    	console.log("filter data")
    	if($scope.filterParams.poli_name != null || $scope.filterParams.poli_type != null){
	    	$scope.filterOptions.useExternalFilter = true;
    	}
    	else
    		$scope.filterOptions.useExternalFilter = false;

    	if($scope.filterParams.poli_name != null)	
	    	$scope.filterOptions.poli_name = $scope.filterParams.poli_name;
	    else
	    	$scope.filterOptions.poli_name = "";

	    if($scope.filterParams.poli_type != null)
	    	$scope.filterOptions.poli_type = $scope.filterParams.poli_type;
	    else
	    	$scope.filterOptions.poli_type = "";

    		
 		// $scope.getPagedDataAsync($scope.filterOptions)
 		console.log($scope.filterOptions);
    }

	$scope.getPagedDataAsync = function getPagedDataAsync(filterOptions) {
		setTimeout(function () {
		  var data = [];
		  // data.push({id:1})
		  // $scope.setPagingData(data);
		  // $scope.grid.core.refresh();

		  if($scope.myData.length == 0){
		  	$http.post(
			    "http://" + $rootScope.address +":"+ $rootScope.port.penanganan + "/get-pasien-list-perawat",
			    {perawat_id : String($scope.currentUser.id)},
			    {headers: {"Content-Type": "application/x-www-formurlencoded"}}
			  ).then(function onSuccess(result){
			    console.log("search success",result);
			    if(result.data.pasien!=null){
				    for(i = 0; i<result.data.pasien.length; i++){
				    	let pasien = {};
				    	pasien.id = result.data.visit[i].id;
						pasien.booknum = result.data.visit[i].booknum;
						pasien.fullname = result.data.pasien[i].fullname;
						pasien.poli_name = result.data.jadwal[i].poli_name;
						pasien.poli_type = result.data.jadwal[i].poli_type;
						pasien.status = result.data.visit[i].status;
				    	data.push(pasien);
				    }
				    $scope.myData = angular.copy(data);
				    $scope.setPagingData(data);
			    }
			  }).catch(function onError(result){
			    console.log('search error',result)
			    $scope.setPagingData(data);
			    toastr.info('Data antrian masih kosong', 'Information');
			  })
		  }
		  if($scope.filterOptions.useExternalFilter){
		  	let listPasien = angular.copy($scope.myData)
		  	for(pasien of listPasien){
		  		if(pasien.poli_name.includes($scope.filterOptions.poli_name) && pasien.poli_type.includes($scope.filterOptions.poli_type))
		  			data.push(pasien)
		  	}
		  	console.log("result filter")
		  	$scope.setPagingData(data);
		  }else{
		  	data = angular.copy($scope.myData)
		  	$scope.setPagingData(data);
		  }
		   
		}, 100);
	};

	var removeRowTemplate ='<div ng-if="row.entity.status==\'booking\'" class="button-wrapper text-center" style="margin-top:10%">'+
							'<button ng-click="grid.appScope.praktek_dokter(row.entity.id)" class="btn btn-primary btn-xs"><i class="ion-forward"></i></button></div>'+
							'<div ng-if="row.entity.status!=\'booking\'" class="button-wrapper text-center" style="margin-top:10%">'+
						    '<button data-toggle="tooltip" data-placement="bottom" title="Sudah cek tensi" class="btn btn-primary btn-xs"><i class="ion-cross"></i></button></div>';
	$scope.gridOptions = { 
		data: 'listData', enablePaging: true, rowHeight: 36, headerRowHeight: 50,
    	paginationPageSizes : $scope.pagingOptions.pageSizes, 
    	paginationPageSize: $scope.pagingOptions.pageSize,
	    columnDefs:[
		    {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"5%"},
		    {displayName:'Nama Pasien',field:'fullname',width:"*"},
		    {displayName:'Nomor antrian',field:'booknum',width:"*"},
		    {displayName:'Nama Poli',field:'poli_name',width:"*"},
		    {displayName:'Tipe Poli',field:'poli_type',width:"*"},
		    {displayName:'Status',field:'status',width:"*"},
	]};


	$scope.gridOptions.onRegisterApi = function(gridApi) {
	  $scope.grid = gridApi;
	  console.log("api 1",$scope.grid)
	};

	$scope.filterOptionsHistoryPraktek = {
		filterText: "",
		poli_name : "",
		poli_type : "",
		useExternalFilter: false,
	};
	$scope.filterParamsHistoryPraktek = {};
	$scope.filterParamsHistoryPraktek = {};
	$scope.pagingOptionsHistoryPraktek = {
		pageSizes: [10, 25, 50],
		pageSize: 10,
		currentPage: 1
	};

	$scope.setPagingDataHistoryPraktek = function setPagingDataHistoryPraktek(data) {
		$scope.unfilteredHistoryPraktekData = data;
		$scope.filterDataPraktek();
		if (!$scope.$$phase) {
			$scope.$apply();
		}
	};

	$scope.getPagedDataAsyncHistoryPraktek = function getPagedDataAsyncHistoryPraktek(filterOptions) {
		setTimeout(function () {
		  var data = [];
		  // data.push({})
		  $scope.setPagingDataHistoryPraktek(data);
		  // /get-detail-history-praktek-by-klinik
		  $http.post(
			    "http://" + $rootScope.address +":"+ $rootScope.port.penanganan + "/get-detail-history-praktek-by-klinik",
			    {klinik_id : String($scope.currentUser.fk_klinik)},
			    {headers: {"Content-Type": "application/x-www-formurlencoded"}}
			  ).then(function onSuccess(result){
			    // console.log("search success",result);
			    if(result.data.history_praktek!=null){
				    for(i = 0; i<result.data.history_praktek.length; i++){
				    	let history_data = {};
				    	history_data.id = result.data.history_praktek[i].id;
				    	history_data.fullname = result.data.dokter[i].fullname;
						history_data.open_time = result.data.history_praktek[i].open_time;
						history_data.close_time = result.data.history_praktek[i].close_time;
						history_data.status = result.data.history_praktek[i].status;
						history_data.remark = result.data.history_praktek[i].remark;
						history_data.poli_name = result.data.jadwal[i].poli_name;
						history_data.poli_type = result.data.jadwal[i].poli_type;
						history_data.jadwal_id = result.data.jadwal[i].id;
				    	data.push(history_data);
				    }
			    }
			    console.log("data history praktek",data);
			    $scope.myDataHistoryPraktek = angular.copy(data);
			    $scope.setPagingDataHistoryPraktek(data);
			  }).catch(function onError(result){
			    console.log('search error',result)
			    $scope.setPagingData(data);
			    toastr.info('Data antrian masih kosong', 'Information');
			  })
					   
		}, 100);
	};

	var editHistoryDataTemplate ='<div class="button-wrapper text-center" style="margin-top:10%">'+
								'<button ng-click="grid.appScope.editHistoryPraktek(row.entity.id)" class="btn btn-primary btn-xs"><i class="ion-forward"></i></button></div>';
	$scope.gridOptionsHistoryPraktek = { 
		data: 'historyPraktekData', enablePaging: true, rowHeight: 36, headerRowHeight: 50,
    	paginationPageSizes : $scope.pagingOptionsHistoryPraktek.pageSizes, 
    	paginationPageSize: $scope.pagingOptionsHistoryPraktek.pageSize,
	    columnDefs:[
		    {displayName:'Action',field:'remove',cellTemplate: editHistoryDataTemplate,width:"5%"},
		    {displayName:'Nama Dokter',field:'fullname',width:"*"},
		    {displayName:'Nama Poli',field:'poli_name',width:"*"},
		    {displayName:'Nama Jenis',field:'poli_type',width:"*"},
		    {displayName:'Jam Buka',field:'open_time',width:"*"},
		    {displayName:'Jam Tutup',field:'close_time',width:"*"},
		    {displayName:'Status',field:'status',width:"*"},
		    {displayName:'Remark',field:'remark',width:"*"},
	]};

	$scope.gridOptionsHistoryPraktek.onRegisterApi = function(gridApi) {
	  $scope.gridHistoryPraktek = gridApi;
	  console.log("api dua",$scope.gridHistoryPraktek)
	};

	$scope.editHistoryPraktek = function editHistoryPraktek(history_praktek_id) {
		console.log("id history praktek", history_praktek_id);
		$location.path('/edit-praktek/' + history_praktek_id );
	}


	$scope.init = function init(){
		$scope.myData = [];
		$scope.filterParams = {};
		$scope.getPagedDataAsync($scope.filterOptions)
		$scope.getPagedDataAsyncHistoryPraktek($scope.filterOptionsHistoryPraktek)
	}

	$scope.init()
}

function PraktekDokterNewController($scope, $rootScope, $stateParams,
 $state, toastr, $http, $location, Visit){
	// console.log($stateParams)
	$scope.init = function init(){
		$scope.praktek_dokter = {};
		Visit.findById({id:Number($stateParams.id)},function(response){
			$scope.praktek_dokter.keluhan=response.keluhan;
		})
	}

	$scope.init();

	$scope.save = function save(praktek_dokter) {
		// console.log("save",praktek_dokter, "id visit", $stateParams.id);
		$http.post(
	        "http://" + $rootScope.address +":"+ $rootScope.port.penanganan + "/cek-tensi",
	        { 	visit_id : $stateParams.id , 
	        	medrec : {
	        		id : 0,
	        		code : $stateParams.id,
	        		fk_pasien : 0,
	        		fk_visit : 0,
	        		tensi : praktek_dokter.tensi,
	        		berat_badan : praktek_dokter.berat_badan,
	        		keluhan : praktek_dokter.keluhan,
	        		gejala : "",
	        		indikasi : "",
	        		diagnosa : "",
	        		fk_dokter : 0,
	        		fk_klinik : 0
	        	}
	        },
	        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
	        console.log("cek tensi success",result);
	        $scope.praktek_dokter = {};
	        $scope.go("/praktek-dokter");
        }).catch(function onError(result){
	        console.log('search error',result)
	        toastr.warning('Process Failed', 'Warning');
        })
	}

	$scope.go = function go(url) {
    $location.path(url);
  }
}

function EditJadwalPraktekController($scope, $rootScope, $stateParams,
 $state, toastr, $http, $location){

	$scope.init = function init(){
		$scope.jadwal_praktek = {};
		$scope.dokter = [];
		$scope.status = ["Buka","Tutup"]
		$http.post(
	        "http://" + $rootScope.address +":"+ $rootScope.port.penanganan + "/get-history-praktek-by-id",
	        { 	
	        	history_jadwal_id : $stateParams.id
	        },
	        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
	        console.log("history_praktek",result);
	        let date;
	        date = new Date(result.data.jadwal.open_time);
	        date.setHours(date.getHours()-7);
	        $scope.jadwal_praktek.open_time = date;
	        date = new Date(result.data.jadwal.close_time);
            date.setHours(date.getHours()-7);
	        $scope.jadwal_praktek.close_time = date;
	        $scope.jadwal_praktek.status = result.data.jadwal.status;
	        $scope.jadwal_praktek.remark = result.data.jadwal.remark;
	        $scope.jadwal_praktek.fk_jadwal_praktek = result.data.jadwal.fk_jadwal_praktek;
	        $scope.jadwal_praktek.id = parseInt($stateParams.id);

	        $http.post("http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-id",
        		{ id : String(result.data.jadwal.Fk_dokter)},
        		{headers: {"Content-Type": "application/x-www-formurlencoded"}}
    		).then(function onSuccess(result){
    			console.log("dokter",result)
    			$scope.jadwal_praktek.dokter = result.data.user;
    		}).catch(function onError(result){
    			console.log("error", result);
    		})
        }).catch(function onError(result){
	        console.log('search error',result)
	        toastr.warning('Process Failed', 'Warning');
        })	

        $http.post(
	        "http://" + $rootScope.address +":"+ $rootScope.port.penanganan + "/get-dokter-by-klinik",
	        { 	
	        	klinik_id : String($scope.currentUser.fk_klinik)
	        },
	        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
	        $scope.dokter = result.data.dokter;
	        console.log("list dokter",$scope.dokter);  
        }).catch(function onError(result){
	        console.log('search error',result)
	        toastr.warning('Process Failed', 'Warning');
        })	
    }

    $scope.save = function save(jadwal){
    	jadwal.fk_dokter = $scope.jadwal_praktek.dokter.id;
    
    	console.log("data-save", jadwal)

    	 $http.post(
	        "http://" + $rootScope.address +":"+ $rootScope.port.penanganan + "/update-history-praktek",
	        { jadwal : jadwal },
	        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
	        console.log("list dokter",$scope.dokter);  
	        toastr.success('Update data berhasil');
	        $scope.go("/praktek-dokter")
        }).catch(function onError(result){
	        console.log('search error',result)
	        toastr.warning('Process Failed', 'Warning');
        })	
    }

	$scope.init();

	$scope.go = function go(url) {
	    $location.path(url);
	}
}