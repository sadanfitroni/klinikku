/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.penanganan', [
      'BlurAdmin.pages'
      // 'BlurAdmin.pages.charts.amCharts',
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider,$ocLazyLoadProvider) {
    $stateProvider
        .state('app.praktek_dokter',{
            url: '/praktek-dokter',
            controller: 'PraktekDokterController',
            templateUrl: 'app/pages/penanganan/praktek_dokter/praktek_dokter.list.html',
            title: 'List Pasien',
            // authenticate: true,
            sidebarMeta: {
              order: 0,
            },
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/penanganan/PraktekDokterController.js'); 
              }]
            }
        })
        .state('app.praktek_dokter_new',{
            url: '/praktek-dokter/:id',
            controller: 'PraktekDokterNewController',
            templateUrl: 'app/pages/penanganan/praktek_dokter/praktek_dokter.new.html',
            title: 'Form Cek Tensi',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/penanganan/PraktekDokterController.js'); 
              }]
            }
        })
        // .state('app.praktek_dokter',{
        //     url: '/praktek-dokter',
        //     controller: 'PraktekDokterController',
        //     templateUrl: 'app/pages/penanganan/view/praktek_dokter.html',
        //     title: 'Cek Tensi',
        //     // authenticate: true,
        //     sidebarMeta: {
        //       order: 0,
        //     },
        // })
        .state('app.entry_tindakan',{
            url: '/entry-tindakan',
            controller: 'EntryTindakanController',
            templateUrl: 'app/pages/penanganan/entry_tindakan/entry_tindakan.list.html',
            title: 'Entry Tindakan',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/penanganan/EntryTindakanController.js'); 
              }]
            }
        })
        .state('app.entry_tindakan_add',{
            url: '/entry-tindakan/:id',
            controller: 'EntryTindakanAddController',
            templateUrl: 'app/pages/penanganan/entry_tindakan/entry_tindakan.new.html',
            title: 'Entry Tindakan',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/penanganan/EntryTindakanController.js'); 
              }]
            }
        })
        .state('app.edit_jadwal_praktek',{
            url: '/edit-praktek/:id',
            controller: 'EditJadwalPraktekController',
            templateUrl: 'app/pages/penanganan/praktek_dokter/jadwal_praktek.new.html',
            title: 'Edit Jadwal Praktek',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/penanganan/PraktekDokterController.js'); 
              }]
            }
        })
        // .state('app.entry_tindakan',{
        //     url: '/entry-tindakan',
        //     controller: 'EntryTindakanController',
        //     templateUrl: 'app/pages/penanganan/view/entry_tindakan.html',
        //     title: 'Entery Tindakan',
        //     // authenticate: true
        // })
  }

})();
