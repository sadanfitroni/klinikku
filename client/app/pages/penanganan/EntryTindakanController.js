angular
.module('BlurAdmin').controller('EntryTindakanController', EntryTindakanController);
angular
.module('BlurAdmin').controller('EntryTindakanAddController', EntryTindakanAddController);

// angular
// .module('BlurAdmin').controller('CekTensiNewController', CekTensiNewController);


function EntryTindakanController($q, $scope, $rootScope, $state, DTOptionsBuilder, DTColumnBuilder, 
  $routeParams, $route, $filter, $location, toastr, $timeout, $http) {
	$scope.filterOptions = {
		filterText: "",
		poli_name : "",
		poli_type : "",
		status : "",
		useExternalFilter: false
	};

	$scope.status = [ {val : "Sudah Cek Tensi"},
	  {val : "Belum Cek Tensi"},
	  {val : "Sudah Entry Tindakan"},
	  {val : "Bayar"}
    ];
	$scope.jadwal_praktek={};

	$scope.filterParams = {};
	$scope.showFilter = true;
	$scope.totalServerItems = 0;
	//$scope.totalServerItem = 0;
	$scope.searchTextEXS = '';
	$scope.pagingOptions = {
		pageSizes: [10, 25, 50],
		pageSize: 10,
		currentPage: 1
	};
	// sort
	$scope.sortOptions = {
		fields: ["code"],
		directions: ["ASC"]
	};

	$scope.setPagingData = function setPagingData(data) {
		$scope.listData = data;
		if (!$scope.$$phase) {
			$scope.$apply();
		}
	};


	$scope.filterData = function filterData(){
    	console.log("filter data",$scope.filterParams)
    	if($scope.filterParams.poli_name != null || $scope.filterParams.poli_type != null || $scope.filterParams.status != null ||
    		$scope.filterParams.dokter!=null){
	    	$scope.filterOptions.useExternalFilter = true;
    	}
    	else
    		$scope.filterOptions.useExternalFilter = false;

    	if($scope.filterParams.poli_name != null)	
	    	$scope.filterOptions.poli_name = $scope.filterParams.poli_name;
	    else
	    	$scope.filterOptions.poli_name = ""

	    if($scope.filterParams.poli_type != null)
	    	$scope.filterOptions.poli_type = $scope.filterParams.poli_type;
	    else
	    	$scope.filterOptions.poli_type = ""

    	if($scope.filterParams.status != null)
    		$scope.filterOptions.status = $scope.filterParams.status.val;
		else
			$scope.filterOptions.status = "";	

		if($scope.filterParams.dokter!=null)
			$scope.filterOptions.dokter = $scope.filterParams.dokter.fullname;
		else
			$scope.filterOptions.dokter = "";			
 		// $scope.getPagedDataAsync($scope.filterOptions)
 		console.log($scope.filterOptions);
    }

	$scope.entry_tindakan = function entry_tindakan(visit_id){
		// console.log(visit_id)
		$location.path('/entry-tindakan/' + visit_id );
	}

    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (angular.isDefined($scope.filterTimeout)) {
            $timeout.cancel($scope.filterTimeout);
        }
        $scope.filterTimeout = $timeout(function () {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.filterOptions);
            }
        }, 500);
    }, true);


	$scope.getPagedDataAsync = function getPagedDataAsync(filterOptions) {
		setTimeout(function () {
		  var data = [];
		  // data.push({id:1})
		  // $scope.setPagingData(data);
		  // $scope.grid.core.refresh();
		  if($scope.myData.length == 0){
		  	if($scope.currentUser.fk_role==25){
		  		url="/get-pasien-list-doctor";
		  		postData={doctor_id : String($scope.currentUser.id)};
		  	}else
		  	if($scope.currentUser.fk_role==24){
		  		url="/get-pasien-list-perawat";
		  		postData={perawat_id : String($scope.currentUser.id)};
		  	}
			$http.post(
		        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-dokter-by-klinik",
		        {klinik_id : $scope.currentUser.fk_klinik.toString()},
		        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
		      ).then(function onSuccess(result){
		        // console.log(result);
		        $scope.dokter = result.data.dokters;
		        $scope.dokterMap={};
		        if(result.data.dokters!=null){
		          result.data.dokters.forEach(function(user){
		              $scope.dokterMap[user.id]=user;
		          });
		        }
		        // console.log($scope.dokters);
			  	$http.post(
				    "http://" + $rootScope.address +":"+ $rootScope.port.penanganan + url,
				    postData,
				    {headers: {"Content-Type": "application/x-www-formurlencoded"}}
				  ).then(function onSuccess(result){
				    if(!result.data.pasien)
				    	result.data.pasien = [];
				    for(i = 0; i<result.data.pasien.length; i++){
				    	let pasien = {};
				    	pasien.id = result.data.visit[i].id;
						pasien.booknum = result.data.visit[i].booknum;
						pasien.fullname = result.data.pasien[i].fullname;
						pasien.poli_name = result.data.jadwal[i].poli_name;
						pasien.poli_type = result.data.jadwal[i].poli_type;
						pasien.status = result.data.visit[i].status;
						if($scope.currentUser.fk_role==24)
							pasien.dokter = $scope.dokterMap[result.data.visit[i].fk_dokter].fullname;
				    	data.push(pasien);
				    }
				    // console.log(data);
				    $scope.myData = angular.copy(data);
				    if($scope.myData.length == 0 )
					    toastr.info('Data antrian masih kosong', 'Information');
				    $scope.setPagingData(data);
				  }).catch(function onError(result){
				    console.log('search error',result)
				    $scope.setPagingData(data);
				    toastr.info('Data antrian masih kosong', 'Information');
				  })
		      }).catch(function onError(result){
		        console.log('get all dokters error',result)
		        toastr.warning('Get data error', 'Warning');
		      })
		  }else if($scope.filterOptions.useExternalFilter){
		  	let listPasien = angular.copy($scope.myData)
		  	for(pasien of listPasien){
		  		if( pasien.poli_name.includes($scope.filterOptions.poli_name) 
		  			&& pasien.poli_type.includes($scope.filterOptions.poli_type)
		  			&& pasien.status.includes($scope.filterOptions.status)
		  			&& pasien.dokter.includes($scope.filterOptions.dokter)
		  		   )
		  			data.push(pasien)
		  	}
		  	console.log("result filter")
		  	$scope.setPagingData(data);

		  }else{
		  	data = angular.copy($scope.myData)
		  	$scope.setPagingData(data);
		  }
		   
		}, 100);
	};

	$scope.entry_tindakan_stat = function entry_tindakan_stat(status){
		if (status == "Sudah Cek Tensi")
			return true
		else
			return false
	}

	var removeRowTemplate = '<div ng-if="grid.appScope.entry_tindakan_stat(row.entity.status)" class="button-wrapper text-center" style="margin-top:10%">'+
						    '<button ng-click="grid.appScope.entry_tindakan(row.entity.id)" class="btn btn-primary btn-xs"><i class="ion-forward"></i></button></div>' +
						    '<div ng-if="!grid.appScope.entry_tindakan_stat(row.entity.status)" class="button-wrapper text-center" style="margin-top:10%">'+
						    '<button data-toggle="tooltip" data-placement="bottom" title="Belum Cek Tensi" class="btn btn-primary btn-xs"><i class="ion-cross"></i></button></div>';
	$scope.gridOptions = { 
		data: 'listData', enablePaging: true, rowHeight: 36, headerRowHeight: 50,
    	paginationPageSizes : $scope.pagingOptions.pageSizes, 
    	paginationPageSize: $scope.pagingOptions.pageSize,
    columnDefs:[
	    {displayName:'Action',field:'action',cellTemplate: removeRowTemplate,width:"7%"},
	    {displayName:'Nama Pasien',field:'fullname',width:"*"},
	    {displayName:'Nomer Antrian',field:'booknum',width:"*"},
	    {displayName:'Nama Poli',field:'poli_name',width:"*"},
	    {displayName:'Tipe Poli',field:'poli_type',width:"*"},
	    {displayName:'Dokter',field:'dokter',width:"*",visible:$scope.currentUser.fk_role==24},
	    {displayName:'Status',field:'status',width:"*"},
	     
	]};

	$scope.gridOptions.onRegisterApi = function(gridApi) {
	  $scope.grid = gridApi;
	};


	$scope.init = function init(){
		$scope.myData = [];
		$scope.role = $rootScope.userRole.name;
		$scope.getPagedDataAsync($scope.filterOptions)
	}
	$scope.init()
}


function EntryTindakanAddController($q,$scope, $rootScope, $state, $http,$stateParams,
	$routeParams, $route, $filter, $location, toastr, $timeout, DTOptionsBuilder, DTColumnBuilder, Medrec, 
	Visit, History_praktek, Jadwal_praktek){
	// $rootScope.address = "localhost";
	$scope.filterOptions = {
		filterText: "",
		useExternalFilter: false
	};
	$scope.uesr = {};
	$scope.showFilter = true;
	//$scope.totalServerItem = 0;
	$scope.pagingOptionsMedrec = {
		pageSizes: [10, 25, 50],
		pageSize: 10,
		currentPage: 1
	};
	
	$scope.gridOptionsMedrec = { 
		data: 'medrecData', enablePaging: true, rowHeight: 36, headerRowHeight: 50,
    	paginationPageSizes : $scope.pagingOptionsMedrec.pageSizes, 
    	paginationPageSize: $scope.pagingOptionsMedrec.pageSize,
    	columnDefs:[
	    // {displayName:'Nama Pasien',field:'fullname',width:"*"},
	    {displayName:'Tanggal Pemeriksaan', field : 'date',width : "*"},
	    {displayName:'Tensi',field:'tensi',width:"*"},
	    {displayName:'Berat badan',field:'berat_badan',width:"*"},
	    {displayName:'keluhan',field:'keluhan',width:"*"},
	    {displayName:'Gejala',field:'gejala',width:"*"},
	    {displayName:'Indikasi',field:'indikasi',width:"*"},
	    {displayName:'Diagnosa',field:'diagnosa',width:"*"},
	    // {displayName:'Tanggal', field : 'date',width : "*"}

	]};

	$scope.pagingOptionsResep = {
		pageSizes: [10, 25, 50],
		pageSize: 10,
		currentPage: 1
	};
	
	$scope.gridOptionsResep = { 
		data: 'resepData', enablePaging: true, rowHeight: 36, headerRowHeight: 50,
    	paginationPageSizes : $scope.pagingOptionsResep.pageSizes, 
    	paginationPageSize: $scope.pagingOptionsResep.pageSize,
    	columnDefs:[
    	{displayName:'Tanggal Pemberian Obat', field : 'date',width : "*"},
    	{displayName:'Obat', field : 'obat',width : "*"},
    	{displayName:'Dosis', field : 'dosis',width : "*"},
    	{displayName:'Kuantitas', field : 'quantity',width : "*"},
	   

	]};

	$scope.gridOptionsMedrec.onRegisterApi = function(gridApi) {
	  $scope.gridMerdec = gridApi;
	};

	$scope.gridOptionsResep.onRegisterApi = function(gridApi) {
	  $scope.gridResep = gridApi;
	};

	function getDateString(d){
        var datestring = ("0" + d.getDate()).slice(-2) + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" +
        d.getFullYear() + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
        return datestring;
    }

	$scope.getPagedDataAsyncMedrec = function getPagedDataAsync(filterOptions) {
		setTimeout(function () {
			var data = [];
			Visit.findOne({
				filter:
					{
						where:{fk_visit:$stateParams.id},
						include:['pasien']
					}
			},function(response){
				let pasien=response.pasien.fullname;
				Medrec.find({
					filter:{
						where:{and:[{fk_pasien:response.fk_pasien},{fk_dokter:response.fk_dokter}]},
						include:['visit']
					}
				},function(response){
					for(i = 0; i<response.length; i++){
				    	let medrec = {};
				    	medrec.fullname = pasien.fullname;
				    	medrec.id = response[i].id;
				    	medrec.tensi = response[i].tensi;
				    	medrec.berat_badan = response[i].berat_badan;
				    	medrec.keluhan = response[i].keluhan;
				    	medrec.gejala = response[i].gejala;
				    	medrec.indikasi = response[i].indikasi;
				    	medrec.diagnosa = response[i].diagnosa;
				    	medrec.date = response[i].visit.date;
				    	medrec.date=getDateString(new Date(medrec.date));
				    	data.push(medrec);
				    }
				    $scope.setPagingDataMedrec(data);
				},function(result){
					console.log('search error',result)
			    	toastr.warning('Data tidak memiliki medical record', 'Warning');
				});
			});
		}, 100);
	};

	$scope.setPagingDataMedrec = function setPagingData(data) {
		$scope.medrecData = data;
		if (!$scope.$$phase) {
			$scope.$apply();
		}
	};

	$scope.getPagedDataAsyncResep = function getPagedDataAsync(filterOptions) {
		setTimeout(function () {
		var data = [];
	  	$http.post(
		    "http://" + $rootScope.address +":"+ $rootScope.port.penanganan + "/get-list-resep-detail-by-pasien-dokter",
		    {visit_id : $stateParams.id , doctor_id : String($scope.currentUser.id)},
		    {headers: {"Content-Type": "application/x-www-formurlencoded"}}
		  ).then(function onSuccess(result){
		    if(result.data.resep!=null){
			    for(i = 0; i<result.data.resep.length; i++){
		    		let resep = {};
					resep.obat = result.data.resep[i].obat;
					resep.dosis = result.data.resep[i].dosis;
					resep.quantity = result.data.resep[i].quantity;
					resep.obat = result.data.resep[i].obat;
					resep.date = result.data.visit[i].date;
			    	data.push(resep);
			    }
			    $scope.setPagingDataResep(data);
		    }
		  }).catch(function onError(result){
		    console.log('search error',result)
		    toastr.warning('Data tidak memiliki medical record', 'Warning');
		  })
		}, 100);
	};

	$scope.setPagingDataResep = function setPagingData(data) {
		$scope.resepData = data;
		if (!$scope.$$phase) {
			$scope.$apply();
		}
	};

	$scope.init = function init(){
		// $scope.formShowStatus = false;
		$scope.entry_tindakan = {};
		$scope.planning = {};
		$scope.list_obat = [];
		$scope.obat = {};
		$scope.getPagedDataAsyncMedrec($scope.filterOptions);
		$scope.getPagedDataAsyncResep($scope.filterOptions);

		Medrec.findOne({filter:{where:{fk_visit:$stateParams.id}}},function(response){
			$scope.entry_tindakan=response;
		},function(response){
			console.log(response);
		});

        $http.post(
        	"http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-visit",
        	{visit_id : $stateParams.id},
        	{headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
        	let date = new Date();
        	let user_birth_year = new Date(result.data.user.birth_date);
        	$scope.user = result.data.user;
        	if($scope.user.gender == "M")
        		$scope.user.gender = "Laki-laki";
        	else
        		$scope.user.gender = "Perempuan"
        	
        	$scope.user.birth_date = getDateString(new Date($scope.user.birth_date)).split(' ')[0]
        	$scope.user.umur =  date.getFullYear() - user_birth_year.getFullYear();
        }).catch(function onError(result) {
        	console.log("error",result)
        	toastr.warning('Get data user Failed', 'Warning');
        });

        Visit.findById({id:$stateParams.id},function(response){
        	$http.post(
        		"http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-praktek-by-id",
        		{id:String(response.fk_jadwal_praktek)},
        		{headers: {"Content-Type": "application/x-www-formurlencoded"}}
        	).then(function onSuccess(result){
        		$scope.currentKlinikId=result.data.praktek.fk_klinik;
        		$http.get(
        			"http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-praktek",
        			{headers: {"Content-Type": "application/x-www-formurlencoded"}}
        		).then(function onSuccess(result){
        			$scope.jadwal_praktek=result.data.praktek;
        			let currentDay = (new Date()).getDay()-1;
        			$scope.jadwal_praktek=$scope.jadwal_praktek.filter(praktek=>praktek.fk_klinik==$scope.currentKlinikId && praktek.day==currentDay);
		        }).catch(function onError(result) {
		        	console.log("error",result)
		        	toastr.warning('No Connection', 'Warning');
		        });
	        }).catch(function onError(result) {
	        	console.log("error",result)
	        	toastr.warning('No Connection', 'Warning');
	        });
        },function(response){
        	console.log("error",result)
        	toastr.warning('No Connection', 'Warning');
        });
	}

	$scope.init();

	$scope.addObat = function addObat(obat) {
		if(obat.obat != null || obat.dosis != null || obat.quantity != null){
			$scope.obat = {};
			let data = angular.copy(obat);
			$scope.list_obat.push(data);
		}else
			toastr.warning("Data Obat,Dosis dan Kuantitas Harus Terisi","warning")
	}

	$scope.deleteObat = function deleteObat(index) {
		$scope.list_obat.splice(index,1);
	}
	$scope.editObat = function deleteObat(index) {
		$scope.obat.obat = $scope.list_obat[index].obat;
		$scope.obat.dosis = $scope.list_obat[index].dosis;
		$scope.obat.quantity = $scope.list_obat[index].quantity;
		$scope.list_obat.splice(index,1);
	}

	function nextJadwalPraktek(date,time,day){
		time=time.match(/(\d+):(\d+):\d+/);
		date.setDate(date.getDate() + (day+1 + 7 - date.getDay()) % 7);
		date.setHours(parseInt(time[1]));
		date.setMinutes(parseInt(time[2]));
		date.setSeconds(0);
		return date;
	}

	function plusSevenHour(date){
		 return date.setHours(date.getHours()+7);
	}

	$scope.$watch('entry_tindakan.flag_konsul',function(){
		$scope.planning.booking_label="Konsul";
	},true);
	$scope.$watch('entry_tindakan.flag_pemeriksaan',function(){
		$scope.planning.booking_label="Pemeriksaan";
	},true);
	$scope.$watch('entry_tindakan.flag_inap',function(){
		$scope.planning.booking_label="Rawat inap";
	},true);
	$scope.$watch('entry_tindakan.flag_operasi',function(){
		$scope.planning.booking_label="Operasi";
	},true);
	
	function getHistoryPraktek(callback){
		History_praktek.find({filter:
			{where:{fk_jadwal_praktek:$scope.planning.booking_ke.id},order:"id DESC",limit:1}
		},function(response){
			function newHistoryPraktek(){
				Jadwal_praktek.findById({id:$scope.planning.booking_ke.id},function(response){
					let newHistoryPraktek={
						book_count:0,
						open_time:plusSevenHour(nextJadwalPraktek(new Date(), response.time_from, response.day)),
						close_time:plusSevenHour(nextJadwalPraktek(new Date(), response.time_to, response.day)),
						fk_jadwal_praktek:$scope.planning.booking_ke.id,
						fk_dokter:response.fk_dokter
					};
					// console.log(newHistoryPraktek);
					History_praktek.upsert(newHistoryPraktek,function(response){
						callback(response);
					});
				});
			}
			if(!response.open_time){
				newHistoryPraktek();
			}else{
				let now = new Date();
				if((new Date(response.open_time)).getDate()==now.getDate())
					callback(response);
				else
					newHistoryPraktek();
			}
		});
	}

	$scope.save = function save(entry_tindakan) {
		// console.log("save",entry_tindakan);
		if($scope.currentUser.fk_role==24)
			perawat_id=$scope.currentUser.id;
		else
			perawat_id=0;
		$http.post(
	        "http://" + $rootScope.address +":"+ $rootScope.port.penanganan + "/entry-tindakan",
	        { 	visit_id : $stateParams.id , 
	        	medrec : {
	        		id : 0,
	        		code : "",
	        		fk_pasien : 0,
	        		fk_visit : 0,
	        		tensi : entry_tindakan.tensi,
	        		berat_badan : entry_tindakan.berat_badan,
	        		keluhan : entry_tindakan.keluhan,
	        		gejala : entry_tindakan.gejala,
	        		indikasi : entry_tindakan.indikasi,
	        		diagnosa : entry_tindakan.diagnosa,
	        		fk_dokter : $scope.currentUser.id,
	        		fk_klinik : 0,
	        		fk_perawat_isi_tindakan :  perawat_id,
	        		flag_bayar : entry_tindakan.flag_bayar,
	        		flag_konsul : entry_tindakan.flag_konsul,
	        		flag_pemeriksaan : entry_tindakan.flag_pemeriksaan,
	        		flag_resep : entry_tindakan.flag_resep,
	        		flag_inap : entry_tindakan.flag_inap,
	        		flag_operasi : entry_tindakan.flag_operasi
	        	}
	        },
	        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
	        console.log("cek tensi success",result);
	        let lastEntry=angular.copy($scope.entry_tindakan);
	        $scope.entry_tindakan = null;
	        $scope.kode_visit = null;
	        $scope.formShowStatus = false;

	        if($scope.planning.booking_ke){
	        	Visit.findById({id:$stateParams.id},function(visit){
					let response=JSON.parse(JSON.stringify(visit));
					delete response.code;
					delete response.date;
					delete response.booknum;
					delete response.fk_dokter;
					delete response.fk_jadwal_praktek;
					delete response.fk_perawat;
					delete response.fk_history_praktek;
					delete response.total;
					delete response.rating;
					delete response.estimasi_tindakan;
					delete response.checking_time;
					delete response.status;
					delete response.keluhan;
					delete response.sequence_kunjungan;
					response.keluhan=lastEntry.keluhan+'\n'+lastEntry.gejala+'\n'+lastEntry.indikasi+'\n'+lastEntry.diagnosa;
					if(visit.fk_root_pendaftaran==0)
						response.fk_root_pendaftaran=visit.id;
					else
						response.fk_root_pendaftaran=visit.fk_root_pendaftaran;
	        		delete response.id;
	        		console.log(response);
					getHistoryPraktek(function(historyPraktek){
						response.booknum=historyPraktek.book_count+1;
						response.fk_dokter=historyPraktek.fk_dokter;
						response.fk_jadwal_praktek=$scope.planning.booking_ke.id;
						response.fk_history_praktek=historyPraktek.id;
						response.estimasi_tindakan=historyPraktek.book_count*$scope.planning.booking_ke.lama_tindakan;
						response.status="booking";
						Visit.create(response);
					});
	        	});
	        }

	        if($scope.list_obat.length > 0){
	        	for(obat of $scope.list_obat){
	        		$http.post(
			        	"http://" + $rootScope.address +":"+ $rootScope.port.penanganan + "/entry-resep",
			        	{ 
			        		visit_id : $stateParams.id,
			        		resep : {
			        			id : 0,
			        			fk_visit : 0,
			        			obat : obat.obat,
			        			dosis : obat.dosis,
			        			price : 0,
			        			quantity : obat.quantity
			        		}
			        	},
			        	{headers: {"Content-Type": "application/x-www-formurlencoded"}}
			        ).catch(function onError(result) {
			        	console.log("error",result)
			        	toastr.warning('Entry Obat Failed', 'Warning');
			        })
	        	}
	        	toastr.success("Entry Obat Success")
	        	
	        }
	        toastr.success("Entry Tindakan Success")
	        $scope.go("/entry-tindakan")
        }).catch(function onError(result){
	        console.log('search error',result)
	        toastr.warning('Process Failed', 'Warning');
        })
	}
	$scope.go = function go(url) {
   	   $location.path(url);
  	}
}