angular.module('BlurAdmin').controller('RoleController', RoleController);
angular.module('BlurAdmin').controller('RoleNewController', RoleNewController);
angular.module('BlurAdmin').controller('RoleEditController', RoleEditController);


function RoleController($q, $scope, $rootScope, $state, DTOptionsBuilder,$http,
 DTColumnBuilder, $routeParams, $route, $filter, $location, toastr, $timeout, $localStorage) {
   $scope.filterOptions = {
    name: "",
    useExternalFilter: false
  };
  $scope.filterParams = [];
  $scope.showFilter = true;
  $scope.totalServerItems = 0;
  //$scope.totalServerItem = 0;
  $scope.searchTextEXS = '';
  $scope.pagingOptions = {
    pageSizes: [10, 25, 50],
    pageSize: 10,
    currentPage: 1
  };
  // sort
  $scope.sortOptions = {
    fields: ["code"],
    directions: ["ASC"]
  };
  $scope.setPagingData = function setPagingData(data, page, pageSize) {
    $scope.myData = data;
    $scope.totalServerItem = data.length;
    //console.log("data.length",data.length)
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };

  $scope.filterData = function(){
    console.log($scope.filterParams);
    if($scope.filterParams.name != null)
      $scope.filterOptions.useExternalFilter = true;
    else 
      $scope.filterOptions.useExternalFilter = false;

    if($scope.filterParams.name != null)
      $scope.filterOptions.name = $scope.filterParams.name;
    else
      $scope.filterOptions.name = ""; 
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);  
  }
  $scope.getPagedDataAsync = function getPagedDataAsync(pageSize, page) {
    setTimeout(function () {
      var data;
      var page = $scope.pagingOptions.currentPage;
      var pageSize = $scope.pagingOptions.pageSize;
      var offset = (page - 1) * pageSize;
      $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-role",
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
        console.log("search success",result);
        data = angular.copy(result.data.role);
        $scope.totalServerItems = result.data.length;
        $scope.listlength = result.data.length;


        if($scope.filterOptions.useExternalFilter){
          let filteredData = [];
          for(role of data){
            if(role.name.includes($scope.filterOptions.name))
              filteredData.push(role);
          }
          $scope.setPagingData(filteredData, page, pageSize);
        }else
          $scope.setPagingData(data, page, pageSize);
      }).catch(function onError(result){
        console.log('search error',result)
        toastr.warning('Data not found', 'Warning');
      })
    }, 100);
  };
  
  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }
  }, true);

  $scope.region = {};
  $scope.reset = function reset() {
    $scope.region = {};
  }

  $scope.go = function go(url) {
    $location.path(url);
  }

  $scope.viewRowIndex = function viewRowIndex(row, index) {
    var id = $scope.myData[index].id;
    console.log('view ID:', id, index);
    $location.path('/role/' + row.entity.id + '/view');
  };

  $scope.editRowIndex = function editRowIndex(row, index) {
    var id = $scope.myData[index].id;
    console.log('edit ID:', id, index)
    $location.path('/role/' + row.entity.id + '/edit');
  };

  $scope.remove = function remove(row, index, confirmation) {
    var id = row.entity.id;
    index = index + ($scope.gridOptions.paginationCurrentPage-1) * $scope.gridOptions.paginationPageSize;
    confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
    if (confirmDelete(confirmation)) {
      $http.put(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/delete-role",
        {id:String(id)},
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
      console.log("api call success ",result);
        message = result.data.message;
        if (message == "Role has user, can't be delete"){
          toastr.warning(result.data.message);
          return
        }
        toastr.success(result.data.message);
        $scope.myData.splice(index, 1);
      }).catch(function onError(result){
        console.log('delete error',result)
        toastr.warning(result, 'Warning');
      })
    }
  };

  var confirmDelete = function confirmDelete(confirmation) {
    return confirmation ? confirm('This action is irreversible. Do you want to delete this Role?') : true;
  };  
  var removeRowTemplate = '<span style="display:block; text-align:center; margin-top : 5px">'+
                          '<button ng-click="grid.appScope.editRowIndex(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-pencil"></i></button><button ng-click="grid.appScope.remove(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-times"></i></button></span>';
  $scope.gridOptions = {
    data: 'myData',
    enablePaging: true, rowHeight: 36, headerRowHeight: 50,
    paginationPageSizes : $scope.pagingOptions.pageSizes, 
    paginationPageSize: $scope.pagingOptions.pageSize,
    columnDefs: [
      {
        displayName: 'Action',
        field: 'remove',
        cellTemplate: removeRowTemplate,
        width: "8%"
      },
      {
        displayName: 'Kode',
        field: 'code',
      },
      {
        displayName: 'Nama Role',
        field: 'name',
      },
    ]
  };

  $scope.gridOptions.onRegisterApi = function(gridApi) {
      $scope.grid = gridApi;
      console.log($scope.grid)
  };

  $scope.init = function init() {
    console.log('INIT MENU: ', $route.current);
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    $scope.reset();
  }
  $scope.init();
}

function RoleNewController($q, $scope, $rootScope, $state,
  DTOptionsBuilder, DTColumnBuilder, $routeParams, $stateParams, $route, 
  $filter, $location, toastr, $timeout,Role, $localStorage, $http) {



  var checklistMenu ='<input style="margin-top : 30%;margin-left:30%" ng-change="grid.appScope.addMenu(row,rowRenderIndex)" '+
                      'type="checkbox" ng-model="row.entity.checklist"></input>';   
  $scope.gridOptions = {
    data: 'listMenu',
    multiSelect : true,
    enableFiltering: false,
    rowHeight: 36,
    headerRowHeight: 36,
    columnDefs: [
        { displayName: '', field:'checklist', width:"5%" ,cellTemplate: checklistMenu},
        { displayName: 'List Menu',field: 'label'},
      ]
  };

  $scope.gridOptions.onRegisterApi = function(gridApi) {
      $scope.grid = gridApi;
      // console.log($scope.grid)
  };

  $scope.addMenu = function addMenu(row,rowRenderIndex){
      // console.log(row.entity);
    if(row.entity.checklist)
      $scope.selectedMenu.push(row.entity);
    else
      $scope.selectedMenu.splice($scope.getIndexSelectedMenu(row.entity.label), 1);

    console.log($scope.selectedMenu)
  }

  $scope.getIndexSelectedMenu = function findSelectedMenu(menu_name){
    for(i=0; i < $scope.selectedMenu.length; i++){
      if($scope.selectedMenu[i].label == menu_name)
        return i;
    }
    return -1;
  }

  $http.get(
      "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-role",
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      $scope.role.code="role_"+(result.data.role.length+1).toString();
    }).catch(function onError(result){
      console.log('search error',result)
      toastr.warning('Data menu is empty', 'Warning');
  })

  $scope.init = function init() {
    $scope.listMenu = [];
    $scope.selectedMenu = []
    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-menu",
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
        for(m of result.data.menu){
          m.checklist = false;
          $scope.listMenu.push(m);          
        }
      }).catch(function onError(result){
        console.log('search error')
        toastr.warning('Data menu is empty', 'Warning');
    })
  }

  $scope.init();

  $scope.role = {};
  $scope.back = function back(){
    $scope.go("role") 
  }


  $scope.go = function go(url) {
    console.log("back")
    $location.path(url);
  }


  $scope.save = function save(role){
    $http.post(
      "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/add-role",
      {role : {id : 0, code : role.code, name:role.name}},
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
    console.log("api call success",result);
      for(data of $scope.selectedMenu){
        let new_menu = angular.copy(data)
        // console.log("insert")
        console.log(new_menu.label)
        $http.post(
         "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/add-acl",
         {
          acl :
                {
                    id: 0,
                    menu: new_menu.label,
                    property: "",
                    AccessType: "*",
                    permision: "ALLOW",
                    principalType: "ROLE",
                    principalId: role.name,
                    model : " ",
                }
        },
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
          console.log("add acl success")
        }).catch(function onError(result){
          console.log('delete error',result)
          toastr.warning('Add data error', 'Warning');
        })
      }
      toastr.success(result.data.message);
      $scope.back();
    }).catch(function onError(result){
      console.log('delete error',result)
      toastr.warning('Add data error', 'Warning');
    })
  }
}

function RoleEditController($q, $scope, $rootScope, $state, User, 
  DTOptionsBuilder, DTColumnBuilder, $routeParams, $stateParams, $route, 
  $filter, $location, toastr, $timeout,Role, $localStorage, $http) {
  
  var checklistMenu ='<input style="margin-top : 30%;margin-left:30%" ng-change="grid.appScope.addMenu(row,rowRenderIndex)" '+
                      'type="checkbox" ng-model="row.entity.checklist"></input>';   
  $scope.gridOptions = {
    data: 'listMenu',
    multiSelect : true,
    enableFiltering: false,
    rowHeight: 36,
    headerRowHeight: 36,
    columnDefs: [
        { displayName: '', field:'checklist', width:"5%" ,cellTemplate: checklistMenu},
        { displayName: 'List Menu',field: 'label'},
      ]
  };

  $scope.gridOptions.onRegisterApi = function(gridApi) {
      $scope.grid = gridApi;
      // console.log($scope.grid)
  };

  $scope.addMenu = function addMenu(row,rowRenderIndex){
      // console.log(row.entity);
    if(row.entity.checklist)
      $scope.selectedMenu.push(row.entity);
    else
      $scope.selectedMenu.splice($scope.getIndexSelectedMenu(row.entity.label), 1);

    console.log($scope.selectedMenu)
  }

  $scope.getIndexSelectedMenu = function findSelectedMenu(menu_name){
    for(i=0; i < $scope.selectedMenu.length; i++){
      if($scope.selectedMenu[i].label == menu_name)
        return i;
    }
    return -1;
  }

  var initiate_list_menu = $scope.$watch('listMenu', function (newVal, oldVal) {
        if (newVal !== oldVal) {
           if($scope.selectedMenu.length == 0)
            $scope.view();
        }
  }, true);
    



  $scope.init = function init() {
    $scope.listMenu = [];
    $scope.selectedMenu = [];
    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-menu",
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
        for(m of result.data.menu){
          m.checklist = false;
          $scope.listMenu.push(m);          
        }
      }).catch(function onError(result){
        console.log('search error')
        toastr.warning('Data menu is empty', 'Warning');
    })
  }

  $scope.checkSelectedMenu = function checkSelectedMenu(new_menu){
    // console.log(menu.label,new_menu.label)
    for(menu of $scope.selectedMenu){
      if(menu.label == new_menu.label){
        return false;
      }
    } 
    return true;
  }

  $scope.view = function view(){
    $http.post(
      "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-role-by-id",
      {id : $stateParams.id},
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      console.log("api call success",result.data.role);
      $scope.role = result.data.role;
      $http.post(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-acl-by-role",
        {role_name : $scope.role.name},
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
        $scope.acl = result.data.acl;
        // console.log($scope.acl)
        if($scope.acl){
          for(data of $scope.acl){
            for(menu of $scope.listMenu){
              if(data.menu == menu.label){
                menu.checklist = true;
                let new_menu = angular.copy(menu)
                if($scope.checkSelectedMenu(new_menu)){
                  $scope.selectedMenu.push(new_menu);
                }
              }
            }
          }
        }
        initiate_list_menu()
      })
    }).catch(function onError(result){
      console.log('get role error',result)
      toastr.warning('get data error', 'Warning');
    })
  }

  $scope.init();

  $scope.back = function back(){
    $scope.go("role") 
  }


  $scope.go = function go(url){
    console.log("back")
    $location.path(url);
  }

  $scope.save = function save(role){
    console.log("selected menu",$scope.selectedMenu)
    $http.put(
      "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/edit-role",
      {role : {id : role.id, code : role.code, name:role.name}},
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      console.log("api call success",result);
      for(data of $scope.selectedMenu){
          let new_menu = angular.copy(data)
          console.log(new_menu.label)
          $http.post(
           "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/add-acl",
           {
            acl :
                  {
                      id: 0,
                      Menu: new_menu.label,
                      property: "",
                      AccessType: "*",
                      permision: "ALLOW",
                      principalType: "ROLE",
                      principalId: role.name,
                  }
          },
          {headers: {"Content-Type": "application/x-www-formurlencoded"}}
          ).then(function onSuccess(result){
            console.log("add acl success")
          }).catch(function onError(result){
            console.log('delete error',result)
            toastr.warning('Add data error', 'Warning');
          })
      }
      toastr.success(result.data.message);
      $scope.back();
    }).catch(function onError(result){
      console.log('Edit error',result)
      toastr.warning(result.data.err, 'Warning');
    })
  } 
}