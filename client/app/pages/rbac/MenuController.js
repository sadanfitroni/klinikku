angular
  .module('BlurAdmin').controller('MenuController', MenuController);
angular
  .module('BlurAdmin').controller('MenuEditController', MenuEditController);
angular
  .module('BlurAdmin').controller('MenuNewController', MenuNewController);

const default_perent = {
      label: 'Parent',
      id: 0
  }

function MenuController($q, $scope, $rootScope, $state, DTOptionsBuilder, DTColumnBuilder, 
  $routeParams, $route, $filter, $location, toastr, $timeout, $http) {
  $scope.filterOptions = {
    label: "",
    useExternalFilter: false
  };

  $scope.filterParams = {};
  $scope.showFilter = true;
  $scope.totalServerItems = 0;
  //$scope.totalServerItem = 0;
  $scope.searchTextEXS = '';
  $scope.pagingOptions = {
    pageSizes: [10, 25, 50],
    pageSize: 10,
    currentPage: 1
  };
  // sort
  $scope.sortOptions = {
    fields: ["code"],
    directions: ["ASC"]
  };
  
  $scope.setPagingData = function setPagingData(data, page, pageSize) {
    $scope.myData = data;
    $scope.totalServerItem = data.length;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };

  $scope.filterData = function(){
    if($scope.filterParams.label != null)
      $scope.filterOptions.useExternalFilter = true;
    else
      $scope.filterOptions.useExternalFilter = false;

    if($scope.filterParams.label != null)
      $scope.filterOptions.label = $scope.filterParams.label;
    else
      $scope.filterOptions.label = "";


    $scope.getPagedDataAsync($scope.pagingOptions.pageSize);    
  }


  $scope.getPagedDataAsync = function getPagedDataAsync(pageSize, page) {
    setTimeout(function () {
      var listMenu = [];
      var page = $scope.pagingOptions.currentPage;
      var pageSize = $scope.pagingOptions.pageSize;
      var offset = (page - 1) * pageSize;
      console.log($scope.filterOptions);
      $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-menu",
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
        console.log("search success",result);
        listMenu = angular.copy(result.data.menu);
        $scope.totalServerItems = result.data.length;
        $scope.listlength = result.data.length;
        
        if($scope.filterOptions.useExternalFilter){
          filteredMenu = [];
          for(data of listMenu){
            if(data.label.includes($scope.filterOptions.label))
              filteredMenu.push(data);
          }
          console.log("filtered data",filteredMenu);
          $scope.setPagingData(filteredMenu, page);
        }else{
          console.log("all data");
          $scope.setPagingData(listMenu, page);
        }

      }).catch(function onError(result){
        console.log('search error')
        toastr.warning('Data not found', 'Warning');
      })
    }, 100);
  };
  
  $scope.$watch('pagingOptions', function (newVal, oldVal) {
    if (newVal !== oldVal) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }
  }, true);

  $scope.$watch('searchFilter', function (newVal, oldVal) {
    if (newVal !== oldVal) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }
  }, true);


  $scope.region = {};
  $scope.reset = function reset() {
    $scope.region = {};
  }

  $scope.go = function go(url) {
    $location.path(url);
  }

  $scope.viewRowIndex = function viewRowIndex(row, index) {
    var id = $scope.myData[index].id;
    console.log('view ID:', id, index);
    // $location.path('/component/menu/' + row.entity.id + '/view');
  };

  $scope.editRowIndex = function editRowIndex(row, index) {
    console.log('edit ID:', index);
    $location.path('/menu/' + row.entity.id + '/edit');
  };

  $scope.remove = function remove(row, index, confirmation) {
    var id = row.entity.id;
    index = index + ($scope.gridOptions.paginationCurrentPage-1) * $scope.gridOptions.paginationPageSize;
    // console.log(id,index);
    confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;

    if (confirmDelete(confirmation)) {
      $http.put(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/delete-menu",
        {id:String(id)},
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
      console.log("api call success",result);
        message = 'Menu was removed';
        toastr.success(message);
        $scope.myData.splice(index, 1);
      }).catch(function onError(result){
        console.log('delete error')
        toastr.warning('Delete data error', 'Warning');
      })
    }
  };

  var confirmDelete = function confirmDelete(confirmation) {
    return confirmation ? confirm('This action is irreversible. Do you want to delete this menu?') : true;
  };
  // var removeRowTemplate = '<span style="display:block; text-align:center;"><button ng-click="viewRowIndex(row, $parent.$index)" class="btn btn-xs btn-default"><i class="fa fa-eye"></i><md-tooltip md-delay="1">List User </md-tooltip></button><button ng-click="editRowIndex(row, $parent.$index)" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button><button ng-click="remove(row, $parent.$index)" class="btn btn-xs btn-default"><i class="fa fa-times"></i></button></span>';
  
  var removeRowTemplate = '<span style="display:block; text-align:center; margin-top:5px">'+
                          '<button ng-click="grid.appScope.editRowIndex(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-pencil"></i></button><button ng-click="grid.appScope.remove(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-times"></i></button></span>';
  $scope.gridOptions = {
    data: 'myData', 
    enablePaging: true, rowHeight: 36, headerRowHeight: 50,
    paginationPageSizes : $scope.pagingOptions.pageSizes, 
    paginationPageSize: $scope.pagingOptions.pageSize,
    columnDefs: [
      {
        displayName: 'Action',
        field: 'remove',
        cellTemplate: removeRowTemplate,
        width: "8%"
      },
      {
        displayName: 'Id',
        field: 'id',
      },
      {
        displayName: 'Label',
        field: 'label',
      },
      {
        displayName: 'URL',
        field: 'url',
      },
      {
        displayName: 'Perent Id',
        field: 'parentId',
      },
      {
        displayName: 'Order',
        field: 'orderMenu',
      },
      {
        displayName: 'Icon',
        field: 'icon',
      },

    ]
  };

  // console.log($scope.gridOptions)

  $scope.gridOptions.onRegisterApi = function(gridApi) {
      $scope.grid = gridApi;
      console.log($scope.grid)
  };

  $scope.init = function init() {
    console.log('INIT MENU: ', $route.current);
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    $scope.reset();
  }

  $scope.init();
}

function MenuEditController($scope, $rootScope, $stateParams,
 $state, DTOptionsBuilder, DTColumnBuilder, toastr, $location,$http) {

  $scope.init = function (){
    let permission_default = [false,false,false,false];
    $scope.menu = {};
    $scope.appModel = [];
    $scope.listAccessType = $rootScope.aclAccessType;
    console.log("list model",$scope.appModel, $scope.listAccessType);
    // $scope.test_string = "test_string";
    $scope.parentMenu = [];
    $scope.menu.parentId = default_perent;
    $scope.menu.order = 0;
    $scope.menu.icon ="";
    //set list model
    for(data of $rootScope.appModel){
      data.accessType = angular.copy(permission_default);
      $scope.appModel.push(data);
    }

    $scope.ionicons = [
      'ion-ionic', 'ion-arrow-right-b', 'ion-arrow-down-b', 'ion-arrow-left-b', 
      'ion-arrow-up-c', 'ion-arrow-right-c', 'ion-arrow-down-c', 'ion-arrow-left-c', 
      'ion-arrow-return-right', 'ion-arrow-return-left', 'ion-arrow-swap', 'ion-arrow-shrink', 
      'ion-arrow-expand', 'ion-arrow-move', 'ion-arrow-resize', 'ion-chevron-up', 'ion-chevron-right', 
      'ion-chevron-down', 'ion-chevron-left', 'ion-navicon-round', 'ion-navicon', 'ion-drag', 'ion-log-in', 
      'ion-log-out', 'ion-checkmark-round', 'ion-checkmark', 'ion-checkmark-circled', 'ion-close-round', 'ion-plus-round', 
      'ion-minus-round', 'ion-information', 'ion-help', 'ion-backspace-outline', 'ion-help-buoy', 'ion-asterisk', 'ion-alert', 
      'ion-alert-circled', 'ion-refresh', 'ion-loop', 'ion-shuffle', 'ion-home', 'ion-search', 'ion-flag', 'ion-star', 
      'ion-heart', 'ion-heart-broken', 'ion-gear-a', 'ion-gear-b', 'ion-toggle-filled', 'ion-toggle', 'ion-settings', 
      'ion-wrench', 'ion-hammer', 'ion-edit', 'ion-trash-a', 'ion-trash-b', 'ion-document', 'ion-document-text', 
      'ion-clipboard', 'ion-scissors', 'ion-funnel', 'ion-bookmark', 'ion-email', 'ion-email-unread', 'ion-folder', 
      'ion-filing', 'ion-archive', 'ion-reply', 'ion-reply-all', 'ion-forward'
    ],
    $scope.selectedItems = [];
    $scope.parentMenu.push(default_perent);
    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-menu",
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
        for(m of result.data.menu){
          $scope.parentMenu.push(m);          
        }
      }).catch(function onError(result){
        console.log('search error')
        toastr.warning('Data menu is empty', 'Warning');
    });
  }
  
  $scope.checkAll = function checkAll(row,index) {
    // let index = row.rowIndex;
    console.log(row);
    let i = 1;
    while(i <=3 ){
      row.entity.accessType[i] = row.entity.accessType[0];
      i++;
    }
    if($scope.statusRemoveAccess($scope.appModel[index].accessType))
      $scope.selectedItems.splice($scope.findModelAccessId($scope.appModel[index].model),1);
    else
      $scope.addAccessType(index);

    console.log($scope.selectedItems);
  }

  $scope.statusRemoveAccess = function statusRemoveAccess(accessMenu){
      let i = 1;
      while(i <= 3){
        if(accessMenu[i])
          return false
        i++;  
      }
      return true;
  }

  $scope.addAccessType = function addAccessType(index){
    let model = $scope.appModel[index].model;
    if($scope.checkModelListAccess(model))
       $scope.selectedItems.push($scope.appModel[index]);
    // else{
      
    //    // $scope.selectedItems[$scope.findModelAccessId(model)] = $scope.appModel[index];

    // }
  }

  $scope.checkModelListAccess = function checkModelListAccess(model){
    if($scope.selectedItems.length > 0){
      for(data of $scope.selectedItems){
        if(data.model == model )
          return false
      }
    }
    return true;
  }

  $scope.findModelAccessId = function findModelAccessId(model){
    if($scope.selectedItems.length == 1)
      return 0;

    for(i=0;i<$scope.selectedItems.length;i++){
      if($scope.selectedItems[i].model == model)
        return i
    }
  
    return -1;
  }

  $scope.checkAccessList = function checkAccessList(row,rowRenderIndex){
    // let index = row.rowIndex;
    let i = 1;
    let allStat = true;
    while(i <=3 ){
      if(row.entity.accessType[i] == false){
        allStat = false;
        break; 
      }
       i++;
    }
    if(allStat)
      row.entity.accessType[0] = true;
    else
      row.entity.accessType[0] = false;

    if($scope.statusRemoveAccess($scope.appModel[rowRenderIndex].accessType))
      $scope.selectedItems.splice($scope.findModelAccessId($scope.appModel[rowRenderIndex].model),1);
    else
      $scope.addAccessType(rowRenderIndex);
    console.log($scope.selectedItems);
  }



  $scope.view = function view() {
    $http.post(
      "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-menu-by-id",
      {id:$stateParams.id},
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      // console.log("get data menu", result)
      $scope.menu = result.data.menu;
      $scope.menu.order = result.data.menu.orderMenu;
      // console.log( $scope.parentMenu);
      for(menu of $scope.parentMenu){
        if(menu.id == result.data.menu.parentId)
          $scope.menu.parentId = menu;
      }
    }).catch(function onError(result){
      // console.log('search error')
      toastr.warning('Data not found', 'Warning');
    })

    // console.log($scope.appModel)

    $http.post(
      "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-menu-model-by-menu",
      { id_menu : $stateParams.id },
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      // console.log("get data menu model", result);
      for(data of result.data.menu_model){
        for(model of $scope.appModel){
          if(data.model == model.model){
            // $scope.selectedItems.push(data)
            if(data.accessType == "*"){
              model.accessType[0] = true;
              model.accessType[1] = true;
              model.accessType[2] = true;
              model.accessType[3] = true; 
            }

            if(data.accessType == "WRITE")
              model.accessType[1] = true;
            if(data.accessType == "READ")
              model.accessType[2] = true;
            if(data.accessType == "MENU")
              model.accessType[3] = true;
            
            $scope.selectedItems.push(model);
          }
        }
      }
      console.log($scope.selectedItems)
    }).catch(function onError(result){
      // console.log('search error')
      toastr.warning('Failed get menu model data or not have menu model', 'Warning');
    })


  }

  $scope.init();
  $scope.view();

  var accessSelectTemplate ='<div class="width:100%"><div class="checkbox-inline" style="margin-left:3%;margin-top:5px;width:10%">' + 
                            '<label>All</label><input ng-change="grid.appScope.checkAll(row,rowRenderIndex)" type="checkbox" ng-model="row.entity.accessType[0]"></input></div>' + 
                            '<div class="checkbox-inline" style="margin-top:5px;width:15%" ng-repeat="(k,v) in grid.appScope.listAccessType">'+
                            '<label>{{ v.label }}</label><input ng-change="grid.appScope.checkAccessList(row,rowRenderIndex)" type="checkbox" ng-model="row.entity.accessType[$index + 1]"' +
                            'value="v.value"></input></div></div>';      
  $scope.gridOptions = {
    data: 'appModel',
    enableFiltering: false,
    rowHeight: 36,
    headerRowHeight: 36,
    columnDefs: [
        { displayName: 'Module',field: 'name',width: "15%"},
        { displayName: 'Access Type',field:'accessType', width:"85%" ,cellTemplate: accessSelectTemplate},
      ]
  };
  $scope.gridOptions.onRegisterApi = function(gridApi) {
      $scope.grid = gridApi;
      // console.log($scope.grid)
  };

  $scope.save = function save(menu) { 
    console.log("data menu", $scope.selectedItems)

    $http.put(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/edit-menu",
        { 
          menu : {
            id : parseInt($stateParams.id),
            url : menu.url,
            label : menu.label,
            parentId : menu.parentId.id,
            orderMenu : menu.order,
            icon : menu.icon,
          }
        },
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
        for(data_model of $scope.selectedItems){
          // console.log(data_model)
            let listAccess = [];
            let menu_model = {};
            menu_model.model = data_model.model;
            menu_model.permission = "ALLOW";
            // menu_model.accessType = $scope.checkAccessType(data_model.accessType);
            menu_model.fk_menu = parseInt($stateParams.id);
            if(data_model.accessType[0]){
              listAccess.push("*")
            }else{
              if(data_model.accessType[1]){
                listAccess.push("WRITE")
              }
              if(data_model.accessType[2]){
                listAccess.push("READ")
              }
              if(data_model.accessType[3]){
                listAccess.push("MENU")
              }              
            }
           
           for(access of listAccess){
             $http.post(
              "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/add-menu-model",
              { 
                menu_model : {
                  id : 0,
                  model : menu_model.model,
                  permission : "ALLOW",
                  accessType : access,
                  fk_menu : menu_model.fk_menu,
                }
              },
              {headers: {"Content-Type": "application/x-www-formurlencoded"}}
            )
           }
           console.log(listAccess);
        }
        toastr.success("Edit menu success")
        $scope.back();
      }).catch(function onError(result){
        console.log('search error',result)
        toastr.warning('Edit menu failed', 'Warning');
    })
  }

  $scope.checkAccessType = function checkAccessType(accessType, access){
      if(accessType[0]){
        return access.push("*")
      }
      if(accessType[1]){
        access.push("WRITE")
      }
      if(accessType[2]){
        access.push("READ")
      }
      if(accessType[3]){
        access.push("MENU")
      }
  }


  
  $scope.back = function back(){
    $scope.go("menu") 
  }


  $scope.go = function go(url) {
    console.log("back")
    $location.path(url);
  }
}

function MenuNewController($scope, $rootScope, $state, toastr, 
  DTOptionsBuilder, DTColumnBuilder, $location,$http) {  

  $scope.init = function (){
    let permission_default = [false,false,false,false];
    $scope.menu = {};
    $scope.appModel = [];
    $scope.listAccessType = $rootScope.aclAccessType;
    console.log("list model",$scope.appModel, $scope.listAccessType);
    // $scope.test_string = "test_string";
    $scope.parentMenu = [];
    $scope.menu.parentId = default_perent;
    $scope.menu.order = 0;
    $scope.menu.icon ="";
    //set list model
    for(data of $rootScope.appModel){
      data.accessType = angular.copy(permission_default);
      $scope.appModel.push(data);
    }

    $scope.ionicons = [
      'ion-ionic', 'ion-arrow-right-b', 'ion-arrow-down-b', 'ion-arrow-left-b', 
      'ion-arrow-up-c', 'ion-arrow-right-c', 'ion-arrow-down-c', 'ion-arrow-left-c', 
      'ion-arrow-return-right', 'ion-arrow-return-left', 'ion-arrow-swap', 'ion-arrow-shrink', 
      'ion-arrow-expand', 'ion-arrow-move', 'ion-arrow-resize', 'ion-chevron-up', 'ion-chevron-right', 
      'ion-chevron-down', 'ion-chevron-left', 'ion-navicon-round', 'ion-navicon', 'ion-drag', 'ion-log-in', 
      'ion-log-out', 'ion-checkmark-round', 'ion-checkmark', 'ion-checkmark-circled', 'ion-close-round', 'ion-plus-round', 
      'ion-minus-round', 'ion-information', 'ion-help', 'ion-backspace-outline', 'ion-help-buoy', 'ion-asterisk', 'ion-alert', 
      'ion-alert-circled', 'ion-refresh', 'ion-loop', 'ion-shuffle', 'ion-home', 'ion-search', 'ion-flag', 'ion-star', 
      'ion-heart', 'ion-heart-broken', 'ion-gear-a', 'ion-gear-b', 'ion-toggle-filled', 'ion-toggle', 'ion-settings', 
      'ion-wrench', 'ion-hammer', 'ion-edit', 'ion-trash-a', 'ion-trash-b', 'ion-document', 'ion-document-text', 
      'ion-clipboard', 'ion-scissors', 'ion-funnel', 'ion-bookmark', 'ion-email', 'ion-email-unread', 'ion-folder', 
      'ion-filing', 'ion-archive', 'ion-reply', 'ion-reply-all', 'ion-forward'
    ],
    $scope.selectedItems = [];
    $scope.parentMenu.push(default_perent);
    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-menu",
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
        for(m of result.data.menu){
          $scope.parentMenu.push(m);          
        }
      }).catch(function onError(result){
        console.log('search error')
        toastr.warning('Data menu is empty', 'Warning');
    });
  }
  $scope.init();
  
  $scope.checkAll = function checkAll(row,index) {
    // let index = row.rowIndex;
    console.log(row);
    let i = 1;
    while(i <=3 ){
      row.entity.accessType[i] = row.entity.accessType[0];
      i++;
    }
    if($scope.statusRemoveAccess($scope.appModel[index].accessType))
      $scope.selectedItems.splice($scope.findModelAccessId($scope.appModel[index].model),1);
    else
      $scope.addAccessType(index);

    console.log($scope.selectedItems);
  }

  $scope.statusRemoveAccess = function statusRemoveAccess(accessMenu){
      let i = 1;
      while(i <= 3){
        if(accessMenu[i])
          return false
        i++;  
      }
      return true;
  }

  $scope.addAccessType = function addAccessType(index){
    let model = $scope.appModel[index].model;
    if($scope.checkModelListAccess(model))
       $scope.selectedItems.push($scope.appModel[index]);
    // else{
      
    //    // $scope.selectedItems[$scope.findModelAccessId(model)] = $scope.appModel[index];

    // }
  }

  $scope.checkModelListAccess = function checkModelListAccess(model){
    if($scope.selectedItems.length > 0){
      for(data of $scope.selectedItems){
        if(data.model == model )
          return false
      }
    }
    return true;
  }

  $scope.findModelAccessId = function findModelAccessId(model){
   if($scope.selectedItems.length == 1)
      return 0;

    for(i=0;i<$scope.selectedItems.length;i++){
      if($scope.selectedItems[i].model == model)
        return i
    }
  
    return -1;
  }

  $scope.checkAccessList = function checkAccessList(row,rowRenderIndex){
    // let index = row.rowIndex;
    let i = 1;
    let allStat = true;
    while(i <=3 ){
      if(row.entity.accessType[i] == false){
        allStat = false;
        break; 
      }
       i++;
    }
    if(allStat)
      row.entity.accessType[0] = true;
    else
      row.entity.accessType[0] = false;

    if($scope.statusRemoveAccess($scope.appModel[rowRenderIndex].accessType))
      $scope.selectedItems.splice($scope.findModelAccessId($scope.appModel[rowRenderIndex].model),1);
    else
      $scope.addAccessType(rowRenderIndex);
    console.log($scope.selectedItems);
  }



  var accessSelectTemplate ='<div class="width:100%"><div class="checkbox-inline" style="margin-left:3%;margin-top:5px;width:10%">' + 
                            '<label>All</label><input ng-change="grid.appScope.checkAll(row,rowRenderIndex)" type="checkbox" ng-model="row.entity.accessType[0]"></input></div>' + 
                            '<div class="checkbox-inline" style="margin-top:5px;width:15%" ng-repeat="(k,v) in grid.appScope.listAccessType">'+
                            '<label>{{ v.label }}</label><input ng-change="grid.appScope.checkAccessList(row,rowRenderIndex)" type="checkbox" ng-model="row.entity.accessType[$index + 1]"' +
                            'value="v.value"></input></div></div>';      
  $scope.gridOptions = {
    data: 'appModel',
    enableFiltering: false,
    rowHeight: 36,
    headerRowHeight: 36,
    columnDefs: [
        { displayName: 'Module',field: 'name',width: "15%"},
        { displayName: 'Access Type',field:'accessType', width:"85%" ,cellTemplate: accessSelectTemplate},
      ]
  };
  $scope.gridOptions.onRegisterApi = function(gridApi) {
      $scope.grid = gridApi;
      // console.log($scope.grid)
  };

  
  $scope.save = function save(menu) {
    // console.log("menu", menu)
    console.log("menu model data",$scope.selectedItems)
    $http.post(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/add-menu",
        { 
          menu : {
            id : 0,
            url : menu.url,
            label : menu.label,
            parentId : menu.parentId.id,
            orderMenu : menu.order,
            icon : menu.icon,
          }
        },
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){      
        console.log("after insert", result)  
        for(data_model of $scope.selectedItems){
          // console.log(data_model)
            let listAccess = [];
            let menu_model = {};
            menu_model.model = data_model.model;
            menu_model.permission = "ALLOW";
            // menu_model.accessType = $scope.checkAccessType(data_model.accessType);
            menu_model.fk_menu = result.data.new_menu.id;
            if(data_model.accessType[0]){
              listAccess.push("*")
            }else{
              if(data_model.accessType[1]){
                listAccess.push("WRITE")
              }
              if(data_model.accessType[2]){
                listAccess.push("READ")
              }
              if(data_model.accessType[3]){
                listAccess.push("MENU")
              }              
            }
           
           for(access of listAccess){
             $http.post(
              "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/add-menu-model",
              { 
                menu_model : {
                  id : 0,
                  model : menu_model.model,
                  permission : "ALLOW",
                  accessType : access,
                  fk_menu : menu_model.fk_menu,
                }
              },
              {headers: {"Content-Type": "application/x-www-formurlencoded"}}
            )
           }
        }
        toastr.success("Add menu successed")
        $scope.back();
      }).catch(function onError(result){
        console.log('search error',result)
        toastr.warning('Add menu failed', 'Warning');
    })
  }

  $scope.back = function back(){
    $scope.go("menu") 
  }


  $scope.go = function go(url) {
    console.log("back")
    $location.path(url);
  }

}
