angular.module('BlurAdmin').controller('UserController', UserController);
angular.module('BlurAdmin').controller('UserEditController', UserEditController);
angular.module('BlurAdmin').controller('UserNewController', UserNewController);
angular.module('BlurAdmin').controller('RegisterController', RegisterController);

// angular
// .module('BlurAdmin').controller('UserViewController', UserViewController);

function UserController($q,$scope, $rootScope, $state, User, DTOptionsBuilder, DTColumnBuilder, $routeParams, 
    $route, $filter, $location, toastr, $timeout, $http, Detail_pasien) {
    $scope.filterOptions = {filterText: "", useExternalFilter: false };
    $scope.showFilter = true;
    $scope.totalServerItems = 0;
    $scope.searchTextEXS = '';
    $scope.pagingOptions = {pageSizes: [10, 25, 50], pageSize: 10, currentPage: 1};
    // sort
    $scope.sortOptions = { fields: ["code"], directions: ["ASC"]};
    $scope.filter={};
    $scope.filter.name = "";
    $scope.myData = [];
    // get needed FKs
    function getRequiredFkData(callback){
        $http.get(
                "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-role",
                {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
        ).then(function onSuccess(result){
                $scope.rolesMap={};
                $scope.roles=angular.copy(result.data.role);
                $scope.roles = $scope.roles.filter(role=>role.id!=27);
                result.data.role.forEach(function(role){
                    $scope.rolesMap[role.id]=role.name;
                });
                $http.get(
                  "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
                  {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                ).then(function onSuccess(result){
                    $scope.kliniks = result.data.klinik;
                    $scope.kliniksMap={};
                    result.data.klinik.forEach(function(klinik){
                        $scope.kliniksMap[klinik.id]=klinik;
                    });
                    callback();
                }).catch(function onError(result){
                  toastr.warning('Get data error', 'Warning');
                });
        }).catch(function onError(response){
                console.log('no connection');
                toastr.warning('No connection', 'Warning');
        });
    }
    function fkMapping(){
        // console.log($scope.myData);
        $scope.myData.forEach(data=>{
                if (data.gender=='M') data.gender="Laki-laki";
                if (data.gender=='F') data.gender="Perempuan";
                data.fk_role=$scope.rolesMap[data.fk_role];
        });
    }
    function getAllPasienByKlinik(klinikId, callback){
        $http.post(
                "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-pasien-by-klinik",
                {klinik_id : klinikId},
                {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
                if(result.data!=null)
                    callback(result.data.pasiens);
        }).catch(function onError(result){
                console.log('get all pasiens error',result)
                toastr.warning('Get data error', 'Warning');
        });
    }
    function getAllKlinikIdByDokter(callback){
        $http.post(
                "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-klinik-id-by-dokter",
                {dokter_id : $scope.currentUser.id.toString()},
                {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
                callback(result.data.klinik_ids)
        }).catch(function onError(result){
                console.log('get all dokters error',result)
                toastr.warning('Get data error', 'Warning');
        });
    }
    function getAllDokterByKlinik(callback){
        $http.post(
                "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-dokter-by-klinik",
                {klinik_id : $scope.currentUser.fk_klinik.toString()},
                {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
                callback(result.data.dokters);
        }).catch(function onError(result){
                console.log('get all dokters error',result)
                toastr.warning('Get data error', 'Warning');
        });
    }
    function getAllUser(callback){
        $http.get(
                    "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user",
                {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
        ).then(function onSuccess(result){
                callback(result.data.user);
        }).catch(function onError(response){
                toastr.warning('No connection', 'Warning');
                console.log("no connection",response)
        });
    }
    function getRawData(callback){
        if($scope.currentUser.fk_role==25){
                getAllKlinikIdByDokter(function(klinik_ids){
                    if(klinik_ids!=null){
                        klinik_ids.forEach(klinik_id=>{
                            getAllPasienByKlinik(klinik_id,function(data){
                                      $scope.myData.push.apply($scope.myData,data);
                                      callback();
                            });
                        });
                    };
                });
        }else{
                getAllUser(function(data){
                    $scope.myData.push.apply($scope.myData, data);
                    if($scope.currentUser.fk_role==1){
                        $scope.myData = $scope.myData.filter(
                            user=>((user.fk_role==1 || user.fk_role==24) && (user.fk_klinik==$scope.currentUser.fk_klinik))
                        );
                        getAllDokterByKlinik(function(data){
                            $scope.myData.push.apply($scope.myData, data);
                            getAllPasienByKlinik($scope.currentUser.fk_klinik.toString(),function(data){
                                      $scope.myData.push.apply($scope.myData, data);
                                      callback();
                            });
                        });
                    }else{
                        callback();
                    }
                });
        }
    }
    $scope.getPagedDataAsync = function getPagedDataAsync(pageSize, page, searchText) {
        setTimeout(function () {
                var data;
                var page = $scope.pagingOptions.currentPage;
                var pageSize = $scope.pagingOptions.pageSize;
                var offset = (page - 1) * pageSize;
                getRawData(function(){
                    fkMapping();
                    $scope.unfiltered_data=$scope.myData;
                    $scope.filterData();
                });
        }, 100);
    };
    $scope.filterData = function filterData(){
        $scope.myData = angular.copy($scope.unfiltered_data);

        if($scope.filter.name != null){
                $scope.myData = $scope.myData.filter(user=>
                   angular.lowercase(user.fullname).includes(angular.lowercase($scope.filter.name))
                );
        }
        if($scope.filter.klinik != null){
                // console.log('masuk filter klinik');
                if($scope.myData!=null){
                    $scope.myData=$scope.myData.filter(data=>data.fk_role!="Dokter" && data.fk_role!="Pasien" && 
                        data.fk_klinik==$scope.filter.klinik.id);
                }
                $http.post(
                  "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-dokter-by-klinik",
                  {klinik_id : $scope.filter.klinik.id.toString()},
                  {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                ).then(function onSuccess(result){
                    $scope.myData.push.apply($scope.myData,result.data.dokters);
                    $http.post(
                    "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-pasien-by-klinik",
                    {klinik_id : $scope.filter.klinik.id.toString()},
                    {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                    ).then(function onSuccess(result){
                    data = result.data.pasiens;
                    $scope.myData.push.apply($scope.myData,data);
                    }).catch(function onError(result){
                    console.log('get all pasiens error',result)
                    toastr.warning('Get data error', 'Warning');
                    });
                }).catch(function onError(result){
                  console.log('get all dokters error',result)
                  toastr.warning('Get data error', 'Warning');
                });
        };
        if($scope.filter.role!=null){
                $scope.myData=$scope.myData.filter(user=>user.fk_role==$scope.filter.role.name);
        };
        if($scope.search.username!=null){
                $scope.myData=$scope.myData.filter(user=>user.fullname.includes($scope.search.username));
        };
        // console.log($scope.myData);
    };
    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (angular.isDefined($scope.filterTimeout)) {
                $timeout.cancel($scope.filterTimeout);
        }
        $scope.filterTimeout = $timeout(function () {
                if (newVal !== oldVal) {
                    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
                }
        }, 500);
    }, true);
    $scope.search={};
    $scope.$watch('search.username',function(){
        // console.log('safdfsad');
        if($scope.search.username!=null){
                $scope.filterData();
        }
    });
    $scope.go = function go(url){
        $location.path(url);
    }
    // $scope.viewRowIndex = function viewRowIndex(row, index){
    //     var id = $scope.myData[index].id;
    //     console.log('view ID:', id, index);
    //     $location.path('/component/user/'+row.entity.id+'/view');
    // };
    $scope.editRowIndex = function editRowIndex(row, index){
        var id = $scope.myData[index].id;
        $location.path('/user/'+row.entity.id+'/edit');
    };

    $scope.remove = function remove( row, index, confirmation ){
        var id = row.entity.id;
        index = index + ($scope.gridOptions.paginationCurrentPage-1) * $scope.gridOptions.paginationPageSize;
        confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
        if (confirmDelete(confirmation)) {
            $http.put(
                "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/delete-user",
                {id:String(id)},
                {headers: {"Content-Type": "application/x-www-formurlencoded"}}
            ).then(function onSuccess(result){
                message = result.data.message;
                toastr.success(result.data.message);
                $scope.myData.splice(index, 1);
                Detail_pasien.findOne({filter:{where:{fk_pasien:Number(id)}}},function(response){
                    Detail_pasien.deleteById({id:response.id})
                });
            }).catch(function onError(result){
                console.log('delete error',result)
                toastr.warning(result, 'Warning');
            })
        }
    };

    var confirmDelete = function confirmDelete(confirmation){
        return confirmation ? confirm('This action is irreversible. Do you want to delete this User?') : true;
    };
    
    var removeRowTemplate = '<span style="display:block; text-align:center;">'+
                                  '<button ng-click="grid.appScope.editRowIndex(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                                  '<i class="fa fa-pencil"></i></button><button ng-click="grid.appScope.remove(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                                  '<i class="fa fa-times"></i></button></span>';
    $scope.gridOptions = { 
        data: 'myData', 
        enablePaging: true, rowHeight: 36, headerRowHeight: 50,
        paginationPageSizes : $scope.pagingOptions.pageSizes, 
        paginationPageSize: $scope.pagingOptions.pageSize,
    columnDefs:[
    
    {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"8%"},
    {displayName:'Email',field:'email',width:"10%"},
    {displayName:'No.Hp',field:'mobile',width:"10%"},
    {displayName:'Nama Lengkap',field:'fullname',width:"10%"},
    {displayName:'Gender',field:'gender',width:"10%"},
    {displayName:'Tanggal Lahir',field:'birth_date',width:"10%"},
    {displayName:'Golongan Darah',field:'blood_type',width:"10%"},
    {displayName:'Alamat',field:'address',width:"10%"},
    {displayName:'ID User',field:'code',width:"10%"},
    {displayName:'Posisi',field:'fk_role',width:"*"},
   
    ]};

    if($scope.currentUser.fk_role == 25){
        var removeRowTemplate = '<span style="display:block; text-align:center;">'+
                                  '<button ng-click="grid.appScope.editRowIndex(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                                  '<i class="fa fa-eye"></i></button>';
        $scope.gridOptions.columnDefs = [
                {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"8%"},
                {displayName:'Email',field:'email',width:"10%"},
                {displayName:'No.Hp',field:'mobile',width:"10%"},
                {displayName:'Nama Lengkap',field:'fullname',width:"10%"},
                {displayName:'Gender',field:'gender',width:"10%"},
                {displayName:'Tanggal Lahir',field:'birth_date',width:"10%"},
                {displayName:'Golongan Darah',field:'blood_type',width:"10%"},
                {displayName:'Alamat',field:'address',width:"10%"},
                {displayName:'ID User',field:'code',width:"10%"},
        ];
    }

    $scope.init = function init(){
        getRequiredFkData(function(){
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        })
    }

    $scope.init();
}

function UserEditController ($scope, $rootScope,$stateParams,$state, toastr, $http, $location, User, 
    Detail_pasien, Provinsi, Kabupaten_kota, Kecamatan, Kelurahan, Klinik, Medrec, Penjamin_pasien, Visit) {
    $scope.currentController="edit";
    $scope.currentUrl=$state.current.url;
    if($state.current.url== "/edit-profile")
        $scope.back_status = false;
    else
        $scope.back_status = true;
    $scope.has_booking = false;
    $scope.user={};
    $scope.roles = [];
    $scope.open = open;
    $scope.open1 = open1;
    $scope.open2 = open2;
    $scope.open3 = open3;
    $scope.datepicker={};
    $scope.datepicker.opened = false;
    $scope.datepicker1={};
    $scope.datepicker1.opened = false;
    $scope.datepicker2={};
    $scope.datepicker2.opened = false;
    $scope.datepicker3={};
    $scope.datepicker3.opened = false;
    $scope.isDokter=false;
    $scope.isPasien=false;
    var totalDokter;
    var totalPasien;
    var totalAdmin;
    var totalPerawat;
    var users;
    $scope.pasienVisitGridOptions={};
    $scope.pasienMedrecGridOptions={};
    $scope.pasienResepGridOptions={};
    $scope.adminAplikasi=false;
    $scope.tipe_user_selected=false;
    $scope.klinik_selected=false;
    if($scope.currentUser.fk_role==27){
        $scope.adminAplikasi=true;
    };

    $scope.days = 
        {'0':'Senin',
        '1':'Selasa',
        '2':'Rabu',
        '3':'Kamis',
        '4':"Jum'at",
        '5':'Sabtu',
        '6':'Minggu'};

    $scope.user = {};
    $scope.detail_pasien={};
    $scope.penjamin={};

    $scope.provinsiList=Provinsi.find();
    $scope.$watch('detail_pasien.provinsi',function(){
        if($scope.detail_pasien.provinsi)
                $scope.kabupatenKotaList=Kabupaten_kota.find({filter:{where:{FID_PROVINCE:$scope.detail_pasien.provinsi._id}}});
    }, true);
    $scope.$watch('detail_pasien.kabupaten_kota',function(){
        if($scope.detail_pasien.kabupaten_kota)
                $scope.kecamatanList=Kecamatan.find({filter:{where:{FID_CITY_REGENCY:$scope.detail_pasien.kabupaten_kota._id}}});
    }, true);
    $scope.$watch('detail_pasien.kecamatan',function(){
        if($scope.detail_pasien.kecamatan)
                $scope.kelurahanList=Kelurahan.find({filter:{where:{FID_DISTRICT:$scope.detail_pasien.kecamatan._id}}});
    }, true);
    $scope.provinsiListpj=$scope.provinsiList;
    $scope.$watch('detail_pasien.pj_provinsi',function(){
        if($scope.detail_pasien.pj_provinsi)
                $scope.kabupatenKotaListpj=Kabupaten_kota.find({filter:{where:{FID_PROVINCE:$scope.detail_pasien.pj_provinsi._id}}});
    }, true);
    $scope.$watch('detail_pasien.pj_kabupaten_kota',function(){
        if($scope.detail_pasien.pj_kabupaten_kota)
                $scope.kecamatanListpj=Kecamatan.find({filter:{where:{FID_CITY_REGENCY:$scope.detail_pasien.pj_kabupaten_kota._id}}});
    }, true);
    $scope.$watch('detail_pasien.pj_kecamatan',function(){
        if($scope.detail_pasien.pj_kecamatan)
                $scope.kelurahanListpj=Kelurahan.find({filter:{where:{FID_DISTRICT:$scope.detail_pasien.pj_kecamatan._id}}});
    }, true);
    $scope.provinsiListPenjamin=$scope.provinsiList;
    $scope.$watch('penjamin.provinsi',function(){
        if($scope.penjamin.provinsi)
                $scope.kabupatenKotaListPenjamin=Kabupaten_kota.find({filter:{where:{FID_PROVINCE:$scope.penjamin.provinsi._id}}});
    }, true);
    $scope.$watch('penjamin.kabupaten_kota',function(){
        if($scope.penjamin.kabupaten_kota)
                $scope.kecamatanListpj=Kecamatan.find({filter:{where:{FID_CITY_REGENCY:$scope.penjamin.kabupaten_kota._id}}});
    }, true);
    $scope.$watch('penjamin.kecamatan',function(){
        if($scope.penjamin.kecamatan)
                $scope.kelurahanListpj=Kelurahan.find({filter:{where:{FID_DISTRICT:$scope.penjamin.kecamatan._id}}});
    }, true);

    $scope.pagingOptions = {pageSizes: [10, 25, 50], pageSize: 10, currentPage: 1};

    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
        $scope.kliniks = result.data.klinik;
        $scope.kliniksMap={};
        result.data.klinik.forEach(function(klinik){
            $scope.kliniksMap[klinik.id]=klinik;
        });
    }).catch(function onError(result){
        toastr.warning('Get data error', 'Warning');
    });

    function getUser(){
        $http.get(
            "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-role",
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
        ).then(function onSuccess(result){
            $scope.roles = angular.copy(result.data.role);
            $scope.roles = $scope.roles.filter(role=>role.id!=27);
            if($stateParams.id==null)$stateParams.id=$scope.currentUser.id.toString();
            $http.post(
                "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-id",
                {id : $stateParams.id},
                {headers: {"Content-Type": "application/x-www-formurlencoded"}}
            ).then(function onSuccess(result){
                $scope.roles.forEach(function(role){
                    if (role.id==result.data.user.fk_role){
                        result.data.user.fk_role=role;
                    }
                })
                $scope.user = result.data.user;
                // console.log($scope.user);
                if ($scope.user.gender=='M') $scope.user.gender="Laki-laki";
                if ($scope.user.gender=='F') $scope.user.gender="Perempuan";
                $scope.user.fk_klinik=$scope.kliniksMap[$scope.user.fk_klinik];
                $http.get("http://hirata.id:3003/api/detail_pasiens/findOne?"+jQuery.param({
                    filter:{
                        "include":["provinsi","kabupaten_kota","kecamatan","kelurahan",
                        "pj_provinsi","pj_kabupaten_kota","pj_kecamatan","pj_kelurahan"],
                        "where":{"fk_pasien":Number($stateParams.id)}
                    }
                })).then(function(response){
                    $scope.detail_pasien=response.data;
                }).catch(function(response){
                    $scope.detail_pasien={};
                });
            }).catch(function onError(result){
                console.log('delete error',result)
                toastr.warning('Get data error', 'Warning');
            })
        }).catch(function onError(response){
                    toastr.warning('No connection', 'Warning');
        })
    }

    getUser();

    function open() {
        $scope.datepicker.opened = true;
    }
    function open1() {
        $scope.datepicker1.opened = true;
    }
    function open2() {
        $scope.datepicker2.opened = true;
    }
    function open3() {
        $scope.datepicker3.opened = true;
    }
    $scope.save = function save(user){
        user.fk_role=user.fk_role.id;
        if ($scope.user.gender=='Laki-laki') $scope.user.gender="M";
        if ($scope.user.gender=='Perempuan') $scope.user.gender="F";
        $http.put(
            "http://hirata.id:3003/api/users?access_token="+$rootScope.token,
            $scope.user
        ).then(function(response){
            toastr.success("Edit success");
            Detail_pasien.upsert($scope.detail_pasien,function(response){
                // console.log(response);
            });
            if($scope.currentUser.fk_role==26)
                $location.path("edit-profile");
            else
                $location.path("user");
        }).catch(function(response){
            /*"http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/update-user",
            {user : user},
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
          ).then(function onSuccess(result){
                toastr.success(result.data.message);
                if($scope.currentUser.fk_role==26)
                    $location.path("edit-profile");
                else
                    $location.path("user");
          }).catch(function onError(response){*/
            console.log('error',response)
            toastr.warning('Edit error', 'Warning');
        });
    }
    $scope.back = function back(){
        if($scope.back_status)
                $location.path("user");
        else
                $location.path("edit-profile");
    }
    $scope.$watch('user.fk_role',function(newVal, oldVal){
        if($scope.user.fk_role!=null){
                // console.log($scope.user.fk_role);
                $scope.tipe_user_selected=true;
                if($scope.user.fk_role.code=="psn"){
                    $scope.klinik_selected=true;
                    $scope.user.fk_klinik=null;
                    $scope.isPasien=true;
                    $scope.isDokter=false;
                }
                if($scope.user.fk_role.code=="dr"){
                    $scope.user.fk_klinik=null;
                    $scope.klinik_selected=true;
                    $scope.isDokter=true;
                    $scope.isPasien=false;
                }
                if($scope.user.fk_role.code=="adm"){
                    $scope.isDokter=false;
                    $scope.isPasien=false;
                    $scope.klinik_selected=true;
                }
                if($scope.user.fk_role.code=="pwt"){
                    $scope.isDokter=false;
                    $scope.isPasien=false;
                    $scope.klinik_selected=false;
                }
                if($scope.user.fk_role.code=="apl"){
                    $scope.user.fk_klinik=null;
                    $scope.isDokter=false;
                    $scope.isPasien=false;
                    $scope.klinik_selected=true;
                }
        }
    },true);
    $scope.$watch('user.fk_klinik',function(newVal, oldVal){
        if($scope.user.fk_role!=null){
                $scope.klinik_selected=true;
                if($scope.user.fk_role.code=="adm"){
                    $scope.isDokter=false;$scope.isPasien=false;
                }
                if($scope.user.fk_role.code=="pwt"){
                    $scope.isDokter=false;$scope.isPasien=false;
                }
        }
    },true);
    var getDateString=function getDateString(d){
        var datestring = ("0" + d.getDate()).slice(-2) + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" +
        d.getFullYear() + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
        return datestring;
    }
    $scope.filter={};
    $scope.filterDataRiwayatPesanan = function filterDataRiwayatPesanan(){
        $scope.pasienVisitData = angular.copy($scope.unfilteredPasienVisitData);
        if($scope.filter.tanggal!=null){
                // $scope.pasienVisitData
                // console.log($scope.pasienVisitData);
                // console.log(getDateString(new Date($scope.filter.tanggal)).split(' ')[0]);
                // console.log(getDateString(new Date($scope.pasienVisitData[0].date)).split(' ')[0]);
                $scope.pasienVisitData = $scope.pasienVisitData.filter(visit=>
                    visit.date.split(' ')[0] == getDateString(new Date($scope.filter.tanggal)).split(' ')[0]);
        }
    };
    $scope.viewRowIndex = function viewRowIndex(row, index){
        var id = $scope.pasienVisitData[index].id;
        console.log("going to detail-pesanan",id);
        $location.path('/detail-pesanan/'+row.entity.id);
    }
    var setGridOptions = function setGridOptions(){
        var removeRowTemplate = '<span style="display:block; text-align:center;">'+
                '<button ng-click="grid.appScope.viewRowIndex(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                '<i class="fa fa-eye"></i></button></span>';
        $scope.pasienVisitGridOptions = { 
                data: 'pasienVisitData',
                enablePaging: true,
                showFooter: true,
                rowHeight: 36,
                headerRowHeight: 36,
                totalServerItems: 'totalServerItems',
                pagingOptions: $scope.pagingOptions,
                enableFiltering: false,
                showFilter: $scope.showFilter,
                keepLastSelected: true,
                multiSelect: false,
                sortInfo: $scope.sortOptions,
                useExternalSorting: true,
        columnDefs:[
        
        {displayName:'Action',field:'remove',width:"75",cellTemplate: removeRowTemplate, visible:$scope.currentUrl=='/riwayat-pesanan'},
        // {displayName:'Id',field:'id',width:"*"},
        // {displayName:'Id Visit',field:'code',width:"*"},
        {displayName:'Waktu Pesan',field:'date',width:"150",cellFilter:'date:"longDate"',visible:$scope.currentUser.fk_role==26},
        {displayName:'No. antrian',field:'booknum',width:"100",visible:$scope.currentUser.fk_role!=25},
        {displayName:'Estimasi waktu ditindak',field:'estimasi_tindakan',width:"*",visible:$scope.currentUser.fk_role==26},
        {displayName:'Waktu checking',field:'checking_time',width:"*",cellFilter: 'date:"dd/MM/yyyy HH:mm"',
        visible:$scope.currentUrl!='/riwayat-pesanan'},
        {displayName:'Nama Klinik',field:'fk_jadwal_praktek.fk_klinik.name',width:"*",visible:$scope.currentUser.fk_role!=1},
        {displayName:'Nama Poli',field:'fk_jadwal_praktek.poli_name',width:"*"},
        {displayName:'Dokter',field:'fk_dokter',width:"*",visible:$scope.currentUser.fk_role!=25},
        {displayName:'Pasien',field:'fk_pasien.fullname',width:"*",visible:$scope.currentUser.fk_role!=26},
        // {displayName:'Fk_history_praktek',field:'fk_history_praktek',width:"*"},
        {displayName:'Perawat',field:'fk_perawat.fullname',width:"*",visible:$scope.currentUser.fk_role!=26},
        // {displayName:'Rating',field:'rating',width:"*"},
        {displayName:'Status',field:'status',width:"*",visible:$scope.currentUrl!='/riwayat-pesanan'},
        // {displayName:'Total',field:'total',width:"*"},
        {displayName:'Status Poliklinik',field:'fk_history_praktek.status',width:"*",visible:$scope.currentUrl=='/riwayat-pesanan'},
        {displayName:'Remark',field:'fk_history_praktek.remark',width:"*",visible:$scope.currentUrl=='/riwayat-pesanan'},
        ]};
        $scope.pasienMedrecGridOptions = { 
                data: 'pasienMedrecData',
                enablePaging: true,
                showFooter: true,
                rowHeight: 36,
                headerRowHeight: 36,
                totalServerItems: 'totalServerItems',
                pagingOptions: $scope.pagingOptions,
                enableFiltering: false,
                showFilter: $scope.showFilter,
                keepLastSelected: true,
                multiSelect: false,
                sortInfo: $scope.sortOptions,
                useExternalSorting: true,
        columnDefs:[
        
        // {displayName:'Id',field:'id',width:"*"},
        // {displayName:'Id Medrec',field:'code',width:"*"},
        {displayName:'Tanggal Kunjungan',field:'fk_visit.estimasi_tindakan',width:"*"},
        {displayName:'Nama Pasien',field:'fk_pasien.fullname',width:"*",visible:$scope.currentUser.fk_role!=26},
        {displayName:'Nama Dokter',field:'fk_visit.fk_dokter',width:"*",visible:$scope.currentUser.fk_role!=25},
        {displayName:'Tensi',field:'tensi',width:"*"},
        {displayName:'Berat_badan',field:'berat_badan',width:"*"},
        {displayName:'Keluhan',field:'keluhan',width:"*"},
        {displayName:'Gejala',field:'gejala',width:"*"},
        {displayName:'Indikasi',field:'indikasi',width:"*"},
        {displayName:'Diagnosa',field:'diagnosa',width:"*"},
        {displayName:'Nama Klinik',field:'fk_klinik.name',width:"*",visible:$scope.currentUser.fk_role!=1},
     
        ]};

        $scope.pasienResepGridOptions = { 
                data: 'pasienResepData',
                enablePaging: true,
                showFooter: true,
                rowHeight: 36,
                headerRowHeight: 36,
                totalServerItems: 'totalServerItems',
                pagingOptions: $scope.pagingOptions,
                enableFiltering: false,
                showFilter: $scope.showFilter,
                keepLastSelected: true,
                multiSelect: false,
                sortInfo: $scope.sortOptions,
                useExternalSorting: true,
        columnDefs:[
                // {displayName:'Id',field:'id',width:"*"},
                {displayName:'Tanggal Kunjungan',field:'fk_visit.estimasi_tindakan',width:"*"},
                {displayName:'Nama Pasien',field:'fk_visit.fk_pasien.fullname',width:"*",visible:$scope.currentUser.fk_role!=26},
                {displayName:'Obat',field:'obat',width:"*"},
                {displayName:'Dosis',field:'dosis',width:"*"},
                {displayName:'Quantity',field:'quantity',width:"*"},
                // {displayName:'Price',field:'price',width:"*"},
        ]};
    };

    $scope.savePenjamin = function savePenjamin(){
        console.log($scope.penjamin);
        let edit=false;
        if($scope.penjamin.id)
            edit=true;
        $scope.penjamin.fk_pasien=Number($stateParams.id);
        Penjamin_pasien.upsert($scope.penjamin,function(){
            let temp = angular.copy($scope.penjamin);
            if(temp.jenis_penjamin=='Umum')
                temp.nama=temp.fullname;
            else if(temp.jenis_penjamin=='BPJS')
                temp.nama="BPJS Kesehatan";
            else
                temp.nama=temp.nama_asuransi;
            $scope.penjaminList.push(temp);
            $scope.penjamin={};
        });
    };

    $scope.editPenjamin = function editPenjamin(row, index){
        $scope.penjamin=row.entity;
        $scope.penjaminList.splice(index,1);
    };
    var confirmDelete = function confirmDelete(confirmation){
        return confirmation ? confirm('This action is irreversible. Do you want to delete this Penjamin?') : true;
    };
    $scope.removePenjamin = function removePenjamin( row, index, confirmation ){
        var id = row.entity.id;
        confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
        if (confirmDelete(confirmation)) {
            Penjamin_pasien.deleteById({id:id},function onSuccess(){
                $scope.penjaminList.splice($scope.penjaminList.indexOf(row.entity),1);
            });
        }
    }
    var penjaminActionTemplate = '<span style="display:block; text-align:center;">'+
      '<button type="button" ng-click="grid.appScope.editPenjamin(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
      '<i class="fa fa-pencil"></i></button><button type="button" ng-click="grid.appScope.removePenjamin(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
      '<i class="fa fa-times"></i></button></span>';
    $scope.penjaminList=Penjamin_pasien.find({filter:{where:{fk_pasien:Number($stateParams.id)}}},
    function(response){
        $scope.penjaminList.forEach(temp=>{
            if(temp.jenis_penjamin=='Umum')
                temp.nama=temp.fullname;
            else if(temp.jenis_penjamin=='BPJS')
                temp.nama="BPJS Kesehatan";
            else
                temp.nama=temp.nama_asuransi;
        });
    },function(response){
        console.log($scope.penjaminList);
    });
    // console.log($scope.penjaminList);
    $scope.penjaminGridOptions = { 
            data: 'penjaminList',
            enablePaging: true,
            showFooter: true,
            rowHeight: 36,
            headerRowHeight: 36,
            totalServerItems: 'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            enableFiltering: false,
            showFilter: $scope.showFilter,
            keepLastSelected: true,
            multiSelect: false,
            sortInfo: $scope.sortOptions,
            useExternalSorting: true,
    columnDefs:[
            {displayName:'Aksi',field:'remove',width:"75",cellTemplate: penjaminActionTemplate},
            {displayName:'Jenis Penjamin',field:'jenis_penjamin',width:"*"},
            {displayName:'Nama',field:'nama',width:"*"},
            {displayName:'No. Anggota/Polis',field:'no_anggota_polis',width:"*"},
    ]};
    var getResep=function getResep(){
        $http.get(
                "http://" + $rootScope.address +":"+ $rootScope.port.penanganan + "/get-resep",
                {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
        ).then(function onSuccess(result){
                // console.log("success",result);
                data = result.data.resep;
                var pasienVisitIds = [];
                $scope.pasienVisitData.forEach(visit=>pasienVisitIds.push(visit.id));
                if(data!=null){
                    data = data.filter(resep=>pasienVisitIds.includes(resep.fk_visit))
                    data.forEach(resep=>{
                        resep.fk_visit=$scope.visitsMap[resep.fk_visit];
                        // console.log($scope.visitsMap);
                    });
                }
                // console.log(data);
                $scope.pasienResepData = data;
        }).catch(function onError(response){
                toastr.warning('No connection', 'Warning');
                console.log("no connection",response)
        });
    }
    var getMedrecPasien=function getMedrecPasien(idVisit){
        Medrec.find({filter:{where:{"fk_visit":idVisit}}},function(response){
            var data = response;
            if($scope.currentUser.fk_role==26){
                // console.log()
                if(data!=null){
                    data.forEach(medrec=>{
                        medrec.fk_visit=$scope.visitsMap[medrec.fk_visit];
                    });
                }
                $scope.pasienMedrecData=data;
                // console.log($scope.pasienMedrecData);
            }
            else{
                if(data!=null){
                    data=data.filter(medrec=>medrec.fk_pasien==$stateParams.id);
                    data.forEach(medrec=>{
                        medrec.fk_visit=$scope.visitsMap[medrec.fk_visit];
                        medrec.fk_klinik=$scope.kliniksMap[medrec.fk_klinik];
                        medrec.fk_pasien=$scope.usersMap[medrec.fk_pasien];
                        // console.log($scope.visitsMap);
                    });
                    $scope.pasienMedrecData = data.filter(medrec=>medrec.fk_klinik.id==$scope.currentUser.fk_klinik);
                }
                getResep();
            }
        },function(response){
            toastr.warning('No connection', 'Warning');
            console.log("no connection",response);
        });
    }
    var getVisit=function getVisit(){
        $scope.doktersMap={};
        $http.post(
                "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-role",
                {role_id : "25"},
                {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
        ).then(function onSuccess(result){
                result.data.user.forEach(dokter=>{
                    $scope.doktersMap[dokter.id]=dokter;
                });
                console.log($scope.doktersMap);
                $http.get(
                    "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-visit",
                    {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                  ).then(function onSuccess(result){
                    if($scope.currentUser.fk_role==25 || $scope.currentUser.fk_role==1){
                        data = result.data.visit;
                        data.forEach(visit=>{
                            visit.fk_jadwal_praktek=$scope.prakteksMap[visit.fk_jadwal_praktek];
                        });
                        if(data!=null){
                            if($scope.currentUser.fk_role==25)
                                      $scope.pasienVisitData = data.filter(visit=>visit.fk_dokter==$scope.currentUser.id && visit.fk_pasien==$stateParams.id);
                            else if($scope.currentUser.fk_role==1)
                                      $scope.pasienVisitData = data.filter(visit=>visit.fk_pasien==$stateParams.id && $scope.currentUser.fk_klinik==visit.fk_jadwal_praktek.fk_klinik.id);
                            // console.log($scope.usersMap);
                            $scope.pasienVisitData.forEach(visit=>{
                                      visit.fk_pasien=$scope.usersMap[visit.fk_pasien];
                                      visit.fk_dokter=$scope.doktersMap[visit.fk_dokter].fullname;
                                      $http.post(
                                        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-history-praktek",
                                        {jadwal_praktek_id : visit.fk_jadwal_praktek.id.toString()},
                                        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                                      ).then(function onSuccess(result){
                                        var d = new Date(new Date(result.data.history_praktek.open_time).getTime() + visit.estimasi_tindakan*60000);
                                        d.setHours(d.getHours() - 7);
                                        visit.estimasi_tindakan=getDateString(d);
                                      }).catch(function onError(response){
                                        toastr.warning('No connection', 'Warning');
                                        console.log("no connection",response)
                                      });
                                      $scope.visitsMap[visit.id]=visit;
                                      // console.log($scope.pasienVisitData);
                                      $scope.totalServerItem = result.data.length;
                                      $scope.listlength = result.data.length;
                                      getMedrecPasien($scope.pasienVisitData[0].id.toString());
                            });
                        }
                    }else
                    if($scope.currentUser.fk_role==26){
                        data = result.data.visit;
                        data.forEach(data=>{
                            // data.fk_pasien=$scope.usersMap[data.fk_pasien];
                            // data.fk_jadwal_praktek=$scope.prakteksMap[data.fk_jadwal_praktek];
                            $scope.visitsMap[data.id]=data;
                        });
                    }
                  }).catch(function onError(response){
                    toastr.warning('No connection', 'Warning');
                    console.log("no connection",response)
                  });
        }).catch(function onError(response){
                toastr.warning('No connection', 'Warning');
                console.log("no connection",response)
        });
    }
    if($scope.currentUser.fk_role==26){
        $scope.prakteksMap={};
        $scope.kliniksMap={};
        $scope.doktersMap={};
        $scope.visitsMap={};
        Visit.find({},function(response){
            if(response!=null){
                $scope.visits=response.filter(visit=>visit.fk_pasien==$scope.currentUser.id).reverse();
                $scope.pasienVisitData=$scope.visits;
                $scope.unfilteredPasienVisitData = $scope.pasienVisitData;
                if($scope.visits.length > 0)
                    $scope.has_booking = true;
                $http.get(
                    "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
                    {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                ).then(function onSuccess(result){
                    result.data.klinik.forEach(klinik=>{
                        $scope.kliniksMap[klinik.id]=klinik;
                    });
                    $http.post(
                        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-role",
                        {role_id : "25"},
                        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                    ).then(function onSuccess(result){
                        result.data.user.forEach(dokter=>{
                                  $scope.doktersMap[dokter.id]=dokter;
                        });
                        $http.get(
                                  "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-praktek",
                                  {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                        ).then(function onSuccess(result){
                                  result.data.praktek.forEach(praktek=>{
                                    praktek.day=$scope.days[praktek.day];
                                    praktek.fk_klinik=$scope.kliniksMap[praktek.fk_klinik];
                                    praktek.fk_dokter=$scope.doktersMap[praktek.fk_dokter];
                                    $scope.prakteksMap[praktek.id]=praktek;
                                  });
                                  $scope.pasienMedrecData=[];
                                  $scope.visits.forEach(visit=>{
                                    $http.post(
                                          "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-history-praktek",
                                          {jadwal_praktek_id : visit.fk_jadwal_praktek.toString()},
                                          {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                                    ).then(function onSuccess(result){
                                          var d = new Date(new Date(result.data.history_praktek.open_time).getTime() + visit.estimasi_tindakan*60000);
                                          // console.log(d);
                                          d.setHours(d.getHours() - 7);
                                          visit.fk_dokter=$scope.doktersMap[visit.fk_dokter].fullname;
                                          visit.estimasi_tindakan=getDateString(d);
                                          visit.date=getDateString(new Date(visit.date));
                                          // visit.chec
                                          $scope.visitsMap[visit.id]=visit;
                                          visit.fk_history_praktek=result.data.history_praktek;
                                    }).catch(function onError(response){
                                          console.log('error',response);
                                          toastr.warning('Get data error', 'Warning');
                                    });
                                    visit.fk_jadwal_praktek=$scope.prakteksMap[visit.fk_jadwal_praktek];
                                  });
                                  if($scope.visits[0]!=null)
                                    getMedrecPasien($scope.visits[0].id.toString());
                                  getResep();
                        }).catch(function onError(response){
                                  console.log('error',response);
                                  toastr.warning('Get data error', 'Warning');
                        });
                    }).catch(function onError(response){
                        console.log('error',response);
                        toastr.warning('Get data error', 'Warning');
                    });
                }).catch(function onError(response){
                    console.log('error',response);
                    toastr.warning('Get data error', 'Warning');
                });
            }
        },function(response){
            console.log('error',response);
            toastr.warning('Get data error', 'Warning');
        });
        setGridOptions();
    }
    if($scope.currentUser.fk_role==25 || $scope.currentUser.fk_role==1){
        $scope.usersMap = {};
        $scope.prakteksMap = {};
        $scope.kliniksMap = {};
        $scope.visitsMap = {};
        $scope.getPagedHistoryVisitAsync = function getPagedHistoryVisitAsync(pageSize, page, searchText) {
                setTimeout(function () {
                    var data;
                    var page = $scope.pagingOptions.currentPage;
                    var pageSize = $scope.pagingOptions.pageSize;
                    var offset = (page - 1) * pageSize;
                    $http.post(
                        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-role",
                        {role_id : "26"},
                        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                    ).then(function onSuccess(result){
                        // console.log("success",result);
                        data = result.data.user;
                        data.forEach(data=>{
                            $scope.usersMap[data.id]=data;
                        })
                        $http.get(
                            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
                            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                            ).then(function onSuccess(result){
                            data = result.data.klinik;
                            data.forEach(data=>{
                                      $scope.kliniksMap[data.id]=data;
                            });
                            $http.get(
                                      "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-praktek",
                                      {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                                  ).then(function onSuccess(result){
                                      data = result.data.praktek;
                                      data.forEach(data=>{
                                        data.fk_klinik=$scope.kliniksMap[data.fk_klinik];
                                        $scope.prakteksMap[data.id]=data;
                                      });
                                      $http.post(
                                        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-role",
                                        {role_id : "24"},
                                        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                                      ).then(function onSuccess(result){
                                        // console.log("success",result);
                                        data = result.data.user;
                                        data.forEach(data=>{
                                              $scope.usersMap[data.id]=data;
                                        });
                                        getVisit();
                                      }).catch(function onError(response){
                                              toastr.warning('No connection', 'Warning');
                                              console.log("no connection",response)
                                      });
                            }).catch(function onError(response){
                                  toastr.warning('No connection', 'Warning');
                                  console.log("no connection",response)
                            });
                            }).catch(function onError(response){
                                  toastr.warning('No connection', 'Warning');
                                  console.log("no connection",response)
                            });
                    }).catch(function onError(response){
                            toastr.warning('No connection', 'Warning');
                            console.log("no connection",response)
                    });
                }, 100);
        };
        
        setGridOptions();
        $scope.getPagedHistoryVisitAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }
}

function UserNewController ($scope, $rootScope, $state, toastr, $http, $location, User, Detail_pasien, 
    Penjamin_pasien, Provinsi, Kabupaten_kota, Kecamatan, Kelurahan, User) {
    // console.log($rootScope);

    $scope.currentController="new";
    $scope.back_status = true;
    $scope.has_booking = false;
    $scope.user = {};
    $scope.roles = [];
    $scope.open = open;
    $scope.open1 = open1;
    $scope.open2 = open2;
    $scope.open3 = open3;
    $scope.datepicker={};
    $scope.datepicker.opened = false;
    $scope.datepicker1={};
    $scope.datepicker1.opened = false;
    $scope.datepicker2={};
    $scope.datepicker2.opened = false;
    $scope.datepicker3={};
    $scope.datepicker3.opened = false;
    $scope.disabled=false;
    $scope.isDokter=false;
    $scope.isPasien=false;
    var totalDokter;
    var totalPasien;
    var totalAdmin;
    var totalPerawat;
    var users;
    $scope.pasienVisitGridOptions={};
    $scope.pasienMedrecGridOptions={};
    $scope.pasienResepGridOptions={};
    $scope.adminAplikasi=false;
    $scope.tipe_user_selected=false;
    $scope.klinik_selected=false;
    if($scope.currentUser.fk_role==27){
        $scope.adminAplikasi=true;
    };

    $scope.user = {};
    $scope.detail_pasien={};
    $scope.penjamin={};

    $scope.provinsiList=Provinsi.find();
    $scope.$watch('detail_pasien.provinsi',function(){
        if($scope.detail_pasien.provinsi)
                $scope.kabupatenKotaList=Kabupaten_kota.find({filter:{where:{FID_PROVINCE:$scope.detail_pasien.provinsi._id}}});
    }, true);
    $scope.$watch('detail_pasien.kabupaten_kota',function(){
        if($scope.detail_pasien.kabupaten_kota)
                $scope.kecamatanList=Kecamatan.find({filter:{where:{FID_CITY_REGENCY:$scope.detail_pasien.kabupaten_kota._id}}});
    }, true);
    $scope.$watch('detail_pasien.kecamatan',function(){
        if($scope.detail_pasien.kecamatan)
                $scope.kelurahanList=Kelurahan.find({filter:{where:{FID_DISTRICT:$scope.detail_pasien.kecamatan._id}}});
    }, true);
    $scope.provinsiListpj=$scope.provinsiList;
    $scope.$watch('detail_pasien.pj_provinsi',function(){
        if($scope.detail_pasien.pj_provinsi)
                $scope.kabupatenKotaListpj=Kabupaten_kota.find({filter:{where:{FID_PROVINCE:$scope.detail_pasien.pj_provinsi._id}}});
    }, true);
    $scope.$watch('detail_pasien.pj_kabupaten_kota',function(){
        if($scope.detail_pasien.pj_kabupaten_kota)
                $scope.kecamatanListpj=Kecamatan.find({filter:{where:{FID_CITY_REGENCY:$scope.detail_pasien.pj_kabupaten_kota._id}}});
    }, true);
    $scope.$watch('detail_pasien.pj_kecamatan',function(){
        if($scope.detail_pasien.pj_kecamatan)
                $scope.kelurahanListpj=Kelurahan.find({filter:{where:{FID_DISTRICT:$scope.detail_pasien.pj_kecamatan._id}}});
    }, true);
    $scope.provinsiListPenjamin=$scope.provinsiList;
    $scope.$watch('penjamin.provinsi',function(){
        if($scope.penjamin.provinsi)
                $scope.kabupatenKotaListPenjamin=Kabupaten_kota.find({filter:{where:{FID_PROVINCE:$scope.penjamin.provinsi._id}}});
    }, true);
    $scope.$watch('penjamin.kabupaten_kota',function(){
        if($scope.penjamin.kabupaten_kota)
                $scope.kecamatanListpj=Kecamatan.find({filter:{where:{FID_CITY_REGENCY:$scope.penjamin.kabupaten_kota._id}}});
    }, true);
    $scope.$watch('penjamin.kecamatan',function(){
        if($scope.penjamin.kecamatan)
                $scope.kelurahanListpj=Kelurahan.find({filter:{where:{FID_DISTRICT:$scope.penjamin.kecamatan._id}}});
    }, true);

    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-role",
        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
    ).then(function onSuccess(result){
    $scope.roles = angular.copy(result.data.role);
    $scope.roles = $scope.roles.filter(role=>role.id!=27);
    }).catch(function onError(response){
    toastr.warning('No connection', 'Warning');
    })
    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user",
        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
    ).then(function onSuccess(result){
        data = result.data.user;
        users = data;
        totalPasien = data.filter(user=>user.fk_role==26).length
        totalDokter = data.filter(user=>user.fk_role==25).length
        if($scope.currentUser.fk_role!=27){
                totalAdmin = data.filter(user=>user.fk_role==1).filter(user=>user.fk_klinik==$scope.currentUser.fk_klinik).length
                totalPerawat = data.filter(user=>user.fk_role==24).filter(user=>user.fk_klinik==$scope.currentUser.fk_klinik).length
        }
    }).catch(function onError(response){
        toastr.warning('No connection', 'Warning');
        console.log("no connection",response)
    });
    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
        $scope.kliniks = result.data.klinik;
        $scope.kliniksMap={};
        result.data.klinik.forEach(function(klinik){
            $scope.kliniksMap[klinik.id]=klinik;
        });
    }).catch(function onError(result){
        toastr.warning('Get data error', 'Warning');
    });

    function addDetailPasien(){
        $http.get("http://hirata.id:3003/api/users/findOne?"+jQuery.param({
            filter:{
                "fields":"id",
                "where":{"code":$scope.user.code}
            },
            access_token:$rootScope.token
        })).then(function onSuccess(response){
            let userId=response.data.id;
            $scope.detail_pasien.fk_pasien=userId;
            console.log($scope.detail_pasien);
            Detail_pasien.create($scope.detail_pasien,function(response){
                console.log(response);
            });
        }).catch(function onError(response){
            console.log(response);
        });
    }

    $scope.savePenjamin = function savePenjamin(){
        // console.log($scope.penjamin);
        let temp = angular.copy($scope.penjamin);
        if(temp.jenis_penjamin=='Umum')
            temp.nama=temp.fullname;
        else if(temp.jenis_penjamin=='BPJS')
            temp.nama="BPJS Kesehatan";
        else
            temp.nama=temp.nama_asuransi;
        $scope.penjaminList.push(temp);
        $scope.penjamin={};
    };

    $scope.editPenjamin = function editPenjamin(row, index){
        $scope.penjamin=row.entity;
        $scope.penjaminList.splice(index,1);
    };
    var confirmDelete = function confirmDelete(confirmation){
        return confirmation ? confirm('This action is irreversible. Do you want to delete this Penjamin?') : true;
    };
    $scope.removePenjamin = function removePenjamin( row, index, confirmation ){
        var id = row.entity.id;
        confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
        if (confirmDelete(confirmation)) {
            Penjamin_pasien.deleteById({id:id},function onSuccess(){
                $scope.penjaminList.splice($scope.penjaminList.indexOf(row.entity),1);
            });
        }
    }
    var penjaminActionTemplate = '<span style="display:block; text-align:center;">'+
      '<button type="button" ng-click="grid.appScope.editPenjamin(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
      '<i class="fa fa-pencil"></i></button><button type="button" ng-click="grid.appScope.removePenjamin(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
      '<i class="fa fa-times"></i></button></span>';
    $scope.penjaminList=[];
    // console.log($scope.penjaminList);
    $scope.penjaminGridOptions = { 
            data: 'penjaminList',
            enablePaging: true,
            showFooter: true,
            rowHeight: 36,
            headerRowHeight: 36,
            totalServerItems: 'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            enableFiltering: false,
            showFilter: $scope.showFilter,
            keepLastSelected: true,
            multiSelect: false,
            sortInfo: $scope.sortOptions,
            useExternalSorting: true,
    columnDefs:[
            {displayName:'Aksi',field:'remove',width:"75",cellTemplate: penjaminActionTemplate},
            {displayName:'Jenis Penjamin',field:'jenis_penjamin',width:"*"},
            {displayName:'Nama',field:'nama',width:"*"},
            {displayName:'No. Anggota/Polis',field:'no_anggota_polis',width:"*"},
    ]};

    $scope.save = function save(user){
        user.fk_role=user.fk_role.id;
        if ($scope.user.gender=='Laki-laki') $scope.user.gender="M";
        if ($scope.user.gender=='Perempuan') $scope.user.gender="F";
        if(user.fk_role==1 || user.fk_role==24){
                if($rootScope.userRole="Admin Aplikasi")
                    user.fk_klinik=user.fk_klinik.id;
                else
                    user.fk_klinik=$scope.currentUser.fk_klinik;
        }else
                user.fk_klinik=0;
        if($scope.disabled){
                $http.post(
                    "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/register-dokter",
                    {klinik_id : $scope.currentUser.fk_klinik.toString(), user_id : $scope.user.id.toString()},
                    {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                ).then(function onSuccess(result){
                    toastr.success(result.data.message);
                    $location.path("user");
                }).catch(function onError(response){
                    console.log('error',response)
                    toastr.warning('Add data error', 'Warning');
                });
        }else{
            User.create(user,function(response){
                toastr.success("insert success");
                if(user.fk_role==25 || user.fk_role==26){
                    var klinik_id;
                    if($scope.currentUser.fk_role==1){
                        klinik_id=$scope.currentUser.fk_klinik.toString()
                        $http.post(
                            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/register-dokter",
                            {klinik_id : klinik_id, user_id : response.id.toString()},
                            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                        ).then(function onSuccess(result){
                            toastr.success(result.data.message);
                        }).catch(function onError(response){
                            console.log('error',response);
                            toastr.warning('Add data error', 'Warning');
                        });
                    }
                    if(user.fk_role==26){
                        addDetailPasien();
                        $scope.penjaminList.forEach(penjamin=>{
                            penjamin.fk_pasien=Number(response.id);
                            Penjamin_pasien.upsert(penjamin,function(response){
                            },function(response){
                                console.log(response);
                            });
                        });
                    }
                }
                $location.path("user");
            },function(response){
                console.log('error',response);
                toastr.warning('Add data error', 'Warning');
            });
        }
    }

    function open() {
        $scope.datepicker.opened = true;
    }
    function open1() {
        $scope.datepicker1.opened = true;
    }
    function open2() {
        $scope.datepicker2.opened = true;
    }
    function open3() {
        $scope.datepicker3.opened = true;
    }
    $scope.back = function back(){
        // console.log($scope.createForm);
        $location.path("user");
    }
    function randomString() {
        var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var length=8;
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }
    $scope.$watch('user.fk_role',function(newVal, oldVal){
        if($scope.user.fk_role!=null){
                // console.log($scope.user.fk_role);
                $scope.tipe_user_selected=true;
                if($scope.user.fk_role.code=="psn"){
                    $scope.klinik_selected=true;
                    $scope.user.fk_klinik=null;
                    $scope.isPasien=true;$scope.isDokter=false;
                    // $scope.user.code="psn"+(totalPasien+1).toString();
                    $scope.user.code = randomString();
                }else
                if($scope.user.fk_role.code=="dr"){
                    $scope.user.fk_klinik=null;
                    $scope.klinik_selected=true;
                    $scope.isDokter=true;$scope.isPasien=false;
                    // $scope.user.code="dr"+(totalDokter+1).toString();
                    $scope.user.code = randomString();
                    if($scope.dokters==null){
                        $http.post(
                            "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-role",
                            {role_id : "25"},
                            {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                        ).then(function onSuccess(result){
                            $scope.dokters = result.data.user;
                            }).catch(function onError(result){
                            console.log('delete error',result)
                            toastr.warning('Get data error', 'Warning');
                        });
                    }
                    if($scope.onSelect==null){
                        $scope.onSelect = function onSelect($item){
                            // console.log($item);
                            $scope.user = $item;
                            if ($scope.user.gender=='M') $scope.user.gender="Laki-laki";
                            if ($scope.user.gender=='F') $scope.user.gender="Perempuan";
                            $scope.user.fk_role={id: 25,code: 'dr', name: 'Dokter'};
                            $scope.disabled=true;
                            // $scope.createForm.$invalid=false;
                            // console.log($scope.createForm);
                        };
                    }
                }else
                if($scope.user.fk_role.code=="adm"){
                    $scope.isDokter=false;$scope.isPasien=false;
                    if($scope.currentUser.fk_role==1){
                        $scope.klinik_selected=true;
                        totalAdmin = data.filter(user=>user.fk_role==1).filter(user=>user.fk_klinik==$scope.currentUser.fk_klinik).length;
                        $scope.user.code="k"+$scope.currentUser.fk_klinik.toString()+"adm"+(totalAdmin+1).toString();
                    }
                    if($scope.currentUser.fk_role==27){
                        $scope.klinik_selected=false;
                        if($scope.user.fk_klinik!=null){
                            totalAdmin = data.filter(user=>user.fk_role==1).filter(user=>user.fk_klinik==$scope.user.fk_klinik.id).length;
                            $scope.user.code="k"+$scope.user.fk_klinik.id+"adm"+(totalAdmin+1).toString();
                        }
                    }
                }else
                if($scope.user.fk_role.code=="pwt"){
                    $scope.isDokter=false;$scope.isPasien=false;
                    if($scope.currentUser.fk_role==1){
                        $scope.klinik_selected=true;
                        totalPerawat = data.filter(user=>user.fk_role==24).filter(user=>user.fk_klinik==$scope.currentUser.fk_klinik).length;
                        $scope.user.code="k"+$scope.currentUser.fk_klinik+"pwt"+(totalPerawat+1).toString();
                    }
                    if($scope.currentUser.fk_role==27){
                        $scope.klinik_selected=false;
                        if($scope.user.fk_klinik!=null){
                            totalPerawat = data.filter(user=>user.fk_role==24).filter(user=>user.fk_klinik==$scope.user.fk_klinik.id).length;
                            $scope.user.code="k"+$scope.user.fk_klinik.id+"pwt"+(totalPerawat+1).toString();
                        }
                    }
                }else
                if($scope.user.fk_role.code=="apl"){
                    $scope.user.fk_klinik=null;
                    $scope.user.code="";
                    $scope.isDokter=false;$scope.isPasien=false;
                    $scope.klinik_selected=true;
                }else{
                    $scope.klinik_selected=true;
                    $scope.user.fk_klinik=null;
                    $scope.isPasien=false;$scope.isDokter=false;
                }
                // console.log((tipe_user_selected && klinik_selected) || currentUser.fk_role==25 || currentUser.fk_role==26);
        }
    },true);
    if($scope.currentUser.fk_role==27){
        $scope.$watch('user.fk_klinik',function(newVal, oldVal){
                if($scope.user.fk_role!=null){
                    $scope.klinik_selected=true;
                    if($scope.user.fk_role.code=="adm"){
                        $scope.isDokter=false;$scope.isPasien=false;
                        if($scope.user.fk_klinik!=null){
                            totalAdmin = data.filter(user=>user.fk_role==1).filter(user=>user.fk_klinik==$scope.user.fk_klinik.id).length
                            $scope.user.code="k"+$scope.user.fk_klinik.id+"adm"+(totalAdmin+1).toString()
                        }
                    }
                    if($scope.user.fk_role.code=="pwt"){
                        $scope.isDokter=false;$scope.isPasien=false;
                        if($scope.user.fk_klinik!=null){
                            totalPerawat = data.filter(user=>user.fk_role==24).filter(user=>user.fk_klinik==$scope.user.fk_klinik.id).length
                            $scope.user.code="k"+$scope.user.fk_klinik.id+"pwt"+(totalPerawat+1).toString()   
                        }
                    }
                }
        },true);
    }
}

function RegisterController ($scope, $rootScope, $state, toastr, $http, $location, $ocLazyLoad){
    $ocLazyLoad.load("app/auth.css");
    console.log("test");
    $scope.user = {};
    $scope.gender = [{label : "Laki-laki"},{label : "Perempuan"}];
    // console.log("user",$scope.user)
    $scope.isDokter=true;
    // console.log(!(isDokter || isPasien || user.fk_role.id==27));
    $scope.register = function register(user){
        // console.log("register",user)
        let gender = "";
        var totalPasien;
        if(user.gender.label == "Laki-laki")
                gender = "M"
        else
                gender = "F"

        $http.get(
                "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user",
                {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
        ).then(function onSuccess(result){
                data = result.data.user;
                totalPasien = data.filter(user=>user.fk_role==26).length;
                $http.post(
                 "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/register-pasien",
                 {
                    user : {
                        code : "psn"+(totalPasien+1).toString(),
                        fullname : user.fullname,
                        email : user.email,
                        password : user.password,
                        gender : gender,
                        mobile : user.mobile
                    }
                },
                {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                ).then(function onSuccess(result){
                  // console.log("Register success"),
                  $state.go('signin');
                }).catch(function onError(result){
                  if(result.data.error.includes('Duplicate')){
                    toastr.warning('Email sudah terdaftar', 'Warning');
                  }else
                  toastr.warning('Register Fail', 'Warning');
                })
        }).catch(function onError(response){
                toastr.warning('No connection', 'Warning');
                console.log("no connection",response)
        });
    }
}
;