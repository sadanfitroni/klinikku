angular.module('BlurAdmin').controller('ListPoliController', ListPoliController);
angular.module('BlurAdmin').controller('DetailPoliController', DetailPoliController);
angular.module('BlurAdmin').controller('DetailBookingController', DetailBookingController);
// angular.module('BlurAdmin').controller('ListPoliNewController', ListPoliNewController);
// angular
// .module('BlurAdmin').controller('ListPoliViewController', ListPoliViewController);

function ListPoliController($scope, $rootScope, $state, toastr, $http, $location) {
    $scope.filter={};
    $scope.prakteks=[];
    $scope.unfiltered_prakteks=[];
    $scope.days=["Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu"];
    $scope.open = open;
    $scope.opened = false;
    $scope.sortParams=['Harga Termurah','Harga Termahal','Buka Paling Awal','Tutup Paling Akhir','Antrian Paling Sedikit','Antrian Paling Banyak'];
    $scope.sort={};
    $scope.price_rate = [
        {min:0, max:100000, display:"<= Rp.100.000"},
        {min:100000, max:200000, display:"<= Rp.100.000 dan >= Rp.200.000"},
        {min:200000, max:1000000000, display:">= Rp.200.000"}
    ];
    $scope.datepicker={};
    var dateToday = new Date();
    var sixDaysLater = new Date();
    sixDaysLater.setDate(dateToday.getDate()+6);
    $scope.datepicker.options = {
        minDate: dateToday.getTime(),
        maxDate: sixDaysLater.getTime(),
    };
    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
    ).then(function onSuccess(result){
        $scope.kliniks={};
        result.data.klinik.forEach(function(klinik){
            $scope.kliniks[klinik.id]=klinik;
        });
        $http.post(
            "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-role",
            {role_id : "25"},
            {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
            $scope.dokters={};
            if(result.data.user)
                result.data.user.forEach(function(user){
                    $scope.dokters[user.id]=user;
                });
            $http.get(
                "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-today-and-later-history-praktek",
                {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
            ).then(function onSuccess(result){
                // var antrianPraktek = {};
                var historyPraktekMap = {};
                if(result.data.history_prakteks!=null){
                    result.data.history_prakteks.forEach(history_praktek=>{
                        // antrianPraktek[history_praktek.fk_jadwal_praktek]=history_praktek.book_count;
                        historyPraktekMap[history_praktek.fk_jadwal_praktek]=history_praktek;
                    });
                }
                $http.get(
                    "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-praktek",
                    {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                ).then(function onSuccess(result){
                    // console.log("result", result);
                    /*result.data.praktek.forEach(function(praktek){
                        praktek.fk_klinik=$scope.kliniks[praktek.fk_klinik];
                        praktek.fk_dokter=$scope.dokters[praktek.fk_dokter];
                        praktek.day=$scope.days[praktek.day];
                        if(historyPraktekMap[praktek.id]!=null){
                            praktek.jumlah_antrian=historyPraktekMap[praktek.id].book_count;
                            praktek.open_time=historyPraktekMap[praktek.id].open_time.substring(0,4);
                            praktek.close_time=historyPraktekMap[praktek.id].close_time.substring(0,4);
                        }
                        else{
                            praktek.jumlah_antrian=0;
                            praktek.open_time=praktek.time_from.substring(0,5);
                            praktek.close_time=praktek.time_to.substring(0,5);
                        }
                    });
                    $scope.unfiltered_prakteks=result.data.praktek;
                    if($scope.currentUser.fk_role==1)
                        $scope.unfiltered_prakteks=$scope.unfiltered_prakteks.filter(praktek=>praktek.fk_klinik.id==$scope.currentUser.fk_klinik);*/
                    console.log("result", result);
                    if(result.data.praktek){
                        result.data.praktek.forEach(function(praktek){
                            praktek.fk_klinik=$scope.kliniks[praktek.fk_klinik];
                            praktek.fk_dokter=$scope.dokters[praktek.fk_dokter];
                            praktek.day=$scope.days[praktek.day];
                            if(historyPraktekMap[praktek.id]!=null){
                                praktek.jumlah_antrian=historyPraktekMap[praktek.id].book_count;
                                praktek.open_time=historyPraktekMap[praktek.id].open_time.substring(0,4);
                                praktek.close_time=historyPraktekMap[praktek.id].close_time.substring(0,4);
                            }
                            else{
                                praktek.jumlah_antrian=0;
                                praktek.open_time=praktek.time_from.substring(0,5);
                                praktek.close_time=praktek.time_to.substring(0,5);
                            }
                        });
                        $scope.unfiltered_prakteks=result.data.praktek;
                        if($scope.currentUser.fk_role==1)
                            $scope.unfiltered_prakteks=$scope.unfiltered_prakteks.filter(praktek=>praktek.fk_klinik.id==$scope.currentUser.fk_klinik);
                    }
                    $scope.filterData();
                }).catch(function onError(response){
                    console.log('error',response);
                    toastr.warning('Get data error', 'Warning');
                });
            }).catch(function onError(response){
                console.log('error',response);
                toastr.warning('Get data error', 'Warning');
            });
        }).catch(function onError(response){
            console.log('error',response);
            toastr.warning('Get data error', 'Warning');
        });    
    }).catch(function onError(response){
        console.log('error',response)
        toastr.warning('Get data error', 'Warning');
    });
    $scope.filterData = function filterData(){
        $scope.prakteks=angular.copy($scope.unfiltered_prakteks);
        console.log($scope.prakteks);
        if($scope.filter.poli_type != null){
            $scope.prakteks=$scope.prakteks.filter(praktek=>
                praktek.poli_type==$scope.filter.poli_type
            );
        };
        if($scope.filter.lokasi != null){
            $scope.prakteks=$scope.prakteks.filter(
                praktek=>praktek.fk_klinik.alamat.toLowerCase().includes($scope.filter.lokasi.toLowerCase())
            );
        };
        if($scope.filter.tanggal!=null){
            $scope.prakteks=$scope.prakteks.filter(praktek=>
                praktek.day==$scope.days[$scope.filter.tanggal.getDay()-1]
            );
        }

        if($scope.filter.harga!=null){
            $scope.prakteks=$scope.prakteks.filter(praktek=>
                (praktek.price_rate >= $scope.filter.harga.min && praktek.price_rate <= $scope.filter.harga.max)
            );
        }
        $scope.sorting();
    }
    function open() {
        $scope.opened = true;
    }
    $scope.$watch('sort',function(newVal, oldVal){
        $scope.sorting();
    },true);
    $scope.sorting = function sorting(){
        // console.log($scope.prakteks);
        switch($scope.sort.param){
            case "Harga Termurah":
                $scope.prakteks.sort(function(a,b){return a.price_rate - b.price_rate});
                break;
            case "Harga Termahal":
                $scope.prakteks.sort(function(a,b){return b.price_rate - a.price_rate});
                break;
            case "Buka Paling Awal":
                $scope.prakteks.sort(function(a,b){return new Date('1970/01/01 ' + a.time_from) - new Date('1970/01/01 ' + b.time_from)});
                break;
            case 'Tutup Paling Akhir':
                $scope.prakteks.sort(function(a,b){return new Date('1970/01/01 ' + b.time_to)} - new Date('1970/01/01 ' + a.time_to));
                break;
            case 'Antrian Paling Sedikit':
                $scope.prakteks.sort(function(a,b){return a.jumlah_antrian - b.jumlah_antrian});
                break;
            case 'Antrian Paling Banyak':
                $scope.prakteks.sort(function(a,b){return b.jumlah_antrian - a.jumlah_antrian});
                break;
        }
    }
}

function DetailPoliController ($scope, $rootScope, toastr, $http, $location, $stateParams, Penjamin_pasien, Visit) {
    $scope.days = 
        {'0':'Senin',
        '1':'Selasa',
        '2':'Rabu',
        '3':'Kamis',
        '4':"Jum'at",
        '5':'Sabtu',
        '6':'Minggu'}
    if($scope.onSelect==null){
        $scope.onSelect = function onSelect($item){
            // console.log($item);
            $scope.pasien = $item;
            // if ($scope.user.gender=='M') $scope.user.gender="Laki-laki";
            // if ($scope.user.gender=='F') $scope.user.gender="Perempuan";
            // $scope.user.fk_role={id: 25,code: 'dr', name: 'Dokter'};
            // $scope.disabled=true;
        };
    }
    if($scope.currentUser.fk_role==1){
        $http.post(
          "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-pasien-by-klinik",
          {klinik_id : $scope.currentUser.fk_klinik.toString()},
          {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
          // console.log(result);
          $scope.pasiens = result.data.pasiens;
          // if(data!=null){
          //   data.forEach(data=>{
          //       if (data.gender=='M') data.gender="Laki-laki";
          //       if (data.gender=='F') data.gender="Perempuan";
          //       data.fk_role=$scope.rolesMap[data.fk_role];
          //   });
          // }
        }).catch(function onError(result){
          console.log('get all pasiens error',result)
          toastr.warning('Get data error', 'Warning');
        })
    }
    $http.post(
        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-praktek-by-id",
        {id : $stateParams.id},
        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
    ).then(function onSuccess(result){
        $scope.praktek=result.data.praktek;
        // console.log("praktek data",$scope.praktek)
        $http.post(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik-by-id",
            {id : $scope.praktek.fk_klinik.toString()},
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
        ).then(function onSuccess(result){
            $scope.praktek.fk_klinik=result.data.klinik;
            $scope.praktek.day=$scope.days[$scope.praktek.day];
            $http.post(
                "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-id",
                {id : $scope.praktek.fk_dokter.toString()},
                {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
            ).then(function onSuccess(result){
                $scope.praktek.fk_dokter=result.data.user;
            }).catch(function onError(response){
                console.log('error',response);
                toastr.warning('Get data error', 'Warning');
            });
        }).catch(function onError(response){
            console.log('error',response);
            toastr.warning('Get data error', 'Warning');
        });
    }).catch(function onError(response){
        console.log('error',response);
        toastr.warning('Get data error', 'Warning');
    });

    $scope.$watch('pasien',function(){
        if($scope.pasien){
            $scope.penjaminList=Penjamin_pasien.find({filter:{where:{fk_pasien:Number($scope.pasien.id)}}},
            function(response){
                $scope.penjaminList.forEach(temp=>{
                    if(temp.jenis_penjamin=='Umum')
                        temp.nama=temp.fullname;
                    else if(temp.jenis_penjamin=='BPJS')
                        temp.nama="BPJS Kesehatan";
                    else
                        temp.nama=temp.nama_asuransi;
                });
            },function(response){
                console.log($scope.penjaminList);
            });
        }
    },true);

    var penjaminActionTemplate = '<span style="display:block; text-align:center;">'+
      '<input type="checkbox" ng-click="grid.appScope.checkPenjamin(row, rowRenderIndex)">'+
      '</input></span>';
    $scope.penjaminGridOptions = { 
            data: 'penjaminList',
            enablePaging: true,
            showFooter: true,
            rowHeight: 36,
            headerRowHeight: 36,
            totalServerItems: 'totalServerItems',
            pagingOptions: $scope.pagingOptions,
            enableFiltering: false,
            showFilter: $scope.showFilter,
            keepLastSelected: true,
            multiSelect: false,
            sortInfo: $scope.sortOptions,
            useExternalSorting: true,
    columnDefs:[
            {displayName:'Aksi',field:'remove',width:"75",cellTemplate: penjaminActionTemplate},
            {displayName:'Jenis Penjamin',field:'jenis_penjamin',width:"*"},
            {displayName:'Nama',field:'nama',width:"*"},
            {displayName:'No. Anggota/Polis',field:'no_anggota_polis',width:"*"},
    ]};

    $scope.addPenjamin=function addPenjamin(){
        $location.path("user/"+$scope.pasien.id+"/edit");
    };

    $scope.checkPenjamin=function checkPenjamin(row,index){
        if(!$scope.penjaminList[index].checked)
            $scope.penjaminList[index].checked=true;
        else
            $scope.penjaminList[index].checked=!$scope.penjaminList[index].checked;
    };
    $scope.book = function book(){
        var user_id;
        if($scope.currentUser.fk_role==1)user_id=$scope.pasien.id.toString();
        if($scope.currentUser.fk_role==26)user_id=$scope.currentUser.id.toString();
        // console.log(user_id);
        $http.post(
            "http://" + $rootScope.address +":"+ $rootScope.port.booking + "/book",
            {user_id : user_id, jadwal_praktek_id : $stateParams.id},
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
        ).then(function onSuccess(result){
            // console.log('bookr',result);
            toastr.success(result.data.message);
            Visit.findById({id:result.data.visit_id},function(response){
                let totalPenjamin=0;
                for(let penjamin of $scope.penjaminList){
                    if(totalPenjamin==5)
                        break;
                    if(penjamin.checked){
                        totalPenjamin+=1;
                        response["fk_penjamin"+String(totalPenjamin)]=penjamin.id;
                    }
                };
                response.keluhan=$scope.keluhan;
                Visit.upsert(response);
            });
            if($scope.currentUser.fk_role==1){
                $location.path("detail-pesanan/"+String(result.data.visit_id));
            }
            if($scope.currentUser.fk_role==26){
                $location.path("detail-pesanan/0");
            }
        }).catch(function onError(response){
            console.log('error',response);
            toastr.warning('Get data error', 'Warning');
        });
    };
}

function DetailBookingController($scope, $rootScope, toastr, $http, $location, $stateParams){
    // console.log('masuk detail booking');
    $scope.has_booking = false;
    // console.log($scope.has_booking)
    $scope.days = 
        {'0':'Senin',
        '1':'Selasa',
        '2':'Rabu',
        '3':'Kamis',
        '4':"Jum'at",
        '5':'Sabtu',
        '6':'Minggu'}
    var postData = {};
    if($stateParams.id==0){
        url="/get-booking-detail";
        postData={user_id : $scope.currentUser.id.toString()};
    }else{
        url="/get-booking-detail-by-visit-id";
        postData={visit_id : $stateParams.id.toString()};
    }
    $http.post(
        "http://" + $rootScope.address +":"+ $rootScope.port.booking + url,
        postData,
        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
    ).then(function onSuccess(result){
        $scope.has_booking = true;
        // console.log('booking dt',result);
        $scope.booknum=result.data.visit.booknum;
        $scope.fk_praktek=result.data.visit.fk_jadwal_praktek;
        $scope.estimasi_tindakan=result.data.visit.estimasi_tindakan;
        $http.post(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-praktek-by-id",
            {id : $scope.fk_praktek.toString()},
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
        ).then(function onSuccess(result){
            // console.log('praktek byid',result);
            $scope.praktek=result.data.praktek;
            $http.post(
                "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik-by-id",
                {id : $scope.praktek.fk_klinik.toString()},
                {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
            ).then(function onSuccess(result){
                // console.log('klinik byid',result);
                $scope.praktek.fk_klinik=result.data.klinik;
                $scope.praktek.day=$scope.days[$scope.praktek.day];
                $http.post(
                    "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-id",
                    {id : $scope.praktek.fk_dokter.toString()},
                    {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                ).then(function onSuccess(result){
                    // console.log('user byid',result);
                    $scope.praktek.fk_dokter=result.data.user;
                    $http.post(
                        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-history-praktek",
                        {jadwal_praktek_id : $scope.praktek.id.toString()},
                        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                    ).then(function onSuccess(result){
                        // console.log('history_praktek',result);
                        var d = new Date(new Date(result.data.history_praktek.open_time).getTime() + $scope.estimasi_tindakan*60000);
                        d.setHours(d.getHours() - 7);
                        // console.log(d);
                        var datestring = ("0" + d.getDate()).slice(-2) + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" +
    d.getFullYear() + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
                        // console.log(datestring);
                        $scope.estimasi_tindakan=datestring;
                        $scope.praktek.fk_history_praktek=result.data.history_praktek;
                        // console.log($scope.praktek);
                    }).catch(function onError(response){
                        console.log('error',response);
                        toastr.warning('Get data error', 'Warning');
                    });
                }).catch(function onError(response){
                    console.log('error',response);
                    toastr.warning('Get data error', 'Warning');
                });
            }).catch(function onError(response){
                console.log('error',response);
                toastr.warning('Get data error', 'Warning');
            });
        }).catch(function onError(response){
            console.log('error',response);
            toastr.warning('Get data error', 'Warning');
        });
    }).catch(function onError(response){
        $scope.has_booking = false
        console.log('error',response);
        // toastr.warning('Get data error', 'Warning');
    });

    $scope.print = function(elementId){
        console.log(elementId);
        var printContents = document.getElementById(elementId).innerHTML;
        var popupWin = window.open('', '_blank', 'width=700,height=700');
        popupWin.document.open();
        popupWin.document.write('<html>\n<head>\n<style type="text/css" media="print">\n@page\n{\nsize:  80mm 80mm;margin: 0mm;\n}</style></head><body onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
    }
}