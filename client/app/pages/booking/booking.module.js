/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.booking', [
      'BlurAdmin.pages'
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider,$ocLazyLoadProvider) {
    $stateProvider
        .state('app.poli',{
            url: '/list-poli',
            controller: 'ListPoliController',
            templateUrl: 'app/pages/booking/poli/poli.list.html',
            title: 'Cari Poli',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/booking/ListPoliController.js'); 
              }]
            }
        })
        .state('app.poli_detail',{
            url: '/list-poli/:id/detail',
            controller: 'DetailPoliController',
            templateUrl: 'app/pages/booking/poli/poli.detail.html',
            title: 'Detail Poli',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/booking/ListPoliController.js'); 
              }]
            }
        })
        .state('app.riwayat_pesanan',{
            url: '/riwayat-pesanan',
            controller: 'UserEditController',
            templateUrl: 'app/pages/rbac/user/user.new.html',
            title: 'Riwayat Pesanan',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/rbac/UserController.js'); 
              }]
            }
        })
        .state('app.booking_detail',{
            url: '/detail-pesanan/:id',
            controller: 'DetailBookingController',
            templateUrl: 'app/pages/booking/poli/booking.detail.html',
            title: 'Detail Booking',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/booking/ListPoliController.js'); 
              }]
            }
        })
  }

})();
