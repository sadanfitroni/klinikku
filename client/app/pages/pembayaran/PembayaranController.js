angular.module('BlurAdmin').controller('PembayaranController', PembayaranController);
angular.module('BlurAdmin').controller('EntryPembayaranController', EntryPembayaranController);
angular.module('BlurAdmin').controller('EntryPembayaranAddController', EntryPembayaranAddController);

function PembayaranController($scope, $rootScope, $state, DTOptionsBuilder, DTColumnBuilder, $filter, 
    $location, toastr, $timeout, $http) {
    $scope.filterOptions = {filterText: "", useExternalFilter: false };
    $scope.showFilter = true;
    $scope.totalServerItems = 0;
    $scope.searchTextEXS = '';
    $scope.pagingOptions = {pageSizes: [10, 25, 50], pageSize: 10, currentPage: 1};
    $scope.filter={};
    $scope.filter.name = "";
    $scope.go = function go(url){
        $location.path(url);
    }
}

function EntryPembayaranController($scope, $rootScope, toastr, $http, $location, $stateParams, $timeout) {
    $scope.filterOptions = {
        filterText: "",
        poli_name : "",
        poli_type : "",
        useExternalFilter: false
    };
    $scope.filterParams = {};
    $scope.showFilter = true;
    $scope.totalServerItems = 0;
    //$scope.totalServerItem = 0;
    $scope.searchTextEXS = '';
    $scope.pagingOptions = {
        pageSizes: [10, 25, 50],
        pageSize: 10,
        currentPage: 1
    };
    $scope.fopen=fopen;
    $scope.fopened = false;
    $scope.topen=topen;
    $scope.topened = false;
    function fopen() {
        $scope.fopened = true;
    }
    function topen() {
        $scope.topened = true;
    }
    $scope.$watch('filter.dari_tanggal',function(){
        $rootScope.fopened=false;
    },true);
    $scope.$watch('filter.sampai_tanggal',function(){
        $rootScope.topened=false;
    },true);
    $scope.setPagingData = function setPagingData(data) {
        $scope.unfilteredData = data;
        $scope.filterData();
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.filterData = function filterData(){
        $scope.listData = angular.copy($scope.unfilteredData);
        console.log($scope.listData);
        // if($scope.filterParams.poli_name != null || $scope.filterParams.poli_type != null){
        //     $scope.filterOptions.useExternalFilter = true;
        // }
        // else
        //     $scope.filterOptions.useExternalFilter = false;
        console.log($scope.filterParams.poli_name);
        if($scope.filterParams.poli_name != null)   
            $scope.listData=$scope.listData.filter(data=>data.fk_jadwal_praktek.poli_name.includes($scope.filterParams.poli_name))
        console.log($scope.listData[0].fk_jadwal_praktek.poli_name.includes($scope.filterParams.poli_name));
        //     $scope.filterOptions.poli_name = $scope.filterParams.poli_name;
        // else
        //     $scope.filterOptions.poli_name = ""

        // if($scope.filterParams.poli_type != null)
        //     $scope.filterOptions.poli_type = $scope.filterParams.poli_type;
        // else
        //     $scope.filterOptions.poli_type = ""
        if($scope.filterParams.dokter != null)   
            $scope.listData=$scope.listData.filter(data=>data.fk_dokter.fullname.includes($scope.filterParams.dokter))
        if($scope.filterParams.pasien != null)   
            $scope.listData=$scope.listData.filter(data=>data.fk_pasien.fullname.includes($scope.filterParams.pasien))
        // console.log($scope.listData[0].checking_time);
        // console.log(new Date($scope.listData[0].checking_time));
        // console.log($scope.filterParams.dari_tanggal);
        // console.log($scope.filterParams.sampai_tanggal);
        // console.log($scope.listData[0].checking_time>=$scope.filterParams.dari_tanggal);
        // console.log($scope.listData[0].checking_time<=$scope.filterParams.sampai_tanggal);
        if($scope.filterParams.dari_tanggal!=null && $scope.filterParams.sampai_tanggal!=null)
            $scope.listData=$scope.listData.filter(data=>(new Date(data.checking_time)>=$scope.filterParams.dari_tanggal && new Date(data.checking_time)<=$scope.filterParams.sampai_tanggal))
        // $scope.getPagedDataAsync($scope.filterOptions)
        // console.log($scope.filterOptions);
    }
    $scope.entryPembayaran = function entryPembayaran(visit_id){
        // console.log(visit_id)
        $location.path('/entry-pembayaran/' + visit_id );
    }
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (angular.isDefined($scope.filterTimeout)) {
            $timeout.cancel($scope.filterTimeout);
        }
        $scope.filterTimeout = $timeout(function () {
            if (newVal !== oldVal) {
                $scope.getPagedDataAsync($scope.filterOptions);
            }
        }, 500);
    }, true);
    $scope.getPagedDataAsync = function getPagedDataAsync(filterOptions) {
        setTimeout(function () {
          var data = [];
          dokterMaps={};
          pasienMaps={};
          if($scope.myData.length == 0){
            console.log(String($scope.currentUser.fk_klinik));
            $http.post(
                "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-dokter-by-klinik",
                {klinik_id : String($scope.currentUser.fk_klinik)},
                {headers: {"Content-Type": "application/x-www-formurlencoded"}}
            ).then(function onSuccess(result){
                result.data.dokters.forEach(dokter=>{
                    dokterMaps[dokter.id]=dokter;
                });
                $http.post(
                    "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-pasien-by-klinik",
                    {klinik_id : String($scope.currentUser.fk_klinik)},
                    {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                ).then(function onSuccess(result){
                    result.data.pasiens.forEach(dokter=>{
                        pasienMaps[dokter.id]=dokter;
                    });
                    $http.get(
                        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-praktek",
                        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                    ).then(function onSuccess(result){
                        if(!result.data.praktek)
                            result.data.praktek = [];
                        prakteks = result.data.praktek.filter(praktek=>$scope.currentUser.fk_klinik==praktek.fk_klinik);
                        praktekIds = [];
                        praktekMaps = {};
                        // console.log(dokterMaps);
                        prakteks.forEach(praktek=>{
                            praktekIds.push(praktek.id);
                            praktekMaps[praktek.id]=praktek;
                        });
                        $http.get(
                            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-visit",
                            {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                        ).then(function onSuccess(result){
                            if(!result.data.visit)
                                result.data.visit = [];
                            visits = result.data.visit.filter(visit=>praktekIds.includes(visit.fk_jadwal_praktek) &&
                                (visit.status=="Sudah Entry Tindakan" || visit.status=="Sudah Bayar"));
                            visits.forEach(visit=>{
                                visit.fk_jadwal_praktek=praktekMaps[visit.fk_jadwal_praktek];
                                visit.fk_dokter=dokterMaps[visit.fk_dokter];
                                visit.fk_pasien=pasienMaps[visit.fk_pasien];
                            });
                            console.log(visits);
                            $scope.myData = angular.copy(visits);
                            $scope.setPagingData(visits);
                        }).catch(function onError(result){
                            console.log('search error',result)
                            $scope.setPagingData(data);
                            toastr.info('Data antrian masih kosong', 'Information');
                        })
                    }).catch(function onError(result){
                        console.log('search error',result)
                    });
                }).catch(function onError(result){
                    console.log('search error',result)
                });
            }).catch(function onError(result){
                console.log('search error',result)
            });
          }else if($scope.filterOptions.useExternalFilter){
            let listPasien = angular.copy($scope.myData)
            for(pasien of listPasien){
                if( pasien.poli_name.includes($scope.filterOptions.poli_name) 
                    && pasien.poli_type.includes($scope.filterOptions.poli_type)
                    && pasien.status.includes($scope.filterOptions.status)
                   )
                    data.push(pasien)
            }
            console.log("result filter")
            $scope.setPagingData(data);

          }else{
            data = angular.copy($scope.myData)
            $scope.setPagingData(data);
          }
           
        }, 100);
    };
    var removeRowTemplate = '<div ng-if="row.entity.status!=\'Sudah Bayar\'" class="button-wrapper text-center" style="margin-top:10%">'+
                            '<button ng-click="grid.appScope.entryPembayaran(row.entity.id)" class="btn btn-primary btn-xs"><i class="ion-forward"></i></button></div>'+
                            '<div ng-if="row.entity.status==\'Sudah Bayar\'" class="button-wrapper text-center" style="margin-top:10%">'+
                            '<button data-toggle="tooltip" data-placement="bottom" title="Sudah Bayar" class="btn btn-primary btn-xs"><i class="ion-cross"></i></button></div>';
    $scope.gridOptions = { 
        data: 'listData', enablePaging: true, rowHeight: 36, headerRowHeight: 50,
        paginationPageSizes : $scope.pagingOptions.pageSizes, 
        paginationPageSize: $scope.pagingOptions.pageSize,
    columnDefs:[
        {displayName:'Action',field:'action',cellTemplate: removeRowTemplate,width:"7%"},
        // {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"8%"},
        // {displayName:'Id',field:'id',width:"*"},
        // {displayName:'Id Visit',field:'code',width:"*"},
        // {displayName:'Waktu Pesan',field:'date',width:"12%",cellFilter:'date:"longDate"',visible:$scope.currentUser.fk_role==26},
        {displayName:'No. antrian',field:'booknum',width:"*",visible:$scope.currentUser.fk_role!=25},
        // {displayName:'Estimasi waktu ditindak',field:'estimasi_tindakan',width:"17%",visible:$scope.currentUser.fk_role==26},
        {displayName:'Checking time',field:'checking_time',width:"15%",cellFilter: 'date:"dd/MM/yyyy HH:mm"'},
        {displayName:'Nama Poli',field:'fk_jadwal_praktek.poli_name',width:"*"},
        {displayName:'Nama Klinik',field:'fk_jadwal_praktek.fk_klinik.name',width:"*",visible:$scope.currentUser.fk_role!=1},
        {displayName:'Dokter',field:'fk_dokter.fullname',width:"*",visible:$scope.currentUser.fk_role!=25},
        {displayName:'Pasien',field:'fk_pasien.fullname',width:"*",visible:$scope.currentUser.fk_role!=26},
        // {displayName:'Fk_history_praktek',field:'fk_history_praktek',width:"*"},
        // {displayName:'Perawat',field:'fk_perawat.fullname',width:"*",visible:$scope.currentUser.fk_role!=26},
        // {displayName:'Rating',field:'rating',width:"*"},
        {displayName:'Status',field:'status',width:"*"},
        // {displayName:'Total',field:'total',width:"*"},
         
    ]};
    $scope.init = function init(){
        $scope.myData = [];
        $scope.getPagedDataAsync($scope.filterOptions)
    }
    $scope.init()
}

function EntryPembayaranAddController($scope, $rootScope, toastr, $http, $location, $stateParams, $timeout) {
    $scope.visit={kode_pembayaran:"k"+$scope.currentUser.fk_klinik+"p"+$stateParams.id};
    $scope.pembayaran = {};
    $scope.list_pembayaran=[];
    $scope.addItem = function addItem(pembayaran) {
        if(pembayaran.item != null || pembayaran.nominal != null || pembayaran.quantity != null){
            $scope.pembayaran = {};
            if(pembayaran.diskon==null)pembayaran.diskon=0;
            pembayaran.subtotal=pembayaran.nominal*pembayaran.quantity-pembayaran.diskon;
            // let data = angular.copy(pembayaran);
            $scope.list_pembayaran.push(pembayaran);
        }else
            toastr.warning("Data Item, Nominal dan Kuantitas Harus Terisi","warning")
    }
    $scope.deleteItem = function deleteItem(index) {
        $scope.list_pembayaran.splice(index,1);
    }
    $scope.editItem = function deleteItem(index) {
        // console.log(index);
        $scope.pembayaran.item = $scope.list_pembayaran[index].item;
        $scope.pembayaran.nominal = $scope.list_pembayaran[index].nominal;
        $scope.pembayaran.quantity = $scope.list_pembayaran[index].quantity;
        $scope.pembayaran.diskon = $scope.list_pembayaran[index].diskon;
        $scope.list_pembayaran.splice(index,1);
    }
    $scope.save = function save(visit){
        // console.log(visit);
        // console.log($scope.list_pembayaran);
        var total=0;
        $scope.list_pembayaran.forEach(pembayaran=>{
            total+=pembayaran.subtotal;
        });
        if(visit.no_polis==null)
            visit.no_polis=""
        if(visit.nama_asuransi==null)
            visit.nama_asuransi=""
        // console.log(total);
        $http.post(
            "http://" + $rootScope.address +":"+ $rootScope.port.pembayaran + "/entry-pembayaran",
            {   visit_id : $stateParams.id , 
                visit : {
                    id : 0,
                    code : "",
                    date : null,
                    booknum : 0,
                    fk_dokter : 0,
                    fk_jadwal_praktek : 0,
                    fk_perawat : 0,
                    fk_pasien : 0,
                    fk_history_praktek : 0,
                    total : total,
                    rating : 0,
                    estimasi_tindakan : 0,
                    checking_time : null,
                    status : "",
                    tipe_pembayaran : visit.tipe_pembayaran,
                    no_polis : visit.no_polis,
                    nama_asuransi : visit.nama_asuransi,
                    kode_pembayaran : visit.kode_pembayaran
                }
            },
            {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
            // console.log("cek tensi success",result);
            if($scope.list_pembayaran.length > 0){
                for(pembayaran of $scope.list_pembayaran){
                    if(pembayaran.diskon==null)
                        pembayaran.diskon=0
                    $http.post(
                        "http://" + $rootScope.address +":"+ $rootScope.port.pembayaran + "/add-item-pembayaran",
                        { 
                            visit_id : $stateParams.id,
                            pembayaran : {
                                id : 0,
                                fk_klinik : 0,
                                fk_visit : 0,
                                fk_dokter : 0,
                                fk_pasien : 0,
                                item : pembayaran.item,
                                nominal : pembayaran.nominal,
                                quantity : pembayaran.quantity,
                                diskon : pembayaran.diskon,
                                subtotal : pembayaran.subtotal,
                            }
                        },
                        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                    ).catch(function onError(result) {
                        console.log("error",result)
                        toastr.warning('Entry Obat Failed', 'Warning');
                    })
                }
                
            }
            toastr.success("Entry Pembayaran Success");
            $scope.go("/entry-pembayaran")
        }).catch(function onError(result){
            console.log('search error',result)
            toastr.warning('Process Failed', 'Warning');
        })
    }
    $scope.go = function go(url) {
       $location.path(url);
    }
}