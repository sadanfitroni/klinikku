/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.pembayaran', [
      'BlurAdmin.pages'
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider,$ocLazyLoadProvider) {
    $stateProvider
        .state('app.entry_pembayaran',{
            url: '/entry-pembayaran',
            controller: 'EntryPembayaranController',
            templateUrl: 'app/pages/pembayaran/pembayaran/entry.pembayaran.html',
            title: 'List Pembayaran',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/pembayaran/PembayaranController.js'); 
              }]
            }
        })
        .state('app.entry_pembayaran_add',{
            url: '/entry-pembayaran/:id',
            controller: 'EntryPembayaranAddController',
            templateUrl: 'app/pages/pembayaran/pembayaran/entry.pembayaran.new.html',
            title: 'Entry Pembayaran',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/pembayaran/PembayaranController.js'); 
              }]
            }
        })
  }

})();
