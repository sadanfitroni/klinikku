angular.module('BlurAdmin').controller('TagihanPembayaranController', TagihanPembayaranController);
angular.module('BlurAdmin').controller('TagihanPembayaranEditController', TagihanPembayaranEditController);
angular.module('BlurAdmin').controller('TagihanPembayaranNewController', TagihanPembayaranNewController);
// angular
// .module('BlurAdmin').controller('TagihanPembayaranViewController', TagihanPembayaranViewController);

function TagihanPembayaranController($q,$scope, $rootScope, $state, DTOptionsBuilder, DTColumnBuilder, $routeParams, 
    $route, $filter, $location, toastr, $timeout, $http) {
    var klinik={};
    $scope.klinikList=[];
    var bulan=[{"id":1, "nama":"Januari"},
{"id":2, "nama":"Februari"},
{"id":3, "nama":"Maret"},
{"id":4, "nama":"April"},
{"id":5, "nama":"Mei"},
{"id":6, "nama":"Juni"},
{"id":7, "nama":"Juli"},
{"id":8, "nama":"Agustus"},
{"id":9, "nama":"September"},
{"id":10, "nama":"Oktober"},
{"id":11, "nama":"November"},
{"id":12, "nama":"Desember"}];
    var bulanObj={};

    $scope.filter={};

    $scope.filterOptions = {filterText: "", useExternalFilter: false };
    $scope.showFilter = true;
    $scope.totalServerItems = 0;
    //$scope.totalServerItem = 0;
    $scope.searchTextEXS = '';
    $scope.pagingOptions = {pageSizes: [10, 25, 50], pageSize: 10, currentPage: 1};
    // sort
    $scope.sortOptions = { fields: ["code"], directions: ["ASC"]};
    $scope.setPagingData = function setPagingData(data, page, pageSize){
        $scope.unFilteredData = angular.copy(data);
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function getPagedDataAsync(pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            var page = $scope.pagingOptions.currentPage;
            var pageSize = $scope.pagingOptions.pageSize;
            var offset = (page - 1) * pageSize;
            $http.get(
                "http://" + $rootScope.address +":"+ $rootScope.port.pembayaran + "/get-tagihan-pembayaran",
                {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
              ).then(function onSuccess(result){
                data = result.data.tagihan_pembayaran;
                data.forEach(data=>{
                    data.klinik=klinik[data.fk_klinik];
                    data.bulan=bulanObj[data.bulan].nama;
                });
                $scope.totalServerItem = result.data.length;
                $scope.listlength = result.data.length;
                $scope.setPagingData(data,page,pageSize);
              }).catch(function onError(response){
                  toastr.warning('No connection', 'Warning');
                  console.log("no connection",response)
              });
        }, 100);
    };
    $scope.filterData = function filterData(){
        $scope.myData = angular.copy($scope.unFilteredData);
        if($scope.filter.klinik){
            $scope.myData=$scope.myData.filter(klinik=>klinik.fk_klinik==$scope.filter.klinik.id)
        }
    }

    $scope.go = function go(url){
        $location.path(url);
    }

    $scope.viewRowIndex = function viewRowIndex(row, index){
        var id = $scope.myData[index].id;
        console.log('view ID:', id, index);
        $location.path('/component/tagihan-langganan/'+row.entity.id+'/view');
    };

    $scope.editRowIndex = function editRowIndex(row, index){
        var id = $scope.myData[index].id;
        $location.path('/tagihan-langganan/'+row.entity.id+'/edit');
    };

    $scope.remove = function remove( row, index, confirmation ){
        var id = row.entity.id;
        index = index + ($scope.gridOptions.paginationCurrentPage-1) * $scope.gridOptions.paginationPageSize;
        confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
        if (confirmDelete(confirmation)) {
          $http.put(
            "http://" + $rootScope.address +":"+ $rootScope.port.pembayaran + "/delete-tagihan-pembayaran",
            {id:String(id)},
            {headers: {"Content-Type": "application/x-www-formurlencoded"}}
          ).then(function onSuccess(result){
            message = result.data.message;
            toastr.success(result.data.message);
            $scope.myData.splice(index, 1);
          }).catch(function onError(result){
            console.log('delete error',result)
            toastr.warning(result, 'Warning');
          })
        }
    };

    var confirmDelete = function confirmDelete(confirmation){
        return confirmation ? confirm('This action is irreversible. Do you want to delete this TagihanPembayaran?') : true;
    };
    
    var removeRowTemplate = '<span style="display:block; text-align:center;">'+
                          '<button ng-click="grid.appScope.editRowIndex(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-pencil"></i></button><button ng-click="grid.appScope.remove(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-times"></i></button></span>';
    $scope.gridOptions = { 
        data: 'myData', 
        enablePaging: true, rowHeight: 36, headerRowHeight: 50,
        paginationPageSizes : $scope.pagingOptions.pageSizes, 
        paginationPageSize: $scope.pagingOptions.pageSize,
        // exporterPdfDefaultStyle: {fontSize: 9},
        // exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
        // exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
        // exporterPdfOrientation: 'portrait',
        // exporterPdfPageSize: 'LETTER',
        // exporterPdfMaxGridWidth: 500,
        // onRegisterApi: function(gridApi){
        //   $scope.gridApi = gridApi;
        // },
    columnDefs:[
    
    {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"*",visible:$scope.currentUser.fk_role==27},
    {displayName:'Tahun',field:'tahun',width:"*"},
    {displayName:'Bulan',field:'bulan',width:"*"},
    {displayName:'Klinik',field:'klinik.name',width:"*",visible:$scope.currentUser.fk_role==27},
    {displayName:'Dari tanggal',field:'dari_tanggal',width:"*",cellFilter: 'date:\'dd/MM/yyyy\''},
    {displayName:'Sampai tanggal',field:'sampai_tanggal',width:"*",cellFilter: 'date:\'dd/MM/yyyy\''},
    {displayName:'Total tagihan',field:'total_tagihan',width:"*"},
    {displayName:'Total kunjungan',field:'total_kunjungan',width:"*"},
    {displayName:'Status',field:'status',width:"*"},
    {displayName:'Tanggal pembayaran',field:'tanggal_pembayaran',width:"*",cellFilter: 'date:\'dd/MM/yyyy\''},
    {displayName:'Jumlah pembayaran',field:'jumlah_pembayaran',width:"*"},
   
    ]};

    $scope.hiddenGrid = { 
        data: 'myData', 
        enablePaging: true, rowHeight: 36, headerRowHeight: 50,
        paginationPageSizes : $scope.pagingOptions.pageSizes, 
        paginationPageSize: $scope.pagingOptions.pageSize,
        exporterPdfDefaultStyle: {fontSize: 9},
        exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
        exporterPdfTableHeaderStyle: {fontSize: 10, bold: true},
        exporterPdfOrientation: 'portrait',
        exporterPdfPageSize: 'A4',
        exporterPdfMaxGridWidth: 400,
        onRegisterApi: function(gridApi){
          $scope.gridApi = gridApi;
        },
    columnDefs:[
    {displayName:'Tahun',field:'tahun',width:"6"},
    {displayName:'Bulan',field:'bulan',width:"6"},
    {displayName:'Klinik',field:'klinik.name',width:"6",visible:$scope.currentUser.fk_role==27},
    {displayName:'Dari tanggal',field:'dari_tanggal',width:"6",cellFilter: 'date:\'dd/MM/yyyy\''},
    {displayName:'Sampai tanggal',field:'sampai_tanggal',width:"6",cellFilter: 'date:\'dd/MM/yyyy\''},
    {displayName:'Total tagihan',field:'total_tagihan',width:"6"},
    {displayName:'Total kunjungan',field:'total_kunjungan',width:"6"},
    {displayName:'Status',field:'status',width:"6"},
    {displayName:'Tanggal pembayaran',field:'tanggal_pembayaran',width:"6",cellFilter: 'date:\'dd/MM/yyyy\''},
    {displayName:'Jumlah pembayaran',field:'jumlah_pembayaran',width:"6"},
    ]};

    $scope.init = function init(){
        bulan.forEach(bulan=>{
            bulanObj[bulan.id]=bulan;
        });
        $http.get(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
            {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
            result.data.klinik.forEach(data=>{
                klinik[data.id]=data;
            });
            $scope.klinikList=result.data.klinik;
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
        }).catch(function onError(result){
        });
    }

    $scope.export = function(){
        // var temp=angular.copy($scope.gridOptions.columnDefs);
        // $scope.gridOptions
        // console.log($scope.gridOptions);
        // console.log($scope.gridApi);
        $scope.gridApi.exporter.pdfExport('visible', 'visible');
    }

    $scope.init();
}

function TagihanPembayaranEditController ($scope, $rootScope,$stateParams,$state, toastr, $http, $location) {
    $scope.bulan=[
        {"id":1, "nama":"Januari"},
        {"id":2, "nama":"Februari"},
        {"id":3, "nama":"Maret"},
        {"id":4, "nama":"April"},
        {"id":5, "nama":"Mei"},
        {"id":6, "nama":"Juni"},
        {"id":7, "nama":"Juli"},
        {"id":8, "nama":"Agustus"},
        {"id":9, "nama":"September"},
        {"id":10, "nama":"Oktober"},
        {"id":11, "nama":"November"},
        {"id":12, "nama":"Desember"}
    ];
    var klinik = {};
    $scope.open_start = open_start;
    $scope.opened_start = false;

    $scope.open_end = open_end;
    $scope.opened_end = false;

    $scope.open_tgl = open_tgl;
    $scope.opened_tgl = false;
  
    function open_start() {
        $scope.opened_start = true;
    }

    function open_end(){
        $scope.opened_end = true;
    }

    function open_tgl(){
        $scope.opened_tgl = true;
    }
    $http.post(
      "http://" + $rootScope.address +":"+ $rootScope.port.pembayaran + "/get-tagihan-pembayaran-by-id",
      {id : $stateParams.id},
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
        var data = result.data.tagihan_pembayaran;
        data.fk_klinik=klinik[data.fk_klinik];
        data.bulan=$scope.bulan[data.bulan-1];
        $scope.tagihanPembayaran = data;
    }).catch(function onError(result){
      console.log('delete error',result)
      toastr.warning('Get data error', 'Warning');
    })
    $scope.save = function save(tagihanPembayaran){
        tagihanPembayaran.bulan=tagihanPembayaran.bulan.id;
        tagihanPembayaran.fk_klinik=tagihanPembayaran.fk_klinik.id;
        $http.put(
            "http://" + $rootScope.address +":"+ $rootScope.port.pembayaran + "/edit-tagihan-pembayaran",
            {tagihan_pembayaran : tagihanPembayaran},
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
          ).then(function onSuccess(result){
                toastr.success(result.data.message);
                $location.path("tagihan-langganan");
          }).catch(function onError(response){
            console.log('error',response)
            toastr.warning('Add data error', 'Warning');
          })
    }
    $scope.back = function back(){
        $location.path("tagihan-langganan");
    }
    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
        result.data.klinik.forEach(data=>{
            klinik[data.id]=data;
        });
        $scope.klinikList=result.data.klinik;
    }).catch(function onError(result){
    });
}

function TagihanPembayaranNewController ($scope, $rootScope, $state, toastr, $http, $location) {
    $scope.tagihanPembayaran = {};
    $scope.open = open;
    $scope.opened = false;
    $scope.bulan=[
        {"id":1, "nama":"Januari"},
        {"id":2, "nama":"Februari"},
        {"id":3, "nama":"Maret"},
        {"id":4, "nama":"April"},
        {"id":5, "nama":"Mei"},
        {"id":6, "nama":"Juni"},
        {"id":7, "nama":"Juli"},
        {"id":8, "nama":"Agustus"},
        {"id":9, "nama":"September"},
        {"id":10, "nama":"Oktober"},
        {"id":11, "nama":"November"},
        {"id":12, "nama":"Desember"}
    ];
    $scope.open_start = open_start;
    $scope.opened_start = false;

    $scope.open_end = open_end;
    $scope.opened_end = false;

    $scope.open_tgl = open_tgl;
    $scope.opened_tgl = false;
  
    function open_start() {
        $scope.opened_start = true;
    }

    function open_end(){
        $scope.opened_end = true;
    }

    function open_tgl(){
        $scope.opened_tgl = true;
    }

    $scope.save = function save(tagihanPembayaran){
        // console.log(tagihanPembayaran);
        tagihanPembayaran.bulan=tagihanPembayaran.bulan.id;
        tagihanPembayaran.fk_klinik=tagihanPembayaran.fk_klinik.id;
        $http.post(
            "http://" + $rootScope.address +":"+ $rootScope.port.pembayaran + "/add-tagihan-pembayaran",
            {tagihan_pembayaran : tagihanPembayaran},
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
          ).then(function onSuccess(result){
                toastr.success(result.data.message);
                $location.path("tagihan-langganan");
          }).catch(function onError(response){
            console.log('error',response)
            toastr.warning('Add data error', 'Warning');
          })
    }

    function open() {
        $scope.opened = true;
    }
    $scope.back = function back(){
        $location.path("tagihan-langganan");
    }
    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
        // result.data.klinik.forEach(data=>{
        //     klinik[data.id]=data;
        // });
        $scope.klinikList=result.data.klinik;
    }).catch(function onError(result){
        console.log(result);
    });
}
