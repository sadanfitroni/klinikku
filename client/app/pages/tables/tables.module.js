/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.tables', [
    "BlurAdmin.pages"
  ])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        // .state('tables', {
        //   url: '/tables',
        //   // template : '<ui-view></ui-view>',
        //   // abstract: true,
        //   controller: 'TablesPageCtrl',
        //   title: 'Tables',
        //   sidebarMeta: {
        //     icon: 'ion-grid',
        //     order: 300,
        //   },
        // })
        .state('app.basic', {
          url: '/basic',
          templateUrl: 'app/pages/tables/basic/tables.html',
          title: 'Basic Tables',
          controller: 'TablesPageCtrl',
          sidebarMeta: {
            order: 0,
          },
        }).state('app.smart', {
          url: '/smart',
          templateUrl: 'app/pages/tables/smart/tables.html',
          title: 'Smart Tables',
          controller: 'TablesPageCtrl',
          sidebarMeta: {
            order: 100,
          },
        });
    $urlRouterProvider.when('/tables','/tables/basic');
  }

})();
