/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.auth', [
      'BlurAdmin.pages'
    ])
  .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider,$ocLazyLoadProvider) {
    $stateProvider
        .state('signin',{
            url: '/signin',
            controller: 'LoginController',
            templateUrl: 'auth.html',
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/auth/AuthController.js'); 
              }]
            }
        }).state('signout',{
            url: '/signout',
            controller: 'LogoutController',
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/auth/AuthController.js'); 
              }]
            }
        });
  }

})();
