angular
.module('BlurAdmin')
.controller('LoginController', LoginController);

angular
.module('BlurAdmin')
.controller('LogoutController', LogoutController);

function LoginController($scope, AuthService,deviceDetector,$state, $rootScope, LoopBackAuth,
 $location, toastr, $localStorage,$http, $route,$ocLazyLoad) {
  $ocLazyLoad.load("app/auth.css")
  $scope.user = {};
  var vm = this;
  vm.data = deviceDetector;
  console.info("Detector",vm.data.browser);
  vm.allData = JSON.stringify(vm.data, null, 2);
  $scope.loadLogin = function loadLogin() {
    $scope.login = function() {
      console.log('USER LOGIN: ', $scope.user, $scope.rememberMe)
      $http.post(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/signin",
        {email:$scope.user.email, password:$scope.user.password},
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
        console.log("result",result);
        if(result.data.message!="Klinik anda sudah melewati masa langganan"){
          $http.post(
            "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-menu-by-user",
            {id_user : String(result.data.user.id)},
            {headers: {"Content-Type": "application/x-www-formurlencoded"}}
          ).then(function onSuccess(result){
            $localStorage["menu"] = [];
            if(result.data.menu){
              for(menu of result.data.menu){
                $localStorage["menu"].push(menu)
              }
            }
            if($rootScope.currentUser.session == 1){
              console.log("has login")
              toastr.warning("Akun Telah login di Perangkat lain");
              // $localStorage.$reset();
              // $rootScope = {};
              return
              // return $state.go('signout');
            }
            $state.go('app.homepage');
          }).catch(function onError(result){
            $localStorage["menu"] = null;
            console.log("error",result)
          })
          var next = $location.nextAfterLogin || '/';
          $rootScope.currentUser = angular.copy(result.data.user);
          $localStorage['session'] = result.data.user;
          $http.post(
            "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-role-by-id",
            {id : String($rootScope.currentUser.fk_role)},
            {headers: {"Content-Type": "application/x-www-formurlencoded"}}
          ).then(function onSuccess(result){
            $localStorage['role'] = result.data.role;
            console.log("role",result.data.role);
            $rootScope.userRole = $localStorage['role'];
            if($rootScope.userRole){
              if($rootScope.userRole.name == "Perawat" || $rootScope.userRole.name =="Admin Klinik"){
                $http.post(
                 "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik-by-id",
                  {id : String($rootScope.currentUser.fk_klinik)},
                  {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                ).then(function onSuccess(result){
                      $rootScope.klinik = result.data.klinik;
                      console.log("data klinik", $rootScope.klinik);
                }).catch(function onError(response){
                  console.log('error',response)
                  toastr.warning('Perawat atau Admin Klinik tidak memiliki data klinik', 'Warning');
                })
              }          
            }
          }).catch(function onError(response){
            toastr.warning("User don't has role");
            console.log("get role error",response)
          })
        }
        else
          toastr.warning(result.data.message);
      }).catch(function onError(response){
          toastr.warning('Username or Password is not found. Please entry by the correct credential', 'Warning');
          console.log("result",response)
      })
    };
  }
  console.info("Browser Detector",vm.allData);
  if (vm.data.browser == "chrome") {
    $scope.loadLogin();
  }else if(vm.data.browser == "ie") {
    if (vm.data.browser_version >= 9) {
        $scope.loadLogin();
    }else {
         toastr.error('Oops, your web browser is no longer supported. Please Update your web browser.') 
         $scope.login = function() {
        toastr.error('Oops, your web browser is no longer supported. Please Update your web browser.')
      };
    }
  }else if(vm.data.browser == "safari") {
    $scope.loadLogin();
  }else if (vm.data.browser == "firefox") {
    if (vm.data.browser_version >= 51) {
      $scope.loadLogin();
    } else {
     toastr.error('Oops, your web browser is no longer supported. Please Update your web browser.') 
     $scope.login = function() {
      toastr.error('Oops, your web browser is no longer supported. Please Update your web browser.')
    };
  }
  } else if (vm.data.browser == "ms-edge") {
    $scope.loadLogin();
  } else{
    $scope.login = function() {
      toastr.error('Oops, your web browser is no longer supported. Please use Google Chrome or Safari instead.')
    };
    toastr.error('Oops, your web browser is no longer supported. Please use Google Chrome or Safari instead.')
  }

  // $scope.rememberMe = false;
  

  /*$scope.logout = function(){
    console.log('LOGOUT...')
    $localStorage[LoopBackAuth.accessTokenId] = null;
    $localStorage = {};
    User.logout().$promise
    .then(function(response) {
      $rootScope.currentUser = null;
      console.log('LOGOUT DONE: ', JSON.stringify($localStorage))
      LoopBackAuth.clearUser();
      LoopBackAuth.clearStorage();
      $state.go('auth.signin');
    }, function(err){
      LoopBackAuth.clearUser();
      LoopBackAuth.clearStorage();
      console.log('LOGOUT DONE ERROR: ', JSON.stringify($localStorage))
    });
  };*/

  /*$scope.register = function() {
    User.create({email: $scope.user.email, password: $scope.user.password}).$promise
    .then(function(response){
      $state.transitionTo('auth.signin');
    }, function(err){});
  };*/
}

function LogoutController ($rootScope,$http, $state,toastr, $scope, LoopBackAuth, $localStorage, $route,$ocLazyLoad) {
  $scope.logout = function(){
    $ocLazyLoad.load("app/auth.css")
    console.log('LOGOUT...')
    $localStorage["menu"] = null;
    $localStorage[LoopBackAuth.accessTokenId] = null;
    
    $http.put(
      "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/signout",
      {id:String($localStorage["session"].id)},
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      $rootScope.currentUser = null;
      $localStorage.$reset();
      $rootScope.serviceSidebar.resetStaticItem();
      console.log('Signout DONE: ', JSON.stringify($localStorage))
      // $route.relaod('signin')
      $state.go('signin');
    }).catch(function onError(result){
      console.log('Signout ERROR',result)
      toastr.warning('Logout Error', 'Warning');
    })
  };
  $scope.logout();
}
