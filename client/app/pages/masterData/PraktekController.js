angular.module('BlurAdmin').controller('PraktekController', PraktekController);
angular.module('BlurAdmin').controller('PraktekEditController', PraktekEditController);
angular.module('BlurAdmin').controller('PraktekNewController', PraktekNewController);
// angular
// .module('BlurAdmin').controller('PraktekViewController', PraktekViewController);

function PraktekController($q,$scope, $rootScope, $state, DTOptionsBuilder, DTColumnBuilder, $routeParams, 
    $route, $filter, $location, toastr, $timeout, $http) {
    $scope.filterOptions = {filterText: "", useExternalFilter: false };
    $scope.showFilter = true;
    $scope.totalServerItems = 0;
    //$scope.totalServerItem = 0;
    $scope.searchTextEXS = '';
    $scope.pagingOptions = {pageSizes: [10, 25, 50], pageSize: 10, currentPage: 1};
    // sort
    $scope.sortOptions = { fields: ["code"], directions: ["ASC"]};
    $scope.days = [
        {'id':0, 'name':'Senin'},
        {'id':1, 'name':'Selasa'},
        {'id':2, 'name':'Rabu'},
        {'id':3, 'name':'Kamis'},
        {'id':4, 'name':"Jum'at"},
        {'id':5, 'name':'Sabtu'},
        {'id':6, 'name':'Minggu'}
    ]
    $scope.daysMap={};
    $scope.search={}
    $scope.days.forEach(function(day){
        $scope.daysMap[day.id]=day;
    });
    $scope.setPagingData = function setPagingData(data, page, pageSize){
        $scope.myData = data;
        console.log(data);
        $scope.filter();
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.filter = function filter(){
      if($scope.currentUser.fk_role!=27){
            $scope.myData = $scope.myData.filter(praktek=>praktek.fk_klinik==$scope.currentUser.fk_klinik);
        };
        console.log($scope.myData);
      if($scope.filter.poli_type!=null){
        $scope.myData = $scope.myData.filter(praktek=>praktek.poli_type==$scope.filter.poli_type);
      };
      if($scope.filter.dokter!=null){
        $scope.myData = $scope.myData.filter(praktek=>praktek.fk_dokter.id==$scope.filter.dokter.id);
      };
      if($scope.myData!=null){
        $scope.myData.forEach(function(data){
            data.fk_klinik=$scope.kliniksMap[data.fk_klinik];
            data.fk_dokter=$scope.usersMap[data.fk_dokter];
            data.day=$scope.daysMap[data.day];
        });
      };
      console.log($scope.myData);
      $scope.unsearched = angular.copy($scope.myData);
    }
    $scope.$watch('search.poli_name',function(){
      if($scope.search.poli_name!=null){
        $scope.myData = $scope.unsearched.filter(praktek=>praktek.poli_name.toLowerCase().includes($scope.search.poli_name.toLowerCase()));
      }
    },true);
    $scope.$watch('search.dokter',function(){
      if($scope.search.dokter!=null){
        $scope.myData = $scope.unsearched.filter(praktek=>praktek.fk_dokter.fullname.toLowerCase().includes($scope.search.dokter.toLowerCase()));
      }
    },true);
    if($scope.currentUser.fk_role==27){
      $http.post(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-role",
        {role_id : "25"},
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
        $scope.dokters = result.data.user;
        $scope.usersMap={};
        if(result.data.user!=null){
          result.data.user.forEach(function(user){
              $scope.usersMap[user.id]=user;
          });
        }
      }).catch(function onError(result){
        console.log('delete error',result)
        toastr.warning('Get data error', 'Warning');
      });
    }else{
      $http.post(
        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-dokter-by-klinik",
        {klinik_id : $scope.currentUser.fk_klinik.toString()},
        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
      ).then(function onSuccess(result){
        console.log(result);
        $scope.dokters = result.data.dokters;
        $scope.usersMap={};
        if(result.data.dokters!=null){
          result.data.dokters.forEach(function(user){
              $scope.usersMap[user.id]=user;
          });
        }
        // console.log($scope.dokters);
      }).catch(function onError(result){
        console.log('get all dokters error',result)
        toastr.warning('Get data error', 'Warning');
      })
    }
    $scope.getPagedDataAsync = function getPagedDataAsync(pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            var page = $scope.pagingOptions.currentPage;
            var pageSize = $scope.pagingOptions.pageSize;
            var offset = (page - 1) * pageSize;
               $http.get(
                  "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
                  {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                ).then(function onSuccess(result){
                  $scope.kliniks = result.data.klinik;
                  $scope.kliniksMap={};
                  result.data.klinik.forEach(function(klinik){
                        $scope.kliniksMap[klinik.id]=klinik;
                    });

                    $http.get(
                        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-praktek",
                        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                      ).then(function onSuccess(result){
                        console.log("success",result);
                        data = result.data.praktek;
                        $scope.totalServerItem = result.data.length;
                        $scope.listlength = result.data.length;
                        $scope.setPagingData(data,page,pageSize);
                      }).catch(function onError(response){
                          toastr.warning('No connection', 'Warning');
                          console.log("no connection",response);
                      });

                  }).catch(function onError(result){
                  toastr.warning('Get data error', 'Warning');
                })
        }, 100);
    };

    $scope.go = function go(url){
        $location.path(url);
    }

    $scope.viewRowIndex = function viewRowIndex(row, index){
        var id = $scope.myData[index].id;
        console.log('view ID:', id, index);
        $location.path('/component/praktek/'+row.entity.id+'/view');
    };

    $scope.editRowIndex = function editRowIndex(row, index){
        var id = $scope.myData[index].id;
        $location.path('/praktek/'+row.entity.id+'/edit');
    };

    $scope.remove = function remove( row, index, confirmation ){
        var id = row.entity.id;
        index = index + ($scope.gridOptions.paginationCurrentPage-1) * $scope.gridOptions.paginationPageSize;
        confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
        if (confirmDelete(confirmation)) {
          $http.put(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/delete-praktek",
            {id:String(id)},
            {headers: {"Content-Type": "application/x-www-formurlencoded"}}
          ).then(function onSuccess(result){
            message = result.data.message;
            toastr.success(result.data.message);
            $scope.myData.splice(index, 1);
          }).catch(function onError(result){
            console.log('delete error',result)
            toastr.warning(result, 'Warning');
          })
        }
    };

    var confirmDelete = function confirmDelete(confirmation){
        return confirmation ? confirm('This action is irreversible. Do you want to delete this Praktek?') : true;
    };
    
    var removeRowTemplate = '<span style="display:block; text-align:center;">'+
                          '<button ng-click="grid.appScope.editRowIndex(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-pencil"></i></button><button ng-click="grid.appScope.remove(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-times"></i></button></span>';
    $scope.gridOptions = { 
        data: 'myData', 
        enablePaging: true, rowHeight: 36, headerRowHeight: 50,
        paginationPageSizes : $scope.pagingOptions.pageSizes, 
        paginationPageSize: $scope.pagingOptions.pageSize,
    columnDefs:[
    
    {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"8%"},
    {displayName:'ID Jadwal Praktek',field:'code',width:"10%"},
    {displayName:'Nama Poli',field:'poli_name',width:"10%"},
    {displayName:'Tipe Poli',field:'poli_type',width:"10%"},
    {displayName:'Hari Praktek',field:'day.name',width:"10%"},
    {displayName:'Jam Buka',field:'time_from',width:"10%"},
    {displayName:'Waktu Selesai',field:'time_to',width:"10%"},
    {displayName:'Dokter',field:'fk_dokter.fullname',width:"10%"},
    {displayName:'Klinik',field:'fk_klinik.name',width:"10%"},
    {displayName:'Estimasi biaya',field:'price_rate',width:"10%"},
    {displayName:'Rating',field:'rating',width:"10%"},
    {displayName:'Status',field:'status',width:"10%"},
    {displayName:'Lama Tindakan',field:'lama_tindakan',width:"10%"},
   
    ]};

    $scope.init = function init(){
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }

    $scope.init();
}

function PraktekEditController ($scope, $rootScope,$stateParams,$state, toastr, $http, $location) {
    $scope.days = [
        {'id':0, 'name':'Senin'},
        {'id':1, 'name':'Selasa'},
        {'id':2, 'name':'Rabu'},
        {'id':3, 'name':'Kamis'},
        {'id':4, 'name':"Jum'at"},
        {'id':5, 'name':'Sabtu'},
        {'id':6, 'name':'Minggu'}
    ];
    $scope.daysMap={};
    $scope.days.forEach(function(day){
        $scope.daysMap[day.id]=day;
    });
    $scope.usersMap={};
    $http.post(
      "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-dokter-by-klinik",
      {klinik_id : $scope.currentUser.fk_klinik.toString()},
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      $scope.dokters = result.data.dokters;
      $scope.dokters.forEach(dokter=>{
        $scope.usersMap[dokter.id]=dokter;
      });
      // console.log($scope.dokters);
    }).catch(function onError(result){
      console.log('get all dokters error',result)
      toastr.warning('Get data error', 'Warning');
    });
    $http.get(
      "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      $scope.kliniks = result.data.klinik;
      $scope.kliniksMap={};
      result.data.klinik.forEach(function(klinik){
            $scope.kliniksMap[klinik.id]=klinik;
        });
      $http.post(
          "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-praktek-by-id",
          {id : $stateParams.id},
          {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
            var temp_date = new Date('1970-01-01T' + result.data.praktek.time_from + 'Z');
            var userTimezoneOffset = 1000*3600*7;
            result.data.praktek.time_from=new Date(temp_date.getTime() - userTimezoneOffset);
            temp_date=new Date('1970-01-01T' + result.data.praktek.time_to + 'Z');
            result.data.praktek.time_to=new Date(temp_date.getTime() - userTimezoneOffset);
            result.data.praktek.fk_klinik=$scope.kliniksMap[result.data.praktek.fk_klinik];
            result.data.praktek.fk_dokter=$scope.usersMap[result.data.praktek.fk_dokter];
            result.data.praktek.day=$scope.daysMap[result.data.praktek.day];
          $scope.praktek = result.data.praktek;
          console.log('result',result);
        }).catch(function onError(result){
          console.log('delete error',result)
          toastr.warning('Get data error', 'Warning');
        })
    }).catch(function onError(result){
      toastr.warning('Get data error', 'Warning');
    })
    $scope.save = function save(praktek){
      praktek.day=praktek.day.id;
      praktek.fk_klinik=praktek.fk_klinik.id;
      praktek.fk_dokter=praktek.fk_dokter.id;
      praktek.time_from=praktek.time_from.toLocaleTimeString().slice(0, -3);
      praktek.time_to=praktek.time_to.toLocaleTimeString().slice(0, -3);
      console.log('going to save',praktek);
        $http.put(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/edit-praktek",
            {praktek : praktek},
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
          ).then(function onSuccess(result){
                toastr.success(result.data.message);
                $location.path("praktek");
          }).catch(function onError(response){
            console.log('error',response)
            toastr.warning('Add data error', 'Warning');
          })
    }
    $scope.back = function back(){
        $location.path("praktek");
    }
}

function PraktekNewController ($scope, $rootScope, $state, toastr, $http, $location) {
    $scope.praktek = {};
    $scope.open = open;
    $scope.opened = false;
    $scope.days = [
        {'id':0, 'name':'Senin'},
        {'id':1, 'name':'Selasa'},
        {'id':2, 'name':'Rabu'},
        {'id':3, 'name':'Kamis'},
        {'id':4, 'name':"Jum'at"},
        {'id':5, 'name':'Sabtu'},
        {'id':6, 'name':'Minggu'}
    ]
    $http.post(
      "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-dokter-by-klinik",
      {klinik_id : $scope.currentUser.fk_klinik.toString()},
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      $scope.dokters = result.data.dokters;
      console.log($scope.dokters);
    }).catch(function onError(result){
      console.log('get all dokters error',result)
      toastr.warning('Get data error', 'Warning');
    });
    $http.get(
      "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      $scope.kliniks = result.data.klinik;
    }).catch(function onError(result){
      console.log('delete error',result)
      toastr.warning('Get data error', 'Warning');
    })
    $http.get(
      "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-praktek",
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      if(result.data.praktek)
        var totalPraktek = result.data.praktek.filter(praktek=>praktek.fk_klinik==$scope.currentUser.fk_klinik).length;
      else
        var totalPraktek = 0;
      $scope.praktek.code = "k"+$scope.currentUser.fk_klinik.toString()+"jp"+(totalPraktek+1).toString();
    }).catch(function onError(result){
      console.log('delete error',result)
      toastr.warning('Get data error', 'Warning');
    })
    $scope.save = function save(praktek){
        praktek.day=praktek.day.id;
        praktek.fk_klinik=$scope.currentUser.fk_klinik;
        praktek.fk_dokter=praktek.fk_dokter.id;
        praktek.time_from=praktek.time_from.toLocaleTimeString().slice(0, -3);
        praktek.time_to=praktek.time_to.toLocaleTimeString().slice(0, -3);
        console.log(praktek);
        $http.post(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/add-praktek",
            {praktek : praktek},
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
          ).then(function onSuccess(result){
                toastr.success(result.data.message);
                $location.path("praktek");
          }).catch(function onError(response){
            console.log('error',response)
            toastr.warning('Add data error', 'Warning');
          })
    }

    function open() {
        $scope.opened = true;
    }
    $scope.back = function back(){
        $location.path("praktek");
    }
}
