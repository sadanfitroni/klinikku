angular.module('BlurAdmin').controller('PendapatanKlinikController', PendapatanKlinikController);

function PendapatanKlinikController($q,$scope, $rootScope, $state, DTOptionsBuilder, DTColumnBuilder, $routeParams, 
    $route, $filter, $location, toastr, $timeout, $http, Visit) {
	$scope.filterOptions = {
		useExternalFilter: false
	};

	$scope.filterParams = {};
	$scope.showFilter = true;
	$scope.totalServerItems = 0;
	//$scope.totalServerItem = 0;
	$scope.searchTextEXS = '';
	$scope.pagingOptions = {
		pageSizes: [10, 25, 50],
		pageSize: 10,
		currentPage: 1
	};

	$scope.open_start = open_start;
    $scope.opened_start = false;

    $scope.open_end = open_end;
    $scope.opened_end = false;
  
    function open_start() {
	    $scope.opened_start = true;
	}

	function open_end(){
		$scope.opened_end = true;
	}
  
	var allDokters=[];
	var visits={};
	var klinik={};
	var dokters={};

	$scope.setPagingData = function setPagingData(data) {
	$scope.myData = data;
	$scope.totalServerItem = data.length;
	if (!$scope.$$phase) {
	  $scope.$apply();
	}
	};


	$scope.filterData = function(){
		if($scope.filterParams.status != null || $scope.filterParams.date_end != null ||
			$scope.filterParams.date_start != null || $scope.filterParams.dokter != null || $scope.filterParams.poli_name)
			$scope.filterOptions.useExternalFilter = true
		else
			$scope.filterOptions.useExternalFilter = false;

		if($scope.filterParams.dokter == null){
			$scope.filterOptions.dokter = {};
			$scope.filterOptions.dokter.fullname = "";
		}else{
			$scope.filterOptions.dokter = $scope.filterParams.dokter;
		}

		if($scope.filterParams.status == null)
			$scope.filterOptions.status = "";
		else
			$scope.filterOptions.status = $scope.filterParams.status;


		if($scope.filterParams.poli_name == null)
			$scope.filterOptions.poli_name = "";
		else
			$scope.filterOptions.poli_name = $scope.filterParams.poli_name;

		if($scope.filterParams.date_start == null)
			$scope.filterOptions.date_start = new Date("1990-01-01");
		else
			$scope.filterOptions.date_start = $scope.filterParams.date_start;

		if($scope.filterParams.date_end == null)
			$scope.filterOptions.date_end = new Date("2100-08-05");
		else
			$scope.filterOptions.date_end = $scope.filterParams.date_end;

		// let date_now = new Date();
		// console.log("tanggal ",date_now, $scope.filterOptions.date_start, $scope.filterOptions.date_end)
		// console.log("Perbandingan ",date_now > $scope.filterOptions.date_start, date_now < $scope.filterOptions.date_end)
		
		$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
	}

	$scope.reset = function(){
		if($scope.existingData.length == 0 ){
			$scope.existingData = angular.copy($scope.myData);
		}
		$scope.filterParams = {};
		$scope.myData=angular.copy($scope.existingData);
	}	

	$scope.$watch('pagingOptions', function (newVal, oldVal) {
		if (newVal !== oldVal) {
		  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
		}
	}, true);

	$scope.$watch('searchFilter', function (newVal, oldVal) {
		if (newVal !== oldVal) {
		  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
		}
	}, true);

	$scope.$watch('filterParams.klinik', function (newVal, oldVal) {
		if (newVal !== oldVal) {
			if (newVal!=null){
				$scope.filterParams.dokter=null;
			  	$http.post(
			        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-dokter-by-klinik",
			        { 	
			        	klinik_id : String(newVal.id)
			        },
			        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
			    ).then(function onSuccess(result){
			        $scope.dokter = result.data.dokters;
			    }).catch(function onError(result){
			    	console.log(result);
			        toastr.warning('Process Failed', 'Warning');
			    });
			}
		}
	}, true);	

	function filter(pembayaran){
		if($scope.existingData.length == 0 ){
			$scope.existingData = angular.copy(pembayaran);
		}
		if($scope.filterParams.klinik){
			// console.log('masuk fklink');
	     	pembayaran=pembayaran.filter(pembayaran=>{
	     		if(pembayaran.fk_klinik==$scope.filterParams.klinik.id)
	     			return true;
	     	});
	     	// console.log(pembayaran);
	    };
	    if($scope.filterParams.dokter){
	    	pembayaran=pembayaran.filter(pembayaran=>{
	     		if(pembayaran.fk_dokter==$scope.filterParams.dokter.id)
	     			return true;
	    	});
	    };
	    if($scope.filterParams.date_start && $scope.filterParams.date_end){
	    	pembayaran=pembayaran.filter(pembayaran=>{
	    		var visitDate=new Date(pembayaran.visit.date.split('T')[0]);
	    		if(visitDate<=$scope.filterParams.date_end && visitDate>=$scope.filterParams.date_start)
	     			return true;
	    	});
	    };
	    return pembayaran;
	}

	function getFks(pembayaran){
		// remove previous total
		if(!pembayaran[pembayaran.length-1].id)
			pembayaran.pop();

	    pembayaran.forEach(pembayaran=>{
	    	if(!visits[pembayaran.fk_visit]){
	    		Visit.findById({id:pembayaran.fk_visit},function(response){
	    			visits[pembayaran.fk_visit]=response;
	    			pembayaran.visit=visits[pembayaran.fk_visit];
	    			pembayaran.klinik=klinik[pembayaran.fk_klinik];
		    		pembayaran.dokter=dokters[pembayaran.fk_dokter];
	    		},function(response){
	    			console.log(response);
	    		});
	    	}else{
	    		pembayaran.visit=visits[pembayaran.fk_visit];
		    	pembayaran.klinik=klinik[pembayaran.fk_klinik];
		    	pembayaran.dokter=dokters[pembayaran.fk_dokter];
	    	}
	    	// console.log()
	    });
	    return pembayaran;
	}

	function getTotal(pembayaran){
		var totalPembayaran=0;
		pembayaran.forEach(pembayaran=>{
	    	totalPembayaran+=pembayaran.subtotal;
		})
	    pembayaran.push({subtotal:totalPembayaran});
	    return pembayaran;
	}

  	$scope.getPagedDataAsync = function getPagedDataAsync(pageSize, page) {
	    setTimeout(function () {
	    	var report = [];
	    	var page = $scope.pagingOptions.currentPage;
	    	var pageSize = $scope.pagingOptions.pageSize;
	    	var offset = (page - 1) * pageSize;
	    	if($scope.existingData.length == 0 ){
	    		$http.get(
	    			"http://" + $rootScope.address +":"+ $rootScope.port.pembayaran + "/get-pembayaran",
		        	{headers: {"Content-Type": "application/x-www-formurlencoded"}}
		     	).then(function onSuccess(result){
		     		var pembayaran=result.data.pembayaran;
		     		if(pembayaran){
					    pembayaran=getFks(pembayaran);
					    pembayaran=getTotal(pembayaran);
			      		$scope.setPagingData(pembayaran);
		     		}
			    }).catch(function onError(result){
		         	console.log(result);
			        toastr.warning('get report data error', 'Warning');
	      		});
	     	}else{
	     		if($scope.existingData.length == 0 ){
					getFks(pembayaran);
			     	pembayaran=filter(pembayaran);
					getTotal(pembayaran);
				    $scope.setPagingData(pembayaran);
				}else{
			     	var pembayaran=angular.copy($scope.existingData);
			     	getFks(pembayaran);
			     	pembayaran=filter(pembayaran);
					getTotal(pembayaran);
				    $scope.setPagingData(pembayaran);
				}
	     	}
	    }, 100);
  	};

  
  $scope.gridOptions = {
    data: 'myData', 
    enablePaging: true, rowHeight: 36, headerRowHeight: 50,
    paginationPageSizes : $scope.pagingOptions.pageSizes, 
    paginationPageSize: $scope.pagingOptions.pageSize,
    columnDefs: [
      { displayName: 'Tanggal',field: 'visit.date',cellFilter: 'date:\'dd/MM/yyyy\''},
      { displayName: 'Klinik',field: 'klinik.name'},
      { displayName: 'Dokter',field: 'dokter.fullname'},
      { displayName: 'Item Biaya',field: 'item'},
      { displayName: 'Cara Bayar',field: 'visit.tipe_pembayaran'},
      { displayName: 'Total',field: 'subtotal'},
    ]
  };

  $scope.gridOptions.onRegisterApi = function(gridApi) {
      $scope.grid = gridApi;
  };

  $scope.go = function go(url) {
    $location.path(url);
  }

  $scope.exportToExcel = function exportToExcel(){
  	console.log("export");
  	var toBeExported=[];
  	$scope.myData.forEach(data=>{
  		var temp={};
  		try{
	  		temp['Tanggal']=data.visit.date;
	  		temp['Klinik']=data.klinik.name;
	  		temp['Dokter']=data.dokter.fullname;
	  		temp['Item Biaya']=data.item;
	  		temp['Cara Bayar']=data.visit.tipe_pembayaran;
  		}catch{}
	  	temp['Total']=data.subtotal;
  		toBeExported.push(temp);
  	});
  	alasql.promise('SELECT * INTO XLSX("report_klinikku.xlsx",{headers:true}) FROM ?',[toBeExported]);
  }


  $scope.init = function init() {
  	$scope.myData = [];
  	$scope.existingData = [];
  	$http.get(
  		"http://" + $rootScope.address +":"+ $rootScope.port.master_data +"/get-klinik",
  		{headers: {"Content-Type": "application/x-www-formurlencoded"}}
  	).then(function onSuccess(result){
  		$scope.kliniks=result.data.klinik;
  		$scope.kliniks.forEach(temp=>{
  			klinik[temp.id]=temp;
  		});
  		// console.log(klinik);
	  	$http.post(
	  		"http://" + $rootScope.address +":"+ $rootScope.port.rbac +"/get-user-by-role",
	  		{role_id: "25"},
	  		{headers: {"Content-Type": "application/x-www-formurlencoded"}}
	  	).then(function onSuccess(result){
	  		allDokters=result.data.user;
	  		$scope.dokter=allDokters;
	  		allDokters.forEach(dokter=>{
	  			dokters[dokter.id]=dokter;
	  		});
  			$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
	  	}).catch(function onError(result){
	  		console.log('search error',result)
		    toastr.warning('No Connection', 'Warning');
	  	});
  	}).catch(function onError(result){
  		console.log('search error',result)
	    toastr.warning('No Connection', 'Warning');
  	});
  }

  $scope.init();
}