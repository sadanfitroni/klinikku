angular.module('BlurAdmin').controller('VisitController', VisitController);
angular.module('BlurAdmin').controller('VisitEditController', VisitEditController);
angular.module('BlurAdmin').controller('VisitNewController', VisitNewController);
// angular
// .module('BlurAdmin').controller('VisitViewController', VisitViewController);

function VisitController($q,$scope, $rootScope, $state, DTOptionsBuilder, DTColumnBuilder, $routeParams, 
    $route, $filter, $location, toastr, $timeout, $http) {
    $scope.filterOptions = {filterText: "", useExternalFilter: false };
    $scope.showFilter = true;
    $scope.totalServerItems = 0;
    //$scope.totalServerItem = 0;
    $scope.searchTextEXS = '';
    $scope.pagingOptions = {pageSizes: [10, 25, 50], pageSize: 10, currentPage: 1};
    // sort
    $scope.sortOptions = { fields: ["code"], directions: ["ASC"]};
    $scope.setPagingData = function setPagingData(data, page, pageSize){
        $scope.myData = data;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function getPagedDataAsync(pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            var page = $scope.pagingOptions.currentPage;
            var pageSize = $scope.pagingOptions.pageSize;
            var offset = (page - 1) * pageSize;
            $http.get(
                "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-visit",
                {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
              ).then(function onSuccess(result){
                console.log("success",result);
                data = result.data.visit;
                $scope.totalServerItem = result.data.length;
                $scope.listlength = result.data.length;
                $scope.setPagingData(data,page,pageSize);
              }).catch(function onError(response){
                  toastr.warning('No connection', 'Warning');
                  console.log("no connection",response)
              });
        }, 100);
    };
    // $scope.$watch('pagingOptions', function (newVal, oldVal) {
    //     if (newVal !== oldVal) {
    //         $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    //     }
    // }, true);
    // $scope.$watch('filterOptions', function (newVal, oldVal) {
    //     if (angular.isDefined($scope.filterTimeout)) {
    //         $timeout.cancel($scope.filterTimeout);
    //     }
    //     $scope.filterTimeout = $timeout(function () {
    //         if (newVal !== oldVal) {
    //             $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    //         }
    //     }, 500);
    // }, true);

    $scope.go = function go(url){
        $location.path(url);
    }

    $scope.viewRowIndex = function viewRowIndex(row, index){
        var id = $scope.myData[index].id;
        console.log('view ID:', id, index);
        $location.path('/component/visit/'+row.entity.id+'/view');
    };

    $scope.editRowIndex = function editRowIndex(row, index){
        var id = $scope.myData[index].id;
        $location.path('/visit/'+row.entity.id+'/edit');
    };

    $scope.remove = function remove( row, index, confirmation ){
        var id = row.entity.id;
        index = index + ($scope.gridOptions.paginationCurrentPage-1) * $scope.gridOptions.paginationPageSize;
        confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
        if (confirmDelete(confirmation)) {
          $http.put(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/delete-visit",
            {id:String(id)},
            {headers: {"Content-Type": "application/x-www-formurlencoded"}}
          ).then(function onSuccess(result){
            message = result.data.message;
            toastr.success(result.data.message);
            $scope.myData.splice(index, 1);
          }).catch(function onError(result){
            console.log('delete error',result)
            toastr.warning(result, 'Warning');
          })
        }
    };

    var confirmDelete = function confirmDelete(confirmation){
        return confirmation ? confirm('This action is irreversible. Do you want to delete this Visit?') : true;
    };
    
    var removeRowTemplate = '<span style="display:block; text-align:center;">'+
                          '<button ng-click="grid.appScope.editRowIndex(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-pencil"></i></button><button ng-click="grid.appScope.remove(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-times"></i></button></span>';
    $scope.gridOptions = { 
        data: 'myData', 
        enablePaging: true, rowHeight: 36, headerRowHeight: 50,
        paginationPageSizes : $scope.pagingOptions.pageSizes, 
        paginationPageSize: $scope.pagingOptions.pageSize,
    columnDefs:[
    
    {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"8%"},
    {displayName:'Kode',field:'code',width:"10%"},
    {displayName:'Tanggal',field:'date',width:"10%"},
    {displayName:'Jumlah Antrian',field:'booknum',width:"10%"},
    {displayName:'Fk_dokter',field:'fk_dokter',width:"10%"},
    {displayName:'Fk_praktek',field:'fk_praktek',width:"10%"},
    {displayName:'Fk_pasien',field:'fk_pasien',width:"10%"},
    {displayName:'Total',field:'total',width:"10%"},
    {displayName:'Rating',field:'rating',width:"10%"},
    {displayName:'Estimasi_tindakan',field:'estimasi_tindakan',width:"10%"},
    {displayName:'Waktu Checking',field:'checking_time',width:"10%"},
    {displayName:'Status',field:'status',width:"10%"},
   
    ]};

    $scope.init = function init(){
        console.log("masuk init");
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }

    $scope.init();
}

function VisitEditController ($scope, $rootScope,$stateParams,$state, toastr, $http, $location) {
    $http.post(
      "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-visit-by-id",
      {id : $stateParams.id},
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      $scope.visit = result.data.visit;
    }).catch(function onError(result){
      console.log('delete error',result)
      toastr.warning('Get data error', 'Warning');
    })
    $scope.save = function save(visit){
        $http.put(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/edit-visit",
            {visit : visit},
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
          ).then(function onSuccess(result){
                toastr.success(result.data.message);
                $location.path("visit");
          }).catch(function onError(response){
            console.log('error',response)
            toastr.warning('Add data error', 'Warning');
          })
    }
    $scope.back = function back(){
        $location.path("visit");
    }
}

function VisitNewController ($scope, $rootScope, $state, toastr, $http, $location) {
    $scope.visit = {};
    $scope.open = open;
    $scope.opened = false;
    $scope.save = function save(visit){
        console.log(visit);
        $http.post(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/add-visit",
            {visit : visit},
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
          ).then(function onSuccess(result){
                toastr.success(result.data.message);
                $location.path("visit");
          }).catch(function onError(response){
            console.log('error',response)
            toastr.warning('Add data error', 'Warning');
          })
    }

    function open() {
        $scope.opened = true;
    }
    $scope.back = function back(){
        $location.path("visit");
    }
}
