angular.module('BlurAdmin').controller('KlinikController', KlinikController);
angular.module('BlurAdmin').controller('KlinikEditController', KlinikEditController);
angular.module('BlurAdmin').controller('KlinikNewController', KlinikNewController);
angular.module('BlurAdmin').controller('RegisterDokterController', RegisterDokterController);
angular.module('BlurAdmin').controller('KlinikAddLanggananController', KlinikAddLanggananController);
// angular
// .module('BlurAdmin').controller('KlinikViewController', KlinikViewController);

function KlinikController($q,$scope, $rootScope, $state, DTOptionsBuilder, DTColumnBuilder, $routeParams, 
    $route, $filter, $location, toastr, $timeout, $http) {
    $scope.filterOptions = {name: "", useExternalFilter: false };
    $scope.filterParams = {};
    $scope.showFilter = true;
    $scope.totalServerItems = 0;
    //$scope.totalServerItem = 0;
    $scope.searchTextEXS = '';
    $scope.pagingOptions = {pageSizes: [10, 25, 50], pageSize: 10, currentPage: 1};
    // sort
    $scope.sortOptions = { fields: ["code"], directions: ["ASC"]};
    var langganan = {};
    $scope.setPagingData = function setPagingData(data, page, pageSize){
        if($scope.currentUser.fk_role!=27){
            $scope.myData = data.filter(klinik=>klinik.id==$scope.currentUser.fk_klinik);
        }else{
            $scope.myData = data;
        }
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };

    $scope.filterData = function filterData(){
        if($scope.filterParams.name != null)
            $scope.filterOptions.useExternalFilter = true;
        else
            $scope.filterOptions.useExternalFilter = false;

        if ($scope.filterParams.name != null)
            $scope.filterOptions.name = $scope.filterParams.name;
        else 
            $scope.filterParams.name = "";

        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions)
    }


    $scope.getPagedDataAsync = function getPagedDataAsync(pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            var page = $scope.pagingOptions.currentPage;
            var pageSize = $scope.pagingOptions.pageSize;
            var offset = (page - 1) * pageSize;
            $http.get(
                "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
                {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
              ).then(function onSuccess(result){
                // console.log("success",result);
                data = angular.copy(result.data.klinik);
                $scope.totalServerItem = result.data.length;
                $scope.listlength = result.data.length;
                data.forEach(klinik=>{
                    if(langganan[klinik.fk_langganan])
                        klinik.status=langganan[klinik.fk_langganan].status;
                    else{
                        $http.post(
                            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-langganan-by-id",
                            {id : klinik.fk_langganan.toString()},
                            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                        ).then(function onSuccess(result){
                            langganan[klinik.fk_langganan]=result.data.langganan;
                            klinik.status=langganan[klinik.fk_langganan].status;
                        }).catch(function onError(response){
                            toastr.warning('No connection', 'Warning');
                            console.log("no connection",response)
                        });
                    }
                });

                if ($scope.filterOptions.useExternalFilter){
                    let filteredData = [];
                    for (klinik of data){
                        if(klinik.name.includes($scope.filterOptions.name))
                            filteredData.push(klinik);
                    }
                    $scope.setPagingData(filteredData,page,pageSize);
                }
                else
                    $scope.setPagingData(data,page,pageSize);
                
              }).catch(function onError(response){
                  toastr.warning('No connection', 'Warning');
                  console.log("no connection",response)
              });
        }, 100);
    };
    // $scope.$watch('pagingOptions', function (newVal, oldVal) {
    //     if (newVal !== oldVal) {
    //         $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    //     }
    // }, true);
    // $scope.$watch('filterOptions', function (newVal, oldVal) {
    //     if (angular.isDefined($scope.filterTimeout)) {
    //         $timeout.cancel($scope.filterTimeout);
    //     }
    //     $scope.filterTimeout = $timeout(function () {
    //         if (newVal !== oldVal) {
    //             $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
    //         }
    //     }, 500);
    // }, true);

    $scope.go = function go(url){
        $location.path(url);
    }

    $scope.viewRowIndex = function viewRowIndex(row, index){
        var id = $scope.myData[index].id;
        console.log('view ID:', id, index);
        $location.path('/component/klinik/'+row.entity.id+'/view');
    };

    $scope.editRowIndex = function editRowIndex(row, index){
        var id = $scope.myData[index].id;
        $location.path('/klinik/'+row.entity.id+'/edit');
    };

    $scope.remove = function remove( row, index, confirmation ){
        var id = row.entity.id;
        index = index + ($scope.gridOptions.paginationCurrentPage-1) * $scope.gridOptions.paginationPageSize;
        confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
        if (confirmDelete(confirmation)) {
            // console.log(row.entity)

           if(row.entity.pict_1){
               $http.delete(
                    "http://localhost:"+$rootScope.port.fe+"/api/image_containers/images/files/"+row.entity.pict_1,
                    {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                )            
           }
           if(row.entity.pict_2){
                $http.delete(
                    "http://localhost:"+$rootScope.port.fe+"/api/image_containers/images/files/"+row.entity.pict_2,
                    {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                )
           }

           if(row.entity.pict_3){
               $http.delete(
                    "http://localhost:"+$rootScope.port.fe+"/api/image_containers/images/files/"+row.entity.pict_3,
                    {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                )

           }

                      // http://localhost:"+$rootScope.port.fe+"/api/image_containers/images/files/

          $http.put(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/delete-klinik",
            {id:String(id)},
            {headers: {"Content-Type": "application/x-www-formurlencoded"}}
          ).then(function onSuccess(result){
            message = result.data.message;
            toastr.success(result.data.message);
            $scope.myData.splice(index, 1);
          }).catch(function onError(result){
            console.log('delete error',result)
            toastr.warning(result, 'Warning');
          })
        }
    };

    var confirmDelete = function confirmDelete(confirmation){
        return confirmation ? confirm('This action is irreversible. Do you want to delete this Klinik?') : true;
    };
    var removeRowTemplate = '<span style="display:block; text-align:center;">'+
                          '<button ng-click="grid.appScope.editRowIndex(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-pencil"></i></button><button ng-show="grid.appScope.currentUser.fk_role==27" ng-click="grid.appScope.remove(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-times"></i></button></span>';
    $scope.gridOptions = { 
        data: 'myData', 
        enablePaging: true, rowHeight: 36, headerRowHeight: 50,
        paginationPageSizes : $scope.pagingOptions.pageSizes, 
        paginationPageSize: $scope.pagingOptions.pageSize,
    columnDefs:[
    
    {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"8%"},
    {displayName:'ID Klinik',field:'code',width:"10%"},
    {displayName:'Nama',field:'name',width:"*"},
    {displayName:'No.Hp',field:'phone',width:"15%"},
    {displayName:'Email',field:'email',width:"10%"},
    {displayName:'Alamat',field:'alamat',width:"*"},
    {displayName:'Status Langganan',field:'status',width:"*"},
    ]};

    $scope.init = function init(){
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }

    $scope.init();
}

function KlinikEditController ($scope, $rootScope,$stateParams,$state, toastr, $http, $location,FileUploader) {
    $scope.currentController="edit";
    var uploader = $scope.uploader = new FileUploader({ 
        scope: $scope,
        url: '/api/image_containers/images/upload',
    });

    var uploader_2 = $scope.uploader_2 = new FileUploader({ 
        scope: $scope,
        url: '/api/image_containers/images/upload',
    });

    var uploader_3 = $scope.uploader_3 = new FileUploader({
        scope: $scope,
        url: '/api/image_containers/images/upload',
    });

    var uploader_4 = $scope.uploader_4 = new FileUploader({
        scope: $scope,
        url: '/api/image_containers/images/upload',
    });

    uploader.onAfterAddingFile = function (item) {
        $scope.image_1 = item;
        $scope.upload.upload_stat_1 = false;
    }

    uploader_2.onAfterAddingFile = function (item){
        $scope.image_2 = item;
        $scope.upload.upload_stat_2 = false;
    }

    uploader_3.onAfterAddingFile = function (item){
        $scope.image_3 = item;
        $scope.upload.upload_stat_3 = false;
    }

    uploader_4.onAfterAddingFile = function (item){
        $scope.image_4 = item;
        $scope.upload.upload_stat_4 = false;
    }

    $scope.upload = {
        upload_stat_1 : false,
        upload_stat_2 : false,
        upload_stat_3 : false,
        upload_stat_4 : false
    };
    $scope.image_1 = null;
    $scope.image_2 = null;
    $scope.image_3 = null;
    $scope.image_4 = null;

    uploader.onCompleteItem = function(item, response, status, headers) {
      // console.info('Complete', response, status, headers);
      $scope.upload.upload_stat_1 = true;
    };

    uploader_2.onCompleteItem = function(item,response,status,headers){
        $scope.upload.upload_stat_2 = true;
    }

    uploader_3.onCompleteItem = function (item,response,status,headers){
        $scope.upload.upload_stat_3 = true;
    }

    uploader_4.onCompleteItem = function (item,response,status,headers){
        $scope.upload.upload_stat_4 = true;
    }

    $scope.pagingOptions = {pageSizes: [10, 25, 50], pageSize: 10, currentPage: 1};
    $http.post(
        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik-by-id",
        {id : $stateParams.id},
        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
      ).then(function onSuccess(result){
            $scope.klinik=result.data.klinik;
      }).catch(function onError(response){
        console.log('error',response)
        toastr.warning('Get data error', 'Warning');
      })
    $scope.save = function save(klinik){

        if(klinik.pict_1){
               $http.delete(
                    "http://localhost:"+$rootScope.port.fe+"/api/image_containers/images/files/"+klinik.pict_1,
                    {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                )            
       }
       if(klinik.pict_2){
            $http.delete(
                "http://localhost:"+$rootScope.port.fe+"/api/image_containers/images/files/"+klinik.pict_2,
                {headers: {"Content-Type": "application/x-www-formurlencoded"}}
            )
       }

       if(klinik.pict_3){
           $http.delete(
                "http://localhost:"+$rootScope.port.fe+"/api/image_containers/images/files/"+klinik.pict_3,
                {headers: {"Content-Type": "application/x-www-formurlencoded"}}
            )
       }

       if(klinik.logo){
           $http.delete(
                "http://localhost:"+$rootScope.port.fe+"/api/image_containers/images/files/"+klinik.logo,
                {headers: {"Content-Type": "application/x-www-formurlencoded"}}
            )
       }

       if($scope.image_1){
         klinik.pict_1 = $scope.image_1.file.name = klinik.code+"-1-"+$scope.image_1.file.name;
         $scope.image_1.upload();
       }

       if($scope.image_2){
            klinik.pict_2= $scope.image_2.file.name = klinik.code+"-2-"+$scope.image_2.file.name;
            $scope.image_2.upload();
       }

       if($scope.image_3){
           klinik.pict_3 = $scope.image_3.file.name = klinik.code+"-3-"+$scope.image_3.file.name;
           $scope.image_3.upload();
       }

       if($scope.image_4){
           klinik.pict_4 = $scope.image_4.file.name = klinik.code+"-logo-"+$scope.image_4.file.name;
           $scope.image_4.upload();
       }

        $http.put(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/edit-klinik",
            {klinik : klinik},
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
          ).then(function onSuccess(result){
                $scope.$watch('upload', function (newVal, oldVal) {
                    // console.log(oldVal)
                    if(newVal.upload_stat_1 && newVal.upload_stat_2 && newVal.upload_stat_3 && newVal.upload_stat_4){               
                        $location.path("klinik");
                        toastr.success(result.data.message);
                    }
                }, true);
          }).catch(function onError(response){
            console.log('error',response)
            toastr.warning('Add data error', 'Warning');
          })
    }
    $scope.back = function back(){
        $location.path("klinik");
    }
    $scope.go = function go(url){
        $location.path('klinik/'+String($stateParams.id)+url);
    }
    $scope.setPagingData = function setPagingData(data, page, pageSize){
        // console.log(data);
        // if($scope.currentUser.fk_role!=27){
            // $scope.myData = data.filter(user=>user.fk_klinik==$scope.currentUser.fk_klinik);
        // }else{
            $scope.myData = data;
        // }
        // console.log($scope.myData);
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.setPagingDataPasien = function setPagingDataPasien(data, page, pageSize){
        // console.log(data);
        // if($scope.currentUser.fk_role!=27){
            // $scope.myData = data.filter(user=>user.fk_klinik==$scope.currentUser.fk_klinik);
        // }else{
            $scope.pasiensData = data;
        // }
        // console.log($scope.pasiensData);
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function getPagedDataAsync(pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            var page = $scope.pagingOptions.currentPage;
            var pageSize = $scope.pagingOptions.pageSize;
            var offset = (page - 1) * pageSize;
            $http.post(
              "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-pasien-by-klinik",
              {klinik_id : $stateParams.id},
              {headers: {"Content-Type": "application/x-www-formurlencoded"}}
            ).then(function onSuccess(result){
              // console.log(result);
              data = result.data.pasiens;
              if(data!=null){
                data.forEach(data=>{
                    if (data.gender=='M') data.gender="Laki-laki";
                    if (data.gender=='F') data.gender="Perempuan";
                    data.fk_role=$scope.rolesMap[data.fk_role];
                });
              }
              // console.log($scope.dokters);
              $scope.setPagingDataPasien(data,page,pageSize);
            }).catch(function onError(result){
              console.log('get all pasiens error',result)
              toastr.warning('Get data error', 'Warning');
            })
        }, 100);
    };

    $scope.getPagedPasienAsync = function getPagedPasienAsync(pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            var page = $scope.pagingOptions.currentPage;
            var pageSize = $scope.pagingOptions.pageSize;
            var offset = (page - 1) * pageSize;
            $http.post(
                "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-all-dokter-by-klinik",
                {klinik_id : $stateParams.id},
                {headers: {"Content-Type": "application/x-www-formurlencoded"}}
              ).then(function onSuccess(result){
                // console.log(result);
                data = result.data.dokters;
                if(data!=null){
                    data.forEach(data=>{
                        if (data.gender=='M') data.gender="Laki-laki";
                        if (data.gender=='F') data.gender="Perempuan";
                        data.fk_role=$scope.rolesMap[data.fk_role];
                    });
                }
                // console.log(data);
                $scope.setPagingData(data,page,pageSize);
                // $scope.usersMap={};
                // result.data.dokters.forEach(function(user){
                //     $scope.usersMap[user.id]=user;
                // });
                // console.log($scope.dokters);
              }).catch(function onError(result){
                console.log('get all dokters error',result)
                toastr.warning('Get data error', 'Warning');
              })
        }, 100);
    };

    $scope.viewRowIndex = function viewRowIndex(row, index){
        var id = $scope.myData[index].id;
        console.log('view ID:', id, index);
        $location.path('/component/praktek/'+row.entity.id+'/view');
    };

    $scope.editRowIndex = function editRowIndex(row, index){
        var id = $scope.myData[index].id;
        index = index + ($scope.gridOptions.paginationCurrentPage-1) * $scope.gridOptions.paginationPageSize;
        $location.path('/user/'+row.entity.id+'/edit');
    };

    $scope.remove = function remove( row, index, confirmation ){
        var id = row.entity.id;
        confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
        if (confirmDelete(confirmation)) {
          $http.put(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/delete-user",
            {id:String(id)},
            {headers: {"Content-Type": "application/x-www-formurlencoded"}}
          ).then(function onSuccess(result){
            message = result.data.message;
            toastr.success(result.data.message);
            $scope.myData.splice(index, 1);
          }).catch(function onError(result){
            console.log('delete error',result)
            toastr.warning(result, 'Warning');
          })
        }
    };

    var confirmDelete = function confirmDelete(confirmation){
        return confirmation ? confirm('This action is irreversible. Do you want to delete this User?') : true;
    };
    
    var removeRowTemplate = '<span style="display:block; text-align:center;">'+
                          '<button ng-click="grid.appScope.editRowIndex(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-pencil"></i></button><button ng-click="grid.appScope.remove(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-times"></i></button></span>';
    $scope.gridOptions = { 
        data: 'myData', 
        enablePaging: true,
        showFooter: true,
        rowHeight: 36,
        headerRowHeight: 36,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        enableFiltering: false,
        showFilter: $scope.showFilter,
        keepLastSelected: true,
        multiSelect: false,
        sortInfo: $scope.sortOptions,
        useExternalSorting: true,
    columnDefs:[
    
    {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"8%"},
    {displayName:'Email',field:'email',width:"10%"},
    {displayName:'No.Hp',field:'mobile',width:"10%"},
    {displayName:'Nama Lengkap',field:'fullname',width:"10%"},
    {displayName:'Gender',field:'gender',width:"10%"},
    {displayName:'Tanggal Lahir',field:'birth_date',width:"10%"},
    {displayName:'Golongan Darah',field:'blood_type',width:"10%"},
    {displayName:'Alamat',field:'address',width:"10%"},
    {displayName:'ID User',field:'code',width:"10%"},
    // {displayName:'Role',field:'fk_role',width:"10%"},
   
    ]};

    $scope.pasienGridOptions = { 
        data: 'pasiensData', 
        enablePaging: true,
        showFooter: true,
        rowHeight: 36,
        headerRowHeight: 36,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        enableFiltering: false,
        showFilter: $scope.showFilter,
        keepLastSelected: true,
        multiSelect: false,
        sortInfo: $scope.sortOptions,
        useExternalSorting: true,
    columnDefs:[
    
    {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"8%"},
    {displayName:'Email',field:'email',width:"10%"},
    {displayName:'No.Hp',field:'mobile',width:"10%"},
    {displayName:'Nama Lengkap',field:'fullname',width:"10%"},
    {displayName:'Gender',field:'gender',width:"10%"},
    {displayName:'Tanggal Lahir',field:'birth_date',width:"10%"},
    {displayName:'Golongan Darah',field:'blood_type',width:"10%"},
    {displayName:'Alamat',field:'address',width:"10%"},
    {displayName:'ID User',field:'code',width:"10%"},
    // {displayName:'Role',field:'fk_role',width:"10%"},
   
    ]};

    $scope.langgananGridOptions = { 
        data: 'langganansData', 
        enablePaging: true,
        showFooter: true,
        rowHeight: 36,
        headerRowHeight: 36,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        enableFiltering: false,
        showFilter: $scope.showFilter,
        keepLastSelected: true,
        multiSelect: false,
        sortInfo: $scope.sortOptions,
        useExternalSorting: true,
    columnDefs:[
    
    {displayName:'No. Kontrak',field:'no_kontrak',width:"10%"},
    {displayName:'Paket (bulan)',field:'paket',width:"10%"},
    {displayName:'Deskripsi',field:'deskripsi',width:"10%"},
    {displayName:'Dari Tanggal',field:'dari_tanggal',cellFilter: 'date:\'dd/MM/yyyy\'',width:"*"},
    {displayName:'Sampai Tanggal',field:'sampai_tanggal',cellFilter: 'date:\'dd/MM/yyyy\'',width:"*"},
    {displayName:'Status',field:'status',width:"10%"},
    ]};

    $scope.init = function init(){
        $http.get(
            "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-role",
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
          ).then(function onSuccess(result){
            $scope.rolesMap={};
            $scope.roles=angular.copy(result.data.role);
            result.data.role.forEach(function(role){
                $scope.rolesMap[role.id]=role.name;
            });
            $http.post(
                "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-langganan-by-klinik",
                {klinik_id : $stateParams.id},
                {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
              ).then(function onSuccess(result){
                $scope.langganansData=result.data.langganan;
                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
                $scope.getPagedPasienAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
            });
        });
    }

    $scope.init();
}

function KlinikNewController ($scope, $rootScope, $state, toastr, $http, $location, FileUploader) {
    $scope.currentController="new";
    var uploader = $scope.uploader = new FileUploader({ 
        scope: $scope,
        url: '/api/image_containers/images/upload',
    });

    var uploader_2 = $scope.uploader_2 = new FileUploader({ 
        scope: $scope,
        url: '/api/image_containers/images/upload',
    });

    var uploader_3 = $scope.uploader_3 = new FileUploader({ 
        scope: $scope,
        url: '/api/image_containers/images/upload',
    });

    var uploader_4 = $scope.uploader_4 = new FileUploader({
        scope: $scope,
        url: '/api/image_containers/images/upload',
    });

    uploader.onAfterAddingFile = function (item) {
        $scope.image_1 = item;
        $scope.upload.upload_stat_1 = false;
    }

    uploader_2.onAfterAddingFile = function (item){
        $scope.image_2 = item;
        $scope.upload.upload_stat_2 = false;
    }

    uploader_3.onAfterAddingFile = function (item){
        $scope.image_3 = item;
        $scope.upload.upload_stat_3 = false;
    }

    uploader_4.onAfterAddingFile = function (item){
        $scope.image_4 = item;
        $scope.upload.upload_stat_4 = false;
    }

    $scope.upload = {
        upload_stat_1 : false,
        upload_stat_2 : false,
        upload_stat_3 : false,
        upload_stat_4 : false
    };


    uploader.onCompleteItem = function(item, response, status, headers) {
      console.info('Complete', response, status, headers);
      $scope.upload.upload_stat_1 = true;
    };

    uploader_2.onCompleteItem = function(item,response,status,headers){
        $scope.upload.upload_stat_2 = true;
    }

    uploader_3.onCompleteItem = function (item,response,status,headers){
        $scope.upload.upload_stat_3 = true;
    }

    uploader_4.onCompleteItem = function (item,response,status,headers){
        $scope.upload.upload_stat_4 = true;
    }

    // $scope.$watch('upload', function (newVal, oldVal) {
    //     if(newVal.upload_stat_1 && newVal.upload_stat_2 && newVal.upload_stat_3){               
    //         $location.path("klinik");
    //         toastr.success('Upload Success');
    //     }
    // }, true);

    uploader = function onSuccessItem(item, response, status, headers){
        console.log("response",response);
    }
    $scope.klinik = {};
    $scope.open = open;
    $scope.opened = false;
    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-klinik",
        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
      ).then(function onSuccess(result){
        $scope.klinik.code = "k"+(result.data.klinik[result.data.klinik.length-1].id+1).toString();
      }).catch(function onError(response){
          toastr.warning('No connection', 'Warning');
          console.log("no connection",response)
      });

    $scope.save = function save(klinik){
        // console.log(klinik);
        // console.log($scope.uploader);
        // var extn = filename.split(".").pop();
        if($scope.image_1){
            klinik.pict_1 = $scope.image_1.file.name = klinik.code+"-1-"+$scope.image_1.file.name;
            $scope.image_1.upload();
       }else
            $scope.upload.upload_stat_1 = true;

       if($scope.image_2){
            klinik.pict_2= $scope.image_2.file.name = klinik.code+"-2-"+$scope.image_2.file.name;
            $scope.image_2.upload();
       }else
            $scope.upload.upload_stat_2 = true;

       if($scope.image_3){
           klinik.pict_3 = $scope.image_3.file.name = klinik.code+"-3-"+$scope.image_3.file.name;
           $scope.image_3.upload();
       }else
            $scope.upload.upload_stat_3 = true;

        if($scope.image_4){
           klinik.logo = $scope.image_4.file.name = klinik.code+"-logo-"+$scope.image_4.file.name;
           $scope.image_4.upload();
       }else
            $scope.upload.upload_stat_4 = true;

        $http.post(
            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/add-klinik",
            {klinik : klinik},
            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
          ).then(function onSuccess(result){
            console.log('afterinsert',result);
            $scope.$watch('upload', function (newVal, oldVal) {
                if(newVal.upload_stat_1 && newVal.upload_stat_2 && newVal.upload_stat_3 && newVal.upload_stat_4){               
                    $location.path("klinik");
                    toastr.success(result.data.message);
                }
            }, true);
          }).catch(function onError(response){
            console.log('error',response)
            toastr.warning('Add data error', 'Warning');
          })
    }

    function open() {
        $scope.opened = true;
    }
    $scope.back = function back(){
        $location.path("klinik");
    }
     var removeRowTemplate = '<span style="display:block; text-align:center;">'+
                          '<button ng-click="grid.appScope.editRowIndex(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-pencil"></i></button><button ng-click="grid.appScope.remove(row, rowRenderIndex)" class="btn btn-xs btn-default">'+
                          '<i class="fa fa-times"></i></button></span>';

    $scope.gridOptions = { 
        data: 'myData', 
        // enablePaging: true, rowHeight: 36, headerRowHeight: 50,
        // paginationPageSizes : $scope.pagingOptions.pageSizes, 
        // paginationPageSize: $scope.pagingOptions.pageSize,
        columnDefs:[
            {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"8%"},
            {displayName:'Email',field:'email',width:"10%"},
            {displayName:'No.Hp',field:'mobile',width:"10%"},
            {displayName:'Nama Lengkap',field:'fullname',width:"10%"},
            {displayName:'Gender',field:'gender',width:"10%"},
            {displayName:'Tanggal Lahir',field:'birth_date',width:"10%"},
            {displayName:'Golongan Darah',field:'blood_type',width:"10%"},
            {displayName:'Alamat',field:'address',width:"10%"},
            {displayName:'ID User',field:'code',width:"10%"},
            // {displayName:'Role',field:'fk_role',width:"10%"},
    ]};

    $scope.pasienGridOptions = { 
        data: 'pasiensData', 
        // enablePaging: true, rowHeight: 36, headerRowHeight: 50,
        // paginationPageSizes : $scope.pagingOptions.pageSizes, 
        // paginationPageSize: $scope.pagingOptions.pageSize,
        columnDefs:[
        
        {displayName:'Action',field:'remove',cellTemplate: removeRowTemplate,width:"8%"},
        {displayName:'Email',field:'email',width:"10%"},
        {displayName:'No.Hp',field:'mobile',width:"10%"},
        {displayName:'Nama Lengkap',field:'fullname',width:"10%"},
        {displayName:'Gender',field:'gender',width:"10%"},
        {displayName:'Tanggal Lahir',field:'birth_date',width:"10%"},
        {displayName:'Golongan Darah',field:'blood_type',width:"10%"},
        {displayName:'Alamat',field:'address',width:"10%"},
        {displayName:'ID User',field:'code',width:"10%"},
        // {displayName:'Role',field:'fk_role',width:"10%"},
       
    ]};
}

function RegisterDokterController($scope, $rootScope, $state, toastr, $http, $location){
    $scope.currentController="register";
    $scope.user = {};
    $scope.roles = [];
    $scope.open = open;
    $scope.opened = false;
    $scope.user.fk_role={id: 25, name: 'Dokter'};
    $scope.disabled=false;
    $scope.isDokter=true;
    var totalDokter;
    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-role",
        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
      ).then(function onSuccess(result){
          $scope.roles = angular.copy(result.data.role);
      }).catch(function onError(response){
          toastr.warning('No connection', 'Warning');
      });
    $http.post(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-role",
      {role_id : "25"},
      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
    ).then(function onSuccess(result){
      $scope.dokters = result.data.user;
      }).catch(function onError(result){
      console.log('delete error',result)
      toastr.warning('Get data error', 'Warning');
    });
    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user",
        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
    ).then(function onSuccess(result){
        data = result.data.user;
        totalDokter = data.filter(user=>user.fk_role==25).length;
        $scope.user.code="dr"+(totalDokter+1).toString();
    }).catch(function onError(response){
        toastr.warning('No connection', 'Warning');
        console.log("no connection",response)
    });
    $scope.save = function save(){
        // console.log("check");
        $scope.user.fk_role=$scope.user.fk_role.id;
            if($scope.disabled){
                $http.post(
                    "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/register-dokter",
                    {klinik_id : $scope.currentUser.fk_klinik.toString(), user_id : $scope.user.id.toString()},
                    {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                ).then(function onSuccess(result){
                    
                    toastr.success(result.data.message);
                    $scope.back();
                }).catch(function onError(response){
                    console.log('error',response)
                    toastr.warning('Add data error', 'Warning');
                });
            }else{
                $http.post(
                    "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/add-user",
                    {user : $scope.user},
                    {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                ).then(function onSuccess(result){
                    toastr.success(result.data.message);
                    $http.post(
                        "http://" + $rootScope.address +":"+ $rootScope.port.rbac + "/get-user-by-role",
                      {role_id : "25"},
                      {headers: {"Content-Type": "application/x-www-formurlencoded"}}
                    ).then(function onSuccess(result){
                        $http.post(
                            "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/register-dokter",
                            {klinik_id : $scope.currentUser.fk_klinik.toString(), user_id : result.data.user[result.data.user.length-1].id.toString()},
                            {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
                        ).then(function onSuccess(result){
                            toastr.success(result.data.message);
                        }).catch(function onError(response){
                            console.log('error',response);
                            toastr.warning('Add data error', 'Warning');
                        });
                    }).catch(function onError(result){
                      console.log('delete error',result)
                      toastr.warning('Get data error', 'Warning');
                    });
                    $location.path("user");
                }).catch(function onError(response){
                    console.log('error',response)
                    toastr.warning('Add data error', 'Warning');
                }
            )};
    }

    function open() {
        $scope.opened = true;
    }
    $scope.back = function back(){
        $location.path("user");
    }
    $scope.onSelect = function onSelect($item){
        // console.log($item);
        $scope.user = $item;
        $scope.user.fk_role={id: 25, name: 'Dokter'};
        $scope.disabled=true;
    };
}

function KlinikAddLanggananController($scope, $rootScope, $state, $stateParams, toastr, $http, $location){
    $scope.paketList=[
        {nama:"3 bulan", value:3},
        {nama:"6 bulan", value:6},
        {nama:"12 bulan", value:12},
    ];
    $scope.open_start = open_start;
    $scope.opened_start = false;

    $scope.open_end = open_end;
    $scope.opened_end = false;
  
    $scope.langganan={};

    function open_start() {
        $scope.opened_start = true;
    }

    function open_end(){
        $scope.opened_end = true;
    }

    $scope.$watch('langganan.paket',function(){
        date=$scope.langganan.dari_tanggal;
        $scope.langganan.sampai_tanggal= new Date(date.setMonth(date.getMonth()+$scope.langganan.paket.value));
    }, true);

    $http.get(
        "http://" + $rootScope.address +":"+ $rootScope.port.master_data + "/get-langganan",
        {headers: {"Content-Type": "application/x-www-form-urlencoded"}}
    ).then(function onSuccess(result){
        data=result.data.langganan;
        data=data.filter(data=>{
            return data.fk_klinik==$stateParams.id && data.status=="active"
        });
        if(data)
            expiredOn=new Date(data.pop().sampai_tanggal);
        else
            expiredOn=new Date();
        $scope.langganan.dari_tanggal=expiredOn;
    }).catch(function onError(response){
        toastr.warning('No connection', 'Warning');
        console.log("no connection",response);
    });
}