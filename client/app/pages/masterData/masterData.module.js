/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.masterData', [
      'BlurAdmin.pages'
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider,$ocLazyLoadProvider) {
    $stateProvider
        .state('app.klinik',{
            url: '/klinik',
            controller: 'KlinikController',
            templateUrl: 'app/pages/masterData/klinik/klinik.list.html',
            title: 'List Klinik',
            // authenticate: true,
            sidebarMeta: {
              order: 0,
            },
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/KlinikController.js'); 
              }]
            }
        })
        .state('app.klinik_new',{
            url: '/klinik/new',
            controller: 'KlinikNewController',
            templateUrl: 'app/pages/masterData/klinik/klinik.new.html',
            title: 'Add Klinik',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/KlinikController.js'); 
              }]
            }
        })
        .state('app.klinik_edit',{
            url: '/klinik/:id/edit',
            controller: 'KlinikEditController',
            templateUrl: 'app/pages/masterData/klinik/klinik.new.html',
            title: 'Edit Klinik',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/KlinikController.js'); 
              }]
            }
        })
        .state('app.visit',{
            url: '/visit',
            controller: 'VisitController',
            templateUrl: 'app/pages/masterData/visit/visit.list.html',
            title: 'List Visit',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/VisitController.js'); 
              }]
            }
        })
        .state('app.visit_new',{
            url: '/visit/new',
            controller: 'VisitNewController',
            templateUrl: 'app/pages/masterData/visit/visit.new.html',
            title: 'Add Visit',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/VisitController.js'); 
              }]
            }
        })
        .state('app.visit_edit',{
            url: '/visit/:id/edit',
            controller: 'VisitEditController',
            templateUrl: 'app/pages/masterData/visit/visit.new.html',
            title: 'Edit Visit',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/VisitController.js'); 
              }]
            }
        })
        .state('app.praktek',{
            url: '/praktek',
            controller: 'PraktekController',
            templateUrl: 'app/pages/masterData/praktek/praktek.list.html',
            title: 'List Jadwal Praktek',
            // authenticate: true,
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/PraktekController.js'); 
              }]
            }
        })
        .state('app.praktek_new',{
            url: '/praktek/new',
            controller: 'PraktekNewController',
            templateUrl: 'app/pages/masterData/praktek/praktek.new.html',
            title: 'Add Jadwal Praktek',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/PraktekController.js'); 
              }]
            }
        })
        .state('app.praktek_edit',{
            url: '/praktek/:id/edit',
            controller: 'PraktekEditController',
            templateUrl: 'app/pages/masterData/praktek/praktek.new.html',
            title: 'Edit Jadwal Praktek',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/masterData/PraktekController.js'); 
              }]
            }
        })
        .state('app.register_dokter',{
          url: '/register-dokter',
          controller: 'RegisterDokterController',
          templateUrl: 'app/pages/masterData/klinik/register-dokter.html',
          title: 'Register Dokter',
          resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('app/pages/masterData/KlinikController.js'); 
            }]
          }
        })
  }

})();
