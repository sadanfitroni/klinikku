/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.report', [
      'BlurAdmin.pages'
      // 'BlurAdmin.pages.charts.amCharts',
  ])
  .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider,$ocLazyLoadProvider) {
    $stateProvider
        .state('app.report',{
            url: '/report',
            controller: 'ReportController',
            templateUrl: 'app/pages/report/report.list.html',
            title: 'Report',
            // authenticate: true
            resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  return $ocLazyLoad.load('app/pages/report/ReportController.js'); 
              }]
            }
        })
  }

})();
