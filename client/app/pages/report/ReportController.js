angular
  .module('BlurAdmin').controller('ReportController', ReportController);
 

function ReportController($q, $scope, $rootScope, $state, DTOptionsBuilder, DTColumnBuilder, 
  $routeParams, $route, $filter, $location, toastr, $timeout, $http) {
	$scope.filterOptions = {
		useExternalFilter: false
	};

	$scope.filterParams = {};
	$scope.showFilter = true;
	$scope.totalServerItems = 0;
	//$scope.totalServerItem = 0;
	$scope.searchTextEXS = '';
	$scope.pagingOptions = {
		pageSizes: [10, 25, 50],
		pageSize: 10,
		currentPage: 1
	};

	$scope.open_start = open_start;
    $scope.opened_start = false;

    $scope.open_end = open_end;
    $scope.opened_end = false;
  
	$scope.setPagingData = function setPagingData(data) {
	$scope.myData = data;
	$scope.totalServerItem = data.length;
	if (!$scope.$$phase) {
	  $scope.$apply();
	}
	};

	$scope.filterData = function(){
		console.log("filter",$scope.filterParams)
		if($scope.filterParams.status != null || $scope.filterParams.date_end != null ||
			$scope.filterParams.date_start != null || $scope.filterParams.dokter != null || $scope.filterParams.poli_name)
			$scope.filterOptions.useExternalFilter = true
		else
			$scope.filterOptions.useExternalFilter = false;

		if($scope.filterParams.dokter == null){
			$scope.filterOptions.dokter = {};
			$scope.filterOptions.dokter.fullname = "";
		}else{
			$scope.filterOptions.dokter = $scope.filterParams.dokter;
		}

		if($scope.filterParams.status == null)
			$scope.filterOptions.status = "";
		else
			$scope.filterOptions.status = $scope.filterParams.status;


		if($scope.filterParams.poli_name == null)
			$scope.filterOptions.poli_name = "";
		else
			$scope.filterOptions.poli_name = $scope.filterParams.poli_name;

		if($scope.filterParams.date_start == null)
			$scope.filterOptions.date_start = new Date("1990-01-01");
		else
			$scope.filterOptions.date_start = $scope.filterParams.date_start;

		if($scope.filterParams.date_end == null)
			$scope.filterOptions.date_end = new Date("2100-08-05");
		else
			$scope.filterOptions.date_end = $scope.filterParams.date_end;

		// let date_now = new Date();
		// console.log("tanggal ",date_now, $scope.filterOptions.date_start, $scope.filterOptions.date_end)
		// console.log("Perbandingan ",date_now > $scope.filterOptions.date_start, date_now < $scope.filterOptions.date_end)
		
		
		$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
	}

	$scope.reset = function(){
		$scope.filterParams = {};
	}	

	function open_start() {
	    $scope.opened_start = true;
	}

	function open_end(){
		$scope.opened_end = true;
	}

	$scope.$watch('pagingOptions', function (newVal, oldVal) {
		if (newVal !== oldVal) {
		  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
		}
	}, true);

	$scope.$watch('searchFilter', function (newVal, oldVal) {
		if (newVal !== oldVal) {
		  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
		}
	}, true);


  $scope.getPagedDataAsync = function getPagedDataAsync(pageSize, page) {
	    setTimeout(function () {
	      var report = [];
	      var page = $scope.pagingOptions.currentPage;
	      var pageSize = $scope.pagingOptions.pageSize;
	      var offset = (page - 1) * pageSize;


	      if($scope.existingData.length == 0 ){
	      	$http.post(
		        "http://" + $rootScope.address +":"+ $rootScope.port.report + "/get-report-data",
		        {klinik_id: String($scope.currentUser.fk_klinik) },
		        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
		     ).then(function onSuccess(result){
		      	console.log("api call success",result);
		      	for(i = 0; i<result.data.visit.length ; i++){
		      		let reportData = {};
		      		reportData.date = result.data.visit[i].date;
		      		reportData.status = result.data.visit[i].status;
		      		reportData.fullname = result.data.user[i].fullname;
		      		reportData.total = result.data.visit[i].total;
		      		reportData.poli_name = result.data.jadwal[i].poli_name;
		      		report.push(reportData);
		      	} 
		      	$scope.existingData = angular.copy(report)
		      	$scope.setPagingData(report);   
	         }).catch(function onError(result){
		        console.log('get report error',result)
		        toastr.warning('get report data error', 'Warning');
	      	})
	     }else{
	     	report = angular.copy($scope.existingData);
	     	let filterData = [];
	     	if($scope.filterOptions.useExternalFilter){
	     		for(data of report){
	     			let tgl = new Date(data.date);
	     			if( 
	     				data.fullname.includes($scope.filterOptions.dokter.fullname) &&
	     				data.poli_name.includes($scope.filterOptions.poli_name) &&
	     				data.status.includes($scope.filterOptions.status) &&
	     				tgl >= $scope.filterOptions.date_start  &&
	     				tgl <= $scope.filterOptions.date_end 
	     			  )
	     				filterData.push(data);
	     		}
	     		$scope.setPagingData(filterData)
	     	}else
		     	$scope.setPagingData($scope.existingData); 
	     }
	      
	      
	    }, 100);
  };

  
  $scope.gridOptions = {
    data: 'myData', 
    enablePaging: true, rowHeight: 36, headerRowHeight: 50,
    paginationPageSizes : $scope.pagingOptions.pageSizes, 
    paginationPageSize: $scope.pagingOptions.pageSize,
    columnDefs: [
      { displayName: 'Tanggal',field: 'date'},
      { displayName: 'Status',field: 'status'},
      { displayName: 'Nama Klinik',field: 'poli_name'},
      { displayName: 'Dokter',field: 'fullname'},	
      { displayName: 'Biaya',field: 'total'},
    ]
  };

  $scope.gridOptions.onRegisterApi = function(gridApi) {
      $scope.grid = gridApi;
      console.log($scope.grid)
  };

  $scope.go = function go(url) {
    $location.path(url);
  }

  $scope.exportToExcel = function exportToExcel(){
  	console.log("export");
  	alasql.promise('SELECT * INTO XLSX("report_klinikku.xlsx",{headers:true}) FROM ?',[$scope.myData]);
  }



  $scope.init = function init() {
  	$scope.status = ["booking","Sudah Cek Tensi","Sudah Entry Tindakan","Bayar"]
  	$scope.myData = [];
  	$scope.existingData = [];
  	$http.post(
	        "http://" + $rootScope.address +":"+ $rootScope.port.penanganan + "/get-dokter-by-klinik",
	        { 	
	        	klinik_id : String($scope.currentUser.fk_klinik)
	        },
	        {headers: {"Content-Type": "application/x-www-formurlencoded"}}
        ).then(function onSuccess(result){
	        $scope.dokter = result.data.dokter;
	        console.log("list dokter",$scope.dokter);  
        }).catch(function onError(result){
	        console.log('search error',result)
	        toastr.warning('Process Failed', 'Warning');
        })	

    console.log('INIT MENU: ', $route.current);
    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
  }

  $scope.init();
}