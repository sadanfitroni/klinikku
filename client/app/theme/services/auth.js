angular
.module('BlurAdmin')
.factory('AuthService', AuthService);

function AuthService(User, $q, $rootScope) {
    function login(email, password) {
      return User
        .login({email: email, password: password})
        .$promise
        .then(function(response) {
          $rootScope.currentUser = angular.copy(response.user);
          // $rootScope.currentUser.id = response.user.id;
          $rootScope.currentUser.tokenId = response.id;
          // $rootScope.currentUser.email = email;
            
        });
    }

    function logout() {
      return User
       .logout()
       .$promise
       .then(function() {
         $rootScope.currentUser = null;
       });
    }

    function register(email, password) {
      return User
        .create({
         email: email,
         password: password
       })
       .$promise;
    }

    return {
      login: login,
      logout: logout,
      register: register
    };
  }