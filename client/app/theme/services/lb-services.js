// CommonJS package manager support
if (typeof module !== 'undefined' && typeof exports !== 'undefined' &&
  module.exports === exports) {
  // Export the *name* of this Angular module
  // Sample usage:
  //
  //   import lbServices from './lb-services';
  //   angular.module('app', [lbServices]);
  //
  module.exports = "lbServices";
}

(function(window, angular, undefined) {
  'use strict';

  var urlBase = "/api";
  var authHeader = 'authorization';

  function getHost(url) {
    var m = url.match(/^(?:https?:)?\/\/([^\/]+)/);
    return m ? m[1] : null;
  }
  // need to use the urlBase as the base to handle multiple
  // loopback servers behind a proxy/gateway where the host
  // would be the same.
  var urlBaseHost = getHost(urlBase) ? urlBase : location.host;

/**
 * @ngdoc overview
 * @name lbServices
 * @module
 * @description
 *
 * The `lbServices` module provides services for interacting with
 * the models exposed by the LoopBack server via the REST API.
 *
 */
  var module = angular.module("lbServices", ['ngResource']);

/**
 * @ngdoc object
 * @name lbServices.History_praktek
 * @header lbServices.History_praktek
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `History_praktek` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "History_praktek",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/history_prakteks/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use History_praktek.jadwalPraktek() instead.
            "prototype$__get__jadwalPraktek": {
              url: urlBase + "/history_prakteks/:id/jadwalPraktek",
              method: "GET",
            },

            // INTERNAL. Use History_praktek.dokter() instead.
            "prototype$__get__dokter": {
              url: urlBase + "/history_prakteks/:id/dokter",
              method: "GET",
            },

            // INTERNAL. Use History_praktek.visits.findById() instead.
            "prototype$__findById__visits": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/history_prakteks/:id/visits/:fk",
              method: "GET",
            },

            // INTERNAL. Use History_praktek.visits.destroyById() instead.
            "prototype$__destroyById__visits": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/history_prakteks/:id/visits/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use History_praktek.visits.updateById() instead.
            "prototype$__updateById__visits": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/history_prakteks/:id/visits/:fk",
              method: "PUT",
            },

            // INTERNAL. Use History_praktek.visits() instead.
            "prototype$__get__visits": {
              isArray: true,
              url: urlBase + "/history_prakteks/:id/visits",
              method: "GET",
            },

            // INTERNAL. Use History_praktek.visits.create() instead.
            "prototype$__create__visits": {
              url: urlBase + "/history_prakteks/:id/visits",
              method: "POST",
            },

            // INTERNAL. Use History_praktek.visits.destroyAll() instead.
            "prototype$__delete__visits": {
              url: urlBase + "/history_prakteks/:id/visits",
              method: "DELETE",
            },

            // INTERNAL. Use History_praktek.visits.count() instead.
            "prototype$__count__visits": {
              url: urlBase + "/history_prakteks/:id/visits/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#create
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/history_prakteks",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#patchOrCreate
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/history_prakteks",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#replaceOrCreate
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/history_prakteks/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#upsertWithWhere
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/history_prakteks/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#exists
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/history_prakteks/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#findById
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/history_prakteks/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#replaceById
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/history_prakteks/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#find
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/history_prakteks",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#findOne
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/history_prakteks/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#updateAll
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/history_prakteks/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#deleteById
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/history_prakteks/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#count
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/history_prakteks/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#prototype$patchAttributes
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - history_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/history_prakteks/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#createChangeStream
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/history_prakteks/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#createMany
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/history_prakteks",
              method: "POST",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.findById() instead.
            "::findById::Jadwal_praktek::historyPrakteks": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks/:fk",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.destroyById() instead.
            "::destroyById::Jadwal_praktek::historyPrakteks": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.updateById() instead.
            "::updateById::Jadwal_praktek::historyPrakteks": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks() instead.
            "::get::Jadwal_praktek::historyPrakteks": {
              isArray: true,
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.create() instead.
            "::create::Jadwal_praktek::historyPrakteks": {
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks",
              method: "POST",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.createMany() instead.
            "::createMany::Jadwal_praktek::historyPrakteks": {
              isArray: true,
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks",
              method: "POST",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.destroyAll() instead.
            "::delete::Jadwal_praktek::historyPrakteks": {
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks",
              method: "DELETE",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.count() instead.
            "::count::Jadwal_praktek::historyPrakteks": {
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks/count",
              method: "GET",
            },

            // INTERNAL. Use Visit.historyPraktek() instead.
            "::get::Visit::historyPraktek": {
              url: urlBase + "/visits/:id/historyPraktek",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.History_praktek#upsert
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#updateOrCreate
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#patchOrCreateWithWhere
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#update
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#destroyById
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#removeById
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#prototype$updateAttributes
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - history_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.History_praktek#modelName
        * @propertyOf lbServices.History_praktek
        * @description
        * The name of the model represented by this $resource,
        * i.e. `History_praktek`.
        */
        R.modelName = "History_praktek";


            /**
             * @ngdoc method
             * @name lbServices.History_praktek#jadwalPraktek
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Fetches belongsTo relation jadwalPraktek.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - history_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R.jadwalPraktek = function() {
          var TargetResource = $injector.get("Jadwal_praktek");
          var action = TargetResource["::get::History_praktek::jadwalPraktek"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.History_praktek#dokter
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Fetches belongsTo relation dokter.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - history_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R.dokter = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::History_praktek::dokter"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.History_praktek.visits
     * @header lbServices.History_praktek.visits
     * @object
     * @description
     *
     * The object `History_praktek.visits` groups methods
     * manipulating `Visit` instances related to `History_praktek`.
     *
     * Call {@link lbServices.History_praktek#visits History_praktek.visits()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.History_praktek#visits
             * @methodOf lbServices.History_praktek
             *
             * @description
             *
             * Queries visits of history_praktek.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - history_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R.visits = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::get::History_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.History_praktek.visits#count
             * @methodOf lbServices.History_praktek.visits
             *
             * @description
             *
             * Counts visits of history_praktek.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - history_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.visits.count = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::count::History_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.History_praktek.visits#create
             * @methodOf lbServices.History_praktek.visits
             *
             * @description
             *
             * Creates a new instance in visits of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - history_praktek id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R.visits.create = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::create::History_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.History_praktek.visits#createMany
             * @methodOf lbServices.History_praktek.visits
             *
             * @description
             *
             * Creates a new instance in visits of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - history_praktek id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R.visits.createMany = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::createMany::History_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.History_praktek.visits#destroyAll
             * @methodOf lbServices.History_praktek.visits
             *
             * @description
             *
             * Deletes all visits of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - history_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.visits.destroyAll = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::delete::History_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.History_praktek.visits#destroyById
             * @methodOf lbServices.History_praktek.visits
             *
             * @description
             *
             * Delete a related item by id for visits.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - history_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for visits
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.visits.destroyById = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::destroyById::History_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.History_praktek.visits#findById
             * @methodOf lbServices.History_praktek.visits
             *
             * @description
             *
             * Find a related item by id for visits.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - history_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for visits
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R.visits.findById = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::findById::History_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.History_praktek.visits#updateById
             * @methodOf lbServices.History_praktek.visits
             *
             * @description
             *
             * Update a related item by id for visits.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - history_praktek id
             *
             *  - `fk` – `{*}` - Foreign key for visits
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R.visits.updateById = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::updateById::History_praktek::visits"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Jadwal_praktek
 * @header lbServices.Jadwal_praktek
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Jadwal_praktek` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Jadwal_praktek",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/jadwal_prakteks/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.findById() instead.
            "prototype$__findById__historyPrakteks": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks/:fk",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.destroyById() instead.
            "prototype$__destroyById__historyPrakteks": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.updateById() instead.
            "prototype$__updateById__historyPrakteks": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Jadwal_praktek.klinik() instead.
            "prototype$__get__klinik": {
              url: urlBase + "/jadwal_prakteks/:id/klinik",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.dokter() instead.
            "prototype$__get__dokter": {
              url: urlBase + "/jadwal_prakteks/:id/dokter",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.visits.findById() instead.
            "prototype$__findById__visits": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/jadwal_prakteks/:id/visits/:fk",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.visits.destroyById() instead.
            "prototype$__destroyById__visits": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/jadwal_prakteks/:id/visits/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Jadwal_praktek.visits.updateById() instead.
            "prototype$__updateById__visits": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/jadwal_prakteks/:id/visits/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks() instead.
            "prototype$__get__historyPrakteks": {
              isArray: true,
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.create() instead.
            "prototype$__create__historyPrakteks": {
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks",
              method: "POST",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.destroyAll() instead.
            "prototype$__delete__historyPrakteks": {
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks",
              method: "DELETE",
            },

            // INTERNAL. Use Jadwal_praktek.historyPrakteks.count() instead.
            "prototype$__count__historyPrakteks": {
              url: urlBase + "/jadwal_prakteks/:id/historyPrakteks/count",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.visits() instead.
            "prototype$__get__visits": {
              isArray: true,
              url: urlBase + "/jadwal_prakteks/:id/visits",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.visits.create() instead.
            "prototype$__create__visits": {
              url: urlBase + "/jadwal_prakteks/:id/visits",
              method: "POST",
            },

            // INTERNAL. Use Jadwal_praktek.visits.destroyAll() instead.
            "prototype$__delete__visits": {
              url: urlBase + "/jadwal_prakteks/:id/visits",
              method: "DELETE",
            },

            // INTERNAL. Use Jadwal_praktek.visits.count() instead.
            "prototype$__count__visits": {
              url: urlBase + "/jadwal_prakteks/:id/visits/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#create
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/jadwal_prakteks",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#patchOrCreate
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/jadwal_prakteks",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#replaceOrCreate
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/jadwal_prakteks/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#upsertWithWhere
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/jadwal_prakteks/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#exists
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/jadwal_prakteks/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#findById
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/jadwal_prakteks/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#replaceById
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/jadwal_prakteks/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#find
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/jadwal_prakteks",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#findOne
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/jadwal_prakteks/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#updateAll
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/jadwal_prakteks/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#deleteById
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/jadwal_prakteks/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#count
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/jadwal_prakteks/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#prototype$patchAttributes
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/jadwal_prakteks/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#createChangeStream
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/jadwal_prakteks/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#searchJadwal
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Pencarian jadwal praktek dokter
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
            "searchJadwal": {
              isArray: true,
              url: urlBase + "/jadwal_prakteks/searchJadwal",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#createMany
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/jadwal_prakteks",
              method: "POST",
            },

            // INTERNAL. Use History_praktek.jadwalPraktek() instead.
            "::get::History_praktek::jadwalPraktek": {
              url: urlBase + "/history_prakteks/:id/jadwalPraktek",
              method: "GET",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks.findById() instead.
            "::findById::Klinik::jadwalPrakteks": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/jadwalPrakteks/:fk",
              method: "GET",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks.destroyById() instead.
            "::destroyById::Klinik::jadwalPrakteks": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/jadwalPrakteks/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks.updateById() instead.
            "::updateById::Klinik::jadwalPrakteks": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/jadwalPrakteks/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks() instead.
            "::get::Klinik::jadwalPrakteks": {
              isArray: true,
              url: urlBase + "/kliniks/:id/jadwalPrakteks",
              method: "GET",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks.create() instead.
            "::create::Klinik::jadwalPrakteks": {
              url: urlBase + "/kliniks/:id/jadwalPrakteks",
              method: "POST",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks.createMany() instead.
            "::createMany::Klinik::jadwalPrakteks": {
              isArray: true,
              url: urlBase + "/kliniks/:id/jadwalPrakteks",
              method: "POST",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks.destroyAll() instead.
            "::delete::Klinik::jadwalPrakteks": {
              url: urlBase + "/kliniks/:id/jadwalPrakteks",
              method: "DELETE",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks.count() instead.
            "::count::Klinik::jadwalPrakteks": {
              url: urlBase + "/kliniks/:id/jadwalPrakteks/count",
              method: "GET",
            },

            // INTERNAL. Use Visit.jadwalPraktek() instead.
            "::get::Visit::jadwalPraktek": {
              url: urlBase + "/visits/:id/jadwalPraktek",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#upsert
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#updateOrCreate
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#patchOrCreateWithWhere
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#update
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#destroyById
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#removeById
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#prototype$updateAttributes
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Jadwal_praktek#modelName
        * @propertyOf lbServices.Jadwal_praktek
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Jadwal_praktek`.
        */
        R.modelName = "Jadwal_praktek";

    /**
     * @ngdoc object
     * @name lbServices.Jadwal_praktek.historyPrakteks
     * @header lbServices.Jadwal_praktek.historyPrakteks
     * @object
     * @description
     *
     * The object `Jadwal_praktek.historyPrakteks` groups methods
     * manipulating `History_praktek` instances related to `Jadwal_praktek`.
     *
     * Call {@link lbServices.Jadwal_praktek#historyPrakteks Jadwal_praktek.historyPrakteks()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#historyPrakteks
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Queries historyPrakteks of jadwal_praktek.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
        R.historyPrakteks = function() {
          var TargetResource = $injector.get("History_praktek");
          var action = TargetResource["::get::Jadwal_praktek::historyPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.historyPrakteks#count
             * @methodOf lbServices.Jadwal_praktek.historyPrakteks
             *
             * @description
             *
             * Counts historyPrakteks of jadwal_praktek.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.historyPrakteks.count = function() {
          var TargetResource = $injector.get("History_praktek");
          var action = TargetResource["::count::Jadwal_praktek::historyPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.historyPrakteks#create
             * @methodOf lbServices.Jadwal_praktek.historyPrakteks
             *
             * @description
             *
             * Creates a new instance in historyPrakteks of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
        R.historyPrakteks.create = function() {
          var TargetResource = $injector.get("History_praktek");
          var action = TargetResource["::create::Jadwal_praktek::historyPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.historyPrakteks#createMany
             * @methodOf lbServices.Jadwal_praktek.historyPrakteks
             *
             * @description
             *
             * Creates a new instance in historyPrakteks of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
        R.historyPrakteks.createMany = function() {
          var TargetResource = $injector.get("History_praktek");
          var action = TargetResource["::createMany::Jadwal_praktek::historyPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.historyPrakteks#destroyAll
             * @methodOf lbServices.Jadwal_praktek.historyPrakteks
             *
             * @description
             *
             * Deletes all historyPrakteks of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.historyPrakteks.destroyAll = function() {
          var TargetResource = $injector.get("History_praktek");
          var action = TargetResource["::delete::Jadwal_praktek::historyPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.historyPrakteks#destroyById
             * @methodOf lbServices.Jadwal_praktek.historyPrakteks
             *
             * @description
             *
             * Delete a related item by id for historyPrakteks.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for historyPrakteks
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.historyPrakteks.destroyById = function() {
          var TargetResource = $injector.get("History_praktek");
          var action = TargetResource["::destroyById::Jadwal_praktek::historyPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.historyPrakteks#findById
             * @methodOf lbServices.Jadwal_praktek.historyPrakteks
             *
             * @description
             *
             * Find a related item by id for historyPrakteks.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for historyPrakteks
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
        R.historyPrakteks.findById = function() {
          var TargetResource = $injector.get("History_praktek");
          var action = TargetResource["::findById::Jadwal_praktek::historyPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.historyPrakteks#updateById
             * @methodOf lbServices.Jadwal_praktek.historyPrakteks
             *
             * @description
             *
             * Update a related item by id for historyPrakteks.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `fk` – `{*}` - Foreign key for historyPrakteks
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
        R.historyPrakteks.updateById = function() {
          var TargetResource = $injector.get("History_praktek");
          var action = TargetResource["::updateById::Jadwal_praktek::historyPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#klinik
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Fetches belongsTo relation klinik.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
        R.klinik = function() {
          var TargetResource = $injector.get("Klinik");
          var action = TargetResource["::get::Jadwal_praktek::klinik"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#dokter
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Fetches belongsTo relation dokter.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R.dokter = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::Jadwal_praktek::dokter"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Jadwal_praktek.visits
     * @header lbServices.Jadwal_praktek.visits
     * @object
     * @description
     *
     * The object `Jadwal_praktek.visits` groups methods
     * manipulating `Visit` instances related to `Jadwal_praktek`.
     *
     * Call {@link lbServices.Jadwal_praktek#visits Jadwal_praktek.visits()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek#visits
             * @methodOf lbServices.Jadwal_praktek
             *
             * @description
             *
             * Queries visits of jadwal_praktek.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R.visits = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::get::Jadwal_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.visits#count
             * @methodOf lbServices.Jadwal_praktek.visits
             *
             * @description
             *
             * Counts visits of jadwal_praktek.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.visits.count = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::count::Jadwal_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.visits#create
             * @methodOf lbServices.Jadwal_praktek.visits
             *
             * @description
             *
             * Creates a new instance in visits of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R.visits.create = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::create::Jadwal_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.visits#createMany
             * @methodOf lbServices.Jadwal_praktek.visits
             *
             * @description
             *
             * Creates a new instance in visits of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R.visits.createMany = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::createMany::Jadwal_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.visits#destroyAll
             * @methodOf lbServices.Jadwal_praktek.visits
             *
             * @description
             *
             * Deletes all visits of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.visits.destroyAll = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::delete::Jadwal_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.visits#destroyById
             * @methodOf lbServices.Jadwal_praktek.visits
             *
             * @description
             *
             * Delete a related item by id for visits.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for visits
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.visits.destroyById = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::destroyById::Jadwal_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.visits#findById
             * @methodOf lbServices.Jadwal_praktek.visits
             *
             * @description
             *
             * Find a related item by id for visits.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for visits
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R.visits.findById = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::findById::Jadwal_praktek::visits"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Jadwal_praktek.visits#updateById
             * @methodOf lbServices.Jadwal_praktek.visits
             *
             * @description
             *
             * Update a related item by id for visits.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - jadwal_praktek id
             *
             *  - `fk` – `{*}` - Foreign key for visits
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R.visits.updateById = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::updateById::Jadwal_praktek::visits"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Klinik
 * @header lbServices.Klinik
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Klinik` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Klinik",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/kliniks/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Klinik.jadwalPrakteks.findById() instead.
            "prototype$__findById__jadwalPrakteks": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/jadwalPrakteks/:fk",
              method: "GET",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks.destroyById() instead.
            "prototype$__destroyById__jadwalPrakteks": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/jadwalPrakteks/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks.updateById() instead.
            "prototype$__updateById__jadwalPrakteks": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/jadwalPrakteks/:fk",
              method: "PUT",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#prototype$__findById__klinikDokters
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Find a related item by id for klinikDokters.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for klinikDokters
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "prototype$__findById__klinikDokters": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/klinikDokters/:fk",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#prototype$__destroyById__klinikDokters
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Delete a related item by id for klinikDokters.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for klinikDokters
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "prototype$__destroyById__klinikDokters": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/klinikDokters/:fk",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#prototype$__updateById__klinikDokters
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Update a related item by id for klinikDokters.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `fk` – `{*}` - Foreign key for klinikDokters
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "prototype$__updateById__klinikDokters": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/klinikDokters/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Klinik.medrecs.findById() instead.
            "prototype$__findById__medrecs": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/medrecs/:fk",
              method: "GET",
            },

            // INTERNAL. Use Klinik.medrecs.destroyById() instead.
            "prototype$__destroyById__medrecs": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/medrecs/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Klinik.medrecs.updateById() instead.
            "prototype$__updateById__medrecs": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/medrecs/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks() instead.
            "prototype$__get__jadwalPrakteks": {
              isArray: true,
              url: urlBase + "/kliniks/:id/jadwalPrakteks",
              method: "GET",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks.create() instead.
            "prototype$__create__jadwalPrakteks": {
              url: urlBase + "/kliniks/:id/jadwalPrakteks",
              method: "POST",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks.destroyAll() instead.
            "prototype$__delete__jadwalPrakteks": {
              url: urlBase + "/kliniks/:id/jadwalPrakteks",
              method: "DELETE",
            },

            // INTERNAL. Use Klinik.jadwalPrakteks.count() instead.
            "prototype$__count__jadwalPrakteks": {
              url: urlBase + "/kliniks/:id/jadwalPrakteks/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#prototype$__get__klinikDokters
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Queries klinikDokters of klinik.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "prototype$__get__klinikDokters": {
              isArray: true,
              url: urlBase + "/kliniks/:id/klinikDokters",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#prototype$__create__klinikDokters
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Creates a new instance in klinikDokters of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "prototype$__create__klinikDokters": {
              url: urlBase + "/kliniks/:id/klinikDokters",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#prototype$__delete__klinikDokters
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Deletes all klinikDokters of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "prototype$__delete__klinikDokters": {
              url: urlBase + "/kliniks/:id/klinikDokters",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#prototype$__count__klinikDokters
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Counts klinikDokters of klinik.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "prototype$__count__klinikDokters": {
              url: urlBase + "/kliniks/:id/klinikDokters/count",
              method: "GET",
            },

            // INTERNAL. Use Klinik.medrecs() instead.
            "prototype$__get__medrecs": {
              isArray: true,
              url: urlBase + "/kliniks/:id/medrecs",
              method: "GET",
            },

            // INTERNAL. Use Klinik.medrecs.create() instead.
            "prototype$__create__medrecs": {
              url: urlBase + "/kliniks/:id/medrecs",
              method: "POST",
            },

            // INTERNAL. Use Klinik.medrecs.destroyAll() instead.
            "prototype$__delete__medrecs": {
              url: urlBase + "/kliniks/:id/medrecs",
              method: "DELETE",
            },

            // INTERNAL. Use Klinik.medrecs.count() instead.
            "prototype$__count__medrecs": {
              url: urlBase + "/kliniks/:id/medrecs/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#create
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/kliniks",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#patchOrCreate
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/kliniks",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#replaceOrCreate
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/kliniks/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#upsertWithWhere
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/kliniks/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#exists
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/kliniks/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#findById
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/kliniks/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#replaceById
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/kliniks/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#find
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/kliniks",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#findOne
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/kliniks/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#updateAll
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/kliniks/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#deleteById
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/kliniks/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#count
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/kliniks/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#prototype$patchAttributes
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/kliniks/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#createChangeStream
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/kliniks/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Klinik#createMany
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/kliniks",
              method: "POST",
            },

            // INTERNAL. Use Jadwal_praktek.klinik() instead.
            "::get::Jadwal_praktek::klinik": {
              url: urlBase + "/jadwal_prakteks/:id/klinik",
              method: "GET",
            },

            // INTERNAL. Use Medrec.klinik() instead.
            "::get::Medrec::klinik": {
              url: urlBase + "/medrecs/:id/klinik",
              method: "GET",
            },

            // INTERNAL. Use Visit.klinik() instead.
            "::get::Visit::klinik": {
              url: urlBase + "/visits/:id/klinik",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Klinik#upsert
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Klinik#updateOrCreate
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Klinik#patchOrCreateWithWhere
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Klinik#update
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Klinik#destroyById
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Klinik#removeById
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Klinik#prototype$updateAttributes
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Klinik#modelName
        * @propertyOf lbServices.Klinik
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Klinik`.
        */
        R.modelName = "Klinik";

    /**
     * @ngdoc object
     * @name lbServices.Klinik.jadwalPrakteks
     * @header lbServices.Klinik.jadwalPrakteks
     * @object
     * @description
     *
     * The object `Klinik.jadwalPrakteks` groups methods
     * manipulating `Jadwal_praktek` instances related to `Klinik`.
     *
     * Call {@link lbServices.Klinik#jadwalPrakteks Klinik.jadwalPrakteks()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Klinik#jadwalPrakteks
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Queries jadwalPrakteks of klinik.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R.jadwalPrakteks = function() {
          var TargetResource = $injector.get("Jadwal_praktek");
          var action = TargetResource["::get::Klinik::jadwalPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.jadwalPrakteks#count
             * @methodOf lbServices.Klinik.jadwalPrakteks
             *
             * @description
             *
             * Counts jadwalPrakteks of klinik.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.jadwalPrakteks.count = function() {
          var TargetResource = $injector.get("Jadwal_praktek");
          var action = TargetResource["::count::Klinik::jadwalPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.jadwalPrakteks#create
             * @methodOf lbServices.Klinik.jadwalPrakteks
             *
             * @description
             *
             * Creates a new instance in jadwalPrakteks of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R.jadwalPrakteks.create = function() {
          var TargetResource = $injector.get("Jadwal_praktek");
          var action = TargetResource["::create::Klinik::jadwalPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.jadwalPrakteks#createMany
             * @methodOf lbServices.Klinik.jadwalPrakteks
             *
             * @description
             *
             * Creates a new instance in jadwalPrakteks of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R.jadwalPrakteks.createMany = function() {
          var TargetResource = $injector.get("Jadwal_praktek");
          var action = TargetResource["::createMany::Klinik::jadwalPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.jadwalPrakteks#destroyAll
             * @methodOf lbServices.Klinik.jadwalPrakteks
             *
             * @description
             *
             * Deletes all jadwalPrakteks of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.jadwalPrakteks.destroyAll = function() {
          var TargetResource = $injector.get("Jadwal_praktek");
          var action = TargetResource["::delete::Klinik::jadwalPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.jadwalPrakteks#destroyById
             * @methodOf lbServices.Klinik.jadwalPrakteks
             *
             * @description
             *
             * Delete a related item by id for jadwalPrakteks.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for jadwalPrakteks
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.jadwalPrakteks.destroyById = function() {
          var TargetResource = $injector.get("Jadwal_praktek");
          var action = TargetResource["::destroyById::Klinik::jadwalPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.jadwalPrakteks#findById
             * @methodOf lbServices.Klinik.jadwalPrakteks
             *
             * @description
             *
             * Find a related item by id for jadwalPrakteks.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for jadwalPrakteks
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R.jadwalPrakteks.findById = function() {
          var TargetResource = $injector.get("Jadwal_praktek");
          var action = TargetResource["::findById::Klinik::jadwalPrakteks"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.jadwalPrakteks#updateById
             * @methodOf lbServices.Klinik.jadwalPrakteks
             *
             * @description
             *
             * Update a related item by id for jadwalPrakteks.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `fk` – `{*}` - Foreign key for jadwalPrakteks
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R.jadwalPrakteks.updateById = function() {
          var TargetResource = $injector.get("Jadwal_praktek");
          var action = TargetResource["::updateById::Klinik::jadwalPrakteks"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Klinik.medrecs
     * @header lbServices.Klinik.medrecs
     * @object
     * @description
     *
     * The object `Klinik.medrecs` groups methods
     * manipulating `Medrec` instances related to `Klinik`.
     *
     * Call {@link lbServices.Klinik#medrecs Klinik.medrecs()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Klinik#medrecs
             * @methodOf lbServices.Klinik
             *
             * @description
             *
             * Queries medrecs of klinik.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R.medrecs = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::get::Klinik::medrecs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.medrecs#count
             * @methodOf lbServices.Klinik.medrecs
             *
             * @description
             *
             * Counts medrecs of klinik.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.medrecs.count = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::count::Klinik::medrecs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.medrecs#create
             * @methodOf lbServices.Klinik.medrecs
             *
             * @description
             *
             * Creates a new instance in medrecs of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R.medrecs.create = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::create::Klinik::medrecs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.medrecs#createMany
             * @methodOf lbServices.Klinik.medrecs
             *
             * @description
             *
             * Creates a new instance in medrecs of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R.medrecs.createMany = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::createMany::Klinik::medrecs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.medrecs#destroyAll
             * @methodOf lbServices.Klinik.medrecs
             *
             * @description
             *
             * Deletes all medrecs of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.medrecs.destroyAll = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::delete::Klinik::medrecs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.medrecs#destroyById
             * @methodOf lbServices.Klinik.medrecs
             *
             * @description
             *
             * Delete a related item by id for medrecs.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for medrecs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.medrecs.destroyById = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::destroyById::Klinik::medrecs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.medrecs#findById
             * @methodOf lbServices.Klinik.medrecs
             *
             * @description
             *
             * Find a related item by id for medrecs.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for medrecs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R.medrecs.findById = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::findById::Klinik::medrecs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Klinik.medrecs#updateById
             * @methodOf lbServices.Klinik.medrecs
             *
             * @description
             *
             * Update a related item by id for medrecs.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - klinik id
             *
             *  - `fk` – `{*}` - Foreign key for medrecs
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R.medrecs.updateById = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::updateById::Klinik::medrecs"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Medrec
 * @header lbServices.Medrec
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Medrec` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Medrec",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/medrecs/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Medrec.klinik() instead.
            "prototype$__get__klinik": {
              url: urlBase + "/medrecs/:id/klinik",
              method: "GET",
            },

            // INTERNAL. Use Medrec.dokter() instead.
            "prototype$__get__dokter": {
              url: urlBase + "/medrecs/:id/dokter",
              method: "GET",
            },

            // INTERNAL. Use Medrec.pasien() instead.
            "prototype$__get__pasien": {
              url: urlBase + "/medrecs/:id/pasien",
              method: "GET",
            },

            // INTERNAL. Use Medrec.fk_perawat() instead.
            "prototype$__get__fk_perawat": {
              url: urlBase + "/medrecs/:id/fk_perawat",
              method: "GET",
            },

            // INTERNAL. Use Medrec.visit() instead.
            "prototype$__get__visit": {
              url: urlBase + "/medrecs/:id/visit",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#create
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/medrecs",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#patchOrCreate
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/medrecs",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#replaceOrCreate
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/medrecs/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#upsertWithWhere
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/medrecs/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#exists
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/medrecs/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#findById
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/medrecs/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#replaceById
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/medrecs/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#find
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/medrecs",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#findOne
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/medrecs/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#updateAll
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/medrecs/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#deleteById
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/medrecs/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#count
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/medrecs/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#prototype$patchAttributes
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - medrec id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/medrecs/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#createChangeStream
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/medrecs/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Medrec#createMany
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/medrecs",
              method: "POST",
            },

            // INTERNAL. Use Klinik.medrecs.findById() instead.
            "::findById::Klinik::medrecs": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/medrecs/:fk",
              method: "GET",
            },

            // INTERNAL. Use Klinik.medrecs.destroyById() instead.
            "::destroyById::Klinik::medrecs": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/medrecs/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Klinik.medrecs.updateById() instead.
            "::updateById::Klinik::medrecs": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kliniks/:id/medrecs/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Klinik.medrecs() instead.
            "::get::Klinik::medrecs": {
              isArray: true,
              url: urlBase + "/kliniks/:id/medrecs",
              method: "GET",
            },

            // INTERNAL. Use Klinik.medrecs.create() instead.
            "::create::Klinik::medrecs": {
              url: urlBase + "/kliniks/:id/medrecs",
              method: "POST",
            },

            // INTERNAL. Use Klinik.medrecs.createMany() instead.
            "::createMany::Klinik::medrecs": {
              isArray: true,
              url: urlBase + "/kliniks/:id/medrecs",
              method: "POST",
            },

            // INTERNAL. Use Klinik.medrecs.destroyAll() instead.
            "::delete::Klinik::medrecs": {
              url: urlBase + "/kliniks/:id/medrecs",
              method: "DELETE",
            },

            // INTERNAL. Use Klinik.medrecs.count() instead.
            "::count::Klinik::medrecs": {
              url: urlBase + "/kliniks/:id/medrecs/count",
              method: "GET",
            },

            // INTERNAL. Use Visit.medrecs() instead.
            "::get::Visit::medrecs": {
              url: urlBase + "/visits/:id/medrecs",
              method: "GET",
            },

            // INTERNAL. Use Visit.medrecs.create() instead.
            "::create::Visit::medrecs": {
              url: urlBase + "/visits/:id/medrecs",
              method: "POST",
            },

            // INTERNAL. Use Visit.medrecs.createMany() instead.
            "::createMany::Visit::medrecs": {
              isArray: true,
              url: urlBase + "/visits/:id/medrecs",
              method: "POST",
            },

            // INTERNAL. Use Visit.medrecs.update() instead.
            "::update::Visit::medrecs": {
              url: urlBase + "/visits/:id/medrecs",
              method: "PUT",
            },

            // INTERNAL. Use Visit.medrecs.destroy() instead.
            "::destroy::Visit::medrecs": {
              url: urlBase + "/visits/:id/medrecs",
              method: "DELETE",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Medrec#upsert
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Medrec#updateOrCreate
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Medrec#patchOrCreateWithWhere
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Medrec#update
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Medrec#destroyById
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Medrec#removeById
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Medrec#prototype$updateAttributes
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - medrec id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Medrec#modelName
        * @propertyOf lbServices.Medrec
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Medrec`.
        */
        R.modelName = "Medrec";


            /**
             * @ngdoc method
             * @name lbServices.Medrec#klinik
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Fetches belongsTo relation klinik.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - medrec id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
        R.klinik = function() {
          var TargetResource = $injector.get("Klinik");
          var action = TargetResource["::get::Medrec::klinik"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Medrec#dokter
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Fetches belongsTo relation dokter.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - medrec id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R.dokter = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::Medrec::dokter"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Medrec#pasien
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Fetches belongsTo relation pasien.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - medrec id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R.pasien = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::Medrec::pasien"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Medrec#fk_perawat
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Fetches belongsTo relation fk_perawat.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - medrec id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R.fk_perawat = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::Medrec::fk_perawat"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Medrec#visit
             * @methodOf lbServices.Medrec
             *
             * @description
             *
             * Fetches belongsTo relation visit.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - medrec id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R.visit = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::get::Medrec::visit"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Menu_model
 * @header lbServices.Menu_model
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Menu_model` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Menu_model",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/menu_models/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Menu_model.menuSidebar() instead.
            "prototype$__get__menuSidebar": {
              url: urlBase + "/menu_models/:id/menuSidebar",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#create
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/menu_models",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#patchOrCreate
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/menu_models",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#replaceOrCreate
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/menu_models/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#upsertWithWhere
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/menu_models/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#exists
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/menu_models/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#findById
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/menu_models/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#replaceById
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/menu_models/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#find
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/menu_models",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#findOne
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/menu_models/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#updateAll
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/menu_models/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#deleteById
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/menu_models/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#count
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/menu_models/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#prototype$patchAttributes
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_model id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/menu_models/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#createChangeStream
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/menu_models/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#createMany
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/menu_models",
              method: "POST",
            },

            // INTERNAL. Use Menu_sidebar.menuModels.findById() instead.
            "::findById::Menu_sidebar::menuModels": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/menu_sidebars/:id/menuModels/:fk",
              method: "GET",
            },

            // INTERNAL. Use Menu_sidebar.menuModels.destroyById() instead.
            "::destroyById::Menu_sidebar::menuModels": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/menu_sidebars/:id/menuModels/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Menu_sidebar.menuModels.updateById() instead.
            "::updateById::Menu_sidebar::menuModels": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/menu_sidebars/:id/menuModels/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Menu_sidebar.menuModels() instead.
            "::get::Menu_sidebar::menuModels": {
              isArray: true,
              url: urlBase + "/menu_sidebars/:id/menuModels",
              method: "GET",
            },

            // INTERNAL. Use Menu_sidebar.menuModels.create() instead.
            "::create::Menu_sidebar::menuModels": {
              url: urlBase + "/menu_sidebars/:id/menuModels",
              method: "POST",
            },

            // INTERNAL. Use Menu_sidebar.menuModels.createMany() instead.
            "::createMany::Menu_sidebar::menuModels": {
              isArray: true,
              url: urlBase + "/menu_sidebars/:id/menuModels",
              method: "POST",
            },

            // INTERNAL. Use Menu_sidebar.menuModels.destroyAll() instead.
            "::delete::Menu_sidebar::menuModels": {
              url: urlBase + "/menu_sidebars/:id/menuModels",
              method: "DELETE",
            },

            // INTERNAL. Use Menu_sidebar.menuModels.count() instead.
            "::count::Menu_sidebar::menuModels": {
              url: urlBase + "/menu_sidebars/:id/menuModels/count",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Menu_model#upsert
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#updateOrCreate
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#patchOrCreateWithWhere
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#update
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#destroyById
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#removeById
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Menu_model#prototype$updateAttributes
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_model id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Menu_model#modelName
        * @propertyOf lbServices.Menu_model
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Menu_model`.
        */
        R.modelName = "Menu_model";


            /**
             * @ngdoc method
             * @name lbServices.Menu_model#menuSidebar
             * @methodOf lbServices.Menu_model
             *
             * @description
             *
             * Fetches belongsTo relation menuSidebar.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_model id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
        R.menuSidebar = function() {
          var TargetResource = $injector.get("Menu_sidebar");
          var action = TargetResource["::get::Menu_model::menuSidebar"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Menu_sidebar
 * @header lbServices.Menu_sidebar
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Menu_sidebar` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Menu_sidebar",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/menu_sidebars/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Menu_sidebar.menuModels.findById() instead.
            "prototype$__findById__menuModels": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/menu_sidebars/:id/menuModels/:fk",
              method: "GET",
            },

            // INTERNAL. Use Menu_sidebar.menuModels.destroyById() instead.
            "prototype$__destroyById__menuModels": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/menu_sidebars/:id/menuModels/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Menu_sidebar.menuModels.updateById() instead.
            "prototype$__updateById__menuModels": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/menu_sidebars/:id/menuModels/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Menu_sidebar.menuModels() instead.
            "prototype$__get__menuModels": {
              isArray: true,
              url: urlBase + "/menu_sidebars/:id/menuModels",
              method: "GET",
            },

            // INTERNAL. Use Menu_sidebar.menuModels.create() instead.
            "prototype$__create__menuModels": {
              url: urlBase + "/menu_sidebars/:id/menuModels",
              method: "POST",
            },

            // INTERNAL. Use Menu_sidebar.menuModels.destroyAll() instead.
            "prototype$__delete__menuModels": {
              url: urlBase + "/menu_sidebars/:id/menuModels",
              method: "DELETE",
            },

            // INTERNAL. Use Menu_sidebar.menuModels.count() instead.
            "prototype$__count__menuModels": {
              url: urlBase + "/menu_sidebars/:id/menuModels/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#create
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/menu_sidebars",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#patchOrCreate
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/menu_sidebars",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#replaceOrCreate
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/menu_sidebars/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#upsertWithWhere
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/menu_sidebars/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#exists
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/menu_sidebars/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#findById
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/menu_sidebars/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#replaceById
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/menu_sidebars/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#find
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/menu_sidebars",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#findOne
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/menu_sidebars/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#updateAll
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/menu_sidebars/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#deleteById
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/menu_sidebars/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#count
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/menu_sidebars/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#prototype$patchAttributes
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_sidebar id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/menu_sidebars/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#createChangeStream
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/menu_sidebars/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#createMany
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/menu_sidebars",
              method: "POST",
            },

            // INTERNAL. Use Menu_model.menuSidebar() instead.
            "::get::Menu_model::menuSidebar": {
              url: urlBase + "/menu_models/:id/menuSidebar",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#upsert
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#updateOrCreate
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#patchOrCreateWithWhere
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#update
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#destroyById
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#removeById
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#prototype$updateAttributes
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_sidebar id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_sidebar` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Menu_sidebar#modelName
        * @propertyOf lbServices.Menu_sidebar
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Menu_sidebar`.
        */
        R.modelName = "Menu_sidebar";

    /**
     * @ngdoc object
     * @name lbServices.Menu_sidebar.menuModels
     * @header lbServices.Menu_sidebar.menuModels
     * @object
     * @description
     *
     * The object `Menu_sidebar.menuModels` groups methods
     * manipulating `Menu_model` instances related to `Menu_sidebar`.
     *
     * Call {@link lbServices.Menu_sidebar#menuModels Menu_sidebar.menuModels()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar#menuModels
             * @methodOf lbServices.Menu_sidebar
             *
             * @description
             *
             * Queries menuModels of menu_sidebar.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_sidebar id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
        R.menuModels = function() {
          var TargetResource = $injector.get("Menu_model");
          var action = TargetResource["::get::Menu_sidebar::menuModels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar.menuModels#count
             * @methodOf lbServices.Menu_sidebar.menuModels
             *
             * @description
             *
             * Counts menuModels of menu_sidebar.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_sidebar id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.menuModels.count = function() {
          var TargetResource = $injector.get("Menu_model");
          var action = TargetResource["::count::Menu_sidebar::menuModels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar.menuModels#create
             * @methodOf lbServices.Menu_sidebar.menuModels
             *
             * @description
             *
             * Creates a new instance in menuModels of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_sidebar id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
        R.menuModels.create = function() {
          var TargetResource = $injector.get("Menu_model");
          var action = TargetResource["::create::Menu_sidebar::menuModels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar.menuModels#createMany
             * @methodOf lbServices.Menu_sidebar.menuModels
             *
             * @description
             *
             * Creates a new instance in menuModels of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_sidebar id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
        R.menuModels.createMany = function() {
          var TargetResource = $injector.get("Menu_model");
          var action = TargetResource["::createMany::Menu_sidebar::menuModels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar.menuModels#destroyAll
             * @methodOf lbServices.Menu_sidebar.menuModels
             *
             * @description
             *
             * Deletes all menuModels of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_sidebar id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.menuModels.destroyAll = function() {
          var TargetResource = $injector.get("Menu_model");
          var action = TargetResource["::delete::Menu_sidebar::menuModels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar.menuModels#destroyById
             * @methodOf lbServices.Menu_sidebar.menuModels
             *
             * @description
             *
             * Delete a related item by id for menuModels.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_sidebar id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for menuModels
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.menuModels.destroyById = function() {
          var TargetResource = $injector.get("Menu_model");
          var action = TargetResource["::destroyById::Menu_sidebar::menuModels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar.menuModels#findById
             * @methodOf lbServices.Menu_sidebar.menuModels
             *
             * @description
             *
             * Find a related item by id for menuModels.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_sidebar id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for menuModels
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
        R.menuModels.findById = function() {
          var TargetResource = $injector.get("Menu_model");
          var action = TargetResource["::findById::Menu_sidebar::menuModels"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Menu_sidebar.menuModels#updateById
             * @methodOf lbServices.Menu_sidebar.menuModels
             *
             * @description
             *
             * Update a related item by id for menuModels.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - menu_sidebar id
             *
             *  - `fk` – `{*}` - Foreign key for menuModels
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Menu_model` object.)
             * </em>
             */
        R.menuModels.updateById = function() {
          var TargetResource = $injector.get("Menu_model");
          var action = TargetResource["::updateById::Menu_sidebar::menuModels"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Resep
 * @header lbServices.Resep
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Resep` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Resep",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/reseps/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Resep.visit() instead.
            "prototype$__get__visit": {
              url: urlBase + "/reseps/:id/visit",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#create
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/reseps",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#patchOrCreate
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/reseps",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#replaceOrCreate
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/reseps/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#upsertWithWhere
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/reseps/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#exists
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/reseps/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#findById
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/reseps/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#replaceById
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/reseps/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#find
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/reseps",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#findOne
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/reseps/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#updateAll
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/reseps/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#deleteById
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/reseps/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#count
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/reseps/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#prototype$patchAttributes
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - resep id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/reseps/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#createChangeStream
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/reseps/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Resep#createMany
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/reseps",
              method: "POST",
            },

            // INTERNAL. Use Visit.reseps.findById() instead.
            "::findById::Visit::reseps": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/visits/:id/reseps/:fk",
              method: "GET",
            },

            // INTERNAL. Use Visit.reseps.destroyById() instead.
            "::destroyById::Visit::reseps": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/visits/:id/reseps/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Visit.reseps.updateById() instead.
            "::updateById::Visit::reseps": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/visits/:id/reseps/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Visit.reseps() instead.
            "::get::Visit::reseps": {
              isArray: true,
              url: urlBase + "/visits/:id/reseps",
              method: "GET",
            },

            // INTERNAL. Use Visit.reseps.create() instead.
            "::create::Visit::reseps": {
              url: urlBase + "/visits/:id/reseps",
              method: "POST",
            },

            // INTERNAL. Use Visit.reseps.createMany() instead.
            "::createMany::Visit::reseps": {
              isArray: true,
              url: urlBase + "/visits/:id/reseps",
              method: "POST",
            },

            // INTERNAL. Use Visit.reseps.destroyAll() instead.
            "::delete::Visit::reseps": {
              url: urlBase + "/visits/:id/reseps",
              method: "DELETE",
            },

            // INTERNAL. Use Visit.reseps.count() instead.
            "::count::Visit::reseps": {
              url: urlBase + "/visits/:id/reseps/count",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Resep#upsert
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Resep#updateOrCreate
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Resep#patchOrCreateWithWhere
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Resep#update
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Resep#destroyById
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Resep#removeById
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Resep#prototype$updateAttributes
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - resep id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Resep#modelName
        * @propertyOf lbServices.Resep
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Resep`.
        */
        R.modelName = "Resep";


            /**
             * @ngdoc method
             * @name lbServices.Resep#visit
             * @methodOf lbServices.Resep
             *
             * @description
             *
             * Fetches belongsTo relation visit.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - resep id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R.visit = function() {
          var TargetResource = $injector.get("Visit");
          var action = TargetResource["::get::Resep::visit"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Role
 * @header lbServices.Role
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Role` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Role",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/roles/:id",
          { 'id': '@id' },
          {

            /**
             * @ngdoc method
             * @name lbServices.Role#create
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/roles",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#patchOrCreate
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/roles",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#replaceOrCreate
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/roles/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#upsertWithWhere
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/roles/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#exists
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/roles/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#findById
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/roles/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#replaceById
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/roles/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#find
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/roles",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#findOne
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/roles/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#updateAll
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/roles/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#deleteById
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/roles/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#count
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/roles/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#prototype$patchAttributes
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - role id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/roles/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#createChangeStream
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/roles/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Role#createMany
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/roles",
              method: "POST",
            },

            // INTERNAL. Use User.role() instead.
            "::get::User::role": {
              url: urlBase + "/users/:id/role",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Role#upsert
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Role#updateOrCreate
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Role#patchOrCreateWithWhere
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Role#update
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Role#destroyById
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Role#removeById
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Role#prototype$updateAttributes
             * @methodOf lbServices.Role
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - role id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Role#modelName
        * @propertyOf lbServices.Role
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Role`.
        */
        R.modelName = "Role";



        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.User
 * @header lbServices.User
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `User` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "User",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/users/:id",
          { 'id': '@id' },
          {

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__findById__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Find a related item by id for accessTokens.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - user id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for accessTokens
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "prototype$__findById__accessTokens": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/users/:id/accessTokens/:fk",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__destroyById__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Delete a related item by id for accessTokens.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - user id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for accessTokens
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "prototype$__destroyById__accessTokens": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/users/:id/accessTokens/:fk",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__updateById__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Update a related item by id for accessTokens.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - user id
             *
             *  - `fk` – `{*}` - Foreign key for accessTokens
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "prototype$__updateById__accessTokens": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/users/:id/accessTokens/:fk",
              method: "PUT",
            },

            // INTERNAL. Use User.role() instead.
            "prototype$__get__role": {
              url: urlBase + "/users/:id/role",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__get__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Queries accessTokens of user.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - user id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "prototype$__get__accessTokens": {
              isArray: true,
              url: urlBase + "/users/:id/accessTokens",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__create__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Creates a new instance in accessTokens of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - user id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "prototype$__create__accessTokens": {
              url: urlBase + "/users/:id/accessTokens",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__delete__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Deletes all accessTokens of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - user id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "prototype$__delete__accessTokens": {
              url: urlBase + "/users/:id/accessTokens",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$__count__accessTokens
             * @methodOf lbServices.User
             *
             * @description
             *
             * Counts accessTokens of user.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - user id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "prototype$__count__accessTokens": {
              url: urlBase + "/users/:id/accessTokens/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#create
             * @methodOf lbServices.User
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/users",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#patchOrCreate
             * @methodOf lbServices.User
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/users",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#replaceOrCreate
             * @methodOf lbServices.User
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/users/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#upsertWithWhere
             * @methodOf lbServices.User
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/users/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#exists
             * @methodOf lbServices.User
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/users/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#findById
             * @methodOf lbServices.User
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/users/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#replaceById
             * @methodOf lbServices.User
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/users/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#find
             * @methodOf lbServices.User
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/users",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#findOne
             * @methodOf lbServices.User
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/users/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#updateAll
             * @methodOf lbServices.User
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/users/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#deleteById
             * @methodOf lbServices.User
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/users/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#count
             * @methodOf lbServices.User
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/users/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$patchAttributes
             * @methodOf lbServices.User
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - user id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/users/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#createChangeStream
             * @methodOf lbServices.User
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/users/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#login
             * @methodOf lbServices.User
             *
             * @description
             *
             * Login a user with username/email and password.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `include` – `{string=}` - Related objects to include in the response. See the description of return value for more details.
             *   Default value: `user`.
             *
             *  - `rememberMe` - `boolean` - Whether the authentication credentials
             *     should be remembered in localStorage across app/browser restarts.
             *     Default: `true`.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * The response body contains properties of the AccessToken created on login.
             * Depending on the value of `include` parameter, the body may contain additional properties:
             *   - `user` - `U+007BUserU+007D` - Data of the currently logged in user. (`include=user`)
             *
             */
            "login": {
              params: {
                include: 'user',
              },
              interceptor: {
                response: function(response) {
                  var accessToken = response.data;
                  LoopBackAuth.setUser(
                    accessToken.id, accessToken.userId, accessToken.user);
                  LoopBackAuth.rememberMe =
                    response.config.params.rememberMe !== false;
                  LoopBackAuth.save();
                  return response.resource;
                },
              },
              url: urlBase + "/users/login",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#logout
             * @methodOf lbServices.User
             *
             * @description
             *
             * Logout a user with access token.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `access_token` – `{string=}` - Do not supply this argument, it is automatically extracted from request headers.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "logout": {
              interceptor: {
                response: function(response) {
                  LoopBackAuth.clearUser();
                  LoopBackAuth.clearStorage();
                  return response.resource;
                },
                responseError: function(responseError) {
                  LoopBackAuth.clearUser();
                  LoopBackAuth.clearStorage();
                  return responseError.resource;
                },
              },
              url: urlBase + "/users/logout",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$verify
             * @methodOf lbServices.User
             *
             * @description
             *
             * Trigger user's identity verification with configured verifyOptions
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - user id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `verifyOptions` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "prototype$verify": {
              url: urlBase + "/users/:id/verify",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#confirm
             * @methodOf lbServices.User
             *
             * @description
             *
             * Confirm a user registration with identity verification token.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `uid` – `{string}` -
             *
             *  - `token` – `{string}` -
             *
             *  - `redirect` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "confirm": {
              url: urlBase + "/users/confirm",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#resetPassword
             * @methodOf lbServices.User
             *
             * @description
             *
             * Reset password for a user with email.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "resetPassword": {
              url: urlBase + "/users/reset",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#changePassword
             * @methodOf lbServices.User
             *
             * @description
             *
             * Change a user's password.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `id` – `{*=}` -
             *
             *  - `oldPassword` – `{string}` -
             *
             *  - `newPassword` – `{string}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "changePassword": {
              url: urlBase + "/users/change-password",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#setPassword
             * @methodOf lbServices.User
             *
             * @description
             *
             * Reset user's password via a password-reset token.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `id` – `{*=}` -
             *
             *  - `newPassword` – `{string}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "setPassword": {
              url: urlBase + "/users/reset-password",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#checkPassword
             * @methodOf lbServices.User
             *
             * @description
             *
             * check loopback password
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `decodePassword` – `{string=}` -
             *
             *  - `password` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `result` – `{boolean=}` -
             */
            "checkPassword": {
              url: urlBase + "/users/checkPassword",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#encodePassword
             * @methodOf lbServices.User
             *
             * @description
             *
             * encode loopback password
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `password` – `{string=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "encodePassword": {
              url: urlBase + "/users/encodePassword",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#createMany
             * @methodOf lbServices.User
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/users",
              method: "POST",
            },

            // INTERNAL. Use History_praktek.dokter() instead.
            "::get::History_praktek::dokter": {
              url: urlBase + "/history_prakteks/:id/dokter",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.dokter() instead.
            "::get::Jadwal_praktek::dokter": {
              url: urlBase + "/jadwal_prakteks/:id/dokter",
              method: "GET",
            },

            // INTERNAL. Use Medrec.dokter() instead.
            "::get::Medrec::dokter": {
              url: urlBase + "/medrecs/:id/dokter",
              method: "GET",
            },

            // INTERNAL. Use Medrec.pasien() instead.
            "::get::Medrec::pasien": {
              url: urlBase + "/medrecs/:id/pasien",
              method: "GET",
            },

            // INTERNAL. Use Medrec.fk_perawat() instead.
            "::get::Medrec::fk_perawat": {
              url: urlBase + "/medrecs/:id/fk_perawat",
              method: "GET",
            },

            // INTERNAL. Use Visit.dokter() instead.
            "::get::Visit::dokter": {
              url: urlBase + "/visits/:id/dokter",
              method: "GET",
            },

            // INTERNAL. Use Visit.perawat() instead.
            "::get::Visit::perawat": {
              url: urlBase + "/visits/:id/perawat",
              method: "GET",
            },

            // INTERNAL. Use Visit.pasien() instead.
            "::get::Visit::pasien": {
              url: urlBase + "/visits/:id/pasien",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.pasien() instead.
            "::get::Detail_pasien::pasien": {
              url: urlBase + "/detail_pasiens/:id/pasien",
              method: "GET",
            },

            // INTERNAL. Use Penjamin_pasien.pasien() instead.
            "::get::Penjamin_pasien::pasien": {
              url: urlBase + "/penjamin_pasiens/:id/pasien",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.User#getCurrent
             * @methodOf lbServices.User
             *
             * @description
             *
             * Get data of the currently logged user. Fail with HTTP result 401
             * when there is no user logged in.
             *
             * @param {function(Object,Object)=} successCb
             *    Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *    `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             */
            'getCurrent': {
              url: urlBase + "/users" + '/:id',
              method: 'GET',
              params: {
                id: function() {
                  var id = LoopBackAuth.currentUserId;
                  if (id == null) id = '__anonymous__';
                  return id;
                },
              },
              interceptor: {
                response: function(response) {
                  LoopBackAuth.currentUserData = response.data;
                  return response.resource;
                },
                responseError: function(responseError) {
                  LoopBackAuth.clearUser();
                  LoopBackAuth.clearStorage();
                  return $q.reject(responseError);
                },
              },
              __isGetCurrentUser__: true,
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.User#upsert
             * @methodOf lbServices.User
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.User#updateOrCreate
             * @methodOf lbServices.User
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.User#patchOrCreateWithWhere
             * @methodOf lbServices.User
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.User#update
             * @methodOf lbServices.User
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.User#destroyById
             * @methodOf lbServices.User
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.User#removeById
             * @methodOf lbServices.User
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.User#prototype$updateAttributes
             * @methodOf lbServices.User
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - user id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];

        /**
         * @ngdoc method
         * @name lbServices.User#getCachedCurrent
         * @methodOf lbServices.User
         *
         * @description
         *
         * Get data of the currently logged user that was returned by the last
         * call to {@link lbServices.User#login} or
         * {@link lbServices.User#getCurrent}. Return null when there
         * is no user logged in or the data of the current user were not fetched
         * yet.
         *
         * @returns {Object} A User instance.
         */
        R.getCachedCurrent = function() {
          var data = LoopBackAuth.currentUserData;
          return data ? new R(data) : null;
        };

        /**
         * @ngdoc method
         * @name lbServices.User#isAuthenticated
         * @methodOf lbServices.User
         *
         * @returns {boolean} True if the current user is authenticated (logged in).
         */
        R.isAuthenticated = function() {
          return this.getCurrentId() != null;
        };

        /**
         * @ngdoc method
         * @name lbServices.User#getCurrentId
         * @methodOf lbServices.User
         *
         * @returns {Object} Id of the currently logged-in user or null.
         */
        R.getCurrentId = function() {
          return LoopBackAuth.currentUserId;
        };

        /**
        * @ngdoc property
        * @name lbServices.User#modelName
        * @propertyOf lbServices.User
        * @description
        * The name of the model represented by this $resource,
        * i.e. `User`.
        */
        R.modelName = "User";


            /**
             * @ngdoc method
             * @name lbServices.User#role
             * @methodOf lbServices.User
             *
             * @description
             *
             * Fetches belongsTo relation role.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - user id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Role` object.)
             * </em>
             */
        R.role = function() {
          var TargetResource = $injector.get("Role");
          var action = TargetResource["::get::User::role"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Visit
 * @header lbServices.Visit
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Visit` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Visit",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/visits/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Visit.medrecs() instead.
            "prototype$__get__medrecs": {
              url: urlBase + "/visits/:id/medrecs",
              method: "GET",
            },

            // INTERNAL. Use Visit.medrecs.create() instead.
            "prototype$__create__medrecs": {
              url: urlBase + "/visits/:id/medrecs",
              method: "POST",
            },

            // INTERNAL. Use Visit.medrecs.update() instead.
            "prototype$__update__medrecs": {
              url: urlBase + "/visits/:id/medrecs",
              method: "PUT",
            },

            // INTERNAL. Use Visit.medrecs.destroy() instead.
            "prototype$__destroy__medrecs": {
              url: urlBase + "/visits/:id/medrecs",
              method: "DELETE",
            },

            // INTERNAL. Use Visit.reseps.findById() instead.
            "prototype$__findById__reseps": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/visits/:id/reseps/:fk",
              method: "GET",
            },

            // INTERNAL. Use Visit.reseps.destroyById() instead.
            "prototype$__destroyById__reseps": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/visits/:id/reseps/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Visit.reseps.updateById() instead.
            "prototype$__updateById__reseps": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/visits/:id/reseps/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Visit.dokter() instead.
            "prototype$__get__dokter": {
              url: urlBase + "/visits/:id/dokter",
              method: "GET",
            },

            // INTERNAL. Use Visit.jadwalPraktek() instead.
            "prototype$__get__jadwalPraktek": {
              url: urlBase + "/visits/:id/jadwalPraktek",
              method: "GET",
            },

            // INTERNAL. Use Visit.perawat() instead.
            "prototype$__get__perawat": {
              url: urlBase + "/visits/:id/perawat",
              method: "GET",
            },

            // INTERNAL. Use Visit.pasien() instead.
            "prototype$__get__pasien": {
              url: urlBase + "/visits/:id/pasien",
              method: "GET",
            },

            // INTERNAL. Use Visit.historyPraktek() instead.
            "prototype$__get__historyPraktek": {
              url: urlBase + "/visits/:id/historyPraktek",
              method: "GET",
            },

            // INTERNAL. Use Visit.klinik() instead.
            "prototype$__get__klinik": {
              url: urlBase + "/visits/:id/klinik",
              method: "GET",
            },

            // INTERNAL. Use Visit.reseps() instead.
            "prototype$__get__reseps": {
              isArray: true,
              url: urlBase + "/visits/:id/reseps",
              method: "GET",
            },

            // INTERNAL. Use Visit.reseps.create() instead.
            "prototype$__create__reseps": {
              url: urlBase + "/visits/:id/reseps",
              method: "POST",
            },

            // INTERNAL. Use Visit.reseps.destroyAll() instead.
            "prototype$__delete__reseps": {
              url: urlBase + "/visits/:id/reseps",
              method: "DELETE",
            },

            // INTERNAL. Use Visit.reseps.count() instead.
            "prototype$__count__reseps": {
              url: urlBase + "/visits/:id/reseps/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#create
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/visits",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#patchOrCreate
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/visits",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#replaceOrCreate
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/visits/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#upsertWithWhere
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/visits/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#exists
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/visits/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#findById
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/visits/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#replaceById
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/visits/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#find
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/visits",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#findOne
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/visits/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#updateAll
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/visits/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#deleteById
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/visits/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#count
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/visits/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#prototype$patchAttributes
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/visits/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#createChangeStream
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/visits/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Visit#createMany
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/visits",
              method: "POST",
            },

            // INTERNAL. Use History_praktek.visits.findById() instead.
            "::findById::History_praktek::visits": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/history_prakteks/:id/visits/:fk",
              method: "GET",
            },

            // INTERNAL. Use History_praktek.visits.destroyById() instead.
            "::destroyById::History_praktek::visits": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/history_prakteks/:id/visits/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use History_praktek.visits.updateById() instead.
            "::updateById::History_praktek::visits": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/history_prakteks/:id/visits/:fk",
              method: "PUT",
            },

            // INTERNAL. Use History_praktek.visits() instead.
            "::get::History_praktek::visits": {
              isArray: true,
              url: urlBase + "/history_prakteks/:id/visits",
              method: "GET",
            },

            // INTERNAL. Use History_praktek.visits.create() instead.
            "::create::History_praktek::visits": {
              url: urlBase + "/history_prakteks/:id/visits",
              method: "POST",
            },

            // INTERNAL. Use History_praktek.visits.createMany() instead.
            "::createMany::History_praktek::visits": {
              isArray: true,
              url: urlBase + "/history_prakteks/:id/visits",
              method: "POST",
            },

            // INTERNAL. Use History_praktek.visits.destroyAll() instead.
            "::delete::History_praktek::visits": {
              url: urlBase + "/history_prakteks/:id/visits",
              method: "DELETE",
            },

            // INTERNAL. Use History_praktek.visits.count() instead.
            "::count::History_praktek::visits": {
              url: urlBase + "/history_prakteks/:id/visits/count",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.visits.findById() instead.
            "::findById::Jadwal_praktek::visits": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/jadwal_prakteks/:id/visits/:fk",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.visits.destroyById() instead.
            "::destroyById::Jadwal_praktek::visits": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/jadwal_prakteks/:id/visits/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Jadwal_praktek.visits.updateById() instead.
            "::updateById::Jadwal_praktek::visits": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/jadwal_prakteks/:id/visits/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Jadwal_praktek.visits() instead.
            "::get::Jadwal_praktek::visits": {
              isArray: true,
              url: urlBase + "/jadwal_prakteks/:id/visits",
              method: "GET",
            },

            // INTERNAL. Use Jadwal_praktek.visits.create() instead.
            "::create::Jadwal_praktek::visits": {
              url: urlBase + "/jadwal_prakteks/:id/visits",
              method: "POST",
            },

            // INTERNAL. Use Jadwal_praktek.visits.createMany() instead.
            "::createMany::Jadwal_praktek::visits": {
              isArray: true,
              url: urlBase + "/jadwal_prakteks/:id/visits",
              method: "POST",
            },

            // INTERNAL. Use Jadwal_praktek.visits.destroyAll() instead.
            "::delete::Jadwal_praktek::visits": {
              url: urlBase + "/jadwal_prakteks/:id/visits",
              method: "DELETE",
            },

            // INTERNAL. Use Jadwal_praktek.visits.count() instead.
            "::count::Jadwal_praktek::visits": {
              url: urlBase + "/jadwal_prakteks/:id/visits/count",
              method: "GET",
            },

            // INTERNAL. Use Medrec.visit() instead.
            "::get::Medrec::visit": {
              url: urlBase + "/medrecs/:id/visit",
              method: "GET",
            },

            // INTERNAL. Use Resep.visit() instead.
            "::get::Resep::visit": {
              url: urlBase + "/reseps/:id/visit",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Visit#upsert
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Visit#updateOrCreate
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Visit#patchOrCreateWithWhere
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Visit#update
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Visit#destroyById
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Visit#removeById
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Visit#prototype$updateAttributes
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Visit` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Visit#modelName
        * @propertyOf lbServices.Visit
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Visit`.
        */
        R.modelName = "Visit";

    /**
     * @ngdoc object
     * @name lbServices.Visit.medrecs
     * @header lbServices.Visit.medrecs
     * @object
     * @description
     *
     * The object `Visit.medrecs` groups methods
     * manipulating `Medrec` instances related to `Visit`.
     *
     * Call {@link lbServices.Visit#medrecs Visit.medrecs()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Visit#medrecs
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Fetches hasOne relation medrecs.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R.medrecs = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::get::Visit::medrecs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit.medrecs#create
             * @methodOf lbServices.Visit.medrecs
             *
             * @description
             *
             * Creates a new instance in medrecs of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R.medrecs.create = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::create::Visit::medrecs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit.medrecs#createMany
             * @methodOf lbServices.Visit.medrecs
             *
             * @description
             *
             * Creates a new instance in medrecs of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R.medrecs.createMany = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::createMany::Visit::medrecs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit.medrecs#destroy
             * @methodOf lbServices.Visit.medrecs
             *
             * @description
             *
             * Deletes medrecs of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.medrecs.destroy = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::destroy::Visit::medrecs"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit.medrecs#update
             * @methodOf lbServices.Visit.medrecs
             *
             * @description
             *
             * Update medrecs of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Medrec` object.)
             * </em>
             */
        R.medrecs.update = function() {
          var TargetResource = $injector.get("Medrec");
          var action = TargetResource["::update::Visit::medrecs"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Visit.reseps
     * @header lbServices.Visit.reseps
     * @object
     * @description
     *
     * The object `Visit.reseps` groups methods
     * manipulating `Resep` instances related to `Visit`.
     *
     * Call {@link lbServices.Visit#reseps Visit.reseps()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Visit#reseps
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Queries reseps of visit.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
        R.reseps = function() {
          var TargetResource = $injector.get("Resep");
          var action = TargetResource["::get::Visit::reseps"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit.reseps#count
             * @methodOf lbServices.Visit.reseps
             *
             * @description
             *
             * Counts reseps of visit.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.reseps.count = function() {
          var TargetResource = $injector.get("Resep");
          var action = TargetResource["::count::Visit::reseps"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit.reseps#create
             * @methodOf lbServices.Visit.reseps
             *
             * @description
             *
             * Creates a new instance in reseps of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
        R.reseps.create = function() {
          var TargetResource = $injector.get("Resep");
          var action = TargetResource["::create::Visit::reseps"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit.reseps#createMany
             * @methodOf lbServices.Visit.reseps
             *
             * @description
             *
             * Creates a new instance in reseps of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
        R.reseps.createMany = function() {
          var TargetResource = $injector.get("Resep");
          var action = TargetResource["::createMany::Visit::reseps"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit.reseps#destroyAll
             * @methodOf lbServices.Visit.reseps
             *
             * @description
             *
             * Deletes all reseps of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.reseps.destroyAll = function() {
          var TargetResource = $injector.get("Resep");
          var action = TargetResource["::delete::Visit::reseps"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit.reseps#destroyById
             * @methodOf lbServices.Visit.reseps
             *
             * @description
             *
             * Delete a related item by id for reseps.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for reseps
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.reseps.destroyById = function() {
          var TargetResource = $injector.get("Resep");
          var action = TargetResource["::destroyById::Visit::reseps"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit.reseps#findById
             * @methodOf lbServices.Visit.reseps
             *
             * @description
             *
             * Find a related item by id for reseps.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for reseps
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
        R.reseps.findById = function() {
          var TargetResource = $injector.get("Resep");
          var action = TargetResource["::findById::Visit::reseps"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit.reseps#updateById
             * @methodOf lbServices.Visit.reseps
             *
             * @description
             *
             * Update a related item by id for reseps.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `fk` – `{*}` - Foreign key for reseps
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Resep` object.)
             * </em>
             */
        R.reseps.updateById = function() {
          var TargetResource = $injector.get("Resep");
          var action = TargetResource["::updateById::Visit::reseps"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit#dokter
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Fetches belongsTo relation dokter.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R.dokter = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::Visit::dokter"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit#jadwalPraktek
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Fetches belongsTo relation jadwalPraktek.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Jadwal_praktek` object.)
             * </em>
             */
        R.jadwalPraktek = function() {
          var TargetResource = $injector.get("Jadwal_praktek");
          var action = TargetResource["::get::Visit::jadwalPraktek"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit#perawat
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Fetches belongsTo relation perawat.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R.perawat = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::Visit::perawat"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit#pasien
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Fetches belongsTo relation pasien.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R.pasien = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::Visit::pasien"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit#historyPraktek
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Fetches belongsTo relation historyPraktek.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `History_praktek` object.)
             * </em>
             */
        R.historyPraktek = function() {
          var TargetResource = $injector.get("History_praktek");
          var action = TargetResource["::get::Visit::historyPraktek"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Visit#klinik
             * @methodOf lbServices.Visit
             *
             * @description
             *
             * Fetches belongsTo relation klinik.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - visit id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Klinik` object.)
             * </em>
             */
        R.klinik = function() {
          var TargetResource = $injector.get("Klinik");
          var action = TargetResource["::get::Visit::klinik"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Image_container
 * @header lbServices.Image_container
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Image_container` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Image_container",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/image_containers/:id",
          { 'id': '@id' },
          {

            /**
             * @ngdoc method
             * @name lbServices.Image_container#getContainers
             * @methodOf lbServices.Image_container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Image_container` object.)
             * </em>
             */
            "getContainers": {
              isArray: true,
              url: urlBase + "/image_containers",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Image_container#createContainer
             * @methodOf lbServices.Image_container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             * This method expects a subset of model properties as request parameters.
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Image_container` object.)
             * </em>
             */
            "createContainer": {
              url: urlBase + "/image_containers",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Image_container#destroyContainer
             * @methodOf lbServices.Image_container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `` – `{undefined=}` -
             */
            "destroyContainer": {
              url: urlBase + "/image_containers/:container",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Image_container#getContainer
             * @methodOf lbServices.Image_container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Image_container` object.)
             * </em>
             */
            "getContainer": {
              url: urlBase + "/image_containers/:container",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Image_container#getFiles
             * @methodOf lbServices.Image_container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Image_container` object.)
             * </em>
             */
            "getFiles": {
              isArray: true,
              url: urlBase + "/image_containers/:container/files",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Image_container#getFile
             * @methodOf lbServices.Image_container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string}` -
             *
             *  - `file` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Image_container` object.)
             * </em>
             */
            "getFile": {
              url: urlBase + "/image_containers/:container/files/:file",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Image_container#removeFile
             * @methodOf lbServices.Image_container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string}` -
             *
             *  - `file` – `{string}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `` – `{undefined=}` -
             */
            "removeFile": {
              url: urlBase + "/image_containers/:container/files/:file",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Image_container#upload
             * @methodOf lbServices.Image_container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string}` -
             *
             * @param {Object} postData Request data.
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `result` – `{object=}` -
             */
            "upload": {
              url: urlBase + "/image_containers/:container/upload",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Image_container#download
             * @methodOf lbServices.Image_container
             *
             * @description
             *
             * <em>
             * (The remote method definition does not provide any description.)
             * </em>
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `container` – `{string}` -
             *
             *  - `file` – `{string}` -
             *
             *  - `req` – `{object=}` -
             *
             *  - `res` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
            "download": {
              url: urlBase + "/image_containers/:container/download/:file",
              method: "GET",
            },
          }
        );




        /**
        * @ngdoc property
        * @name lbServices.Image_container#modelName
        * @propertyOf lbServices.Image_container
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Image_container`.
        */
        R.modelName = "Image_container";



        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Pembayaran
 * @header lbServices.Pembayaran
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Pembayaran` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Pembayaran",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/pembayarans/:id",
          { 'id': '@id' },
          {

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#create
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/pembayarans",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#patchOrCreate
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/pembayarans",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#replaceOrCreate
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/pembayarans/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#upsertWithWhere
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/pembayarans/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#exists
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/pembayarans/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#findById
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/pembayarans/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#replaceById
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/pembayarans/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#find
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/pembayarans",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#findOne
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/pembayarans/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#updateAll
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/pembayarans/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#deleteById
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/pembayarans/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#count
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/pembayarans/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#prototype$patchAttributes
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - pembayaran id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/pembayarans/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#createChangeStream
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/pembayarans/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#createMany
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/pembayarans",
              method: "POST",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#upsert
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#updateOrCreate
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#patchOrCreateWithWhere
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#update
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#destroyById
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#removeById
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Pembayaran#prototype$updateAttributes
             * @methodOf lbServices.Pembayaran
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - pembayaran id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Pembayaran` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Pembayaran#modelName
        * @propertyOf lbServices.Pembayaran
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Pembayaran`.
        */
        R.modelName = "Pembayaran";



        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Provinsi
 * @header lbServices.Provinsi
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Provinsi` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Provinsi",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/provinsis/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Provinsi.kabupatenKota.findById() instead.
            "prototype$__findById__kabupatenKota": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/provinsis/:id/kabupatenKota/:fk",
              method: "GET",
            },

            // INTERNAL. Use Provinsi.kabupatenKota.destroyById() instead.
            "prototype$__destroyById__kabupatenKota": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/provinsis/:id/kabupatenKota/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Provinsi.kabupatenKota.updateById() instead.
            "prototype$__updateById__kabupatenKota": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/provinsis/:id/kabupatenKota/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Provinsi.kabupatenKota() instead.
            "prototype$__get__kabupatenKota": {
              isArray: true,
              url: urlBase + "/provinsis/:id/kabupatenKota",
              method: "GET",
            },

            // INTERNAL. Use Provinsi.kabupatenKota.create() instead.
            "prototype$__create__kabupatenKota": {
              url: urlBase + "/provinsis/:id/kabupatenKota",
              method: "POST",
            },

            // INTERNAL. Use Provinsi.kabupatenKota.destroyAll() instead.
            "prototype$__delete__kabupatenKota": {
              url: urlBase + "/provinsis/:id/kabupatenKota",
              method: "DELETE",
            },

            // INTERNAL. Use Provinsi.kabupatenKota.count() instead.
            "prototype$__count__kabupatenKota": {
              url: urlBase + "/provinsis/:id/kabupatenKota/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#create
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/provinsis",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#patchOrCreate
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/provinsis",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#replaceOrCreate
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/provinsis/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#upsertWithWhere
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/provinsis/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#exists
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/provinsis/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#findById
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/provinsis/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#replaceById
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/provinsis/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#find
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/provinsis",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#findOne
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/provinsis/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#updateAll
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/provinsis/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#deleteById
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/provinsis/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#count
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/provinsis/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#prototype$patchAttributes
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - provinsi id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/provinsis/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#createChangeStream
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/provinsis/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#createMany
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/provinsis",
              method: "POST",
            },

            // INTERNAL. Use Kabupaten_kota.provinsi() instead.
            "::get::Kabupaten_kota::provinsi": {
              url: urlBase + "/kabupaten_kota/:id/provinsi",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.provinsi() instead.
            "::get::Detail_pasien::provinsi": {
              url: urlBase + "/detail_pasiens/:id/provinsi",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.pj_provinsi() instead.
            "::get::Detail_pasien::pj_provinsi": {
              url: urlBase + "/detail_pasiens/:id/pj_provinsi",
              method: "GET",
            },

            // INTERNAL. Use Penjamin_pasien.provinsi() instead.
            "::get::Penjamin_pasien::provinsi": {
              url: urlBase + "/penjamin_pasiens/:id/provinsi",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Provinsi#upsert
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#updateOrCreate
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#patchOrCreateWithWhere
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#update
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#destroyById
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#removeById
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Provinsi#prototype$updateAttributes
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - provinsi id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Provinsi#modelName
        * @propertyOf lbServices.Provinsi
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Provinsi`.
        */
        R.modelName = "Provinsi";

    /**
     * @ngdoc object
     * @name lbServices.Provinsi.kabupatenKota
     * @header lbServices.Provinsi.kabupatenKota
     * @object
     * @description
     *
     * The object `Provinsi.kabupatenKota` groups methods
     * manipulating `Kabupaten_kota` instances related to `Provinsi`.
     *
     * Call {@link lbServices.Provinsi#kabupatenKota Provinsi.kabupatenKota()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Provinsi#kabupatenKota
             * @methodOf lbServices.Provinsi
             *
             * @description
             *
             * Queries kabupatenKota of provinsi.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - provinsi id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R.kabupatenKota = function() {
          var TargetResource = $injector.get("Kabupaten_kota");
          var action = TargetResource["::get::Provinsi::kabupatenKota"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Provinsi.kabupatenKota#count
             * @methodOf lbServices.Provinsi.kabupatenKota
             *
             * @description
             *
             * Counts kabupatenKota of provinsi.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - provinsi id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.kabupatenKota.count = function() {
          var TargetResource = $injector.get("Kabupaten_kota");
          var action = TargetResource["::count::Provinsi::kabupatenKota"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Provinsi.kabupatenKota#create
             * @methodOf lbServices.Provinsi.kabupatenKota
             *
             * @description
             *
             * Creates a new instance in kabupatenKota of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - provinsi id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R.kabupatenKota.create = function() {
          var TargetResource = $injector.get("Kabupaten_kota");
          var action = TargetResource["::create::Provinsi::kabupatenKota"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Provinsi.kabupatenKota#createMany
             * @methodOf lbServices.Provinsi.kabupatenKota
             *
             * @description
             *
             * Creates a new instance in kabupatenKota of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - provinsi id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R.kabupatenKota.createMany = function() {
          var TargetResource = $injector.get("Kabupaten_kota");
          var action = TargetResource["::createMany::Provinsi::kabupatenKota"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Provinsi.kabupatenKota#destroyAll
             * @methodOf lbServices.Provinsi.kabupatenKota
             *
             * @description
             *
             * Deletes all kabupatenKota of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - provinsi id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.kabupatenKota.destroyAll = function() {
          var TargetResource = $injector.get("Kabupaten_kota");
          var action = TargetResource["::delete::Provinsi::kabupatenKota"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Provinsi.kabupatenKota#destroyById
             * @methodOf lbServices.Provinsi.kabupatenKota
             *
             * @description
             *
             * Delete a related item by id for kabupatenKota.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - provinsi id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for kabupatenKota
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.kabupatenKota.destroyById = function() {
          var TargetResource = $injector.get("Kabupaten_kota");
          var action = TargetResource["::destroyById::Provinsi::kabupatenKota"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Provinsi.kabupatenKota#findById
             * @methodOf lbServices.Provinsi.kabupatenKota
             *
             * @description
             *
             * Find a related item by id for kabupatenKota.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - provinsi id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for kabupatenKota
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R.kabupatenKota.findById = function() {
          var TargetResource = $injector.get("Kabupaten_kota");
          var action = TargetResource["::findById::Provinsi::kabupatenKota"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Provinsi.kabupatenKota#updateById
             * @methodOf lbServices.Provinsi.kabupatenKota
             *
             * @description
             *
             * Update a related item by id for kabupatenKota.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - provinsi id
             *
             *  - `fk` – `{*}` - Foreign key for kabupatenKota
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R.kabupatenKota.updateById = function() {
          var TargetResource = $injector.get("Kabupaten_kota");
          var action = TargetResource["::updateById::Provinsi::kabupatenKota"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Kecamatan
 * @header lbServices.Kecamatan
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Kecamatan` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Kecamatan",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/kecamatans/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Kecamatan.kabupatenKota() instead.
            "prototype$__get__kabupatenKota": {
              url: urlBase + "/kecamatans/:id/kabupatenKota",
              method: "GET",
            },

            // INTERNAL. Use Kecamatan.kelurahans.findById() instead.
            "prototype$__findById__kelurahans": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kecamatans/:id/kelurahans/:fk",
              method: "GET",
            },

            // INTERNAL. Use Kecamatan.kelurahans.destroyById() instead.
            "prototype$__destroyById__kelurahans": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kecamatans/:id/kelurahans/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Kecamatan.kelurahans.updateById() instead.
            "prototype$__updateById__kelurahans": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kecamatans/:id/kelurahans/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Kecamatan.kelurahans() instead.
            "prototype$__get__kelurahans": {
              isArray: true,
              url: urlBase + "/kecamatans/:id/kelurahans",
              method: "GET",
            },

            // INTERNAL. Use Kecamatan.kelurahans.create() instead.
            "prototype$__create__kelurahans": {
              url: urlBase + "/kecamatans/:id/kelurahans",
              method: "POST",
            },

            // INTERNAL. Use Kecamatan.kelurahans.destroyAll() instead.
            "prototype$__delete__kelurahans": {
              url: urlBase + "/kecamatans/:id/kelurahans",
              method: "DELETE",
            },

            // INTERNAL. Use Kecamatan.kelurahans.count() instead.
            "prototype$__count__kelurahans": {
              url: urlBase + "/kecamatans/:id/kelurahans/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#create
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/kecamatans",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#patchOrCreate
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/kecamatans",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#replaceOrCreate
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/kecamatans/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#upsertWithWhere
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/kecamatans/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#exists
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/kecamatans/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#findById
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/kecamatans/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#replaceById
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/kecamatans/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#find
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/kecamatans",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#findOne
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/kecamatans/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#updateAll
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/kecamatans/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#deleteById
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/kecamatans/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#count
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/kecamatans/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#prototype$patchAttributes
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kecamatan id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/kecamatans/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#createChangeStream
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/kecamatans/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#createMany
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/kecamatans",
              method: "POST",
            },

            // INTERNAL. Use Kelurahan.kecamatan() instead.
            "::get::Kelurahan::kecamatan": {
              url: urlBase + "/kelurahans/:id/kecamatan",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.kecamatan() instead.
            "::get::Detail_pasien::kecamatan": {
              url: urlBase + "/detail_pasiens/:id/kecamatan",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.pj_kecamatan() instead.
            "::get::Detail_pasien::pj_kecamatan": {
              url: urlBase + "/detail_pasiens/:id/pj_kecamatan",
              method: "GET",
            },

            // INTERNAL. Use Penjamin_pasien.kecamatan() instead.
            "::get::Penjamin_pasien::kecamatan": {
              url: urlBase + "/penjamin_pasiens/:id/kecamatan",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#upsert
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#updateOrCreate
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#patchOrCreateWithWhere
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#update
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#destroyById
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#removeById
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#prototype$updateAttributes
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kecamatan id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Kecamatan#modelName
        * @propertyOf lbServices.Kecamatan
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Kecamatan`.
        */
        R.modelName = "Kecamatan";


            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#kabupatenKota
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Fetches belongsTo relation kabupatenKota.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kecamatan id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R.kabupatenKota = function() {
          var TargetResource = $injector.get("Kabupaten_kota");
          var action = TargetResource["::get::Kecamatan::kabupatenKota"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Kecamatan.kelurahans
     * @header lbServices.Kecamatan.kelurahans
     * @object
     * @description
     *
     * The object `Kecamatan.kelurahans` groups methods
     * manipulating `Kelurahan` instances related to `Kecamatan`.
     *
     * Call {@link lbServices.Kecamatan#kelurahans Kecamatan.kelurahans()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Kecamatan#kelurahans
             * @methodOf lbServices.Kecamatan
             *
             * @description
             *
             * Queries kelurahans of kecamatan.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kecamatan id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.kelurahans = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::get::Kecamatan::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan.kelurahans#count
             * @methodOf lbServices.Kecamatan.kelurahans
             *
             * @description
             *
             * Counts kelurahans of kecamatan.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kecamatan id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.kelurahans.count = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::count::Kecamatan::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan.kelurahans#create
             * @methodOf lbServices.Kecamatan.kelurahans
             *
             * @description
             *
             * Creates a new instance in kelurahans of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kecamatan id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.kelurahans.create = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::create::Kecamatan::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan.kelurahans#createMany
             * @methodOf lbServices.Kecamatan.kelurahans
             *
             * @description
             *
             * Creates a new instance in kelurahans of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kecamatan id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.kelurahans.createMany = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::createMany::Kecamatan::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan.kelurahans#destroyAll
             * @methodOf lbServices.Kecamatan.kelurahans
             *
             * @description
             *
             * Deletes all kelurahans of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kecamatan id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.kelurahans.destroyAll = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::delete::Kecamatan::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan.kelurahans#destroyById
             * @methodOf lbServices.Kecamatan.kelurahans
             *
             * @description
             *
             * Delete a related item by id for kelurahans.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kecamatan id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for kelurahans
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.kelurahans.destroyById = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::destroyById::Kecamatan::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan.kelurahans#findById
             * @methodOf lbServices.Kecamatan.kelurahans
             *
             * @description
             *
             * Find a related item by id for kelurahans.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kecamatan id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for kelurahans
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.kelurahans.findById = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::findById::Kecamatan::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kecamatan.kelurahans#updateById
             * @methodOf lbServices.Kecamatan.kelurahans
             *
             * @description
             *
             * Update a related item by id for kelurahans.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kecamatan id
             *
             *  - `fk` – `{*}` - Foreign key for kelurahans
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.kelurahans.updateById = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::updateById::Kecamatan::kelurahans"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Kabupaten_kota
 * @header lbServices.Kabupaten_kota
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Kabupaten_kota` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Kabupaten_kota",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/kabupaten_kota/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Kabupaten_kota.provinsi() instead.
            "prototype$__get__provinsi": {
              url: urlBase + "/kabupaten_kota/:id/provinsi",
              method: "GET",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.findById() instead.
            "prototype$__findById__kelurahans": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kabupaten_kota/:id/kelurahans/:fk",
              method: "GET",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.destroyById() instead.
            "prototype$__destroyById__kelurahans": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kabupaten_kota/:id/kelurahans/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.updateById() instead.
            "prototype$__updateById__kelurahans": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kabupaten_kota/:id/kelurahans/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans() instead.
            "prototype$__get__kelurahans": {
              isArray: true,
              url: urlBase + "/kabupaten_kota/:id/kelurahans",
              method: "GET",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.create() instead.
            "prototype$__create__kelurahans": {
              url: urlBase + "/kabupaten_kota/:id/kelurahans",
              method: "POST",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.destroyAll() instead.
            "prototype$__delete__kelurahans": {
              url: urlBase + "/kabupaten_kota/:id/kelurahans",
              method: "DELETE",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.count() instead.
            "prototype$__count__kelurahans": {
              url: urlBase + "/kabupaten_kota/:id/kelurahans/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#create
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/kabupaten_kota",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#patchOrCreate
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/kabupaten_kota",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#replaceOrCreate
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/kabupaten_kota/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#upsertWithWhere
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/kabupaten_kota/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#exists
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/kabupaten_kota/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#findById
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/kabupaten_kota/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#replaceById
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/kabupaten_kota/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#find
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/kabupaten_kota",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#findOne
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/kabupaten_kota/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#updateAll
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/kabupaten_kota/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#deleteById
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/kabupaten_kota/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#count
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/kabupaten_kota/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#prototype$patchAttributes
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kabupaten_kota id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/kabupaten_kota/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#createChangeStream
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/kabupaten_kota/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#createMany
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/kabupaten_kota",
              method: "POST",
            },

            // INTERNAL. Use Provinsi.kabupatenKota.findById() instead.
            "::findById::Provinsi::kabupatenKota": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/provinsis/:id/kabupatenKota/:fk",
              method: "GET",
            },

            // INTERNAL. Use Provinsi.kabupatenKota.destroyById() instead.
            "::destroyById::Provinsi::kabupatenKota": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/provinsis/:id/kabupatenKota/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Provinsi.kabupatenKota.updateById() instead.
            "::updateById::Provinsi::kabupatenKota": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/provinsis/:id/kabupatenKota/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Provinsi.kabupatenKota() instead.
            "::get::Provinsi::kabupatenKota": {
              isArray: true,
              url: urlBase + "/provinsis/:id/kabupatenKota",
              method: "GET",
            },

            // INTERNAL. Use Provinsi.kabupatenKota.create() instead.
            "::create::Provinsi::kabupatenKota": {
              url: urlBase + "/provinsis/:id/kabupatenKota",
              method: "POST",
            },

            // INTERNAL. Use Provinsi.kabupatenKota.createMany() instead.
            "::createMany::Provinsi::kabupatenKota": {
              isArray: true,
              url: urlBase + "/provinsis/:id/kabupatenKota",
              method: "POST",
            },

            // INTERNAL. Use Provinsi.kabupatenKota.destroyAll() instead.
            "::delete::Provinsi::kabupatenKota": {
              url: urlBase + "/provinsis/:id/kabupatenKota",
              method: "DELETE",
            },

            // INTERNAL. Use Provinsi.kabupatenKota.count() instead.
            "::count::Provinsi::kabupatenKota": {
              url: urlBase + "/provinsis/:id/kabupatenKota/count",
              method: "GET",
            },

            // INTERNAL. Use Kecamatan.kabupatenKota() instead.
            "::get::Kecamatan::kabupatenKota": {
              url: urlBase + "/kecamatans/:id/kabupatenKota",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.kabupaten_kota() instead.
            "::get::Detail_pasien::kabupaten_kota": {
              url: urlBase + "/detail_pasiens/:id/kabupaten_kota",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.pj_kabupaten_kota() instead.
            "::get::Detail_pasien::pj_kabupaten_kota": {
              url: urlBase + "/detail_pasiens/:id/pj_kabupaten_kota",
              method: "GET",
            },

            // INTERNAL. Use Penjamin_pasien.kabupaten_kota() instead.
            "::get::Penjamin_pasien::kabupaten_kota": {
              url: urlBase + "/penjamin_pasiens/:id/kabupaten_kota",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#upsert
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#updateOrCreate
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#patchOrCreateWithWhere
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#update
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#destroyById
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#removeById
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#prototype$updateAttributes
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kabupaten_kota id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Kabupaten_kota#modelName
        * @propertyOf lbServices.Kabupaten_kota
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Kabupaten_kota`.
        */
        R.modelName = "Kabupaten_kota";


            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#provinsi
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Fetches belongsTo relation provinsi.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kabupaten_kota id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
        R.provinsi = function() {
          var TargetResource = $injector.get("Provinsi");
          var action = TargetResource["::get::Kabupaten_kota::provinsi"];
          return action.apply(R, arguments);
        };
    /**
     * @ngdoc object
     * @name lbServices.Kabupaten_kota.kelurahans
     * @header lbServices.Kabupaten_kota.kelurahans
     * @object
     * @description
     *
     * The object `Kabupaten_kota.kelurahans` groups methods
     * manipulating `Kelurahan` instances related to `Kabupaten_kota`.
     *
     * Call {@link lbServices.Kabupaten_kota#kelurahans Kabupaten_kota.kelurahans()}
     * to query all related instances.
     */


            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota#kelurahans
             * @methodOf lbServices.Kabupaten_kota
             *
             * @description
             *
             * Queries kelurahans of kabupaten_kota.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kabupaten_kota id
             *
             *  - `options` – `{object=}` -
             *
             *  - `filter` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.kelurahans = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::get::Kabupaten_kota::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota.kelurahans#count
             * @methodOf lbServices.Kabupaten_kota.kelurahans
             *
             * @description
             *
             * Counts kelurahans of kabupaten_kota.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kabupaten_kota id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
        R.kelurahans.count = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::count::Kabupaten_kota::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota.kelurahans#create
             * @methodOf lbServices.Kabupaten_kota.kelurahans
             *
             * @description
             *
             * Creates a new instance in kelurahans of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kabupaten_kota id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.kelurahans.create = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::create::Kabupaten_kota::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota.kelurahans#createMany
             * @methodOf lbServices.Kabupaten_kota.kelurahans
             *
             * @description
             *
             * Creates a new instance in kelurahans of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kabupaten_kota id
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.kelurahans.createMany = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::createMany::Kabupaten_kota::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota.kelurahans#destroyAll
             * @methodOf lbServices.Kabupaten_kota.kelurahans
             *
             * @description
             *
             * Deletes all kelurahans of this model.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kabupaten_kota id
             *
             *  - `options` – `{object=}` -
             *
             *  - `where` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.kelurahans.destroyAll = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::delete::Kabupaten_kota::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota.kelurahans#destroyById
             * @methodOf lbServices.Kabupaten_kota.kelurahans
             *
             * @description
             *
             * Delete a related item by id for kelurahans.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kabupaten_kota id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for kelurahans
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * This method returns no data.
             */
        R.kelurahans.destroyById = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::destroyById::Kabupaten_kota::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota.kelurahans#findById
             * @methodOf lbServices.Kabupaten_kota.kelurahans
             *
             * @description
             *
             * Find a related item by id for kelurahans.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kabupaten_kota id
             *
             *  - `options` – `{object=}` -
             *
             *  - `fk` – `{*}` - Foreign key for kelurahans
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.kelurahans.findById = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::findById::Kabupaten_kota::kelurahans"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Kabupaten_kota.kelurahans#updateById
             * @methodOf lbServices.Kabupaten_kota.kelurahans
             *
             * @description
             *
             * Update a related item by id for kelurahans.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kabupaten_kota id
             *
             *  - `fk` – `{*}` - Foreign key for kelurahans
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.kelurahans.updateById = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::updateById::Kabupaten_kota::kelurahans"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Kelurahan
 * @header lbServices.Kelurahan
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Kelurahan` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Kelurahan",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/kelurahans/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Kelurahan.kecamatan() instead.
            "prototype$__get__kecamatan": {
              url: urlBase + "/kelurahans/:id/kecamatan",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#create
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/kelurahans",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#patchOrCreate
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/kelurahans",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#replaceOrCreate
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/kelurahans/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#upsertWithWhere
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/kelurahans/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#exists
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/kelurahans/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#findById
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/kelurahans/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#replaceById
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/kelurahans/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#find
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/kelurahans",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#findOne
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/kelurahans/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#updateAll
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/kelurahans/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#deleteById
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/kelurahans/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#count
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/kelurahans/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#prototype$patchAttributes
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kelurahan id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/kelurahans/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#createChangeStream
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/kelurahans/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#createMany
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/kelurahans",
              method: "POST",
            },

            // INTERNAL. Use Kecamatan.kelurahans.findById() instead.
            "::findById::Kecamatan::kelurahans": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kecamatans/:id/kelurahans/:fk",
              method: "GET",
            },

            // INTERNAL. Use Kecamatan.kelurahans.destroyById() instead.
            "::destroyById::Kecamatan::kelurahans": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kecamatans/:id/kelurahans/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Kecamatan.kelurahans.updateById() instead.
            "::updateById::Kecamatan::kelurahans": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kecamatans/:id/kelurahans/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Kecamatan.kelurahans() instead.
            "::get::Kecamatan::kelurahans": {
              isArray: true,
              url: urlBase + "/kecamatans/:id/kelurahans",
              method: "GET",
            },

            // INTERNAL. Use Kecamatan.kelurahans.create() instead.
            "::create::Kecamatan::kelurahans": {
              url: urlBase + "/kecamatans/:id/kelurahans",
              method: "POST",
            },

            // INTERNAL. Use Kecamatan.kelurahans.createMany() instead.
            "::createMany::Kecamatan::kelurahans": {
              isArray: true,
              url: urlBase + "/kecamatans/:id/kelurahans",
              method: "POST",
            },

            // INTERNAL. Use Kecamatan.kelurahans.destroyAll() instead.
            "::delete::Kecamatan::kelurahans": {
              url: urlBase + "/kecamatans/:id/kelurahans",
              method: "DELETE",
            },

            // INTERNAL. Use Kecamatan.kelurahans.count() instead.
            "::count::Kecamatan::kelurahans": {
              url: urlBase + "/kecamatans/:id/kelurahans/count",
              method: "GET",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.findById() instead.
            "::findById::Kabupaten_kota::kelurahans": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kabupaten_kota/:id/kelurahans/:fk",
              method: "GET",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.destroyById() instead.
            "::destroyById::Kabupaten_kota::kelurahans": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kabupaten_kota/:id/kelurahans/:fk",
              method: "DELETE",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.updateById() instead.
            "::updateById::Kabupaten_kota::kelurahans": {
              params: {
                'fk': '@fk',
              },
              url: urlBase + "/kabupaten_kota/:id/kelurahans/:fk",
              method: "PUT",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans() instead.
            "::get::Kabupaten_kota::kelurahans": {
              isArray: true,
              url: urlBase + "/kabupaten_kota/:id/kelurahans",
              method: "GET",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.create() instead.
            "::create::Kabupaten_kota::kelurahans": {
              url: urlBase + "/kabupaten_kota/:id/kelurahans",
              method: "POST",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.createMany() instead.
            "::createMany::Kabupaten_kota::kelurahans": {
              isArray: true,
              url: urlBase + "/kabupaten_kota/:id/kelurahans",
              method: "POST",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.destroyAll() instead.
            "::delete::Kabupaten_kota::kelurahans": {
              url: urlBase + "/kabupaten_kota/:id/kelurahans",
              method: "DELETE",
            },

            // INTERNAL. Use Kabupaten_kota.kelurahans.count() instead.
            "::count::Kabupaten_kota::kelurahans": {
              url: urlBase + "/kabupaten_kota/:id/kelurahans/count",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.kelurahan() instead.
            "::get::Detail_pasien::kelurahan": {
              url: urlBase + "/detail_pasiens/:id/kelurahan",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.pj_kelurahan() instead.
            "::get::Detail_pasien::pj_kelurahan": {
              url: urlBase + "/detail_pasiens/:id/pj_kelurahan",
              method: "GET",
            },

            // INTERNAL. Use Penjamin_pasien.kelurahan() instead.
            "::get::Penjamin_pasien::kelurahan": {
              url: urlBase + "/penjamin_pasiens/:id/kelurahan",
              method: "GET",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#upsert
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#updateOrCreate
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#patchOrCreateWithWhere
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#update
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#destroyById
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#removeById
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#prototype$updateAttributes
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kelurahan id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Kelurahan#modelName
        * @propertyOf lbServices.Kelurahan
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Kelurahan`.
        */
        R.modelName = "Kelurahan";


            /**
             * @ngdoc method
             * @name lbServices.Kelurahan#kecamatan
             * @methodOf lbServices.Kelurahan
             *
             * @description
             *
             * Fetches belongsTo relation kecamatan.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - kelurahan id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
        R.kecamatan = function() {
          var TargetResource = $injector.get("Kecamatan");
          var action = TargetResource["::get::Kelurahan::kecamatan"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Generic_param
 * @header lbServices.Generic_param
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Generic_param` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Generic_param",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/generic_params/:id",
          { 'id': '@id' },
          {

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#create
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/generic_params",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#patchOrCreate
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/generic_params",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#replaceOrCreate
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/generic_params/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#upsertWithWhere
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/generic_params/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#exists
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/generic_params/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#findById
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/generic_params/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#replaceById
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/generic_params/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#find
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/generic_params",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#findOne
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/generic_params/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#updateAll
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/generic_params/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#deleteById
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/generic_params/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#count
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/generic_params/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#prototype$patchAttributes
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - generic_param id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/generic_params/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#createChangeStream
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/generic_params/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#createMany
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/generic_params",
              method: "POST",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Generic_param#upsert
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#updateOrCreate
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#patchOrCreateWithWhere
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#update
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#destroyById
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#removeById
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Generic_param#prototype$updateAttributes
             * @methodOf lbServices.Generic_param
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - generic_param id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Generic_param` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Generic_param#modelName
        * @propertyOf lbServices.Generic_param
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Generic_param`.
        */
        R.modelName = "Generic_param";



        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Katalog_jasa
 * @header lbServices.Katalog_jasa
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Katalog_jasa` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Katalog_jasa",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/katalog_jasas/:id",
          { 'id': '@id' },
          {

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#create
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/katalog_jasas",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#patchOrCreate
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/katalog_jasas",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#replaceOrCreate
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/katalog_jasas/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#upsertWithWhere
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/katalog_jasas/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#exists
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/katalog_jasas/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#findById
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/katalog_jasas/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#replaceById
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/katalog_jasas/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#find
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/katalog_jasas",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#findOne
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/katalog_jasas/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#updateAll
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/katalog_jasas/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#deleteById
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/katalog_jasas/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#count
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/katalog_jasas/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#prototype$patchAttributes
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - katalog_jasa id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/katalog_jasas/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#createChangeStream
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/katalog_jasas/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#createMany
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/katalog_jasas",
              method: "POST",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#upsert
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#updateOrCreate
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#patchOrCreateWithWhere
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#update
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#destroyById
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#removeById
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Katalog_jasa#prototype$updateAttributes
             * @methodOf lbServices.Katalog_jasa
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - katalog_jasa id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Katalog_jasa` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Katalog_jasa#modelName
        * @propertyOf lbServices.Katalog_jasa
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Katalog_jasa`.
        */
        R.modelName = "Katalog_jasa";



        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Stock
 * @header lbServices.Stock
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Stock` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Stock",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/stocks/:id",
          { 'id': '@id' },
          {

            /**
             * @ngdoc method
             * @name lbServices.Stock#create
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/stocks",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#patchOrCreate
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/stocks",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#replaceOrCreate
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/stocks/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#upsertWithWhere
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/stocks/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#exists
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/stocks/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#findById
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/stocks/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#replaceById
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/stocks/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#find
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/stocks",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#findOne
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/stocks/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#updateAll
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/stocks/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#deleteById
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/stocks/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#count
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/stocks/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#prototype$patchAttributes
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - stock id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/stocks/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#createChangeStream
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/stocks/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Stock#createMany
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/stocks",
              method: "POST",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Stock#upsert
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Stock#updateOrCreate
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Stock#patchOrCreateWithWhere
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Stock#update
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Stock#destroyById
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Stock#removeById
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Stock#prototype$updateAttributes
             * @methodOf lbServices.Stock
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - stock id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Stock` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Stock#modelName
        * @propertyOf lbServices.Stock
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Stock`.
        */
        R.modelName = "Stock";



        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Detail_pasien
 * @header lbServices.Detail_pasien
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Detail_pasien` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Detail_pasien",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/detail_pasiens/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Detail_pasien.pasien() instead.
            "prototype$__get__pasien": {
              url: urlBase + "/detail_pasiens/:id/pasien",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.kecamatan() instead.
            "prototype$__get__kecamatan": {
              url: urlBase + "/detail_pasiens/:id/kecamatan",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.kelurahan() instead.
            "prototype$__get__kelurahan": {
              url: urlBase + "/detail_pasiens/:id/kelurahan",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.kabupaten_kota() instead.
            "prototype$__get__kabupaten_kota": {
              url: urlBase + "/detail_pasiens/:id/kabupaten_kota",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.provinsi() instead.
            "prototype$__get__provinsi": {
              url: urlBase + "/detail_pasiens/:id/provinsi",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.pj_kecamatan() instead.
            "prototype$__get__pj_kecamatan": {
              url: urlBase + "/detail_pasiens/:id/pj_kecamatan",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.pj_kelurahan() instead.
            "prototype$__get__pj_kelurahan": {
              url: urlBase + "/detail_pasiens/:id/pj_kelurahan",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.pj_kabupaten_kota() instead.
            "prototype$__get__pj_kabupaten_kota": {
              url: urlBase + "/detail_pasiens/:id/pj_kabupaten_kota",
              method: "GET",
            },

            // INTERNAL. Use Detail_pasien.pj_provinsi() instead.
            "prototype$__get__pj_provinsi": {
              url: urlBase + "/detail_pasiens/:id/pj_provinsi",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#create
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/detail_pasiens",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#patchOrCreate
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/detail_pasiens",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#replaceOrCreate
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/detail_pasiens/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#upsertWithWhere
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/detail_pasiens/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#exists
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/detail_pasiens/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#findById
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/detail_pasiens/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#replaceById
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/detail_pasiens/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#find
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/detail_pasiens",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#findOne
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/detail_pasiens/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#updateAll
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/detail_pasiens/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#deleteById
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/detail_pasiens/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#count
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/detail_pasiens/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#prototype$patchAttributes
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - detail_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/detail_pasiens/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#createChangeStream
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/detail_pasiens/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#createMany
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/detail_pasiens",
              method: "POST",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#upsert
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#updateOrCreate
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#patchOrCreateWithWhere
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#update
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#destroyById
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#removeById
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#prototype$updateAttributes
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - detail_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Detail_pasien` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Detail_pasien#modelName
        * @propertyOf lbServices.Detail_pasien
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Detail_pasien`.
        */
        R.modelName = "Detail_pasien";


            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#pasien
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Fetches belongsTo relation pasien.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - detail_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R.pasien = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::Detail_pasien::pasien"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#kecamatan
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Fetches belongsTo relation kecamatan.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - detail_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
        R.kecamatan = function() {
          var TargetResource = $injector.get("Kecamatan");
          var action = TargetResource["::get::Detail_pasien::kecamatan"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#kelurahan
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Fetches belongsTo relation kelurahan.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - detail_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.kelurahan = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::get::Detail_pasien::kelurahan"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#kabupaten_kota
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Fetches belongsTo relation kabupaten_kota.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - detail_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R.kabupaten_kota = function() {
          var TargetResource = $injector.get("Kabupaten_kota");
          var action = TargetResource["::get::Detail_pasien::kabupaten_kota"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#provinsi
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Fetches belongsTo relation provinsi.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - detail_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
        R.provinsi = function() {
          var TargetResource = $injector.get("Provinsi");
          var action = TargetResource["::get::Detail_pasien::provinsi"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#pj_kecamatan
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Fetches belongsTo relation pj_kecamatan.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - detail_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
        R.pj_kecamatan = function() {
          var TargetResource = $injector.get("Kecamatan");
          var action = TargetResource["::get::Detail_pasien::pj_kecamatan"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#pj_kelurahan
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Fetches belongsTo relation pj_kelurahan.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - detail_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.pj_kelurahan = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::get::Detail_pasien::pj_kelurahan"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#pj_kabupaten_kota
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Fetches belongsTo relation pj_kabupaten_kota.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - detail_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R.pj_kabupaten_kota = function() {
          var TargetResource = $injector.get("Kabupaten_kota");
          var action = TargetResource["::get::Detail_pasien::pj_kabupaten_kota"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Detail_pasien#pj_provinsi
             * @methodOf lbServices.Detail_pasien
             *
             * @description
             *
             * Fetches belongsTo relation pj_provinsi.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - detail_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
        R.pj_provinsi = function() {
          var TargetResource = $injector.get("Provinsi");
          var action = TargetResource["::get::Detail_pasien::pj_provinsi"];
          return action.apply(R, arguments);
        };


        return R;
      }]);

/**
 * @ngdoc object
 * @name lbServices.Penjamin_pasien
 * @header lbServices.Penjamin_pasien
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Penjamin_pasien` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
  module.factory(
    "Penjamin_pasien",
    [
      'LoopBackResource', 'LoopBackAuth', '$injector', '$q',
      function(LoopBackResource, LoopBackAuth, $injector, $q) {
        var R = LoopBackResource(
        urlBase + "/penjamin_pasiens/:id",
          { 'id': '@id' },
          {

            // INTERNAL. Use Penjamin_pasien.pasien() instead.
            "prototype$__get__pasien": {
              url: urlBase + "/penjamin_pasiens/:id/pasien",
              method: "GET",
            },

            // INTERNAL. Use Penjamin_pasien.kecamatan() instead.
            "prototype$__get__kecamatan": {
              url: urlBase + "/penjamin_pasiens/:id/kecamatan",
              method: "GET",
            },

            // INTERNAL. Use Penjamin_pasien.kelurahan() instead.
            "prototype$__get__kelurahan": {
              url: urlBase + "/penjamin_pasiens/:id/kelurahan",
              method: "GET",
            },

            // INTERNAL. Use Penjamin_pasien.kabupaten_kota() instead.
            "prototype$__get__kabupaten_kota": {
              url: urlBase + "/penjamin_pasiens/:id/kabupaten_kota",
              method: "GET",
            },

            // INTERNAL. Use Penjamin_pasien.provinsi() instead.
            "prototype$__get__provinsi": {
              url: urlBase + "/penjamin_pasiens/:id/provinsi",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#create
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
            "create": {
              url: urlBase + "/penjamin_pasiens",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#patchOrCreate
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
            "patchOrCreate": {
              url: urlBase + "/penjamin_pasiens",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#replaceOrCreate
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Replace an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
            "replaceOrCreate": {
              url: urlBase + "/penjamin_pasiens/replaceOrCreate",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#upsertWithWhere
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
            "upsertWithWhere": {
              url: urlBase + "/penjamin_pasiens/upsertWithWhere",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#exists
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Check whether a model instance exists in the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `exists` – `{boolean=}` -
             */
            "exists": {
              url: urlBase + "/penjamin_pasiens/:id/exists",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#findById
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Find a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `filter` – `{object=}` - Filter defining fields and include - must be a JSON-encoded string ({"something":"value"})
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
            "findById": {
              url: urlBase + "/penjamin_pasiens/:id",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#replaceById
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Replace attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
            "replaceById": {
              url: urlBase + "/penjamin_pasiens/:id/replace",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#find
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Find all instances of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
            "find": {
              isArray: true,
              url: urlBase + "/penjamin_pasiens",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#findOne
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Find first instance of the model matched by filter from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `filter` – `{object=}` - Filter defining fields, where, include, order, offset, and limit - must be a JSON-encoded string (`{"where":{"something":"value"}}`).  See https://loopback.io/doc/en/lb3/Querying-data.html#using-stringified-json-in-rest-queries for more details.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
            "findOne": {
              url: urlBase + "/penjamin_pasiens/findOne",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#updateAll
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
            "updateAll": {
              url: urlBase + "/penjamin_pasiens/update",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#deleteById
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
            "deleteById": {
              url: urlBase + "/penjamin_pasiens/:id",
              method: "DELETE",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#count
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Count instances of the model matched by where from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `count` – `{number=}` -
             */
            "count": {
              url: urlBase + "/penjamin_pasiens/count",
              method: "GET",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#prototype$patchAttributes
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - penjamin_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
            "prototype$patchAttributes": {
              url: urlBase + "/penjamin_pasiens/:id",
              method: "PATCH",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#createChangeStream
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Create a change stream.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Data properties:
             *
             *  - `changes` – `{ReadableStream=}` -
             */
            "createChangeStream": {
              url: urlBase + "/penjamin_pasiens/change-stream",
              method: "POST",
            },

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#createMany
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Create a new instance of the model and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *   This method does not accept any parameters.
             *   Supply an empty object or omit this argument altogether.
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Array.<Object>,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Array.<Object>} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
            "createMany": {
              isArray: true,
              url: urlBase + "/penjamin_pasiens",
              method: "POST",
            },
          }
        );



            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#upsert
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
        R["upsert"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#updateOrCreate
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Patch an existing model instance or insert a new one into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `data` – `{object=}` - Model instance data
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
        R["updateOrCreate"] = R["patchOrCreate"];

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#patchOrCreateWithWhere
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Update an existing model instance or insert a new one into the data source based on the where criteria.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
        R["patchOrCreateWithWhere"] = R["upsertWithWhere"];

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#update
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Update instances of the model matched by {{where}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `where` – `{object=}` - Criteria to match model instances
             *
             * @param {Object} postData Request data.
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * Information related to the outcome of the operation
             */
        R["update"] = R["updateAll"];

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#destroyById
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
        R["destroyById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#removeById
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Delete a model instance by {{id}} from the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - Model id
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
        R["removeById"] = R["deleteById"];

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#prototype$updateAttributes
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Patch attributes for a model instance and persist it into the data source.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - penjamin_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `data` – `{object=}` - An object of model property name/value pairs
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Penjamin_pasien` object.)
             * </em>
             */
        R["prototype$updateAttributes"] = R["prototype$patchAttributes"];


        /**
        * @ngdoc property
        * @name lbServices.Penjamin_pasien#modelName
        * @propertyOf lbServices.Penjamin_pasien
        * @description
        * The name of the model represented by this $resource,
        * i.e. `Penjamin_pasien`.
        */
        R.modelName = "Penjamin_pasien";


            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#pasien
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Fetches belongsTo relation pasien.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - penjamin_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `User` object.)
             * </em>
             */
        R.pasien = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::Penjamin_pasien::pasien"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#kecamatan
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Fetches belongsTo relation kecamatan.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - penjamin_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kecamatan` object.)
             * </em>
             */
        R.kecamatan = function() {
          var TargetResource = $injector.get("Kecamatan");
          var action = TargetResource["::get::Penjamin_pasien::kecamatan"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#kelurahan
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Fetches belongsTo relation kelurahan.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - penjamin_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kelurahan` object.)
             * </em>
             */
        R.kelurahan = function() {
          var TargetResource = $injector.get("Kelurahan");
          var action = TargetResource["::get::Penjamin_pasien::kelurahan"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#kabupaten_kota
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Fetches belongsTo relation kabupaten_kota.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - penjamin_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Kabupaten_kota` object.)
             * </em>
             */
        R.kabupaten_kota = function() {
          var TargetResource = $injector.get("Kabupaten_kota");
          var action = TargetResource["::get::Penjamin_pasien::kabupaten_kota"];
          return action.apply(R, arguments);
        };

            /**
             * @ngdoc method
             * @name lbServices.Penjamin_pasien#provinsi
             * @methodOf lbServices.Penjamin_pasien
             *
             * @description
             *
             * Fetches belongsTo relation provinsi.
             *
             * @param {Object=} parameters Request parameters.
             *
             *  - `id` – `{*}` - penjamin_pasien id
             *
             *  - `options` – `{object=}` -
             *
             *  - `refresh` – `{boolean=}` -
             *
             *  - `options` – `{object=}` -
             *
             * @param {function(Object,Object)=} successCb
             *   Success callback with two arguments: `value`, `responseHeaders`.
             *
             * @param {function(Object)=} errorCb Error callback with one argument:
             *   `httpResponse`.
             *
             * @returns {Object} An empty reference that will be
             *   populated with the actual data once the response is returned
             *   from the server.
             *
             * <em>
             * (The remote method definition does not provide any description.
             * This usually means the response is a `Provinsi` object.)
             * </em>
             */
        R.provinsi = function() {
          var TargetResource = $injector.get("Provinsi");
          var action = TargetResource["::get::Penjamin_pasien::provinsi"];
          return action.apply(R, arguments);
        };


        return R;
      }]);


  module
  .factory('LoopBackAuth', function() {
    var props = ['accessTokenId', 'currentUserId', 'rememberMe'];
    var propsPrefix = '$LoopBack$';

    function LoopBackAuth() {
      var self = this;
      props.forEach(function(name) {
        self[name] = load(name);
      });
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.save = function() {
      var self = this;
      var storage = this.rememberMe ? localStorage : sessionStorage;
      props.forEach(function(name) {
        save(storage, name, self[name]);
      });
    };

    LoopBackAuth.prototype.setUser = function(accessTokenId, userId, userData) {
      this.accessTokenId = accessTokenId;
      this.currentUserId = userId;
      this.currentUserData = userData;
    };

    LoopBackAuth.prototype.clearUser = function() {
      this.accessTokenId = null;
      this.currentUserId = null;
      this.currentUserData = null;
    };

    LoopBackAuth.prototype.clearStorage = function() {
      props.forEach(function(name) {
        save(sessionStorage, name, null);
        save(localStorage, name, null);
      });
    };

    return new LoopBackAuth();

    // Note: LocalStorage converts the value to string
    // We are using empty string as a marker for null/undefined values.
    function save(storage, name, value) {
      try {
        var key = propsPrefix + name;
        if (value == null) value = '';
        storage[key] = value;
      } catch (err) {
        console.log('Cannot access local/session storage:', err);
      }
    }

    function load(name) {
      var key = propsPrefix + name;
      return localStorage[key] || sessionStorage[key] || null;
    }
  })
  .config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('LoopBackAuthRequestInterceptor');
  }])
  .factory('LoopBackAuthRequestInterceptor', ['$q', 'LoopBackAuth',
    function($q, LoopBackAuth) {
      return {
        'request': function(config) {
          // filter out external requests
          var host = getHost(config.url);
          if (host && config.url.indexOf(urlBaseHost) === -1) {
            return config;
          }

          if (LoopBackAuth.accessTokenId) {
            config.headers[authHeader] = LoopBackAuth.accessTokenId;
          } else if (config.__isGetCurrentUser__) {
            // Return a stub 401 error for User.getCurrent() when
            // there is no user logged in
            var res = {
              body: { error: { status: 401 }},
              status: 401,
              config: config,
              headers: function() { return undefined; },
            };
            return $q.reject(res);
          }
          return config || $q.when(config);
        },
      };
    }])

  /**
   * @ngdoc object
   * @name lbServices.LoopBackResourceProvider
   * @header lbServices.LoopBackResourceProvider
   * @description
   * Use `LoopBackResourceProvider` to change the global configuration
   * settings used by all models. Note that the provider is available
   * to Configuration Blocks only, see
   * {@link https://docs.angularjs.org/guide/module#module-loading-dependencies Module Loading & Dependencies}
   * for more details.
   *
   * ## Example
   *
   * ```js
   * angular.module('app')
   *  .config(function(LoopBackResourceProvider) {
   *     LoopBackResourceProvider.setAuthHeader('X-Access-Token');
   *  });
   * ```
   */
  .provider('LoopBackResource', function LoopBackResourceProvider() {
    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#setAuthHeader
     * @methodOf lbServices.LoopBackResourceProvider
     * @param {string} header The header name to use, e.g. `X-Access-Token`
     * @description
     * Configure the REST transport to use a different header for sending
     * the authentication token. It is sent in the `Authorization` header
     * by default.
     */
    this.setAuthHeader = function(header) {
      authHeader = header;
    };

    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#getAuthHeader
     * @methodOf lbServices.LoopBackResourceProvider
     * @description
     * Get the header name that is used for sending the authentication token.
     */
    this.getAuthHeader = function() {
      return authHeader;
    };

    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#setUrlBase
     * @methodOf lbServices.LoopBackResourceProvider
     * @param {string} url The URL to use, e.g. `/api` or `//example.com/api`.
     * @description
     * Change the URL of the REST API server. By default, the URL provided
     * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
     */
    this.setUrlBase = function(url) {
      urlBase = url;
      urlBaseHost = getHost(urlBase) || location.host;
    };

    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#getUrlBase
     * @methodOf lbServices.LoopBackResourceProvider
     * @description
     * Get the URL of the REST API server. The URL provided
     * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
     */
    this.getUrlBase = function() {
      return urlBase;
    };

    this.$get = ['$resource', function($resource) {
      var LoopBackResource = function(url, params, actions) {
        var resource = $resource(url, params, actions);

        // Angular always calls POST on $save()
        // This hack is based on
        // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
        resource.prototype.$save = function(success, error) {
          // Fortunately, LoopBack provides a convenient `upsert` method
          // that exactly fits our needs.
          var result = resource.upsert.call(this, {}, this, success, error);
          return result.$promise || result;
        };
        return resource;
      };

      LoopBackResource.getUrlBase = function() {
        return urlBase;
      };

      LoopBackResource.getAuthHeader = function() {
        return authHeader;
      };

      return LoopBackResource;
    }];
  });
})(window, window.angular);
